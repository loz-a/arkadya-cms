<?php
namespace CMSNav;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
//    DependencyIndicatorInterface,
    FormElementProviderInterface,
    ServiceProviderInterface,
    ViewHelperProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\ClassMapAutoloader' => [
                __DIR__ . '/autoload_classmap.php',
            ],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [ __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__ ],
            ],
        ];
    } // getAutoloaderConfig()


//    public function getModuleDependencies()
//    {
//        return [
//            'Application',
//            'Common',
//            'CMSAdmin',
//            'CMSLocale'
//        ];
//    } // getModuleDependencies()


    public function getViewHelperConfig()
    {
        return [
            'invokables' => [
                'mainMenuWidget'   => 'CMSNav\View\Helper\MainMenuWidget',
                'asideMenuWidget'  => 'CMSNav\View\Helper\AsideMenuWidget',
                'footerMenuWidget' => 'CMSNav\View\Helper\FooterMenuWidget',
            ],
        ];
    } // getViewHelperConfig()


    public function getFormElementConfig()
    {
        return [
            'invokables' => [
                'CMSNav\PagesSelect'   => 'CMSNav\Form\Element\PagesSelect',
                'CMSNav\ParentsSelect' => 'CMSNav\Form\Element\ParentsSelect',
                'CMSNav\Add'           => 'CMSNav\Form\Add',
                'CMSNav\Edit'          => 'CMSNav\Form\Edit',
                'CMSNav\Delete'        => 'CMSNav\Form\Delete',

                'CMSNav\Taxonomy\TagsSelect' => 'CMSNav\Form\Taxonomy\Element\TagsSelect',
                'CMSNav\Taxonomy\Add'        => 'CMSNav\Form\Taxonomy\Add',

                'CMSNav\Tags\Add'    => 'CMSNav\Form\Tags\Add',
                'CMSNav\Tags\Edit'   => 'CMSNav\Form\Tags\Edit',
                'CMSNav\Tags\Delete' => 'CMSNav\Form\Tags\Delete',
            ],
        ];
    } // getFormElementConfig()


    public function getServiceConfig()
    {
        return [
            'invokables' => [
                'CMSNav\Service\Navigation' => 'CMSNav\Service\Navigation',
                'CMSNav\Service\Tags' => 'CMSNav\Service\Tags',
            ],
            'factories' => [
                'CMSNav\Options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['cms_nav_options']) ? $config['cms_nav_options'] : [];
                    return new Options\ModuleOptions($options);
                },
                'CMSNav\Mapper\Navigation' => function($sm) {
                    return (new Mapper\Navigation())
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setOptions($sm -> get('CMSNav\Options'))
                        -> setCurrentLocaleService($sm -> get('CMSLocale\CurrentLocale'));
                },
                'CMSNav\Mapper\Tags' => function($sm) {
                    return (new Mapper\Tags())
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setOptions($sm -> get('CMSNav\Options'));
                },
                'Navigation' => 'CMSNav\Service\NavigationFactory',
            ]
        ];
    } // getServiceConfig()
}
