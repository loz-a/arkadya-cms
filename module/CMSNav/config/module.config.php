<?php
return [
    'controllers' => [
        'invokables' => [
            'CMSNav\Controller\Index'    => 'CMSNav\Controller\IndexController',
            'CMSNav\Controller\Tags'     => 'CMSNav\Controller\TagsController',
            'CMSNav\Controller\Taxonomy' => 'CMSNav\Controller\TaxonomyController',
        ]
    ], // controllers


    'view_manager' => [
        'template_map' => [
            'cms-nav/widget/main-menu'   => __DIR__ . '/../view/cms-nav/widget/main-menu.phtml',
            'cms-nav/widget/aside-menu'  => __DIR__ . '/../view/cms-nav/widget/aside-menu.phtml',
            'cms-nav/widget/footer-menu' => __DIR__ . '/../view/cms-nav/widget/footer-menu.phtml',

            'cms-nav/index/manage'      => __DIR__ . '/../view/cms-nav/index/manage.phtml',
            'cms-nav/index/add'         => __DIR__ . '/../view/cms-nav/index/add.phtml',
            'cms-nav/index/add-submit'  => __DIR__ . '/../view/cms-nav/index/add.phtml',
            'cms-nav/index/edit'        => __DIR__ . '/../view/cms-nav/index/add.phtml',
            'cms-nav/index/edit-submit' => __DIR__ . '/../view/cms-nav/index/add.phtml',
            'cms-nav/index/delete'      => __DIR__ . '/../view/cms-nav/index/delete.phtml',
            'cms-nav/index/sitemap'     => __DIR__ . '/../view/cms-nav/index/sitemap.phtml',
            'cms-nav/index/sitemapxml'  => __DIR__ . '/../view/cms-nav/index/sitemapxml.phtml',

            'cms-nav/taxonomy/manage'        => __DIR__ . '/../view/cms-nav/taxonomy/manage.phtml',
            'cms-nav/taxonomy/attach'        => __DIR__ . '/../view/cms-nav/taxonomy/attach.phtml',
            'cms-nav/taxonomy/attach-submit' => __DIR__ . '/../view/cms-nav/taxonomy/attach.phtml',

            'cms-nav/tags/manage'       => __DIR__ . '/../view/cms-nav/tags/manage.phtml',
            'cms-nav/tags/add'          => __DIR__ . '/../view/cms-nav/tags/add.phtml',
            'cms-nav/tags/add-submit'   => __DIR__ . '/../view/cms-nav/tags/add.phtml',
            'cms-nav/tags/edit'         => __DIR__ . '/../view/cms-nav/tags/add.phtml',
            'cms-nav/tags/edit-submit'  => __DIR__ . '/../view/cms-nav/tags/add.phtml',
            'cms-nav/tags/delete'       => __DIR__ . '/../view/cms-nav/tags/delete.phtml',
            'cms-nav/tags/delete-confirm' => __DIR__ . '/../view/cms-nav/tags/delete.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ]
    ], // view_manager


    'router' => [
        'routes' => [
            'sitemap' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/sitemap.html',
                    'defaults' => [
                        'controller' => 'CMSNav\Controller\Index',
                        'action' => 'sitemap'
                    ]
                ]
            ], // sitemap

            'sitemap_xml' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/sitemap.xml',
                    'defaults' => [
                        'controller' => 'CMSNav\Controller\Index',
                        'action' => 'sitemapxml'
                    ]
                ]
            ], // sitemapxml

            'manage_navigation' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/navigation',
                    'defaults' => [
                        'controller' => 'CMSNav\Controller\Index',
                        'action'     => 'manage'
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [

                    'add' => [
                        'type' => 'Literal',
                        'options' => [ 'route' => '/add' ],
                        'may_terminate' => false,
                        'child_routes' => [

                            'relative_url' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route' => '/relative-url',
                                    'defaults' => [ 'action' => 'add' ],
                                ],
                                'may_terminate' => true
                            ],

                            'absolute_url' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route' => '/absolute-url',
                                    'defaults' => [ 'action' => 'add' ],
                                ],
                                'may_terminate' => true
                            ],

                            'cms_page' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route' => '/cms-page',
                                    'defaults' => [ 'action' => 'add' ],
                                ],
                                'may_terminate' => true
                            ],

                            'submit' => [
                                'type'    => 'Literal',
                                'options' => [
                                    'route' => '/submit',
                                    'defaults' => [ 'action' => 'add-submit' ]
                                ],
                                'may_terminate' => true
                            ],

                        ]
                    ], // add

                    'edit' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/edit/:id',
                            'constraints' => ['id' => '\d+'],
                        ],
                        'may_terminate' => false,
                        'child_routes' => [

                            'send' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'edit']
                                ],
                                'may_terminate' => true
                            ],

                            'submit' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'edit-submit']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ], // edit

                    'delete' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/delete/:id',
                            'constraints' => ['id' => '\d+'],
                        ],
                        'may_terminate' => false,
                        'child_routes' => [

                            'send' => [
                                'type' => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'delete']
                                ],
                                'may_terminate' => true
                            ],

                            'confirm' => [
                                'type' => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'delete-confirm']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ], // delete

                    'taxonomy' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/:navigation_item_id/tags',
                            'constraints' => ['navigation_item_id' => '\d+'],
                            'defaults' => [
                                'controller' => 'CMSNav\Controller\Taxonomy',
                                'action'     => 'manage'
                            ]
                        ],
                        'may_terminate' => true,
                        'child_routes' => [

                            'attach_tag' => [
                                'type' => 'Literal',
                                'options' => [ 'route' => '/attach' ],
                                'may_terminate' => false,
                                'child_routes' => [

                                    'send' => [
                                        'type'    => 'Method',
                                        'options' => [
                                            'verb'     => 'get',
                                            'defaults' => ['action' => 'attach']
                                        ],
                                        'may_terminate' => true,
                                    ],

                                    'submit' => [
                                        'type'    => 'Method',
                                        'options' => [
                                            'verb'     => 'post',
                                            'defaults' => ['action' => 'attach-submit']
                                        ],
                                        'may_terminate' => true
                                    ]
                                ]
                            ], // attach_tag

                            'detach_tag' => [
                                'type'    => 'Segment',
                                'options' => [
                                    'route'       => '/detach/:id',
                                    'constraints' => ['id' => '\d+'],
                                    'defaults' => ['action' => 'detach']
                                ],
                                'may_terminate' => true,
                            ], // detach_tag

                        ]
                    ] // taxonomy

                ],
            ], // manage_nagivation

            'manage_navigation_tags' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/navigation-tags',
                    'defaults' => [
                        'controller' => 'CMSNav\Controller\Tags',
                        'action'     => 'manage'
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [

                    'add' => [
                        'type' => 'Literal',
                        'options' => [ 'route' => '/add' ],
                        'may_terminate' => false,
                        'child_routes' => [

                            'send' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'add']
                                ],
                                'may_terminate' => true,
                            ],

                            'submit' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'add-submit']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ], // add

                    'edit' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/edit/:id',
                            'constraints' => ['id' => '\d+'],
                        ],
                        'may_terminate' => false,
                        'child_routes' => [

                            'send' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'edit']
                                ],
                                'may_terminate' => true
                            ],

                            'submit' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'edit-submit']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ], // edit

                    'delete' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/delete/:id',
                            'constraints' => ['id' => '\d+'],
                        ],
                        'may_terminate' => false,
                        'child_routes' => [

                            'send' => [
                                'type' => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'delete']
                                ],
                                'may_terminate' => true
                            ],

                            'confirm' => [
                                'type' => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'delete-confirm']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ], // delete
                ]
            ] // tags

        ]
    ], // router


    'route_layouts' => [
        'manage_navigation'                  => 'layout/admin',
        'manage_navigation/add/absolute_url' => 'layout/admin',
        'manage_navigation/add/relative_url' => 'layout/admin',
        'manage_navigation/add/cms_page'     => 'layout/admin',
        'manage_navigation/add/submit'       => 'layout/admin',
        'manage_navigation/edit/send'        => 'layout/admin',
        'manage_navigation/edit/submit'      => 'layout/admin',
        'manage_navigation/delete/send'      => 'layout/admin',
        'manage_navigation/delete/confirm'   => 'layout/admin',

        'manage_navigation/taxonomy'                    => 'layout/admin',
        'manage_navigation/taxonomy/attach_tag/send'    => 'layout/admin',
        'manage_navigation/taxonomy/attach_tag/submit'  => 'layout/admin',

        'manage_navigation_tags'                => 'layout/admin',
        'manage_navigation_tags/add/send'       => 'layout/admin',
        'manage_navigation_tags/add/submit'     => 'layout/admin',
        'manage_navigation_tags/edit/send'      => 'layout/admin',
        'manage_navigation_tags/edit/submit'    => 'layout/admin',
        'manage_navigation_tags/delete/send'    => 'layout/admin',
        'manage_navigation_tags/delete/confirm' => 'layout/admin',

        'sitemap' => 'layout/layout',
    ], // route_layouts


    'admin_navigation' => [
        'cms_navigation' => [
            'index' => 2,
            'header' => [
                'label' => 'Navigation',
                'icon' => 'icon-list-alt',
                'class_name' => 'list-alt',
            ],
            'items' => [
                [
                    'label' => 'Nav. Items',
                    'route_name' => 'manage_navigation'
                ],
                [
                    'label' => 'Tags',
                    'route_name' => 'manage_navigation_tags'
                ],
            ]
        ]
    ],

];