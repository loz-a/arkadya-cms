<?php
namespace CMSNav\Page;

use Zend\Navigation\Page\Uri as BaseUri;

class Uri extends BaseUri
{
    protected $tags = [];
    protected $mobileClass;

    public function setTags(array $tags)
    {
        $this -> tags = $tags;
        return $this;
    } // setTags()


    public function getTags()
    {
        return $this -> tags;
    } // getTags()


    public function hasTags()
    {
        return (bool) count($this -> tags);
    } // hasTags()


    public function hasTag($tag)
    {
        return in_array($tag, $this -> tags);
    } // hasTag()


    public function setMobileClass($class)
    {
        $this -> mobileClass = $class;
        return $this;
    } // setMobileClass()


    public function getMobileClass()
    {
        return $this -> mobileClass;
    } // getMobileClass()


    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'tags' => $this -> getTags(),
            ]
        );
    }

} // Uri
