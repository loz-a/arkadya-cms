<?php
namespace CMSNav\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

class MainMenuWidget extends AbstractHelper
{
    public function __invoke()
    {
        $vm = new ViewModel();
        $vm -> setTemplate('cms-nav/widget/main-menu');
        return $this -> getView() -> render($vm);
    } // __invoke()

} // MainMenuWidget
