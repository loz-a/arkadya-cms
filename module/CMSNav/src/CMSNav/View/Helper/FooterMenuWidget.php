<?php
namespace CMSNav\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

class FooterMenuWidget extends AbstractHelper
{
    public function __invoke()
    {
        $vm = new ViewModel();
        $vm -> setTemplate('cms-nav/widget/footer-menu');
        return $this -> getView() -> render($vm);
    } // __invoke()

} // FooterMenuWidget
