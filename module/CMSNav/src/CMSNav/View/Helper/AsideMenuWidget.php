<?php
namespace CMSNav\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

class AsideMenuWidget extends AbstractHelper
{
    public function __invoke()
    {
        $vm = new ViewModel();
        $vm -> setTemplate('cms-nav/widget/aside-menu');
        return $this -> getView() -> render($vm);
    } // __invoke()

} // AsideMenuWidget
