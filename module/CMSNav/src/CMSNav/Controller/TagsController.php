<?php
namespace CMSNav\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Common\Service\ServiceInterface;
use Common\Service\ServiceAwareInterface;
use Common\Controller\CrudTrait;

class TagsController extends AbstractActionController
    implements ServiceAwareInterface
{
    use CrudTrait;

    protected $navService;

    public function getAddFormAlias()
    {
        return 'CMSNav\Tags\Add';
    } // getAddFormAlias()


    public function getEditFormAlias()
    {
        return 'CMSNav\Tags\Edit';
    } // getEditFormAlias()


    public function getDeleteFormAlias()
    {
        return 'CMSNav\Tags\Delete';
    } // getDeleteFormAlias()


    public function getManageRouteName()
    {
        return 'manage_navigation_tags';
    } // getManageRouteName()


    public function getService()
    {
        if (null === $this -> navService) {
            $service = $this -> serviceLocator -> get('CMSNav\Service\Tags');
            $this -> setService($service);
        }
        return $this -> navService;
    } // getService()


    public function setService(ServiceInterface $service)
    {
        $this -> navService = $service;
    } // setService()

} // IndexController;
