<?php
namespace CMSNav\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Common\Service\ServiceInterface;
use Common\Service\ServiceAwareInterface;

class TaxonomyController extends AbstractActionController
    implements ServiceAwareInterface
{
    protected $navService;

    public function manageAction()
    {
        $navItemId = (int) $this -> params('navigation_item_id');
        $entities = $this -> getService() -> getMapper() -> fetchTagsByNavigationItemId($navItemId);

        return array(
            'entities'  => $entities,
            'navItemId' => $navItemId
        );
    } // indexAction()


    public function attachAction()
    {
        $navItemId = (int) $this -> params('navigation_item_id');
        return [
            'form'      => $this -> getService() -> getForm('CMSNav\Taxonomy\Add'),
            'navItemId' => $navItemId
        ];
    } // attachAction()


    public function attachSubmitAction()
    {
        $navItemId = $this -> params('navigation_item_id');
        $form = $this -> getService() -> getForm('CMSNav\Taxonomy\Add');
        $form -> setData($this -> request -> getPost());

        if ($form -> isValid()) {
            $result = $this -> getService() -> getMapper() -> attachTag($form -> getData());
            if ($result) {
                $this -> flashMessenger() -> addMessage('Adding was successfully');
                return $this -> redirect() -> toRoute('manage_navigation/taxonomy', ['navigation_item_id' => $navItemId]);
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return [
            'form'      => $form,
            'navItemId' => $navItemId
        ];
    } // attachSubmitAction()


    public function detachAction()
    {
        $navItemId = (int) $this -> params('navigation_item_id');
        $id   = $this -> params('id');

        $this -> getService() -> getMapper() -> detachTag($id);
        $this -> flashMessenger() -> addMessage('Detaching was successfully');
        return $this -> redirect() -> toRoute('manage_navigation/taxonomy', ['navigation_item_id' => $navItemId]);
    } // detachAction()


    public function getService()
    {
        if (null === $this -> navService) {
            $service = $this -> serviceLocator -> get('CMSNav\Service\Navigation');
            $this -> setService($service);
        }
        return $this -> navService;
    } // getService()


    public function setService(ServiceInterface $service)
    {
        $this -> navService = $service;
    } // setService()

} // IndexController;
