<?php
namespace CMSNav\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Common\Service\ServiceInterface;
use Common\Service\ServiceAwareInterface;
use Common\Controller\CrudTrait;

class IndexController extends AbstractActionController
    implements ServiceAwareInterface
{
    use CrudTrait;

    protected $navService;

    public function getAddFormAlias()
    {
        return 'CMSNav\Add';
    } // getAddFormAlias()


    public function getEditFormAlias()
    {
        return 'CMSNav\Edit';
    } // getEditFormAlias()


    public function getDeleteFormAlias()
    {
        return 'CMSNav\Delete';
    } // getDeleteFormAlias()


    public function getManageRouteName()
    {
        return 'manage_navigation';
    } // getManageRouteName()


    public function sitemapAction()
    {
        return [];
    } // sitemapAction()


    public function sitemapxmlAction()
    {
        $this
            -> getResponse()
            -> getHeaders()
            -> addHeaders(['Content-type' => 'text/xml']);

        $vm = new ViewModel();
        $vm -> setTerminal(true);
        return $vm;
    } // sitemapxmlAction()


    public function getService()
    {
        if (null === $this -> navService) {
            $service = $this -> serviceLocator -> get('CMSNav\Service\Navigation');
            $this -> setService($service);
        }
        return $this -> navService;
    } // getService()


    public function setService(ServiceInterface $service)
    {
        $this -> navService = $service;
    } // setService()

} // IndexController;
