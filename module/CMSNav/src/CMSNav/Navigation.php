<?php
namespace CMSNav;

use Zend\Navigation\Navigation as BaseNavigation;
use IteratorIterator;
use Zend\Navigation\AbstractContainer;

class Navigation extends BaseNavigation
{
    public function findByTag($tag, AbstractContainer $parent = null)
    {
        if (!$parent) {
            $parent = $this;
        }
        $iterator = new IteratorIterator($parent);
        return $this -> filterByTag($tag, $iterator);
    } // findByTag()


    protected function filterByTag($tag, $items)
    {
        $found = [];
        foreach ($items as $item) {
            if (!$item -> hasTags() or !in_array($tag, $item -> getTags())) {
                continue;
            }
            $children = $item -> getPages();
            if (count($children)) {
                $item -> setPages($this -> filterByTag($tag, $item -> getPages()));
            }
            $found[] = $item;
        }
        return $found;
    } // filterByTag()

} // Navigation
