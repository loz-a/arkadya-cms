<?php
namespace CMSNav\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Common\Service\ServiceInterface;
use Common\Service\OptionsTrait;
use Common\Service\FormTrait;
use Common\Mapper\MapperTrait;

class Tags implements
    ServiceManagerAwareInterface,
    ServiceInterface
{
    use OptionsTrait, MapperTrait, FormTrait;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> serviceManager = $serviceManager;
    } // setServiceManager()


    public function add(array $data)
    {
        return $this -> getMapper() -> insert($data);
    } // add()


    public function edit(array $data)
    {
        return $this -> getMapper() -> update($data);
    } // edit()


    public function getPopulatedFormById($id, $formAlias)
    {
        $data = $this -> getMapper() -> findById($id);
        if (!$data) {
            return null;
        }

        $form = $this -> getForm($formAlias);
        $form -> setData($data);
        return $form;
    } // getPopulatedFormById()

} // Tags
