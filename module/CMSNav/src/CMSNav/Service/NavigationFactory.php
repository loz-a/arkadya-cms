<?php
namespace CMSNav\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use CMSNav\Navigation;
use CMSNav\Page\Uri as NavigationItem;

class NavigationFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $baseUrl = $serviceLocator -> get('Router') -> assemble([], ['name' => 'home']);
        $baseUrl = ($baseUrl === '/') ? '' : substr($baseUrl, 0, -1);
        
        $items = $serviceLocator -> get('CMSNav\Mapper\Navigation') -> getAllNavigationItems();
        $items = $this -> parseItems($items);

        $container = new Navigation();
        foreach ($items as $item) {
            $page = new NavigationItem();
            $page
                -> setUri(strpos($item['url'], 'http') === 0 ? $item['url'] : $baseUrl . $item['url'])
                -> setLabel($item['label'])
                -> setClass($item['css_class'])
                -> setMobileClass($item['mobile_css_class'])
                -> setId($item['alias'])
                -> setOrder($item['position'])
                -> setTags($item['tags'])
                -> set('identifier', $item['id']);

            if ($item['parent_id']) {
                $parentPage = $container -> findBy('identifier', $item['parent_id']);
                $parentPage -> addPage($page);
            }
            else {
                $container -> addPage($page);
            }
        }
        return $container;
    } // createService()

    
    protected function parseItems($items)
    {
        $filteredSet = [];
        foreach ($items as $item) {
            $id = $item['id'];
            if (!isset($filteredSet[$item['id']])) {
                $filteredSet[$id]['id']        = $id;
                $filteredSet[$id]['parent_id'] = (int) $item['parent_id'];
                $filteredSet[$id]['alias']     = $item['alias'];
                $filteredSet[$id]['position']  = $item['position'];
                $filteredSet[$id]['css_class'] = $item['css_class'];
                $filteredSet[$id]['mobile_css_class'] = $item['mobile_css_class'];
                $filteredSet[$id]['label']     = $item['label'];
                $filteredSet[$id]['url']       = $item['url'];
                $filteredSet[$id]['tags'][]    = $item['tag'];
            }
            else {
                $filteredSet[$id]['tags'][] = $item['tag'];
            }
        }
        return $filteredSet;
    } // parseItems()

} // NavigationFacotry
