<?php
namespace CMSNav\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Common\Service\ServiceInterface;
use Common\Service\OptionsTrait;
use Common\Service\FormTrait;
use Common\Mapper\MapperTrait;
use CMSNav\Form\Add;
use CMSNav\Filter\PageAliasToUri;
use Common\Filter\Translit as TranslitFilter;

class Navigation implements
    ServiceManagerAwareInterface,
    ServiceInterface
{
    use OptionsTrait, MapperTrait, FormTrait;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> serviceManager = $serviceManager;
    } // setServiceManager()


    protected function preProccessData(array $data)
    {
        if ($data['type'] === Add::CMS_PAGE) {
            $filter = (new PageAliasToUri())
                -> setRouter($this -> serviceManager -> get('Router'))
                -> setRouteName('page');
            $data['url'] = $filter -> filter($data['url']);
        }

        $translit = new TranslitFilter();
        $translit -> setSpaceReplacer('-');
        $data['alias'] = strtolower($translit -> filter($data['alias']));

        return $data;
    } // preProccessData()


    public function add(array $data)
    {
        $data = $this -> preProccessData($data);
        return $this -> getMapper() -> insert($data);
    } // add()


    public function edit(array $data)
    {
        $data = $this -> preProccessData($data);
        return $this -> getMapper() -> update($data);
    } // edit()


    public function getPopulatedFormById($id, $formAlias)
    {
        $data = $this -> getMapper() -> findById($id);
        if (!$data) {
            return null;
        }

        $form = $this -> getForm($formAlias);
        $form -> setData($data);
        return $form;
    } // getPopulatedFormById()

} // Navigation
