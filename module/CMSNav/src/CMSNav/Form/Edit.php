<?php
namespace CMSNav\Form;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Form\Form;
use Zend\Form\Element;

class Edit extends Form
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function init()
    {
        $sl      = $this -> getServiceLocator() -> getServiceLocator();
        $options = $sl   -> get('CMSNav\Options');

        $this
            -> setName('Edit item')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $id = (new Element\Hidden('id'));

        $alias = (new Element\Text('alias'))
            -> setOptions([ 'label' => 'Alias:' ])
            -> setAttributes([
            'class'     => 'span4',
            'required'  => true,
            'maxlength' => $options -> getAliasMaxLength()
        ]);

        $label = (new Element\Text('label'))
            -> setOptions([ 'label' => 'Label:' ])
            -> setAttributes([
            'class'     => 'span4',
            'required'  => true,
            'maxlength' => $options -> getLabelMaxLength()
        ]);

        $parent = $this -> getServiceLocator() -> get('CMSNav\ParentsSelect')
            -> setName('parent')
            -> setOptions([ 'label' => 'Parent:' ])
            -> setAttributes( [ 'required' => false ] );

        $pos = (new Element\Text('position'))
            -> setOptions([ 'label' => 'Position:' ])
            -> setAttributes([
            'class'    => 'span1',
            'required' => true
        ]);

        $css = (new Element\Text('css_class'))
            -> setOptions([ 'label' => 'Css class:' ])
            -> setAttributes([
            'class'     => 'span4',
            'maxlength' => $options -> getCssClassMaxLength()
        ]);


        $mobileCss = (new Element\Text('mobile_css_class'))
            -> setOptions([ 'label' => 'Mobile css class:' ])
            -> setAttributes([
            'class'     => 'span4',
            'maxlength' => $options -> getMobileCssClassMaxLength()
        ]);


        $url = (new Element\Text('url'))
            -> setOptions([ 'label' => 'Relative url:' ])
            -> setAttributes([
            'class'    => 'span4',
            'required' => true,
            'maxlength' => $options -> getUrlMaxLength()
        ]);


        $csrf = (new Element\Csrf('csrf'));
        $csrf -> setOptions([
            'csrf_options' => [
                'timeout' => 6000
            ]
        ]);

        $submit = (new Element\Submit('submit'))
            -> setAttributes([
            'class' => 'btn btn-primary',
            'type'  => 'submit',
            'value' => 'Ok'
        ]);

        $this
            -> add($id)
            -> add($alias)
            -> add($label)
            -> add($parent)
            -> add($url)
            -> add($pos)
            -> add($css)
            -> add($mobileCss)
            -> add($csrf)
            -> add($submit)
            -> setInputFilter(new FormInput($sl));

    } // init()


    public function getFormAction()
    {
        return 'manage_navigation/edit/submit';
    } // getFormAction()

} // Edit
