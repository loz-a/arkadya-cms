<?php
namespace CMSNav\Form\Taxonomy;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Form\Form;
use Zend\Form\Element;

class Add extends Form
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function init()
    {
        $sl        = $this -> getServiceLocator() -> getServiceLocator();
        $options   = $sl   -> get('CMSNav\Options');
        $navItemId = $sl -> get('Application') -> getMVCEvent() -> getRouteMatch() -> getParam('navigation_item_id');

        $this
            -> setName('Attach tag')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $navItem = (new Element\Hidden('navigation_item_id'))
            -> setValue($navItemId);

        $tagId = $this -> getServiceLocator() -> get('CMSNav\Taxonomy\TagsSelect')
            -> setName('tag_id')
            -> setOptions([ 'label' => 'Tag:' ])
            -> setAttributes([ 'required'  => true ]);

        $csrf = new Element\Csrf('csrf');

        $submit = (new Element\Submit('submit'))
            -> setAttributes([
            'class' => 'btn btn-primary',
            'type'  => 'submit',
            'value' => 'Ok'
        ]);

        $this
            -> add($navItem)
            -> add($tagId)
            -> add($csrf)
            -> add($submit)
            -> setInputFilter(new FormInput($sl));
        
    } // init()


    public function getFormAction()
    {
        return 'manage_navigation/taxonomy/attach_tag/submit';
    } // getFormAction()

} // Add
