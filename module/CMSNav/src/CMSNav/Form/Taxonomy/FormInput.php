<?php
namespace CMSNav\Form\Taxonomy;

use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorInterface;

class FormInput extends InputFilter
{
    public function __construct(ServiceLocatorInterface $sl)
    {
        $options = $sl -> get('CMSNav\Options');
        $params  = $sl -> get('Request') -> getPost() -> toArray();

        $this
            -> add([
                'name'     => 'navigation_item_id',
                'required' => true,
                'filters'  => [
                    [ 'name' => 'Int' ]
                ]
            ])
            -> add([
                'name'     => 'tag_id',
                'required' => true,
                'validators' => [
                    [ 'name' => 'Db\NoRecordExists',
                        'options' => [
                            'adapter' => $sl -> get('Zend\Db\Adapter\Adapter'),
                            'table' => $options -> getNavigationsTagsTableName(),
                            'field' => 'tag_id',
                            'exclude' => sprintf('navigation_id = %s', (int) $params['navigation_item_id'])
                        ]
                    ]
                ]
            ]);

    } // __construct()

} // FormInput
