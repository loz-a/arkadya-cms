<?php
namespace CMSNav\Form\Taxonomy\Element;

use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class TagsSelect extends Select
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function init()
    {
        $sl = $this -> getServiceLocator() -> getServiceLocator();
        $res = $sl -> get('CMSNav\Mapper\Tags') -> fetchAll();

        $aliases = ['' => 'Select tag'];
        if ($res) {
            foreach ($res as $r) {
                $aliases[$r['id']] = $r['tag'];
            }
        }

        $this -> setValueOptions($aliases);
    } // init()

} // PagesSelect
