<?php
namespace CMSNav\Form\Element;

use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class PagesSelect extends Select
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function init()
    {
        $sl = $this -> getServiceLocator() -> getServiceLocator();
        $res = $sl -> get('CMSPages\Mapper') -> fetchAliases();

        $aliases = ['' => 'Select page'];
        if ($res) {
            foreach ($res as $r) {
                $aliases[$r['alias']] = $r['alias'];
            }
        }

        $this -> setValueOptions($aliases);
    } // init()

} // PagesSelect
