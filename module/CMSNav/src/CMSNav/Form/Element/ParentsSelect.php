<?php
namespace CMSNav\Form\Element;

use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class ParentsSelect extends Select
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function init()
    {
        $sl = $this -> getServiceLocator() -> getServiceLocator();
        $res = $sl -> get('CMSNav\Mapper\Navigation') -> fetchParents();

        $parents = ['' => 'none'];
        if ($res) {
            foreach ($res as $r) {
                $parents[$r['id']] = $r['alias'];
            }
        }
        $this -> setValueOptions($parents);
    } // init()
}
