<?php
namespace CMSNav\Form\Tags;

use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorInterface;

class FormInput extends InputFilter
{
    public function __construct(ServiceLocatorInterface $sl)
    {
        $options = $sl -> get('CMSNav\Options');
        $params  = $sl -> get('Request') -> getPost() -> toArray();

        if (array_key_exists('id', $params)) {
            $this
                -> add([
                    'name'     => 'id',
                    'required' => true,
                    'filters'  => [
                        [ 'name' => 'Int' ]
                    ]
                ]);
        }

        $this
            -> add([
                'name'     => 'tag',
                'required' => true,
                'filters'  => [
                    [ 'name' => 'StripTags' ],
                    [ 'name' => 'StringTrim' ],
                    [ 'name' => 'Callback',
                        'options' => [
                            'callback' => function($value) {
                                return str_replace(' ', '_', $value);
                            }
                        ]
                    ]
                ],
                'validators' => [
                    [ 'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'utf-8',
                            'max' => $options -> getTagMaxLength()
                        ]
                    ],
                    [ 'name' => 'Db\NoRecordExists',
                        'options' => [
                            'adapter' => $sl -> get('Zend\Db\Adapter\Adapter'),
                            'table' => $options -> getTagsTableName(),
                            'field' => 'tag',
                            'exclude' => array_key_exists('id', $params)
                                ? sprintf('id != %s', (int) $params['id']) : null
                        ]
                    ]
                ]
            ]);

    } // __construct()

} // FormInput
