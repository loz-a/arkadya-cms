<?php
namespace CMSNav\Form\Tags;

use Zend\Form\Element\Hidden;

class Edit extends Add
{
    public function init()
    {
        parent::init();
        $this -> setName('Edit tag');
        $this -> add(new Hidden('id'));
    } // init()


    public function getFormAction()
    {
        return 'manage_navigation_tags/edit/submit';
    } // getFormAction()

} // Edit
