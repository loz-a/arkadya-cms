<?php
namespace CMSNav\Form\Tags;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Form\Form;
use Zend\Form\Element;

class Add extends Form
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function init()
    {
        $sl      = $this -> getServiceLocator() -> getServiceLocator();
        $options = $sl   -> get('CMSNav\Options');

        $this
            -> setName('Add tag')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $tag = (new Element\Text('tag'))
            -> setOptions([ 'label' => 'Tag:'])
            -> setAttributes([
                'class'     => 'span4',
                'required'  => true,
                'maxlength' => $options -> getTagMaxLength()
            ]);

        $csrf = new Element\Csrf('csrf');

        $submit = (new Element\Submit('submit'))
            -> setAttributes([
            'class' => 'btn btn-primary',
            'type'  => 'submit',
            'value' => 'Ok'
        ]);

        $this
            -> add($tag)
            -> add($csrf)
            -> add($submit)
            -> setInputFilter(new FormInput($sl));
        
    } // init()


    public function getFormAction()
    {
        return 'manage_navigation_tags/add/submit';
    } // getFormAction()

} // Add
