<?php
namespace CMSNav\Form\Tags;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Form\Form;
use Zend\Validator\Db\NoRecordExists;

class Delete extends Form
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function getActionRouteName()
    {
        return 'manage_navigation_tags/delete/confirm';
    } // getActionRouteName()


    public function init()
    {
        $sl      = $this -> getServiceLocator() -> getServiceLocator();
        $options = $sl   -> get('CMSNav\Options');

        $this
            -> setName('Delete tag')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add([
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden',
                'options' => [
                    'type' => 'hidden'
                ]
            ])
            -> add([
                'name' => 'tag',
                'type' => 'Zend\Form\Element\Hidden',
                'options' => [
                    'type' => 'hidden'
                ]
            ])
            -> add([
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf',
                'options' => [
                    'csrf_options' => [
                        'timeout' => 600
                    ]
                ]
            ])
            -> add([
                'name' => 'confirm',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => [
                    'name'  => 'confirm',
                    'class' => 'btn btn-danger',
                    'type'  => 'submit',
                    'value' => 'yes'
                ]
            ]);

        $this
            -> getInputFilter()
            -> get('id')
            -> getValidatorChain()
            -> attachByName('Db\NoRecordExists', [
                'adapter' => $sl -> get('Zend\Db\Adapter\Adapter'),
                'table'   => $options -> getNavigationsTagsTableName(),
                'field'   => 'tag_id',
                'message' => [ NoRecordExists::ERROR_RECORD_FOUND => 'This tag attached to some navigation item' ]
            ]);

    } // init()

} // Delete
