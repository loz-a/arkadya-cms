<?php
namespace CMSNav\Form;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Form\Form;
use Zend\Form\Element;
use LogicException;

class Add extends Form
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const RELATIVE_URL = 'relative_url';
    const ABSOLUTE_URL = 'absolute_url';
    const CMS_PAGE     = 'cms_page';
    
    public function init()
    {
        $sl      = $this -> getServiceLocator() -> getServiceLocator();
        $options = $sl   -> get('CMSNav\Options');
        $urlType = $this -> getUrlType();
        
        $this
            -> setName('Create item')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $type = (new Element\Hidden('type'))
            -> setValue($urlType);

        $alias = (new Element\Text('alias'))
            -> setOptions([ 'label' => 'Alias:' ])
            -> setAttributes([
                'class'     => 'span4',
                'required'  => true,
                'maxlength' => $options -> getAliasMaxLength()
            ]);

        $label = (new Element\Text('label'))
            -> setOptions([ 'label' => 'Label:' ])
            -> setAttributes([
                'class'     => 'span4',
                'required'  => true,
                'maxlength' => $options -> getLabelMaxLength()
            ]);

        $parent = $this -> getServiceLocator() -> get('CMSNav\ParentsSelect')
            -> setName('parent')
            -> setOptions([ 'label' => 'Parent:' ])
            -> setAttributes( [ 'required' => false ] );

        $pos = (new Element\Text('position'))
            -> setOptions([ 'label' => 'Position:' ])
            -> setAttributes([
                'class'    => 'span1',
                'required' => true
            ]);

        $css = (new Element\Text('css_class'))
            -> setOptions([ 'label' => 'Css class:' ])
            -> setAttributes([
                'class'     => 'span4',
                'maxlength' => $options -> getCssClassMaxLength()
            ]);

        $mobileCss = (new Element\Text('mobile_css_class'))
            -> setOptions([ 'label' => 'Mobile css class:' ])
            -> setAttributes([
            'class'     => 'span4',
            'maxlength' => $options -> getMobileCssClassMaxLength()
        ]);


        $url = null;
        switch ($urlType) {
            case static::ABSOLUTE_URL:
                $url = (new Element\Url('url'))
                    -> setOptions([ 'label' => 'Absolute url:' ])
                    -> setAttributes([
                        'class'    => 'span4',
                        'required' => true,
                        'maxlength' => $options -> getUrlMaxLength()
                    ]);
                break;
            case static::RELATIVE_URL:
                $url = (new Element\Text('url'))
                    -> setOptions([ 'label' => 'Relative url:' ])
                    -> setAttributes([
                        'class'    => 'span4',
                        'required' => true,
                        'maxlength' => $options -> getUrlMaxLength()
                    ]);
                break;
            case static::CMS_PAGE:
                $url = $this -> getServiceLocator() -> get('CMSNav\PagesSelect')
                    -> setName('url')
                    -> setOptions([ 'label' => 'CMS Page:' ])
                    -> setAttributes( [ 'required' => true ] );
                break;
        }

        $csrf = (new Element\Csrf('csrf'));
        $csrf -> setOptions([
            'csrf_options' => [
                'timeout' => 6000
            ]
        ]);

        $submit = (new Element\Submit('submit'))
            -> setAttributes([
            'class' => 'btn btn-primary',
            'type'  => 'submit',
            'value' => 'Ok'
        ]);

        $this
            -> add($type)
            -> add($alias)
            -> add($label)
            -> add($parent)
            -> add($url)
            -> add($pos)
            -> add($css)
            -> add($mobileCss)
            -> add($csrf)
            -> add($submit)
            -> setInputFilter(new FormInput($sl));
        
    } // init()


    public function getFormAction()
    {
        return 'manage_navigation/add/submit';
    } // getFormAction()


    protected function getUrlType()
    {
        $sl   = $this -> getServiceLocator() -> getServiceLocator();
        $mrn  = $sl -> get('Application') -> getMvcEvent() -> getRouteMatch() -> getMatchedRouteName();
        $type = end(explode('/', $mrn));
        $types = [static::ABSOLUTE_URL, static::RELATIVE_URL, static::CMS_PAGE];

        if (!in_array($type, $types)) {
            $type = $sl -> get('Request') -> getPost('type');
            if (!in_array($type, $types)) {
                throw new LogicException('Wrong url type');
            }
        }
        return $type;
    } // getUrlType()

} // Add
