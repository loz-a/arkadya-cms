<?php
namespace CMSNav\Form;

use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorInterface;

class FormInput extends InputFilter
{
    public function __construct(ServiceLocatorInterface $sl)
    {
        $options = $sl -> get('CMSNav\Options');
        $params  = $sl -> get('Request') -> getPost() -> toArray();

        if (array_key_exists('id', $params)) {
            $this
                -> add([
                    'name'     => 'id',
                    'required' => true,
                    'filters'  => [
                        [ 'name' => 'Int' ]
                    ]
                ]);
        }
        else {
            $this
                -> add([
                    'name'     => 'type',
                    'required' => true,
                    'filters'  => [
                        [ 'name' => 'StripTags' ],
                        [ 'name' => 'StringTrim' ]
                    ]
                ]);
        }

        $this
            -> add([
                'name'     => 'alias',
                'required' => true,
                'filters'  => [
                    [ 'name' => 'StripTags' ],
                    [ 'name' => 'StringTrim' ],
                ],
                'validators' => [
                    [ 'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'utf-8',
                            'max' => $options -> getAliasMaxLength()
                        ]
                    ],
                    [ 'name' => 'Db\NoRecordExists',
                        'options' => [
                            'adapter' => $sl -> get('Zend\Db\Adapter\Adapter'),
                            'table' => $options -> getTableName(),
                            'field' => 'alias',
                            'exclude' => array_key_exists('id', $params)
                                ? sprintf('id != %s', (int) $params['id']) : null
                        ]
                    ]
                ]
            ])
            -> add([
                'name'     => 'label',
                'required' => true,
                'filters'  => [
                    [ 'name' => 'StripTags' ],
                    [ 'name' => 'StringTrim' ],
                ],
                'validators' => [
                    [ 'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'utf-8',
                            'max' => $options -> getLabelMaxLength()
                        ]
                    ],
                ]
            ])
            -> add([
                'name' => 'parent',
                'required' => false,
                'filters'  => [
                    [ 'name' => 'Int' ]
                ]
            ])
            -> add([
                'name' => 'position',
                'required' => false,
                'validators'  => [
                    [ 'name' => 'Digits' ]
                ]
            ])
            -> add([
                'name' => 'css_class',
                'required' => false,
                'filters'  => [
                    [ 'name' => 'StripTags' ],
                    [ 'name' => 'StringTrim' ],
                ],
                'validators' => [
                    [ 'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'utf-8',
                            'max' => $options -> getCssClassMaxLength()
                        ]
                    ],
                ]
            ])
            -> add([
                'name' => 'mobile_css_class',
                'required' => false,
                'filters'  => [
                    [ 'name' => 'StripTags' ],
                    [ 'name' => 'StringTrim' ],
                ],
                'validators' => [
                    [ 'name' => 'StringLength',
                        'options' => [
                            'encoding' => 'utf-8',
                            'max' => $options -> getMobileCssClassMaxLength()
                        ]
                    ],
                ]
            ]);

            if ($params['type'] === Add::ABSOLUTE_URL) {
                $this -> add([
                    'name' => 'url',
                    'required' => true,
                    'filters'  => [
                        [ 'name' => 'StripTags' ],
                        [ 'name' => 'StringTrim' ]
                    ],
                    'validators' => [
                        [ 'name' => 'Uri',
                            'options' => [
                                'allow_absolute' => true,
                                'allow_relative' => false
                            ]
                        ]
                    ]
                ]);
            }
            else if ($params['type'] === Add::RELATIVE_URL) {
                $this -> add([
                    'name' => 'url',
                    'required' => true,
                    'filters'  => [
                        [ 'name' => 'StripTags' ],
                        [ 'name' => 'StringTrim' ]
                    ],
                    'validators' => [
                        [ 'name' => 'Uri',
                            'options' => [
                                'allow_relative' => true,
                                'allow_absolute' => false
                            ]
                        ]
                    ]
                ]);
            }
            else {
                $this -> add([
                    'name'     => 'url',
                    'required' => true,
                ]);
            }

    } // __construct()
    
} // FormInput
