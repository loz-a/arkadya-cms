<?php
namespace CMSNav\Options;

interface NavigationInterface
{
    public function setUrlMaxLength($maxlen);

    public function getUrlMaxLength();

    public function setCssClassMaxLength($maxlen);

    public function getCssClassMaxLength();

    public function setMobileCssClassMaxLength($maxlen);

    public function getMobileCssClassMaxLength();

    public function setAliasMaxLength($maxlen);

    public function getAliasMaxLength();
}
