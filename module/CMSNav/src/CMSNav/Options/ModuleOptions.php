<?php
namespace CMSNav\Options;

use Common\Mapper\Options\AbstractOptions;

class ModuleOptions extends AbstractOptions implements
    NavigationInterface,
    TranslationInterface,
    TagInterface
{
    protected $tableName = 'navigation';

    /**
     * @var int
     */
    protected $urlMaxLength = 510;

    /**
     * @param int $maxlen
     * @return ModuleOptions
     */
    public function setUrlMaxLength($maxlen)
    {
        $this -> urlMaxLength = (int) $maxlen;
        return $this;
    }

    /**
     * @return int
     */
    public function getUrlMaxLength()
    {
        return $this -> urlMaxLength;
    }

    /**
     * @var int
     */
    protected $cssClassMaxLength = 50;

    /**
     * @param int $maxlen
     * @return ModuleOptions
     */
    public function setCssClassMaxLength($maxlen)
    {
        $this -> cssClassMaxLength = (int) $maxlen;
        return $this;
    } // setCssClassMaxLength()


    public function getCssClassMaxLength()
    {
        return $this -> cssClassMaxLength;
    } // getCssClassMaxLength()


    /**
     * @var int
     */
    protected $mobileCssClassMaxLength = 50;

    /**
     * @param int $maxlen
     * @return ModuleOptions
     */
    public function setMobileCssClassMaxLength($maxlen)
    {
        $this -> mobileCssClassMaxLength = (int) $maxlen;
        return $this;
    } // setCssClassMaxLength()


    public function getMobileCssClassMaxLength()
    {
        return $this -> mobileCssClassMaxLength;
    } // getCssClassMaxLength()



    /**
     * @var int
     */
    protected $aliasMaxLength = 50;

    /**
     * @param int $maxlen
     * @return ModuleOptions
     */
    public function setAliasMaxLength($maxlen)
    {
        $this -> aliasMaxLength = (int) $maxlen;
        return $this;
    } // setAliasMaxLength()

    /**
     * @return int
     */
    public function getAliasMaxLength()
    {
        return $this -> aliasMaxLength;
    } // getAliasMaxLength()


    /**
     * @var string
     */
    protected $translationTableName = 'navigation_translation';

    /**
     * @param string $tableName
     * @return ModuleOptions
     */
    public function setTranslationTableName($tableName)
    {
        $this -> translationTableName = (string) $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTranslationTableName()
    {
        return $this -> translationTableName;
    }


    protected $labelMaxLength = 255;

    public function setLabelMaxLength($maxlen)
    {
        $this -> labelMaxLength = (int) $maxlen;
        return $this;
    }

    public function getLabelMaxLength()
    {
        return $this -> labelMaxLength;
    } // setLabelMaxLength()


    protected $tagsTableName = 'navigation_tags';

    public function setTagsTableName($tableName)
    {
        $this -> tagsTableName = (string) $tableName;
        return $this;
    }

    public function getTagsTableName()
    {
        return $this -> tagsTableName;
    }


    protected $navigationsTagsTableName = 'navigations_tags';

    public function setNavigationsTagsTableName($tableName)
    {
        $this -> navigationsTagsTableName = (string) $tableName;
        return $this;
    }

    public function getNavigationsTagsTableName()
    {
        return $this -> navigationsTagsTableName;
    }


    protected $tagMaxLength = 50;

    public function setTagMaxLength($maxlen)
    {
        $this -> tagMaxLength = (int) $maxlen;
        return $this;
    }

    public function getTagMaxLength()
    {
        return $this -> tagMaxLength;
    }


} // ModuleOptions
