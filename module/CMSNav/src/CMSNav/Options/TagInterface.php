<?php
namespace CMSNav\Options;

interface TagInterface
{
    public function setTagsTableName($tableName);

    public function getTagsTableName();

    public function setNavigationsTagsTableName($tableName);

    public function getNavigationsTagsTableName();

    public function setTagMaxLength($maxlen);

    public function getTagMaxLength();
}
