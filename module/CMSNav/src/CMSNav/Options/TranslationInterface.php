<?php
namespace CMSNav\Options;

interface TranslationInterface
{
    public function setTranslationTableName($tableName);

    public function getTranslationTableName();

    public function setLabelMaxLength($maxlen);

    public function getLabelMaxLength();
}
