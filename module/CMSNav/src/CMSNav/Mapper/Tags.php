<?php
namespace CMSNav\Mapper;

use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Common\Mapper\AbstractDbMapper;
use CMSLocale\Service\CurrentLocale as CurLocaleService;
use Zend\Db\Sql\Expression;

class Tags extends AbstractDbMapper
{
    public function fetchAll()
    {
        $select = $this
            -> select()
            -> columns(['id', 'tag'])
            -> from(['t' => $this -> options -> getTagsTableName()]);

        $result = $this -> selectExecute($select);
        return $result -> count() ? $result : null;
    } // fetchAll()


    public function findById($id)
    {
        $select = $this
            -> select()
            -> columns(['id', 'tag'])
            -> from($this -> options -> getTagsTableName())
            -> where(['id' => (int) $id ])
            -> limit(1);

        $result = $this -> selectExecute($select);
        return $result -> count() ? $result -> current() : null;
    } // findById()


    public function findByTag($tag)
    {
        $select = $this
            -> select()
            -> columns(['id', 'tag'])
            -> from($this -> options -> getTagsTableName())
            -> where(['tag' => $tag])
            -> limit(1);

        $result = $this -> selectExecute($select);
        return $result -> count() ? $result -> current() : null;
    } // findById()


    public function isTagAttached($tagId, $navigationItemId)
    {
        $select = $this
            -> select()
            -> columns(['id'])
            -> from($this -> options -> getNavigationsTagsTableName())
            -> where([ 'tag_id' => $tagId ])
            -> where([ 'navigation_id' => $navigationItemId ])
            -> limit(1);

        return (bool) $this -> selectExecute($select) -> count();
    } // isTagAttached()


    public function insert($entity, $tableName = null, HydratorInterface $hydrator = null)
    {
        parent::insert([
            'tag' => $entity['tag']
        ],
        $this -> options -> getTagsTableName());

        return $this -> getConnection() -> getLastGeneratedValue();
    } // insert()


    public function update($entity, $where, $tableName = null, HydratorInterface $hydrator = null)
    {
        $id = $entity['id'];

        parent::update([
            'tag' => $entity['tag']
        ], [
            'id' => $entity['id']
        ],
        $this -> options -> getTagsTableName());

        return $id;
    } // insert()


    public function deleteById($id)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> options -> getTagsTableName());
        $del = $sql -> delete() -> where(['id' => $id]);
        $statement = $sql -> prepareStatementForSqlObject($del);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // removeById()

} // Navigation
