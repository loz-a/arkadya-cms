<?php
namespace CMSNav\Mapper;

use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Common\Mapper\AbstractDbMapper;
use CMSLocale\Service\CurrentLocale as CurLocaleService;
use CMSNav\Form\Add as AddForm;
use Zend\Db\Sql\Expression;

class Navigation extends AbstractDbMapper
{
    protected $types = [
        AddForm::ABSOLUTE_URL => 1,
        AddForm::RELATIVE_URL => 2,
        AddForm::CMS_PAGE     => 3
    ];

    /**
     * @var \CMSLocale\Service\CurrentLocale
     */
    protected $curLocaleService;

    /**
     * @param \CMSLocale\Service\CurrentLocale $service
     * @return Navigation
     */
    public function setCurrentLocaleService(CurLocaleService $service)
    {
        $this -> curLocaleService = $service;
        return $this;
    } // setCurrentLocaleService()


    /**
     * @return \CMSLocale\Service\CurrentLocale
     * @throws \LogicException
     */
    public function getCurrentLocaleService()
    {
        if ( !$this -> curLocaleService ) {
            throw new \LogicException('Current locale service is undefined');
        }
        return $this -> curLocaleService;
    } // getCurrentLocaleService()


    public function getAllNavigationItems()
    {
        $localeId = $this -> getCurrentLocaleService() -> getEntity() -> getId();

        $select = $this
            -> select()
            -> columns(['id', 'parent_id', 'alias', 'position', 'css_class', 'mobile_css_class', 'url'])
            -> from([ 'n' => $this -> options -> getTableName() ])
            -> join(
                ['nt' => $this -> options -> getTranslationTableName()],
                new Expression('`n`.`id` = `nt`.`id` AND `nt`.`locale_id` = ' . $localeId),
                ['label'],
                'left'
            )
            -> join(
                ['nsts' => $this -> options -> getNavigationsTagsTableName()],
                'n.id = nsts.navigation_id',
                [],
                'left'
            )
            -> join(
                ['nts' => $this -> options -> getTagsTableName()],
                'nts.id = nsts.tag_id',
                ['tag'],
                'left'
            )
            -> order('n.parent_id, n.position');

        $result = $this -> selectExecute($select);
        return $result -> count() ? $result : null;
    } // getAllNavigationItems()


    public function fetchAll()
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $select = $this
            -> select()
            -> columns(['id', 'alias', 'position', 'css_class', 'mobile_css_class', 'url', 'type'])
            -> from(['n' => $this -> options -> getTableName()])
            -> join(
                ['np' => $this -> options -> getTableName()],
                'n.parent_id = np.id',
                ['parent' => 'alias'],
                'left'
            )
            -> join(
                ['nt' => $this -> options -> getTranslationTableName()],
                new Expression('`n`.`id` = `nt`.`id` AND `nt`.`locale_id` = ' . $localeEntity -> getId()),
                ['label'],
                'left'
            );

        $result = $this -> selectExecute($select);
        return $result -> count() ? $result : null;
    } // fetchAll()


    public function fetchParents()
    {
        $select = $this
            -> select()
            -> columns(['id', 'alias'])
            -> from($this -> getTableName());

        $result = $this -> selectExecute($select);
        return count($result) ? $result : null;
    } // fetchParentItems()

    
    public function findById($id)
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $select = $this
            -> select()
            -> columns(['id', 'alias', 'position', 'css_class', 'mobile_css_class', 'url', 'type', 'parent' => 'parent_id'])
            -> from(['n' => $this -> options -> getTableName()])
            -> join(
                ['nt' => $this -> options -> getTranslationTableName()],
                new Expression('n.id = nt.id AND nt.locale_id = ' . $localeEntity -> getId()),
                ['label'],
                'left'
            )
            -> where(['n.id' => (int) $id ])
            -> limit(1);

        $result = $this -> selectExecute($select);
        return $result -> count() ? $result -> current() : null;
    } // findById()


    public function lookupByTranslation($id)
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $select = $this
            -> select()
            -> columns(['id'])
            -> from($this -> options -> getTranslationTableName())
            -> where(['id' => (int) $id ])
            -> where(['locale_id' => $localeEntity -> getId()])
            -> limit(1);

        $result = $this -> selectExecute($select);
        return (bool) $result -> count();
    } // lookupByTranslation()


    public function insert($entity, $tableName = null, HydratorInterface $hydrator = null)
    {
        $id = null;

        $this -> getConnection() -> beginTransaction();
        try {
            $this -> insertNavigationItem($entity);
            $id = $this -> getConnection() -> getLastGeneratedValue();

            $this -> insertTranslation($id, $entity);
            $this -> getConnection() -> commit();
        }
        catch(\Exception $e) {
            $this -> getConnection() -> rollback();
            throw new \Exception('Navigation item can\'t be created');
        }
        return $id;
    } // insert()


    public function insertNavigationItem($data)
    {
        $insertData = [];
        if ($data['parent']) {
            $insertData['parent_id'] = $data['parent'];
        }

        $insertData['type']  = $this -> types[$data['type']];
        $insertData['alias'] = $data['alias'];
        $insertData['position'] = $data['position'];
        $insertData['css_class'] = $data['css_class'];
        $insertData['mobile_css_class'] = $data['mobile_css_class'];
        $insertData['url'] = $data['url'];

        return parent::insert($insertData);
    } // insertNavigationItem()


    public function insertTranslation($id, array $data)
    {
        return parent::insert([
                'id' => $id,
                'locale_id' => $this -> getCurrentLocaleService() -> getEntity() -> getId(),
                'label' => $data['label']
            ],
            $this -> options -> getTranslationTableName());
    } // insertTranslation()


    public function update($entity, $where, $tableName = null, HydratorInterface $hydrator = null)
    {
        $id = $entity['id'];

        $this -> getConnection() -> beginTransaction();
        try {
            $this -> updateNavigationItem($entity);

            if ($this -> lookupByTranslation($id)) {
                $this -> updateTranslation($entity);
            }
            else {
                $this -> insertTranslation($id, $entity);
            }
            $this -> getConnection() -> commit();
        }
        catch(\Exception $e) {
            $this -> getConnection() -> rollback();
            throw new \Exception('Navigation item can\'t be created');
        }
        return $id;
    } // insert()


    public function updateNavigationItem($data)
    {
        $updateData = [];
        if ($data['parent']) {
            $updateData['parent_id'] = $data['parent'];
        }

        $updateData['type']  = $this -> types[$data['type']];
        $updateData['alias'] = $data['alias'];
        $updateData['position'] = $data['position'];
        $updateData['css_class'] = $data['css_class'];
        $updateData['mobile_css_class'] = $data['mobile_css_class'];
        $updateData['url'] = $data['url'];

        return parent::update($updateData, ['id' => $data['id']]);
    } // insertNavigationItem()


    public function updateTranslation($data)
    {
        return parent::update(
        [
            'label' => $data['label']
        ],
        [
            'id' => $data['id'],
            'locale_id' => $this -> getCurrentLocaleService() -> getEntity() -> getId()
        ],
        $this -> options -> getTranslationTableName());
    } // updateTranslation()


    public function deleteById($id)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> getTableName());
        $del = $sql -> delete() -> where(['id' => $id]);
        $statement = $sql -> prepareStatementForSqlObject($del);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // removeById()


    public function fetchTagsByNavigationItemId($navigationItemId)
    {
        $select = $this
            -> select()
            -> columns(['id', 'tag'])
            -> from(['t' => $this -> options -> getTagsTableName()])
            -> join(
            ['nt' => $this -> options -> getNavigationsTagsTableName()],
            new Expression('`t`.`id` = `nt`.`tag_id` AND `nt`.`navigation_id` = ' . (int) $navigationItemId)
        );

        $result = $this -> selectExecute($select);
        return $result -> count() ? $result : null;
    } // fetchTagsByNavigationItemId()


    public function isTagAttached($tagId, $navigationItemId)
    {
        $select = $this
            -> select()
            -> columns(['id'])
            -> from($this -> options -> getNavigationsTagsTableName())
            -> where([ 'tag_id' => $tagId ])
            -> where([ 'navigation_id' => $navigationItemId ])
            -> limit(1);

        return (bool) $this -> selectExecute($select) -> count();
    } // isTagAttached()


    public function attachTag($tag)
    {
        parent::insert([
            'tag_id' => $tag['tag_id'],
            'navigation_id' => $tag['navigation_item_id']
        ],
        $this -> options -> getNavigationsTagsTableName());

        return $this -> getConnection() -> getLastGeneratedValue();
    } // insert()


    public function detachTag($id)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> options -> getNavigationsTagsTableName());
        $del = $sql -> delete() -> where(['id' => $id]);
        $statement = $sql -> prepareStatementForSqlObject($del);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // detachTag()

} // Navigation
