<?php
namespace CMSNav\Filter;

use Zend\Filter\AbstractFilter;
use Zend\Mvc\Router\RouteInterface;
use LogicException;

class PageAliasToUri extends AbstractFilter
{
    /**
     * @var \Zend\Mvc\Router\RouteInterface
     */
    protected $router;
    protected $routeName;
    protected $aliasParam = 'alias' ;

    /**
     * @param \Zend\Mvc\Router\RouteInterface $router
     * @return PageAliasToUri
     */
    public function setRouter(RouteInterface $router)
    {
        $this -> router = $router;
        return $this;
    } // setRouter()


    /**
     * @return \Zend\Mvc\Router\RouteInterface
     * @throws \LogicException
     */
    public function getRouter()
    {
        if (null === $this -> router) {
            throw new LogicException('Router is undefined');
        }
        return $this -> router;
    } // getRouter()


    public function setRouteName($routeName)
    {
        $this -> routeName = (string) $routeName;
        return $this;
    } // setRouteName()


    public function setAliasParam($aliasParam)
    {
        $this -> aliasParam = (string) $aliasParam;
        return $this;
    } // setAliasParam()


    public function __construct(array $options = [])
    {
        $this -> setOptions($options);
    } // __construct()

    public function filter($value)
    {
        $url = $this -> getRouter()
            -> assemble([ $this -> aliasParam => $value ],[ 'name' => $this -> routeName ]);

        if (get_class($this -> getRouter()) === 'CMSLocale\Mvc\Router\Http\MultilingualTreeRouteStack') {
            $url = substr($url, 3);
        }
        return $url;
    } // filter()

} // PageAliasToUri
