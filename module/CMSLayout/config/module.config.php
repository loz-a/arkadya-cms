<?php
return [
    'cms_layout' => [
        'ignore_response_status_codes' => [
            404,
            403
        ],
        'ignore_routes' => [

        ]
    ]
];