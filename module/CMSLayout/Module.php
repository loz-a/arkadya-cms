<?php
namespace CMSLayout;

use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\EventManager\EventInterface;
use Zend\Mvc\MvcEvent;

class Module implements 
    BootstrapListenerInterface,
    ConfigProviderInterface,
    AutoloaderProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';    
    } // getConfig()


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    } // getAutoloaderConfig()


    public function onBootstrap(EventInterface $e)
    {
        $e  -> getApplication()
            -> getEventManager()
            -> getSharedManager()
            -> attach(
                'Zend\Mvc\Controller\AbstractController',
                MvcEvent::EVENT_DISPATCH,
                [$this, 'onDispatch'],
                100
            );
    } // onBootsrap()


    public function onDispatch(MvcEvent $e)
    {
        $controller = $e -> getTarget();
        $routeName  = $e -> getRouteMatch() -> getMatchedRouteName();
        $config     = $e -> getApplication() -> getServiceManager() -> get('Config');
        
        if (in_array($routeName, $config['cms_layout']['ignore_routes'])) {
            return;
        }

        $statusCode = $e -> getApplication() -> getResponse() -> getStatusCode();
        if (in_array($statusCode, $config['cms_layout']['ignore_response_status_codes'])) {
            return;
        }

        if (isset($config['route_layouts'][$routeName])) {
            $controller -> layout($config['route_layouts'][$routeName]);
        }
    } // onDispatch()

} // Module