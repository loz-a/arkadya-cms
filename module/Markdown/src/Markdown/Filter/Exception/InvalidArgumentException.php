<?php
namespace Markdown\Filter\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
} // InvalidArgumentException
