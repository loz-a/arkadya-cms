<?php
namespace Markdown\Filter;

use Zend\Filter\AbstractFilter;
use \Traversable;
use Zend\Stdlib\ArrayUtils;
use dflydev\markdown\IMarkdownParser as MarkdownParserInterface;

class Markdown extends AbstractFilter
{
    /**
     * @var \dflydev\markdown\IMarkdownParser
     */
    protected $parser;

    public function __construct($options)
    {
        if ( !$options ) {
            $options = array();
        }
        else if ( $options instanceof Traversable ) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        else if ( is_object($options) ) {
            $options = array('parser' => $options);
        }

        if ( !is_array($options) ) {
            throw new Exception\InvalidArgumentException('Invalid options argument provided to filter');
        }
        $this -> setOptions($options);
    } // __construct()


    public function setParser(MarkdownParserInterface $parser)
    {
        $this -> parser = $parser;
        return $this;
    } // setParser()


    public function filter($value)
    {
        if ( null === $this -> parser ) {
            throw new Exception\LogicException('Markdown parser is undefined');
        }
        $value = (string) $value;
        return $this -> parser -> transformMarkdown($value);
    } // filter()

} // MarkdownFilter
