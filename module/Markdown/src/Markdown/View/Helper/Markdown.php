<?php
namespace Markdown\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Markdown\Filter\Markdown as MarkdownFilter;
use dflydev\markdown\IMarkdownParser as MarkdownParserInterface;

class Markdown extends AbstractHelper
{
    /**
     * @var \dflydev\markdown\IMarkdownParser
     */
    protected $parser;

    /**
     * @param \dflydev\markdown\IMarkdownParser $parser
     * @return Markdown
     */
    public function setParser(MarkdownParserInterface $parser)
    {
        $this -> parser = $parser;
        return $this;
    } // setFilter()


    public function __invoke($value)
    {
        if ( null === $this -> parser ) {
            throw new Exception\LogicException('Markdown parser is undefined');
        }
        return $this -> parser -> transformMarkdown($value);
    } // __invoke()

} // Markdown
