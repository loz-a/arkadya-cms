<?php
namespace Markdown;

interface MarkdownParserInterface
{
    public function transform($text);
}
