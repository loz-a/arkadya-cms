<?php
namespace Markdown;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use dflydev\markdown\MarkdownParser;
use dflydev\markdown\MarkdownExtraParser;

class MarkdownFactory implements FactoryInterface
{
    const PARSER = 'Markdown';
    const PARSER_EXTRA = 'MarkdownExtra';

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $parserType = $serviceLocator -> get('markdown_options') -> getParserType();

        if ( $parserType === self::PARSER_EXTRA ) {
            return new MarkdownExtraParser();
        }
        return new MarkdownParser();
    } // createService()

} // MarkdownFactory
