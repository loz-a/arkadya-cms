<?php
namespace Markdown\Options;

use Zend\Stdlib\AbstractOptions;
use Markdown\MarkdownFactory;

class ModuleOptions extends AbstractOptions
    implements MarkdownInterface
{
    protected $parserType = MarkdownFactory::PARSER_EXTRA;

    public function setParserType($parserType)
    {
        $this -> parserType = (string) $parserType;
        return $this;
    }

    public function getParserType()
    {
        return $this -> parserType;
    }

} // ModuleOptions
