<?php
namespace Markdown\Options;

interface MarkdownInterface
{
    public function setParserType($parserType);

    public function getParserType();
}
