<?php
namespace Markdown;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use dflydev\markdown\IMarkdownParser as MarkdownParserInterface;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ServiceProviderInterface,
    ViewHelperProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    } // getAutoloaderConfig()


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'markdown' => function($sm) {
                    $parser = $sm -> getServiceLocator() -> get('Markdown\Parser');
                    $helper = new View\Helper\Markdown();
                    $helper -> setParser($parser);
                    return $helper;
                }
            )
        );
    } // getViewHelperConfig()


    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'markdown_options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['markdown']) ? $config['markdown'] : null;
                    return new Options\ModuleOptions($options);
                },
                'Markdown\Parser' => 'Markdown\MarkdownFactory'
            )
        );
    } // getServiceConfig()

} //  Module
