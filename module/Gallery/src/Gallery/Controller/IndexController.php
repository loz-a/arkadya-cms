<?php
namespace Gallery\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\PhpEnvironment\Response as HttpResponse;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as PaginatorAdapter;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    /**
     * @var \Gallery\Service\Gallery
     */
    protected $galleryService;


    public function getGalleryService()
    {
        if (null === $this -> galleryService) {
            $this -> galleryService = $this -> serviceLocator -> get('Gallery\Service');
        }
        return $this -> galleryService;
    } // getGalleryService()


    public function indexAction()
    {
        $page = (int) $this -> params() -> fromQuery('page', 1);

        $images = $this -> getGalleryService() -> getGalleryMapper() -> fetchAll();
        $images = new Paginator(new PaginatorAdapter($images));
        $images -> setCurrentPageNumber($page);
        $images -> setItemCountPerPage($images -> getDefaultItemCountPerPage());

        if ($this -> getRequest() -> isXmlHttpRequest()) {
            $vm = new ViewModel();
            $vm
                -> setTerminal(true)
                -> setTemplate('gallery/index/index/ajax')
                -> setVariable('images', iterator_to_array($images -> getCurrentItems()));
            return $vm;
        }
        else {
            return array(
                'uploadPath' => $this -> getServiceLocator() -> get('gallery_options') -> getUploadPath(),
                'images' => iterator_to_array($images -> getCurrentItems()),
                'paging' => $images -> getPages()
            );
        }

    } // indexAction()


    public function manageAction()
    {
        return array(
            'images' => $this -> getGalleryService() -> getGalleryMapper() -> fetchAll()
        );
    } // manageAction()


    public function uploadAction()
    {
        $form = $this -> getGalleryService() -> getForm('Gallery\UploadImage');
        $prg  = $this -> fileprg($form);

        if ($prg instanceof HttpResponse) {
            return $prg;
        }
        else if (is_array($prg)) {

            if ($form -> isValid()) {
                $result = $this -> getGalleryService() -> upload($form -> getData());
                if ($result) {
                    $this -> flashMessenger() -> addMessage('Image was uploaded');
                    return $this -> redirect() -> toRoute('manage_gallery');
                }
            }
            $this -> flashMessenger() -> addMessage('Something was wrong');
        }

        return array(
            'form' => $form
        );
    } // uploadAction()


    public function editAction()
    {
        $id = $this -> params('id');
        $form = $this -> getGalleryService() -> getPopulatedFormById($id);

        if (!$form) {
            return $this -> notFoundAction();
        }

        return array(
            'form' => $form
        );
    } // editAction()


    public function editSubmitAction()
    {
        $form = $this -> getGalleryService() -> getForm('Gallery\EditImage');
        $form -> setData($this -> getRequest() -> getPost());

        if ($form -> isValid()) {
            $result = $this -> getGalleryService() -> edit($form -> getData());
            if ($result) {
                $this -> flashMessenger() -> addMessage('Image attributes was edited');
                return $this -> redirect() -> toRoute('manage_gallery');
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // editSubmitAction()


    public function deleteAction()
    {
        $id = $this -> params('id');
        if (!$id) {
            return $this -> notFoundAction();
        }

        if ($this -> getRequest() -> isPost()) {
            $confirm = $this -> getRequest() -> getPost() -> get('confirm', 'no');

            if (strtolower($confirm) === 'yes') {
                $this -> getGalleryService() -> deleteImage($id);
                $this -> flashMessenger() -> addMessage('Image was removed');
            }
            return $this -> redirect() -> toRoute('manage_gallery');
        }
        else {
            $image = $this -> getGalleryService() -> getGalleryMapper() -> findById($id);
            if (!$image) {
                return $this -> notFoundAction();
            }
        }

        return array(
            'image' => $image
        );
    } // deleteAction()

} // IndexController
