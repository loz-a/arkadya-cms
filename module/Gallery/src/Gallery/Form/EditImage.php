<?php
namespace Gallery\Form;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class EditImage extends Form
    implements ServiceLocatorAwareInterface
{
    /**
     * @var \Zend\Form\FormElementManager
     */
    protected $fem;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this -> fem = $serviceLocator;
        return $this;
    } // setServiceLocator()


    public function getServiceLocator()
    {
        return $this -> fem;
    } // getServiceLocator()


    public function getActionRouteName()
    {
        return 'manage_gallery/edit/submit';
    } // getActionRouteName()


    public function init()
    {
        $modOptions = $this -> fem -> getServiceLocator() -> get('gallery_options');

        $this
            -> setName('UploadImage')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add(array(
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden',
            ))
            -> add(array(
                'name' => 'title',
                'options' => array(
                    'label' => 'Title'
                ),
                'attributes' => array(
                    'required'  => true,
                    'maxlength' => $modOptions -> getTitleMaxLength()
                )
            ))
            -> add(array(
                'name' => 'alt',
                'options' => array(
                    'label' => 'Alt'
                ),
                'attributes' => array(
                    'required'  => true,
                    'maxlength' => $modOptions -> getAltMaxLength()
                )
            ))
            -> add(array(
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf',
                'options' => array(
                    'csrf_options' => array(
                        'timeout' => 6000
                    )
                )
            ))
            -> add(array(
                'name' => 'submit',
                'attributes' => array(
                    'class' => 'btn btn-primary',
                    'type'  => 'submit',
                    'value' => 'Ok'
                )
            ));

    } // init()

} // EditImage
