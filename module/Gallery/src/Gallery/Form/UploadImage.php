<?php
namespace Gallery\Form;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UploadImage extends Form
    implements ServiceLocatorAwareInterface
{
    /**
     * @var \Zend\Form\FormElementManager
     */
    protected $fem;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this -> fem = $serviceLocator;
        return $this;
    } // setServiceLocator()


    public function getServiceLocator()
    {
        return $this -> fem;
    } // getServiceLocator()


    public function getActionRouteName()
    {
        return 'manage_gallery/upload';
    } // getActionRouteName()


    public function init()
    {
        $modOptions = $this -> fem -> getServiceLocator() -> get('gallery_options');

        $this
            -> setName('UploadImage')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add(array(
                'name'    => 'image_file',
                'type'    => 'Zend\Form\Element\File',
                'options' => array(
                    'label' => 'Image'
                ),
                'attributes' => array(
                    'required' => true,
                    'class'    => 'span4'
                )
            ))
            -> add(array(
                'name' => 'title',
                'options' => array(
                    'label' => 'Title'
                ),
                'attributes' => array(
                    'required'  => true,
                    'maxlength' => $modOptions -> getTitleMaxLength()
                )
            ))
            -> add(array(
                'name' => 'alt',
                'options' => array(
                    'label' => 'Alt'
                ),
                'attributes' => array(
                    'required'  => true,
                    'maxlength' => $modOptions -> getAltMaxLength()
                )
            ))
            -> add(array(
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf',
                'options' => array(
                    'csrf_options' => array(
                        'timeout' => 6000
                    )
                )
            ))
            -> add(array(
                'name' => 'submit',
                'attributes' => array(
                    'class' => 'btn btn-primary',
                    'type'  => 'submit',
                    'value' => 'Ok'
                )
            ));

    } // init()

} // UploadImage
