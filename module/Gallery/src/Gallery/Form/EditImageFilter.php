<?php
namespace Gallery\Form;

use Zend\InputFilter\InputFilter;
use Gallery\Options\GalleryInterface as OptionsInterface;

class EditImageFilter extends InputFilter
{
    public function __construct(OptionsInterface $galleryOptions)
    {
        $this
            -> add(array(
                'name' => 'id',
                'required' => true,
                'filters' => array(
                    array('name' => 'Int')
                )
            ))
            -> add(array(
                'name'     => 'title',
                'required' => true,
                'break_chain_on_failure' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => $galleryOptions -> getTitleMaxLength()
                        )
                    )
                )
            ))
            -> add(array(
                'name'     => 'alt',
                'required' => true,
                'break_chain_on_failure' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => $galleryOptions -> getAltMaxLength()
                        )
                    )
                )
            ));
    } // __construct()

} // UploadImageFilter
