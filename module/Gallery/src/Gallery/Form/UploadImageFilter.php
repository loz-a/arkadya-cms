<?php
namespace Gallery\Form;

use Zend\InputFilter\InputFilter;
use Zend\Http\PhpEnvironment\Request as HttpRequest;
use Gallery\Options\GalleryInterface as OptionsInterface;
use Common\Filter\Translit as TranslitFilter;
use Common\Filter\Imagick\Strategy\CropThumbnail;

class UploadImageFilter extends InputFilter
{
    /**
     * @var \Zend\Http\PhpEnvironment\Request
     */
    protected $request;

    public function __construct(OptionsInterface $galleryOptions, HttpRequest $request)
    {
        $this -> request = $request;

        $this
            -> add(array(
                'name'     => 'image_file',
                'required' => true,
                'break_chain_on_failure' => true,
                'validators' => array(
                    array('name' => 'File\IsImage'),
                    array(
                        'name'    => 'File\Extension',
                        'options' => array(
                            'extension' => array('jpeg', 'jpg')
                        )
                    ),
                    array(
                        'name'    => 'File\Size',
                        'options' => array(
                            'max' => '2MB'
                        )
                    )
                ),
                'filters' => array(
                    array(
                        'name'    => 'FileRenameUpload',
                        'options' => array(
                            'target'    => $this -> getFileTarget($galleryOptions),
                            'randomize' => true
                        )
                    ),
                    array(
                        'name' => 'Common\Filter\Imagick\Resizer',
                        'options' => array(
                            'width'     => $galleryOptions -> getLargeImageWidth(),
                            'height'    => $galleryOptions -> getLargeImageHeigth(),
                            'quality'   => $galleryOptions -> getLargeImageQuality(),
                        ),
                    ),
                    array(
                        'name' => 'Common\Filter\Imagick\Resizer',
                        'options' => array(
                            'width'     => $galleryOptions -> getMediumImageWidth(),
                            'height'    => $galleryOptions -> getMediumImageHeigth(),
                            'quality'   => $galleryOptions -> getMediumImageQuality(),
                            'thumb_dir' => $galleryOptions -> getMediumImageThumbDir()
                        )
                    ),
                    array(
                        'name' => 'Common\Filter\Imagick\Resizer',
                        'options' => array(
                            'strategy'  => new CropThumbnail(),
                            'width'     => $galleryOptions -> getSmallImageWidth(),
                            'height'    => $galleryOptions -> getSmallImageHeigth(),
                            'quality'   => $galleryOptions -> getSmallImageQuality(),
                            'thumb_dir' => $galleryOptions -> getSmallImageThumbDir()
                        )
                    )
                )
            ))
            -> add(array(
                'name'     => 'title',
                'required' => true,
                'break_chain_on_failure' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => $galleryOptions -> getTitleMaxLength()
                        )
                    )
                )
            ))
            -> add(array(
                'name'     => 'alt',
                'required' => true,
                'break_chain_on_failure' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => $galleryOptions -> getAltMaxLength()
                        )
                    )
                )
            ));
    } // __construct()


    protected function getFileTarget(OptionsInterface $galleryOptions)
    {
        $docRoot    = $this -> request -> getServer('DOCUMENT_ROOT');
        $uploadPath = $galleryOptions -> getUploadPath();
        $targetDir  = sprintf('%s%s', $docRoot, $uploadPath);

        if (!is_dir($targetDir)) {
            mkdir($targetDir, 0777, true);
        }

        $translit = new TranslitFilter();
        $translit -> setSpaceReplacer('-');
        $file = $this -> request -> getFiles('image_file');

        return $targetDir . DIRECTORY_SEPARATOR . $translit($file['name']);
    } // getFileTarget()

} // UploadImageFilter
