<?php
namespace Gallery\Entity;

interface ImageInterface
{
    public function setId($id);

    public function getId();

    public function setRelativePath($path);

    public function getRelativePath();

    public function setAbsolutePath($path);

    public function getAbsolutePath();

    public function setTitle($title);

    public function getTitle();

    public function setAlt($alt);

    public function getAlt();

    public function getThumbsPaths();
}
