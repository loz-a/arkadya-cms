<?php
namespace Gallery\Entity;

use Common\Image\ThumbsPathsResolver;

class Image implements ImageInterface
{
    protected $id;
    protected $title;
    protected $alt;
    protected $relPath;
    protected $absPath;

    /**
     * @var \Common\Image\ThumbsPathsResolver
     */
    protected $thumbsPathsResolver;


    public function __construct()
    {
        $this -> thumbsPathsResolver = new ThumbsPathsResolver();
    } // __construct()


    public function __clone()
    {
        $this -> thumbsPathsResolver = new ThumbsPathsResolver();
    } // __clone()


    public function setId($id)
    {
        $this -> id = (int) $id;
        return $this;
    } // setId()

    public function getId()
    {
        return $this -> id;
    } // getId()


    public function setRelativePath($path)
    {
        $this -> relPath = (string) $path;
        $this -> thumbsPathsResolver -> setRelativePath($this -> relPath);
        return $this;
    } // setRelativePath()

    public function getRelativePath()
    {
        return $this -> relPath;
    } // getRelativePath()


    public function setAbsolutePath($path)
    {
        $this -> absPath = (string) $path;
        $this -> thumbsPathsResolver -> setAbsolutePath($this -> absPath);
        return $this;
    } // setAbsolutePath()


    public function getAbsolutePath()
    {
        return $this -> absPath;
    } // getAbsolutePath()


    public function setTitle($title)
    {
        $this -> title = (string) $title;
        return $this;
    } // setTitle()

    public function getTitle()
    {
        return $this -> title;
    } // getTitle()


    public function setAlt($alt)
    {
        $this -> alt = (string) $alt;
        return $this;
    } // setAlt()

    public function getAlt()
    {
        return $this -> alt;
    } // getAlt()


    public function getThumbsPaths()
    {
        return $this -> thumbsPathsResolver;
    } // getPaths()

} // Image
