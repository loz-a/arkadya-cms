<?php
namespace Gallery\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\AbstractOptions;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;
use Common\Mapper\AbstractDbMapper;

class Gallery implements ServiceManagerAwareInterface
{
    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    protected $sm;

    /**
     * @var \Zend\Stdlib\AbstractOptions
     */
    protected $options;

    /**
     * @var \Common\Mapper\AbstractDbMapper
     */
    protected $galleryMapper;

    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     * @return Gallery
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> sm = $serviceManager;
        return $this;
    } // setServiceManager()


    /**
     * @param \Zend\Stdlib\AbstractOptions $options
     * @return Gallery
     */
    public function setOptions(AbstractOptions $options)
    {
        $this -> options = $options;
        return $this;
    } // setOptions()


    /**
     * @return null|\Zend\Stdlib\AbstractOptions
     */
    public function getOptions()
    {
        if ( null === $this -> options ) {
            $this -> setOptions($this -> sm -> get('gallery_options'));
        }
        return $this -> options;
    } // getOptions()


    /**
     * @param \Common\Mapper\AbstractDbMapper $mapper
     * @return Gallery
     */
    public function setGalleryMapper(AbstractDbMapper $mapper)
    {
        $this -> galleryMapper = $mapper;
        return $this;
    } // setGalleryMapper()


    /**
     * @return \Common\Mapper\AbstractDbMapper
     */
    public function getGalleryMapper()
    {
        if( null === $this -> galleryMapper ) {
            $this -> setGalleryMapper($this -> sm -> get('Gallery\Mapper\Gallery'));
        }
        return $this -> galleryMapper;
    } // getGalleryMapper()


    /**
     * @param string $alias
     * @return \Zend\Form\Form
     */
    public function getForm($alias)
    {
        return $this -> sm -> get('FormElementManager') -> get($alias);
    } // getForm()


    public function upload(array $data)
    {
        $filename = $data['image_file']['tmp_name'];
        $image['absolute_path'] = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $filename);
        $image['relative_path'] = sprintf('%s/%s', $this -> getOptions() -> getUploadPath(), pathinfo($filename, PATHINFO_BASENAME));
        $image['title'] = $data['title'];
        $image['alt']   = $data['alt'];

        return $this -> getGalleryMapper() -> insert($image);
    } // upload()


    public function edit(array $data)
    {
        $id = (int) $data['id'];
        $image = $this -> getGalleryMapper() -> findById($id);

        if (!$image) {
            return null;
        }

        $hydrator = new ClassMethodsHydrator();
        $image = $hydrator -> hydrate($data, $image);
        $this -> getGalleryMapper() -> update($image, array('id' => $id));
        return $image;
    } // edit()


    public function deleteImage($id)
    {
        $id = (int) $id;
        $image = $this -> getGalleryMapper() -> findById($id);

        if (!$image) {
            return null;
        }

        $f  = $image -> getAbsolutePath();
        $fm = $image -> getThumbsPaths() -> getAbsolutePathToMedium();
        $fs = $image -> getThumbsPaths() -> getAbsolutePathToSmall();

        $this -> getGalleryMapper() -> removeById($id);

        unlink($f); unlink($fm); unlink($fs);
        return true;
    } // deleteById()


    /**
     * @param int $id
     * @return null|\Zend\Form\Form
     */
    public function getPopulatedFormById($id)
    {
        $image = $this -> getGalleryMapper() -> findById($id);
        if (!$image) {
            return null;
        }

        $hydrator = new ClassMethodsHydrator();
        $form = $this -> getForm('Gallery\EditImage');
        $form -> setData($hydrator -> extract($image));
        return $form;
    } // getPopulatedFormById()

} // Gallery
