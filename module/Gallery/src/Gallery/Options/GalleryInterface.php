<?php
namespace Gallery\Options;

interface GalleryInterface
{
    public function setTableName($tableName);

    public function getTableName();

    public function setTranslationTableName($tableName);

    public function getTranslationTableName();

    public function setUploadPath($uploadPath);

    public function getUploadPath();

    public function setLargeImageWidth($width);

    public function getLargeImageWidth();

    public function setLargeImageHeigth($height);

    public function getLargeImageHeigth();

    public function setLargeImageQuality($quality);

    public function getLargeImageQuality();

    public function setMediumImageWidth($width);

    public function getMediumImageWidth();

    public function setMediumImageHeigth($height);

    public function getMediumImageHeigth();

    public function setMediumImageQuality($quality);

    public function getMediumImageQuality();

    public function getMediumImageThumbDir();

    public function setSmallImageWidth($width);

    public function getSmallImageWidth();

    public function setSmallImageHeigth($height);

    public function getSmallImageHeigth();

    public function setSmallImageQuality($quality);

    public function getSmallImageQuality();

    public function getSmallImageThumbDir();

    public function setImageMaxLength($maxlen);

    public function getImageMaxLength();

    public function setAltMaxLength($maxlen);

    public function getAltMaxLength();

    public function setTitleMaxLength($maxlen);

    public function getTitleMaxLength();
}
