<?php
namespace Gallery\Options;

use Common\Mapper\Options\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements GalleryInterface
{
    protected $tableName = 'gallery';

    protected $translationsTableName = 'gallery_translation';

    public function setTranslationTableName($tableName)
    {
        $this -> translationsTableName  = (string) $tableName;
        return $this;
    }

    public function getTranslationTableName()
    {
        return $this -> translationsTableName;
    }


    protected $uploadPath = '/media/gallery';

    public function setUploadPath($uploadPath)
    {
        $this -> uploadPath = (string) $uploadPath;
        return $this;
    }

    public function getUploadPath()
    {
        return $this -> uploadPath;
    }


    protected $largeImageWidth = 600;

    public function setLargeImageWidth($width)
    {
        $this -> largeImageWidth = (int) $width;
        return $this;
    }

    public function getLargeImageWidth()
    {
        return $this -> largeImageWidth;
    }


    protected $largeImageHeight = 600;

    public function setLargeImageHeigth($height)
    {
        $this -> largeImageHeight = (int) $height;
        return $this;
    }

    public function getLargeImageHeigth()
    {
        return $this -> largeImageHeight;
    }


    protected $largeImageQuality = 85;

    public function setLargeImageQuality($quality)
    {
        $this -> largeImageQuality = (int) $quality;
        return $this;
    }

    public function getLargeImageQuality()
    {
        return $this -> largeImageQuality;
    }


    protected $mediumImageWidth = 480;

    public function setMediumImageWidth($width)
    {
        $this -> mediumImageWidth = (int) $width;
        return $this;
    }

    public function getMediumImageWidth()
    {
        return $this -> mediumImageWidth;
    }


    protected $mediumImageHeight = 320;

    public function setMediumImageHeigth($height)
    {
        $this -> mediumImageHeight = (int) $height;
        return $this;
    }

    public function getMediumImageHeigth()
    {
        return $this -> mediumImageHeight;
    }


    protected $mediumImageQuality = 75;

    public function setMediumImageQuality($quality)
    {
        $this -> mediumImageQuality = (int) $quality;
        return $this;
    }

    public function getMediumImageQuality()
    {
        return $this -> mediumImageQuality;
    }


    public function getMediumImageThumbDir()
    {
        return 'medium';
    }


    protected $smallImageWidth = 70;

    public function setSmallImageWidth($width)
    {
        $this -> smallImageWidth = (int) $width;
        return $this;
    }

    public function getSmallImageWidth()
    {
        return $this -> smallImageWidth;
    }


    protected $smallImageHeight = 45;

    public function setSmallImageHeigth($height)
    {
        $this -> smallImageHeight = (int) $height;
        return $this;
    }

    public function getSmallImageHeigth()
    {
        return $this -> smallImageHeight;
    }


    protected $smallImageQuality = 65;

    public function setSmallImageQuality($quality)
    {
        $this -> smallImageQuality = (int) $quality;
    }

    public function getSmallImageQuality()
    {
        return $this -> smallImageQuality;
    }


    public function getSmallImageThumbDir()
    {
        return 'small';
    }


    protected $imageMaxLength = 255;

    public function setImageMaxLength($maxlen)
    {
        $this -> imageMaxLength = (int) $maxlen;
        return $this;
    }

    public function getImageMaxLength()
    {
        return $this -> imageMaxLength;
    }


    protected $altMaxLength = 50;

    public function setAltMaxLength($maxlen)
    {
        $this -> altMaxLength = (int) $maxlen;
        return $this;
    }

    public function getAltMaxLength()
    {
        return $this -> altMaxLength;
    }


    protected $titleMaxLength = 50;

    public function setTitleMaxLength($maxlen)
    {
        $this -> titleMaxLength = (int) $maxlen;
        return $this;
    }

    public function getTitleMaxLength()
    {
        return $this -> titleMaxLength;
    }

} // ModuleOptions
