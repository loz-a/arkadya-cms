<?php
namespace Gallery\Mapper;

use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Common\Mapper\AbstractDbMapper;
use CMSLocale\Service\CurrentLocale as CurLocaleService;
use ArrayIterator;

class Gallery extends AbstractDbMapper
    implements GalleryInterface
{
    /**
     * @var \CMSLocale\Service\CurrentLocale
     */
    protected $curLocaleService;

    /**
     * @param \CMSLocale\Service\CurrentLocale $service
     * @return Gallery
     */
    public function setCurrentLocaleService(CurLocaleService $service)
    {
        $this -> curLocaleService = $service;
        return $this;
    } // setCurrentLocaleService()


    /**
     * @return \CMSLocale\Service\CurrentLocale
     * @throws \LogicException
     */
    public function getCurrentLocaleService()
    {
        if ( !$this -> curLocaleService ) {
            throw new \LogicException('Current locale service is undefined');
        }
        return $this -> curLocaleService;
    } // getCurrentLocaleService()


    public function fetchAll()
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $sql = "SELECT `g`.`id`, `g`.`relative_path`, `g`.`absolute_path`, `t`.`alt`, `t`.`title`
                FROM `{$this -> getTableName()}` as g
                LEFT JOIN `{$this -> options -> getTranslationTableName()}` as t
                ON `g`.`id` = `t`.`id` AND `t`.`locale_id` = {$localeEntity -> getId()}";

        $statement = $this -> getDbAdapter() -> query($sql);
        $result = $statement -> execute();
        $resultSet = $this -> getResultSet();
        $resultSet -> initialize($result);

        // buffer for paging
        $resultSet -> buffer();

        return $resultSet -> count() ? $resultSet : new ArrayIterator(array());
    } // fetchAll()


    public function findById($id)
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();
        $id = (int) $id;

        $sql = "SELECT `g`.`id`, `g`.`relative_path`, `g`.`absolute_path`, `t`.`alt`, `t`.`title`
                FROM `{$this -> getTableName()}` as g
                LEFT JOIN `{$this -> options -> getTranslationTableName()}` as t
                ON `g`.`id` = `t`.`id` AND `t`.`locale_id` = {$localeEntity -> getId()}
                WHERE `g`.`id` = {$id}
                LIMIT 1";

        $statement = $this -> getDbAdapter() -> query($sql);
        $result = $statement -> execute();
        $resultSet = $this -> getResultSet();
        $resultSet -> initialize($result);

        return $resultSet -> count() ? $resultSet -> current() : null;
    } // findById()


    public function lookupTranslationById($id, $exclude = null)
    {
        $select = $this
            -> select()
            -> columns(array('id'))
            -> from($this -> options -> getTranslationTableName())
            -> where(array('id' => $id))
            -> where(array('locale_id' => $this -> getCurrentLocaleService() -> getEntity() -> getId()))
            -> limit(1);

        if ($exclude) {
            $select -> where($exclude);
        }
        return (bool) count($this -> selectExecute($select));
    } // lookupTranslationById()


    public function insert($entity, $tableName = null, HydratorInterface $hydrator = null)
    {
        $id = null;
        $rowData = $this -> entityToArray($entity, $hydrator);

        $this -> getConnection() -> beginTransaction();
        try {

            $image = array(
                'relative_path' => $rowData['relative_path'],
                'absolute_path' => $rowData['absolute_path']
            );
            parent::insert($image);
            $id = $this -> getConnection() -> getLastGeneratedValue();

            $translation = array(
                'id'        => $id,
                'locale_id' => $this -> getCurrentLocaleService() -> getEntity() -> getId(),
                'title'     => $rowData['title'],
                'alt'       => $rowData['alt']
            );

            parent::insert($translation, $this -> options -> getTranslationTableName());
            unset($image, $translation, $rowData);

            $this -> getConnection() -> commit();
        }
        catch(\Exception $e) {
            $this -> getConnection() -> rollback();
            throw new \Exception('Image can\'t addded');
        }
        return $id;
    } // insert()


    public function update($entity, $where, $tableName = null, HydratorInterface $hydrator = null)
    {
        $rowData  = $this -> entityToArray($entity, $hydrator);
        $localeId = $this -> getCurrentLocaleService() -> getEntity() -> getId();
        $id = (int) $where[id];

        $this -> getConnection() -> beginTransaction();
        try {
            $image = array(
                'alt'   => $rowData['alt'],
                'title' => $rowData['title']
            );

            if ($this -> lookupTranslationById($id)) {
                $where = array(
                    'id' => $id,
                    'locale_id' => $localeId
                );
                parent::update($image, $where, $this -> options -> getTranslationTableName());
            }
            else {
                $image['id'] = $id;
                $image['locale_id'] = $localeId;
                parent::insert($image, $this -> options -> getTranslationTableName());
            }
            unset($image, $rowData);
            $this -> getConnection() -> commit();
        }
        catch(\Exception $e) {
            $this -> getConnection() -> rollback();
            throw new \Exception('Image attributes can\'t be updated');
        }
        return $id;
    } // update()


    public function removeById($id)
    {
        $sql  = new Sql($this -> getDbAdapter(), $this -> getTableName());
        $del  = $sql -> delete() -> where(array('id' => $id));
        $stmt = $sql -> prepareStatementForSqlObject($del);
        $res  = $stmt -> execute();
        return $res -> getAffectedRows();
    } // removeById()

} // Gallery
