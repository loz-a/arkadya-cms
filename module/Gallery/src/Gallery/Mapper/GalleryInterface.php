<?php
namespace Gallery\Mapper;

interface GalleryInterface
{
    public function fetchAll();

    public function findById($id);

    public function lookupTranslationById($id);
}
