<?php
return array(

    'controllers' => array(
        'invokables' => array(
            'Gallery\Controller\Index' => 'Gallery\Controller\IndexController',
        )
    ),

    'view_manager' => array(
        'template_map' => array(
            'gallery/index/index'       => __DIR__ . '/../view/gallery/index/index.phtml',
            'gallery/index/index/ajax'  => __DIR__ . '/../view/gallery/index/index.ajax.phtml',
            'gallery/index/manage'      => __DIR__ . '/../view/gallery/index/manage.phtml',
            'gallery/index/upload'      => __DIR__ . '/../view/gallery/index/upload.phtml',
            'gallery/index/edit'        => __DIR__ . '/../view/gallery/index/edit.phtml',
            'gallery/index/edit-submit' => __DIR__ . '/../view/gallery/index/edit.phtml',
            'gallery/index/delete'      => __DIR__ . '/../view/gallery/index/delete.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    ),

    'router' => array(
        'routes' => array(
            'gallery' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/gallery.html',
                    'defaults' => array(
                        'controller' => 'Gallery\Controller\Index',
                        'action' => 'index'
                    )
                )
            ),

            'manage_gallery' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/manage-gallery',
                    'defaults' => array(
                        'controller' => 'Gallery\Controller\Index',
                        'action' => 'manage'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'upload' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route' => '/upload',
                            'defaults' => array(
                                'action' => 'upload'
                            )
                        ),
                        'may_terminate' => true
                    ),
                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'       => '/edit/:id',
                            'constraints' => array('id' => '\d+')
                        ),
                        'may_terminate' => false,
                        'child_routes' => array(
                            'send' => array(
                                'type'    => 'Method',
                                'options' => array(
                                    'verb'     => 'get',
                                    'defaults' => array('action' => 'edit')
                                ),
                                'may_terminate' => true
                            ),
                            'submit' => array(
                                'type'    => 'Method',
                                'options' => array(
                                    'verb'     => 'post',
                                    'defaults' => array('action' => 'edit-submit')
                                ),
                                'may_terminate' => true
                            )
                        )
                    ),
                    'delete' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route'       => '/delete/:id',
                            'constraints' => array('id' => '\d+'),
                            'defaults'    => array('action' => 'delete')
                        )
                    )
                ),
            ), // manage_gallery
        ) // routes
    ), // router

    'route_layouts' => array(
        'manage_gallery'             => 'layout/admin',
        'manage_gallery/upload'      => 'layout/admin',
        'manage_gallery/edit/send'   => 'layout/admin',
        'manage_gallery/edit/submit' => 'layout/admin',
        'manage_gallery/delete'      => 'layout/admin',
        'gallery'                    => 'layout/layout',
    ),

    'admin_navigation' => array(
        'gallery' => array(
            'index' => 5,
            'header' => array(
                'label' => 'Gallery',
                'icon' => 'icon-picture',
                'clas_name' => 'gallery'
            ),
            'items' => array(
                array(
                    'label' => 'All Images',
                    'route_name' => 'manage_gallery'
                ),
                array(
                    'label' => 'Upload',
                    'route_name' => 'manage_gallery/upload'
                )
            )
        )
    ),
);