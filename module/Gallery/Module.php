<?php
namespace Gallery;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class Module implements
    ConfigProviderInterface,
    AutoloaderProviderInterface,
    DependencyIndicatorInterface,
    FormElementProviderInterface,
    ServiceProviderInterface
{
    public function getModuleDependencies()
    {
        return array('Common', 'CMSPages');
    } // getModuleDependencies()


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    } // getAutoloaderConfig()


    public function getFormElementConfig()
    {
        return array(
            'factories' => array(
                'Gallery\UploadImage' => function($sm) {
                    $sl = $sm -> getServiceLocator();
                    $filter = new Form\UploadImageFilter($sl -> get('gallery_options'), $sl -> get('Request'));
                    $form = new Form\UploadImage();
                    $form -> setInputFilter($filter);
                    return $form;
                },
                'Gallery\EditImage' => function($sm) {
                    $sl = $sm -> getServiceLocator();
                    $filter = new Form\EditImageFilter($sl -> get('gallery_options'));
                    $form = new Form\EditImage();
                    $form -> setInputFilter($filter);
                    return $form;
                }
            )
        );
    } // getFormElementConfig()

    
    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'Gallery\Service' => 'Gallery\Service\Gallery',
            ),
            'factories' => array(
                'gallery_options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['gallery_options']) ? $config['gallery_options'] : array();
                    return new Options\ModuleOptions($options);
                },
                'Gallery\Mapper\Gallery' => function($sm) {
                    $mapper = new Mapper\Gallery();
                    $mapper
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setEntityPrototype(new Entity\Image())
                        -> setHydrator(new ClassMethodsHydrator())
                        -> setOptions($sm -> get('gallery_options'))
                        -> setCurrentLocaleService($sm -> get('CMSLocale\CurrentLocale'));
                    return $mapper;
                }
            )
        );
    } // getServiceConfig()

} // Module
