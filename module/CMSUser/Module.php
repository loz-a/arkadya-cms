<?php
namespace CMSUser;

use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class Module implements
    DependencyIndicatorInterface,
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ControllerPluginProviderInterface,
    FormElementProviderInterface,
    ViewHelperProviderInterface,
    ServiceProviderInterface
{
    public function getModuleDependencies()
    {
        return ['Common'];
    } // getModuleDependencies()


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    } // getAutoloaderConfig()


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getControllerPluginConfig()
    {
        return array(
            'factories' => array(
                'userIdentity' => function($sm) {
                    return (new Controller\Plugin\UserIdentity())
                        -> setIdentityService($sm -> getServiceLocator() -> get('CMSUser\Identity'));
                }
            ),
        );
    } // getControllerPluginConfig()


    public function getFormElementConfig()
    {
        return array(
            'invokables' => array(
                'CMSUser\Login'      => 'CMSUser\Form\Login'
            ),
        );
    } // getFormElementConfig()


    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'userIdentity' => function($sm) {
                    return (new View\Helper\UserIdentity())
                        -> setIdentityService($sm -> getServiceLocator() -> get('CMSUser\Identity'));
                },
            )
        );
    } // getViewHelperConfig()


    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'CMSUser\Service\Users' => 'CMSUser\Service\Users',
            ),
            'factories' => array(
                'CMSUser\Options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['cms_user_options']) ? $config['cms_user_options'] : array();
                    return new Options\ModuleOptions($options);
                },
                'CMSUser\Mapper\Users' => function($sm) {
                    return (new Mapper\Users())
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setOptions($sm -> get('CMSUser\Options'))
                        -> setHydrator(new ClassMethodsHydrator())
                        -> setEntityPrototype(new Entity\User())
                        -> setRolesTableName($sm -> get('CMSRbac\Options') -> getRolesTableName());
                },
                'CMSUser\Identity' => function($sm) {
                    return (new Service\UserIdentity())
                        -> setAuthService($sm -> get('CMSUser\Auth'))
                        -> setMapper($sm -> get('CMSUser\Mapper\Users'));
                },
                'CMSUser\Auth' => 'CMSUser\Authentication\AuthServiceFactory'
            ),
        );
    } // getServiceConfig()

} // Module