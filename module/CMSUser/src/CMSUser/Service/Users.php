<?php
namespace CMSUser\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Common\Service\ServiceInterface;
use Common\Service\OptionsTrait;
use Common\Mapper\MapperTrait;
use Common\Service\FormTrait;
use Zend\Crypt\Password\Bcrypt;

class Users implements
    ServiceManagerAwareInterface,
    EventManagerAwareInterface,
    ServiceInterface
{
    use EventManagerAwareTrait, OptionsTrait, MapperTrait, FormTrait;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    protected $authService;

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> serviceManager = $serviceManager;
    } // setServiceManager()


    public function getAuthService()
    {
        if (null === $this -> authService) {
            $this -> authService = $this -> serviceManager -> get('CMSUser\Auth');
        }
        return $this -> authService;
    } // getAuthService()


    public function changePassword($passwd)
    {
        $auth = $this -> getAuthService();
        if (!$auth -> hasIdentity()) {
            return false;
        }
        $identity = $auth -> getIdentity();
        $bcrypt = new Bcrypt();
        $bcrypt -> setCost($this -> getOptions() -> getPasswordCost());

        return (bool) $this
            -> getMapper()
            -> update(
                ['password' => $bcrypt -> create($passwd)],
                ['id' => $identity]
            );
    } // changePassword()


    public function login($login, $passwd)
    {
        $auth = $this -> getAuthService();

        if ($auth -> hasIdentity()) {
            $auth -> clearIdentity();
        }

        $auth
            -> getAdapter()
            -> setIdentity($login)
            -> setCredential($passwd);

        $result = $auth -> authenticate();

        if ($result -> isValid()) {
            $this -> getMapper() -> update(
                ['last_login' => time()],
                ['id' => $result -> getIdentity()]
            );
        }
        return $result;
    } // login()


    public function logout()
    {
        $this -> getAuthService() -> clearIdentity();
    } // logout()


    public function add(array $data)
    {
    } // add()

    public function edit(array $data)
    {
    } // edit()


} // User
