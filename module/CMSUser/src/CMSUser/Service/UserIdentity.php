<?php
namespace CMSUser\Service;

use Zend\Authentication\AuthenticationService;
use CMSUser\Mapper\Users as UsersMapper;

class UserIdentity
{
    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @var UserSMapper
     */
    protected $userMapper;

    /**
     * @var \stdClass
     */
    protected $user;

    /**
     * @return \stdClass
     */
    public function getIdentity()
    {
        $user = $this -> user;
        $auth = $this -> getAuthService();

        if (!$auth -> hasIdentity()) {
            $this -> user = null;
            return null;
        }

        $identity = $auth -> getIdentity();
        if (!$user or $user -> id != $identity) {
            $user = $this -> getMapper() -> findById($identity);
            if (!$user) {
                return null;
            }
            $user['password'] = null;
            $this -> user = (object) $user;
        }
        return $this -> user;
    } // getIdentity()


    /**
     * @param \Zend\Authentication\AuthenticationService $authService
     * @return UserIdentity
     */
    public function setAuthService(AuthenticationService $authService)
    {
        $this -> authService = $authService;
        return $this;
    } // setAuthService()

    /**
     * @return \Zend\Authentication\AuthenticationService
     */
    public function getAuthService()
    {
        return $this -> authService;
    } // getAuthService()


    /**
     * @param \CMSUser\Mapper\Users $mapper
     * @return UserIdentity
     */
    public function setMapper(UsersMapper $mapper)
    {
        $this -> userMapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @return \CMSUser\Mapper\Users
     */
    public function getMapper()
    {
        return $this -> userMapper;
    } // getMapper()

} // UserIdentity
