<?php
namespace CMSUser\View\Helper;

use Zend\View\Helper\AbstractHelper;
use CMSUser\Service\UserIdentity as IdentityService;

class UserIdentity extends AbstractHelper
{
    /**
     * @var \CMSUser\Service\UserIdentity
     */
    protected $userIdentity;

    public function __invoke()
    {
        return $this -> getIdentityService() -> getIdentity();
    } // __invoke()


    /**
     * @param \CMSUser\Service\UserIdentity $identityService
     * @return UserIdentity
     */
    public function setIdentityService(IdentityService $identityService)
    {
        $this -> userIdentity = $identityService;
        return $this;
    } // setIdentityService()


    /**
     * @return \CMSUser\Service\UserIdentity
     * @throws \LogicException
     */
    public function getIdentityService()
    {
        if ( !$this -> userIdentity ) {
            throw new \LogicException('User identity service is undefined');
        }
        return $this -> userIdentity;
    } // getIdentityService()

} // UserIdenity
