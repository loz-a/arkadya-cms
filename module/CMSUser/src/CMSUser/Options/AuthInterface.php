<?php
namespace CMSUser\Options;

interface AuthInterface
{
    public function setIdentityColumn($identityColumn);

    public function getIdentityColumn();

    public function setPasswordCost($passwdCost);

    public function getPasswordCost();
}
