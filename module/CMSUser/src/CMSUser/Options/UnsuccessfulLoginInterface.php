<?php
namespace CMSUser\Options;

interface UnsuccessfulLoginInterface
{
    public function setUnsuccessfulLogTableName($tableName);

    public function getUnsuccessfulLogTableName();

    public function setUnsuccessfulLogCount($count);

    public function getUnsuccessfulLogCount();

    public function setUnsuccessfulLogPauseDuration($duration);

    public function getUnsuccessfulLogPauseDuration();
}
