<?php
namespace CMSUser\Options;

interface RolesInterface
{
    public function setRolesTableName($tableName);

    public function getRolesTableName();

    public function setRoleMaxLength($maxlen);

    public function getRoleMaxLength();
}
