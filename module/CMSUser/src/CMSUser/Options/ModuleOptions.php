<?php
namespace CMSUser\Options;

use Common\Mapper\Options\AbstractOptions;

class ModuleOptions extends AbstractOptions implements
    AuthInterface,
    UnsuccessfulLoginInterface
{
    protected $tableName = 'users';

    protected $passwdCost = 14;

    public function setPasswordCost($passwdCost)
    {
        $this -> passwdCost = (int) $passwdCost;
    } // setPasswordCost()

    public function getPasswordCost()
    {
        return $this -> passwdCost;
    } // getPasswordCost()


    protected $identityColumn = 'login';

    public function setIdentityColumn($identityColumn)
    {
        $this -> identityColumn = (string) $identityColumn;
    }

    public function getIdentityColumn()
    {
        return $this -> identityColumn;
    }


    protected $unsuccessfulLLogTableName = 'users_unsuccessful_login';

    public function setUnsuccessfulLogTableName($tableName)
    {
        $this -> unsuccessfulLLogTableName = (string) $tableName;
    }

    public function getUnsuccessfulLogTableName()
    {
        return $this -> unsuccessfulLLogTableName;
    }


    protected $unsuccessfulLogCount = 3;

    public function setUnsuccessfulLogCount($count)
    {
        $this -> unsuccessfulLogCount = (int) $count;
    }

    public function getUnsuccessfulLogCount()
    {
        return $this -> unsuccessfulLogCount;
    }


    protected $unsuccessfulLogPauseDuration = 30;

    public function setUnsuccessfulLogPauseDuration($duration)
    {
        $this -> unsuccessfulLogPauseDuration = (int) $duration;
    }

    public function getUnsuccessfulLogPauseDuration()
    {
        return $this -> unsuccessfulLogPauseDuration;
    }

} // ModuleOptions
