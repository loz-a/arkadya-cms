<?php
namespace CMSUser\Mapper;

use Common\Mapper\AbstractDbMapper;
use LogicException;

class Users extends AbstractDbMapper implements
    UsersInterface,
    UnsuccessfulLoginInterface
{
    protected $rolesTableName;

    public function setRolesTableName($tableName)
    {
        $this -> rolesTableName = (string) $tableName;
        return $this;
    } // setRolesTableName()


    public function getRolesTableName()
    {
        if (null === $this -> rolesTableName) {
            throw new LogicException('Roles table name is undefined');
        }
        return $this -> rolesTableName;
    } // getRolesTableName()


    public function fetchAll()
    {
        $select = $this
            -> select()
            -> columns(['id', 'login', 'last_login'])
            -> from(['u' => $this -> options -> getTableName()])
            -> join(
                ['r' => $this -> getRolesTableName()],
                'u.role_id = r.id',
                ['role']
            );

        return $this -> selectWith($select);
    } // fetchAll()


    public function findById($id)
    {
        $select = $this
            -> select()
            -> columns(['id', 'login', 'password', 'last_login', 'role_id'])
            -> from(['u' => $this -> options -> getTableName()])
            -> join(
                ['r' => $this -> getRolesTableName()],
                'u.role_id = r.id',
                ['role']
            )
            -> where(['u.id' => $id])
            -> limit(1);

        return $this -> selectExecute($select) -> current();
    } // findById()


    public function findBy($where)
    {
        $select = $this
            -> select()
            -> columns(['id', 'login', 'password', 'last_login'])
            -> from(['u' => $this -> options -> getTableName()])
            -> join(
                ['r' => $this -> getRolesTableName()],
                'u.role_id = r.id',
                ['role']
            )
            -> where($where);

        return $this -> selectWith($select);
    } // findBy()


    public function findUnsuccessfulLogDataById($id)
    {
        $select = $this
            -> select()
            -> columns(['id', 'attempt_time', 'count'])
            -> from($this -> options -> getUnsuccessfulLogTableName())
            -> where(['id' => $id]);

        return $this -> selectExecute($select) -> current();
    } // findUnsuccessfulLogDataById()


    public function updateUnsuccessfulDataById(array $data, $id)
    {
        $tableName = $this -> options -> getUnsuccessfulLogTableName();
        return $this -> update($data, ['id' => $id], $tableName);
    } // updateUnsuccessfulDataById()


    public function insertUnsuccessfulData(array $data)
    {
        $tableName = $this -> options -> getUnsuccessfulLogTableName();
        return $this -> insert($data, $tableName);
    } // insertUnsuccessfulData()

} // Users
