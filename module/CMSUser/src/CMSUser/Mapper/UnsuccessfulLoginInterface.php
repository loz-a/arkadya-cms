<?php
namespace CMSUser\Mapper;

interface UnsuccessfulLoginInterface
{
    public function findUnsuccessfulLogDataById($id);

    public function updateUnsuccessfulDataById(array $data, $id);

    public function insertUnsuccessfulData(array $data);
}
