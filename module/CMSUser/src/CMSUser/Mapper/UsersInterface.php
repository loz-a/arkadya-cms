<?php
namespace CMSUser\Mapper;

interface UsersInterface
{
    public function fetchAll();

    public function findById($id);

    public function findBy($where);
}
