<?php
namespace CMSUser\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory;

class Login extends Form
{
    public function getActionRouteName()
    {
        return 'login/submit';
    } // getActionRouteName()


    public function init()
    {
        $this
            -> setName('Sign in')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8')
            -> setAttribute('class', 'form-signin');

        $this
            -> add([
                'name' => 'login',
                'options' => [
                    'label' => 'Login'
                ],
                'attributes' => [
                    'required'    => true,
                    'placeholder' => 'Login',
                    'class'       => 'input-block-level'
                ]
            ])
            -> add([
                'name' => 'password',
                'type' => 'Zend\Form\Element\Password',
                'options' => [
                    'type' => 'password'
                ],
                'attributes' => [
                    'required'    => true,
                    'placeholder' => 'Password',
                    'class'       => 'input-block-level'
                ]
            ])
            -> add([
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf',
                'options' => [
                    'csrf_options' => [
                        'timeout' => 6000
                    ]
                ]
            ])
            -> add([
                'name' => 'submit',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => [
                    'type'  => 'submit',
                    'class' => 'btn btn-large btn-primary',
                    'value' => 'Sign in'
                ]
            ]);

        $this -> setInputFilter(
            (new Factory()) -> createInputFilter([
                [
                    'name'     => 'login',
                    'required' => true,
                    'filters'  => [
                        ['name' => 'StringTrim']
                    ]
                ],
                [
                    'name'     => 'password',
                    'required' => true
                ]
            ])
        );
    } // init()

} // Form
