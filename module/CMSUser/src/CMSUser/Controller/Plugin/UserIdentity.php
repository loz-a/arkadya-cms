<?php
namespace CMSUser\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use CMSUser\Service\UserIdentity as IdentityService;

class UserIdentity extends AbstractPlugin
{
    /**
     * @var \CMSUser\Service\UserIdentity
     */
    protected $identityService;

    /**
     * @param \CMSUser\Service\UserIdentity $identityService
     * @return UserIdentity
     */
    public function setIdentityService(IdentityService $identityService)
    {
        $this -> identityService = $identityService;
        return $this;
    } // setIdentityService()


    /**
     * @return \CMSUser\Service\UserIdentity
     * @throws \LogicException
     */
    public function getIdentityService()
    {
        if ( !$this -> identityService ) {
            throw new \LogicException('User identity service is undefined');
        }
        return $this -> identityService;
    } // getIdentityService()


    public function __invoke()
    {
        return $this -> getIdentityService() -> getIdentity();
    } // __invoke()

} // UserIdentity