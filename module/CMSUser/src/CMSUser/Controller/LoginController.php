<?php
namespace CMSUser\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Common\Service\ServiceAwareInterface;
use CMSUser\Authentication\Result as AuthResult;
use Common\Service\ServiceInterface;

class LoginController extends AbstractActionController
    implements ServiceAwareInterface
{
    /**
     * @var \CMSUser\Service\Users
     */
    protected $usersService;

    /**
     * @return \CMSUser\Service\Users|\Common\Service\ServiceInterface
     */
    public function getService()
    {
        if (null === $this -> usersService) {
            $service = $this -> serviceLocator -> get('CMSUser\Service\Users');
            $this -> setService($service);
        }
        return $this -> usersService;
    } // getService()


    /**
     * @param \Common\Service\ServiceInterface $service
     */
    public function setService(ServiceInterface $service)
    {
        $this -> usersService = $service;
    } // setService()


    public function indexAction()
    {
        $vm = new ViewModel();
        $vm -> setVariable('form', $this -> getService() -> getForm('CMSUser\Login'));
        $vm -> setTerminal(true);
        $vm -> setTemplate('cms-user/login/index');
        return $vm;
    } // indexAction()


    public function loginSubmitAction()
    {
        $form = $this -> getService() -> getForm('CMSUser\Login');
        $form -> setData($this -> request -> getPost());

        if ($form -> isValid()) {
            $login = $form -> get('login') -> getValue();
            $paswd = $form -> get('password') -> getValue();

            $result = $this -> getService() -> login($login, $paswd);

            if ($result -> isValid()) {
                $this -> flashMessenger() -> addMessage('Now you are logging');
                return $this -> redirect() -> toRoute('home');
            }
            else {
                if ( $result -> getCode() === AuthResult::FAILURE_TIMEOUT ) {
                    $this -> flashMessenger() -> addMessage(current($result -> getMessages()));
                }
                else {
                    $this -> flashMessenger() -> addMessage('Authorization error. Please check login or/and password');
                }
                sleep(1); // small bruteforce shield
            }
        }
        else {
            $this -> flashMessenger() -> addMessage('Authorization error. Please check login or/and password');
        }
        return $this -> redirect() -> toRoute('login/send');
    } // loginSubmitAction()


    public function logoutAction()
    {
        $this -> getService() -> logout();
        $this -> flashMessenger() -> addMessage('Logout successfull');
        return $this -> redirect() -> toRoute('home');
    } // logoutAction()

} // LoginController
