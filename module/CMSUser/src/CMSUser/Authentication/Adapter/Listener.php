<?php
namespace CMSUser\Authentication\Adapter;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Crypt\Password\Bcrypt;
use CMSUser\Authentication\Result as AuthResult;
use CMSUser\Authentication\Adapter\Event;

class Listener implements ListenerAggregateInterface
{
    protected $listeners = array();

    /**
     * @param \Zend\EventManager\EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events)
    {
        $this -> listeners[] = $events -> attach('authenticate', array($this, 'authenticate'));
        $this -> listeners[] = $events -> attach('authenticate.post', array($this, 'authenticateAfter'));
    } // attach()


    /**
     * @param \Zend\EventManager\EventManagerInterface $events
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ( $this -> listeners as $key => $listener ) {
            $events -> detach($listener);
            unset($this -> listeners[$key]);
            unset($listener);
        }
    } // detach()


    public function authenticate($e)
    {
        $service  = $e -> getTarget() -> getService();
        $options = $service -> getOptions();

        $identity    = $e -> getTarget() -> getIdentity();
        $identityCol = $options -> getIdentityColumn();

        $usersSet = $service -> getMapper() -> findBy([$identityCol => $identity]);

        if (!count($usersSet)) {
            $e -> setCode(AuthResult::FAILURE_IDENTITY_NOT_FOUND)
               -> setMessages(['A record with the supplied identity could not be found.']);
            return false;
        }
        else if (count($usersSet) > 1 ) {
            $e -> setCode(AuthResult::FAILURE_IDENTITY_AMBIGUOUS)
               -> setMessages(['More than one record matches the supplied identity.']);
            return false;
        }
        $user = $usersSet -> current();
        $e -> setIdentity($user -> getId());
        $credential = $e -> getTarget() -> getCredential();

        // Brute force protection
        $uld  = $service -> getMapper() -> findUnsuccessfulLogDataById($user -> getId());
        if ($uld) {
            $ulpd = $options -> getUnsuccessfulLogPauseDuration();
            $ulc  = $options -> getUnsuccessfulLogCount();

            if ((int) $uld['count'] > $ulc
                and time() < ($uld['attempt_time'] + $ulpd)
            ) {
                $e -> setCode(AuthResult::FAILURE_TIMEOUT);
                $e -> setMessages(["Authorization error. Please, expect the next $ulpd seconds to login."]);
                return false;
            }
        }

        $bcrypt = new Bcrypt();
        $bcrypt -> setCost($options -> getPasswordCost());
        $result = $bcrypt -> verify($credential, $user -> getPassword());

        if ( !$result ) {
            $e -> setCode(AuthResult::FAILURE_CREDENTIAL_INVALID)
               -> setMessages(['Supplied credential is invalid.']);
        }
        else {
            $e -> setCode(AuthResult::SUCCESS)
               -> setMessages(['Authentication successful.']);
        }

        return $result;
    } // authenticate()


    public function authenticateAfter($e)
    {
        switch($e -> getCode()) {
            case AuthResult::SUCCESS:
                $this -> resetUnsuccessfulLogData($e);
                break;
            case AuthResult::FAILURE_CREDENTIAL_INVALID:
                $this -> updateUnsuccessfulLogData($e);
                break;
            case AuthResult::FAILURE_TIMEOUT:
                $this -> updateUnsuccessfulLogData($e, true);
                break;
        }
    } // authenticateAfter()


    protected function updateUnsuccessfulLogData(Event $e, $onlyAttemptTime = false)
    {
        $service = $e -> getTarget() -> getService();
        $options = $service -> getOptions();
        $userId  = $e -> getIdentity();
        $uld     = $service -> getMapper() -> findUnsuccessfulLogDataById($userId);

        if (!$uld) {
            $data = [
                'id'    => $userId,
                'count' => 1,
                'attempt_time' => time()
            ];
            $service -> getMapper() -> insertUnsuccessfulData($data);
        }
        else {
            $data = [];
            if (!$onlyAttemptTime) {
                $data['count'] = ++$uld['count'];
            }
            $data['attempt_time'] = time();
            $service -> getMapper() -> updateUnsuccessfulDataById($data, $userId);
        }
    } // updateUnsuccessfulLogData()


    public function resetUnsuccessfulLogData($e)
    {
        $userId = $e -> getIdentity();
        $e  -> getTarget()
            -> getService()
            -> getMapper()
            -> updateUnsuccessfulDataById(['count' => 0], $userId);
    } // resetUnsuccessfulLogData()

} // Listener
