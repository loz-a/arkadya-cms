<?php
namespace CMSUser\Authentication\Adapter;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\EventManager\Event as BaseEvent;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use CMSUser\Authentication\Result as AuthResult;
use Common\Service\ServiceAwareInterface;
use Common\Service\ServiceInterface;

class Adapter implements
    AdapterInterface,
    EventManagerAwareInterface,
    ServiceAwareInterface
{
    use EventManagerAwareTrait;

    /**
     * @var Event
     */
    protected $event;

    /**
     * @var \CMSUser\Service\Users
     */
    protected $usersService;

    /**
     * @var string
     */
    protected $identity;

    /**
     * @var string
     */
    protected $credential;


    public function authenticate()
    {
        $e = $this -> getEvent();
        $this -> getEventManager() -> trigger(__FUNCTION__, $e);
        $this -> getEventManager() -> trigger(__FUNCTION__ . '.post', $e);

        return new AuthResult(
            $e -> getCode(),
            $e -> getIdentity(),
            $e -> getMessages()
        );
    } // authenticate()


    public function getEvent()
    {
        if (null === $this -> event) {
            $this -> setEvent(new Event());
            $this -> event -> setTarget($this);
        }
        return $this -> event;
    } // getEvent()


    public function setEvent(BaseEvent $e)
    {
        if (!$e instanceof Event) {
            $params = $e -> getParams();
            $e = new Event();
            $e -> setParams($params);
            unset($params);
        }
        $this -> event = $e;
        return $this;
    } // setEvent()


    /**
     * setIdentity() - set the value to be used as the identity
     *
     * @param  string $value
     * @return Adapter
     */
    public function setIdentity($value)
    {
        $this -> identity = $value;
        return $this;
    } // setIdentity()


    public function getIdentity()
    {
        if (null === $this -> identity) {
            throw new Exception\LogicException(
                'A value for the identity was not provided prior to authentication with DbTable');
        }
        return $this -> identity;
    } // getIdentity()


    /**
     * setCredential() - set the credential value to be used, optionally can specify a treatment
     * to be used, should be supplied in parametrized form, such as 'MD5(?)' or 'PASSWORD(?)'
     *
     * @param  string $credential
     * @return Adapter
     */
    public function setCredential($credential)
    {
        $this -> credential = $credential;
        return $this;
    } // setCredential()


    public function getCredential()
    {
        if (null === $this -> credential) {
            throw new Exception\LogicException('A credential value was not provided prior to authentication');
        }
        return $this -> credential;
    } // getCredential()


    /**
     * @return \CMSUser\Service\Users|\Common\Service\ServiceInterface
     * @throws Exception\LogicException
     */
    public function getService()
    {
        if (null === $this -> usersService) {
            throw new Exception\LogicException('UsersService in undefiend');
        }
        return $this -> usersService;
    } // getService()


    /**
     * @param \Common\Service\ServiceInterface $service
     * @return Adapter
     */
    public function setService(ServiceInterface $service)
    {
        $this -> usersService = $service;
        return $this;
    } // setService()

} // Adapter
