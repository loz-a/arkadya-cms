<?php
namespace CMSUser\Authentication\Adapter;

use Zend\EventManager\Event as BaseEvent;

class Event extends BaseEvent
{
    const IDENTITY_KEY = 'identity';
    const MESSAGES_KEY = 'messages';
    const CODE_KEY     = 'code';

    protected $identity;
    protected $messages;
    protected $code;

    public function getIdentity()
    {
        return $this -> identity;
    } // getIdentity()


    public function setIdentity($identity = null)
    {
        if ( null === $identity ) {
            $this
                -> setCode()
                -> setMessages();
        }
        $this -> setParam(self::IDENTITY_KEY, $identity);
        $this -> identity = $identity;
        return $this;
    } // setIdentity()


    public function getCode()
    {
        return $this -> code;
    } // getCode()


    public function setCode($code = null)
    {
        $this -> setParam(self::CODE_KEY, $code);
        $this -> code = $code;
        return $this;
    } // setCode()


    public function getMessages()
    {
        return $this -> messages;
    } // getMessage()


    public function setMessages(array $messages = array())
    {
        $this -> setParam(self::MESSAGES_KEY, $messages);
        $this -> messages = $messages;
        return $this;
    } // setMessages()

} // Event
