<?php
namespace CMSUser\Authentication;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\AuthenticationService;
use CMSUser\Authentication\Adapter\Adapter as AuthAdapter;
use CMSUser\Authentication\Adapter\Listener as AuthListener;

class AuthServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $adapter = new AuthAdapter();
        $adapter -> setService($serviceLocator -> get('CMSUser\Service\Users'));
        $adapter -> getEventManager() -> attachAggregate(new AuthListener());

        $authService = new AuthenticationService();
        $authService -> setAdapter($adapter);
        return $authService;
    } // createService()

} // AuthServiceFactory
