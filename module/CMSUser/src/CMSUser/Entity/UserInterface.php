<?php
namespace CMSUser\Entity;

interface UserInterface
{
    public function setId($id);

    public function getId();

    public function setLogin($login);

    public function getLogin();

    public function setPassword($password);

    public function getPassword();

    public function setRole($role);

    public function getRole();

    public function setLastLogin($timestamp);

    public function getLastLogin();
}
