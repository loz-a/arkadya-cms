<?php
namespace CMSUser\Entity;

class User implements UserInterface
{
    protected $id;
    protected $login;
    protected $password;
    protected $role;
    protected $lastLogin;


    public function setId($id)
    {
        $this -> id = (int) $id;
        return $this;
    }

    public function getId()
    {
        return $this -> id;
    }


    public function setLogin($login)
    {
        $this -> login = (string) $login;
        return $this;
    }

    public function getLogin()
    {
        return $this -> login;
    }


    public function setPassword($password)
    {
        $this -> password = (string) $password;
        return $this;
    }

    public function getPassword()
    {
        return $this -> password;
    }


    public function setRole($role)
    {
        $this -> role = (int) $role;
        return $this;
    }

    public function getRole()
    {
        return $this -> role;
    }


    public function setLastLogin($timestamp)
    {
        $this -> lastLogin = (int) $timestamp;
        return $this;
    }

    public function getLastLogin()
    {
        return $this -> lastLogin;
    }

} // User
