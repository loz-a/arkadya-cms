<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'CMSUser\Controller\UsersManager' => 'CMSUser\Controller\UsersManagerController',
            'CMSUser\Controller\Login'        => 'CMSUser\Controller\LoginController',
        )
    ),

    'view_manager' => array(
        'template_map' => array(
            'cms-user/login/index'        => __DIR__ . '/../view/cms-user/login/index.phtml',
            'cms-user/login/login-submit' => __DIR__ . '/../view/cms-user/login/index.phtml',
        ),
        'template_path_stack' => array(
            'cms-user' => __DIR__ . '/../view'
        )
    ),

    'router' => array(
        'routes' => array(

            'login' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/login',
                    'defaults' => array('controller' => 'CMSUser\Controller\Login')
                ),
                'may_terminate' => false,
                'child_routes' => array(
                    'send' => array(
                        'type'    => 'Method',
                        'options' => array(
                            'verb'     => 'get',
                            'defaults' => array('action' => 'index')
                        ),
                        'may_terminate' => true
                    ),
                    'submit' => array(
                        'type' => 'Method',
                        'options' => array(
                            'verb'     => 'post',
                            'defaults' => array('action' => 'login-submit')
                        )
                    ),
                )
            ), // login

            'logout' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/logout',
                    'defaults' => array(
                        'controller' => 'CMSUser\Controller\Login',
                        'action' => 'logout'
                    )
                )
            ), // logout

        )
    ), // router


    'route_layouts' => array(
        'login/send'   => 'layout/admin',
        'login/submit' => 'layout/admin',
    ),


    'admin_navigation' => array(
        'users' => array(
            'index' => 6,
            'header' => array(
                'label' => 'Users',
                'icon'  => 'icon-user',
                'class_name' => 'user'
            ),
            'items' => array(

            )
        )
    ),

);