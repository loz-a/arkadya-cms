<?php
/**
 * User: Андрій
 * Date: 05.10.14
 * Time: 18:09 
 */

namespace Application\Service;


use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;

class StartupBanner implements ServiceManagerAwareInterface
{
    /**
     * @var ServiceManager
     */
    protected $sm;

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     * @return $this
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> sm = $serviceManager;
        return $this;
    } // setServiceManager()


    public function getForm()
    {
        return $this -> sm -> get('FormElementManager') -> get('Application\UploadStartupBanner');
    } // getForm()
} 