<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use CMSOptions\Service\Options;

class StartupBannerScript extends AbstractHelper
{
    /**
     * @var Options
     */
    protected $options;

    /**
     * @param Options $options
     * @return $this
     */
    public function setOptionsService(Options $options)
    {
        $this -> options = $options;
        return $this;
    } // setOptionsService()


    /**
     * @return Options
     * @throws Exception\UndefinedOptionsServiceException
     */
    public function getOptionsService()
    {
        if (null === $this -> options) {
            throw new Exception\UndefinedOptionsServiceException('Options service is undefined');
        }
        return $this -> options;
    } // getOptionsService()


    /**
     * @return string
     */
    public function __invoke()
    {
        $isDisplayBanner = (int) $this -> getOptionsService() -> getOption('startupbanner_display_banner') -> getValue();

        if ($isDisplayBanner) {
            return $this -> getView() -> inlineScript() -> offsetSetFile(0, $this -> getView() -> basePath() . '/scripts/app/startup-banner.js');
        }
        return '';
    } // __invoke()

}