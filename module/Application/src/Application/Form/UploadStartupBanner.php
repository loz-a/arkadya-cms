<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UploadStartupBanner extends Form
    implements ServiceLocatorAwareInterface
{
    /*
     * @var \Zend\Form\FormElementManager
     */
    protected $fem;


    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return $this
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this -> fem = $serviceLocator;
        return $this;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this -> fem;
    }


    public function init()
    {
        $this
            -> setName('Startup Banner')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8')
            -> setAttribute('id', 'startup-banner');

        $this
            -> add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'is_visible',
                'options' => array(
                    'label' => 'Display banner',
                    'use_hidden_element' => false
                ),
                'attributes' => array(
                    'id' => 'visibility-switch'
                )
            ))

            -> add(array(
                'name' => 'upload_banner',
                'type' => 'Zend\Form\Element\File',
                'attributes' => array(
                    'required' => false,
                    'class'    => 'span4'
                )
            ))

            -> add(array(
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf'
            ))

            -> add(array(
                'name' => 'submit',
                'options' => array(
                    'label' => 'Ok',
                ),
                'attributes' => array(
                    'class' => 'btn btn-primary',
                    'type'  => 'submit',
                )
            ))

            -> add(array(
                'name' => 'cancel',
                'options' => array(
                    'label' => 'Cancel',
                ),
                'attributes' => array(
                    'class' => 'btn',
                    'type'  => 'button',
                )
            ));

    } // init()


    public function getActionRouteName()
    {
        return 'startup_banner';
    } // getActionRouteName()

} 