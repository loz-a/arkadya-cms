<?php
namespace Application\Form\Validator;

use Application\Options\OptionsInterface;
use Common\Form\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Application\Options\Exception\InvalidOptionsException;
use Common\Filter\Imagick\Strategy\CropThumbnail;

class UploadStartupBanner implements InputFilterAwareInterface
{
    /**
     * @var InputFilterInterface
     */
    protected $inputFilter;

    /**
     * @var OptionsInterface
     */
    protected $options;

    /**
     * @param InputFilterInterface $inputFilter
     * @return void|InputFilterAwareInterface
     * @throws \Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception('Not used');
    } // setInputFilter()

    /**
     * Retrieve input filter
     *
     * @return InputFilterInterface
     */
    public function getInputFilter()
    {
        if (!$this -> inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $options     = $this -> getOptions();

            $inputFilter -> add($factory -> createInput(
                array(
                    'name'     => 'upload_banner',
                    'required' => false,
                    'filters'  => array(
                        array(
                            'name'    => 'File\RenameUpload',
                            'options' => array(
                                'target'    => sprintf('%s/public%s', getcwd(), $options -> getStartupBannerFilename()),
                                'overwrite' => true
                            )
                        ),
                        array(
                            'name'    => 'Common\Filter\Imagick\Resizer',
                            'options' => array(
                                'strategy'  => new CropThumbnail(),
                                'width'   => $options -> getStartupBannerWidth(),
                                'height'  => $options -> getStartupBannerHeight(),
                                'quality' => $options -> getStartupBannerQuality()
                            )
                        )
                    ),
                    'validators' => array(
//                        array(
//                            'name' => 'File\IsImage'
//                        ),
                        array (
                            'name' => 'File\Extension',
                            'options' => array(
                                'jpeg', 'jpg'
                            ),
                        ),
                        array(
                            'name' => 'File\Size',
                            'options' => array(
                                'max' => '2MB'
                            )
                        )
                    ),
                )
            ));

            $inputFilter->add($factory->createInput([
                'name' => 'is_visible',
                'filters' => array(
                    array(
                        'name' => 'Int'
                    )
                )
            ]));

            $this -> inputFilter = $inputFilter;
        }

        return $this -> inputFilter;
    } // getInputFilter()


    public function setOptions(OptionsInterface $options)
    {
        $this -> options = $options;
        return $this;
    } // setOptions()


    public function getOptions()
    {
        if (null === $this -> options) {
            throw new InvalidOptionsException('Options is undefined');
        }
        return $this -> options;
    } // getOptions()

} 