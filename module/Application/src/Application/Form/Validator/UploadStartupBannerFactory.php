<?php
namespace Application\Form\Validator;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UploadStartupBannerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options   = $serviceLocator -> get('application_options');

        $validator = new UploadStartupBanner();
        $validator -> setOptions($options);
        return $validator;
    } // createService()

} 