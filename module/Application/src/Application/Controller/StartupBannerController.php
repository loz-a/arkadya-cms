<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\PhpEnvironment\Response;
use Zend\View\Model\ViewModel;
use Application\Form\Validator;

class StartupBannerController extends AbstractActionController
{
    /**
     * @var \Application\Service\StartupBanner
     */
    protected $service;

    public function indexAction()
    {
        $request        = $this -> getRequest();
        $form           = $this -> getService() -> getForm();
        $optionsService = $this -> getServiceLocator() -> get('CMSOptions\Service');

        $validator = $this -> serviceLocator -> get('Application\Form\Validator\UploadStartupBanner');
        $form -> setInputFilter($validator -> getInputFilter());

        $prg = $this -> fileprg($form);

        if ($prg instanceof Response) {
            return $prg;
        }
        else if (is_array($prg)) {

            if ($form -> isValid()) {
                $isVisible = (int) $form -> get('is_visible') -> isChecked();
                $optionsService -> setOption('startupbanner_display_banner', $isVisible);

                $this -> flashMessenger() -> addMessage('Banner settings have been changed');
                return $this -> redirect() -> toRoute('startup_banner');
            }
            else {
                $this -> flashMessenger() -> addMessage('Something was wrong');
            }
        }

        $option    = $optionsService -> getOption('startupbanner_display_banner');
        $isVisible = (null === $option) ? 0 : (int) $option -> getValue();
        $form -> get('is_visible') -> setChecked($isVisible);

        $this
            -> getResponse()
            -> getHeaders()
            -> addHeaderLine('Cache-Control', 'no-cache, must-revalidate')
            -> addHeaderLine('Pragma', 'no-cache');

        return array(
            'form' => $form,
            'startupBannerFilename' => $this -> getServiceLocator() -> get('application_options') -> getStartupBannerFilename()
        );
    } // indexAction()


    public function getService()
    {
        if (null === $this -> service) {
            $this -> service = $this -> getServiceLocator() -> get('Application\Service\StartupBanner');
        }
        return $this -> service;
    } // getService()

} 