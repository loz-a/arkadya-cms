<?php
namespace Application\Options;


interface OptionsInterface
{
    public function setStartupBannerWidth($width);

    public function getStartupBannerWidth();

    public function setStartupBannerHeight($height);

    public function getStartupBannerHeight();

    public function setStartupBannerQuality($quality);

    public function getStartupBannerQuality();

    public function setStartupBannerFilename($filename);

    public function getStartupBannerFilename();
} 