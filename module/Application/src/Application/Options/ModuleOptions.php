<?php
namespace Application\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements OptionsInterface
{
    protected $startupBannerWidth = 800;

    public function setStartupBannerWidth($width)
    {
        $this -> startupBannerWidth = (int) $width;
        return $this;
    }

    public function getStartupBannerWidth()
    {
        return $this -> startupBannerWidth;
    }


    protected $startupBannerHeight = 497;

    public function setStartupBannerHeight($height)
    {
        $this -> startupBannerHeight = (int) $height;
        return $this;
    }

    public function getStartupBannerHeight()
    {
        return $this -> startupBannerHeight;
    }


    protected $startupBannerQuality = 60;

    public function setStartupBannerQuality($quality)
    {
        $this -> startupBannerQuality = (int) $quality;
        return $this;
    }

    public function getStartupBannerQuality()
    {
        return $this -> startupBannerQuality;
    }


    protected $startupBannerFilename = '/img/startup-banner.jpg';

    public function setStartupBannerFilename($filename)
    {
        $this -> startupBannerFilename = (string) $filename;
        return $this;
    }

    public function getStartupBannerFilename()
    {
        return $this -> startupBannerFilename;
    }

}