��    G      T  a   �                !     6  
   B     M  4   d     �     �  �   �     q  	   y     �     �     �  	   �  	   �  
   �     �  
   �     �     �     �  >     N   C     �     �  I   �  �   �     �	     �	  S   �	     &
     .
     3
     9
     P
     `
     v
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
  	             '     ,     ?     G  4   \     �     �     �  2   �  M   �  .   5  <   d     �  	   �  9   �  "     e   '     �     �     �     �  �  �     J     g     x     �  1   �  z   �  #   W     {  .  �     �     �  %   �          #     ?     \     x     �     �  !   �     �     �  ~     w   �     �  6     �   B  {  �  "   V     y  �   �                '  .   /  7   ^  *   �     �     �     �            )   '  *   Q     |  (   �     �     �     �  
   �  !        )     ?  h   T  
   �  %   �     �  m     �   z  F     ^   f  >   �       _     K   s  �   �     u     �     �     �         >   *      D      1   ,   +   	   C      (          &                        $   2            =          3         ;          :       7   !      A   E   B   
   ?   @   '   )              8      /       0          %           -   6      F           "                        4                        G   <                     .   9          5   #        3 Dzerelna Str. A 404 error occurred Access deny Add. place Additional information Affordable and the best in quality and comfort rooms All rights reserved. An error occurred Arcadia sanatorium - health complex in Morshin. Vacation at a resort in the Morshina sanatoriums. Rooms photos. Affordable prices. Treatment. Nutrition. Tel. +38(03260)04-06-17 Arkadia Attention Book a room Category Check in Check in: Check out Check out: Complete booking Controller Customer's properties File For children For lovers of sport and physical education we offer to our gym For the little ones we have are equipped with a children's room and playground Homepage If outside, your e-mail If you reside in the territory of Ukraine, enter your mobile phone number If, within two days, you do not call the staff of our receptions, please contact us by phone. Perhaps for some reason the order was not sent to us, or the details of the customer were not true Lviv region Message Morshin resort, rest in a Morshin sanatoriums, photos, reviews - sanatorium Arcadia Morshyn Name Name: No Exception available Online ordering Oops! page not found  Our contacts Per 2 persons Per person Phone Phone: Previous exceptions Request for reservation Reservation Reservation room from Reservation room type Residence Residence type: Rest Sanatorium Arkadia Sitemap Skeleton Application Spa treatments, which have no analogues in the world Sport Stack trace Submit a request The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. Therapeutic recreation complex Treatment We cannot determine at this time why a 404 was generated. You have applied for a reservation You will have the opportunity to enjoy the strong taste of fragrant Italian coffee in our cafe Dejavu Your personal data resolves to %s to uah Project-Id-Version: arkadya.com.ua
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-05-28 12:09+0200
PO-Revision-Date: 2013-05-28 12:09+0200
Last-Translator: Andriy Lozinsky <loz.andriy@gmail.com>
Language-Team: loz.andriy <loz.andriy@gmail.com>
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: D:\Sitecraft\www\arkadya-dev\ZendSkeletonApplication\
X-Generator: Poedit 1.5.5
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: module
 ул.Джерельная, 3 Ошибка 404 Доступ запрещен Доп. место: Дополнительная информация Доступные по цене и лучшие по качеству и комфортабельности номера  Все права защищены. Произошла ошибка Санаторий Аркадия - лечебно-оздоровительный комплекс в Моршине. Отдых на курорте в санатории Моршина. Фото номеров. Доступные цены . Лечение. Питание. Тел. +38(03260) 6-04-17.  Аркадия Внимание Забронировать номер Категория Дата заселения Дата заселения: Дата выселения Дата выселения: Завершить бронь Контроллер Реквизиты клиента Файл Для детей Для любителей спорта и физкультуры предлагаем посетить наш спортзал Для самых маленьких у нас оборудованы детская комната и площадка Главная Если за ее пределами, свой e-mail Если вы проживаете на територии Украины, укажите номер своего мобильного телефона Если в течение двух дней Вам не позвонят сотрудники нашего ресепшина, пожалуйста, свяжитесь с нами по телефону. Возможно, по каким-то причинам заказ не был нам отправлен, или реквизиты заказчика были не верны Львовская область, Сообщение Моршин курорт, отдых в санатории Моршина, фото, отзывы - санаторий Аркадия Моршин Имя Имя: Нет имеющихся исключений Запрос на онлайн-бронирование Ой! Страница не найдена Наши контакты За 2 человек: За человека Телефон Телефон: Предыдущие исключения Запрос на бронирование Бронирование Бронирование номера с Тип номера Проживание Тип номера: Отдых Санаторий Аркадия Карта сайта Skeleton Application Лечение минеральными водами, не имеющими аналогов в мире Спорт Развертывание стека Отослать заявку Для запрашиваемого URL не может быть достигнуто направление. Запрашиваемый контроллер не может быть сопоставлен с существующими классом контроллера. Запрашиваемый контроллер не доступен. Запрашиваемый контроллер не смог отправить запрос. Лечебно-оздоровительный комплекс Лечение Мы не можем определить причину создания страницы 404. Вы создали запрос на онлайн-бронирование У вас будет возможность насладиться крепким вкусом ароматного итальянского кофе в нашем кафе "Dejavu" Вашы данные разрешает для %s до грн. 