<?php
namespace Application;

use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\EventManager\EventInterface;

class Module implements
    BootstrapListenerInterface,
    ConfigProviderInterface,
    AutoloaderProviderInterface,
    FormElementProviderInterface,
    ServiceProviderInterface,
    ViewHelperProviderInterface
{
    public function onBootstrap(EventInterface $e)
    {
        $e->getApplication()->getServiceManager()->get('translator');
    } // onBootstrap()


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    } // getAutoloaderConfig()


    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'startupBannerScript' => function($sm) {
                    return (new View\Helper\StartupBannerScript())
                        -> setOptionsService($sm -> getServiceLocator() -> get('CMSOptions\Service'));
                }
            )
        );
    } // getViewHelperConfig()


    public function getFormElementConfig()
    {
        return array(
            'invokables' => array(
                'Application\UploadStartupBanner' => 'Application\Form\UploadStartupBanner'
            )
        );
    } // getFormElementConfig()


    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'application_options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['application_options']) ? $config['application_options'] : array();
                    return new Options\ModuleOptions($options);
                },
                'Application\Form\Validator\UploadStartupBanner' => 'Application\Form\Validator\UploadStartupBannerFactory'
            ),
            'invokables' => array(
                'Application\Service\StartupBanner' => 'Application\Service\StartupBanner'
            )
        );
    } // getServiceConfig()
}
