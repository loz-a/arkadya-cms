<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Residence\Controller\Index' => 'Residence\Controller\IndexController',
            'Residence\Controller\Reservation' => 'Residence\Controller\ReservationController',
            'Residence\Controller\UploadImages' => 'Residence\Controller\UploadImagesController',
        )
    ),
    'view_manager' => array(
        'template_map' => array(
            'residence/index/index'          => __DIR__ . '/../view/residence/index/index.phtml',
            'residence/index/view'           => __DIR__ . '/../view/residence/index/view.phtml',
            'residence/index/manage'         => __DIR__ . '/../view/residence/index/manage.phtml',
            'residence/index/add'            => __DIR__ . '/../view/residence/index/add.phtml',
            'residence/index/add-submit'     => __DIR__ . '/../view/residence/index/add.phtml',
            'residence/index/edit'           => __DIR__ . '/../view/residence/index/add.phtml',
            'residence/index/edit-submit'    => __DIR__ . '/../view/residence/index/add.phtml',
            'residence/index/delete'         => __DIR__ . '/../view/residence/index/delete.phtml',
            'residence/upload-images/index'  => __DIR__ . '/../view/residence/upload-images/index.phtml',
            'residence/upload-images/upload' => __DIR__ . '/../view/residence/upload-images/upload.phtml',
            'residence/reservation/index'    => __DIR__ . '/../view/residence/reservation/index.phtml',
            'residence/reservation/submit'   => __DIR__ . '/../view/residence/reservation/submit.phtml',
            'residence/widget/quick-reservation'   => __DIR__ . '/../view/residence/reservation/widget/quick-reservation.phtml',
            'residence/widget/confirm-reservation' => __DIR__ . '/../view/residence/reservation/widget/confirm-reservation.phtml',
            'residence/widget/prices'              => __DIR__ . '/../view/residence/reservation/widget/prices.phtml',
            'residence/email/reservation-notify'   => __DIR__ . '/../view/residence/reservation/email/notify.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        )
    ),

    'router' => array(
        'routes' => array(

            'manage_residence' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/manage-residence',
                    'defaults' => array(
                        'controller' => 'Residence\Controller\Index',
                        'action'     => 'manage'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'add' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/add'
                        ),
                        'may_terminate' => false,
                        'child_routes' => array(
                            'send' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb' => 'get',
                                    'defaults' => array(
                                        'action' => 'add'
                                    )
                                ),
                                'may_terminate' => true
                            ),
                            'submit' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb' => 'post',
                                    'defaults' => array(
                                        'action' => 'add-submit'
                                    )
                                ),
                                'may_terminate' => true
                            )
                        )
                    ),
                    'edit' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/edit/:id',
                            'constraints' => array(
                                'id' => '\d+'
                            )
                        ),
                        'may_terminate' => false,
                        'child_routes' => array(
                            'send' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb' => 'get',
                                    'defaults' => array(
                                        'action' => 'edit'
                                    )
                                ),
                                'may_terminate' => true
                            ),
                            'submit' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb' => 'post',
                                    'defaults' => array(
                                        'action' => 'edit-submit'
                                    )
                                ),
                                'may_terminate' => true
                            )
                        )
                    ),
                    'delete' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/delete/:id',
                            'constraints' => array(
                                'id' => '\d+'
                            ),
                            'defaults' => array(
                                'action' => 'delete'
                            )
                        ),
                    ),
                    'images_list' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/images-list/:type_id',
                            'constraints' => array(
                                'type_id' => '\d+'
                            ),
                            'defaults' => array(
                                'controller' => 'Residence\Controller\UploadImages',
                                'action'     => 'index'
                            )
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'upload' => array(
                                'type'    => 'Literal',
                                'options' => array(
                                    'route'    => '/upload',
                                    'defaults' => array(
                                        'action' => 'upload'
                                    )
                                ),
                                'may_terminate' => true
                            ),
                            'remove' => array(
                                'type' => 'Segment',
                                'options' => array(
                                    'route' => '/remove/:filename',
                                    'defaults' => array(
                                        'action' => 'remove'
                                    )
                                ),
                                'may_terminate' => true
                            )
                        )
                    )
                )
            ),

            'residence' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/residence.html',
                    'defaults' => array(
                        'controller' => 'Residence\Controller\Index',
                        'action'     => 'index'
                    )
                )
            ),

            'view_residence' => array(
                'type' => 'Regex',
                'options' => array(
                    'regex' => '/residence/(?<type>[a-z0-9_\-]+)\.html',
                    'defaults' => array(
                        'controller' => 'Residence\Controller\Index',
                        'action'     => 'view'
                    ),
                    'spec' => '/residence/%type%.html'
                )
            ),


            'prices' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/prices.html',
                    'defaults' => array(
                        'controller' => 'Residence\Controller\Index',
                        'action'     => 'prices'
                    )
                )
            ),

            'reservation' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/reservation',
                    'defaults' => array(
                        'controller' => 'Residence\Controller\Reservation',
                        'action'     => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(

                    'submit' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route'    => '/submit',
                            'defaults' => array(
                                'action' => 'submit'
                            )
                        ),
                    )
                )
            ),

        )
    ),

    'route_layouts' => array(
        'manage_residence'             => 'layout/admin',
        'manage_residence/add/send'    => 'layout/admin',
        'manage_residence/add/submit'  => 'layout/admin',
        'manage_residence/edit/send'   => 'layout/admin',
        'manage_residence/edit/submit' => 'layout/admin',
        'manage_residence/delete'      => 'layout/admin',
        'manage_residence/images_list' => 'layout/admin',
        'manage_residence/images_list/upload' => 'layout/admin',
        'residence'                    => 'layout/layout',
        'view_residence'               => 'layout/layout',
        'reservation'                  => 'layout/layout',
        'reservation/submit'           => 'layout/layout',
    ),

    'admin_navigation' => array(
        'residence' => array(
            'index' => 4,
            'header' => array(
                'label' => 'Residence',
                'icon' => 'icon-bookmark',
                'class_name' => 'bookmark',
            ),
            'items' => array(
                array(
                    'label' => 'All Room Types',
                    'route_name' => 'manage_residence'
                ),
                array(
                    'label' => 'Add Type',
                    'route_name' => 'manage_residence/add/send'
                )
            )
        )
    ),

);