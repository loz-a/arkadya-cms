<?php
namespace Residence;

use Residence\View\Helper\ResidencePricesWidget;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class Module implements
    ConfigProviderInterface,
    AutoloaderProviderInterface,
    DependencyIndicatorInterface,
    FormElementProviderInterface,
    ViewHelperProviderInterface,
    ServiceProviderInterface
{
    public function getModuleDependencies()
    {
        return array('Common', 'CMSPages');
    } // getModuleDependencies()


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    } // getAutoloaderConfig()


    public function getFormElementConfig()
    {
        return array(
            'invokables' => array(
                'Residence\UploadImage' => 'Residence\Form\UploadImage',
            ),
            'factories' => array(
                'Residence\Type\Add' => function($sm) {
                    $inputFilter = $sm -> getServiceLocator() -> get('Residence\Type\InputFilter');

                    $form = new Form\Type\Add();
                    $form -> setInputFilter($inputFilter);
                    return $form;
                },
                'Residence\Type\Edit' => function($sm) {
                    $inputFilter = $sm -> getServiceLocator() -> get('Residence\Type\InputFilter');

                    $form = new Form\Type\Edit();
                    $form -> setInputFilter($inputFilter);
                    return $form;
                },
                'Residence\Reservation' => function($sm) {
                    $options = $sm -> getServiceLocator() -> get('residence_options');
                    $form = new Form\Reservation();
                    $form -> setInputFilter(new Form\ReservationFilter($options));
                    return $form;
                }
            )
        );
    } // getFormElementConfig()


    public function getViewHelperConfig()
    {
        return array(
            'invokables' => array(
                'quickReservationWidget' => 'Residence\View\Helper\QuickReservationWidget',
            ),
            'factories' => array(
                'residencePricesWidget' => function($sm) {
                    $helper = new ResidencePricesWidget();
                    $helper -> setMapper($sm -> getServiceLocator() -> get('Residence\Mapper\Type'));
                    return $helper;
                }
            )
        );
    } // getViewHelperConfig()

    
    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'Residence\Type\InputFilter' => 'Residence\Form\Type\TypeFilter',
                'Residence\Service' => 'Residence\Service\Residence',
            ),
            'factories' => array(
                'residence_options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['residence_options']) ? $config['residence_options'] : array();
                    return new Options\ModuleOptions($options);
                },
                'Residence\Mapper\Type' => function($sm) {
                    $mapper = new Mapper\Type();
                    $mapper
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setEntityPrototype(new Entity\Type())
                        -> setHydrator(new ClassMethodsHydrator())
                        -> setOptions($sm -> get('residence_options'))
                        -> setCurrentLocaleService($sm -> get('CMSLocale\CurrentLocale'));
                    return $mapper;
                },
            )
        );
    } // getServiceConfig()

} // Module
