<?php
namespace Residence\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\PhpEnvironment\Response;
use \GlobIterator;

class UploadImagesController extends AbstractActionController
{
    /**
     * @var \Residence\Service\Residence
     */
    protected $residenceService;


    public function indexAction()
    {
        $typeId     = (int) $this -> params('type_id');
        $modOptions = $this -> getResidenceService() -> getOptions();
        $docRoot    = $this -> getRequest() -> getServer('DOCUMENT_ROOT');
        $uploadPath = $modOptions -> getUploadPath();
        $path       = str_replace(array('/', '\\'), DS, $docRoot . $uploadPath);
        $imagesMask = sprintf('%s%s%s%s*.{jpeg,JPEG,jpg,JPG}', $path, DS,$typeId, DS);

        return array(
            'uploadPath' => sprintf('%s/%s', $uploadPath, $typeId),
            'images'     => glob($imagesMask, GLOB_BRACE),
            'thumbDir'   => $modOptions -> getImageThumbDir(),
            'typeId'    => $typeId
        );
    } // index


    public function uploadAction()
    {
        $typeId = (int) $this -> params('type_id');
        if ( !$typeId ) {
            return $this -> notFoundAction();
        }

        $form = $this -> getResidenceService() -> getForm('Residence\UploadImage');
        $form -> get('type_id') -> setValue($typeId);

        $prg = $this -> fileprg($form);

        if ( $prg instanceof Response ) {
            return $prg;
        }
        else if ( is_array($prg) ) {

            if ( $form -> isValid() ) {
                $this -> flashMessenger() -> addMessage('Image was uploaded');
                return $this -> redirect() -> toRoute('manage_residence/images_list', array('type_id' => $typeId));
            }
            else {
                $this -> flashMessenger() -> addMessage('Something was wrong');
            }
        }
        return array(
            'typeId' => $typeId,
            'form'   => $form
        );
    } // upload


    public function removeAction()
    {
        $modOptions = $this -> getResidenceService() -> getOptions();
        $typeId     = (int) $this -> params('type_id');
        $filename   = $this -> params('filename');
        $docRoot    = $this -> getRequest() -> getServer('DOCUMENT_ROOT');
        $uploadPath = $modOptions -> getUploadPath();
        $path       = str_replace(array('/', '\\'), DS, $docRoot . $uploadPath);

        $f = glob(sprintf('%s%s%s%s%s*', $path, DS, $typeId, DS, $filename));
        if ( $f ) {
            unlink(current($f));
        }

        $thumbDir = $modOptions -> getImageThumbDir();
        $f = glob(sprintf('%s%s%s%s%s%s%s*', $path, DS, $typeId, DS, $thumbDir, DS, $filename));
        if ( $f ) {
            unlink(current($f));
        }

        $this -> flashMessenger() -> addMessage('Image was removed');
        return $this -> redirect() -> toRoute('manage_residence/images_list', array('type_id' => $typeId));
    } // remove


    protected function getResidenceService()
    {
        if ( null === $this -> residenceService ) {
            $this -> residenceService = $this -> serviceLocator -> get('Residence\Service');
        }
        return $this -> residenceService;
    } // getResidenceService()


} // UploadImagesController
