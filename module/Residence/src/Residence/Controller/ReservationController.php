<?php
namespace Residence\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ReservationController extends AbstractActionController
{
    /**
     * @var \Residence\Service\Residence
     */
    protected $residenceService;

    protected function getResidenceService()
    {
        if ( null === $this -> residenceService ) {
            $this -> residenceService = $this -> serviceLocator -> get('Residence\Service');
        }
        return $this -> residenceService;
    } // getResidenceService()


    public function indexAction()
    {
        $this
            -> getServiceLocator()
            -> get('ViewHelperManager')
            -> get('quickReservationWidget')
            -> setConfirm(true);

        $form = $this -> getResidenceService() -> getForm('Residence\Reservation');
        if ( $this -> getRequest() -> isPost() ) {
            $data = $this -> getRequest() -> getPost();
            unset($data['submit']);
            $form -> setData($data);
        }

        return array(
            'form' => $form
        );
    } // index


    public function submitAction()
    {
        $form = $this -> getResidenceService() -> getForm('Residence\Reservation');
        $form -> setData($this -> getRequest() -> getPost());

        if ($this -> getRequest() -> isPost()) {

            $message = '';
            if ($form -> isValid()) {

                $data = $form -> getData();

                if ( $data['client_phone'] or $data['client_email']) {
                    $residenceType = $this -> getResidenceService() -> getTypeMapper() -> getResidenceById($data['residence_type']);

                    if (!$residenceType) {
                        return $this -> notFoundAction();
                    }
                    $data['residence_type'] = $residenceType;

                    $modConfig = $this -> getServiceLocator() -> get('residence_options');

                    $this
                        -> getServiceLocator()
                        -> get('CMSMailer')
                        -> sendHtml(array(
                            'from'     => $modConfig -> getReservationMailFrom(),
                            'fromName' => $modConfig -> getReservationMailFromName(),
                            'to'       => $modConfig -> getReservationMailTo(),
                            'toName'   => $modConfig -> getReservationMailToName(),
                            'template' => 'residence/email/reservation-notify',
                            'template_vars' => $data,
                            'subject'  => $this -> getServiceLocator() -> get('translator') -> translate('Online ordering'),

                        ));

                    return array('reservationData' => $data);
                }
                $message = 'Please input phone or email';
            }

            $this
                -> getServiceLocator()
                -> get('ViewHelperManager')
                -> get('quickReservationWidget')
                -> setConfirm(true);

            $viewModel = new ViewModel(array(
                'form' => $form,
                'message' => $message
            ));
            return $viewModel -> setTemplate('residence/reservation/index');
        }
        return $this -> notFoundAction();
    } // submit

} // ReservationController
