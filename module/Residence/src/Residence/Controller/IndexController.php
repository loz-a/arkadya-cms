<?php
namespace Residence\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    /**
     * @var \Residence\Service\Residence
     */
    protected $residenceService;


    public function indexAction()
    {
        $pagesService = $this -> serviceLocator -> get('CMSPages\Service');
        $page         = $pagesService -> getMapper() -> findByAlias('residence');

        if ( !$page ) {
            return $this -> notFoundAction();
        }

        $date = gmdate("D, d M Y H:i:s", strtotime($page -> getCreated()));
        $lastMod = gmdate("D, d M Y H:i:s", strtotime($page -> getUpdated()));

        $this
            -> getRequest()
            -> getHeaders()
            -> addHeaders(array(
               sprintf('Date: %s GMT', $date),
               sprintf('Last-Modified: %s GMT', $lastMod)
            ));

        return array(
            'page'  => $page,
        );
    } // index


    public function pricesAction()
    {
        $pagesService = $this -> serviceLocator -> get('CMSPages\Service');
        $page         = $pagesService -> getMapper() -> findByAlias('prices');

        return array(
            'types' => $this -> getResidenceService() -> getTypeMapper() -> fetchAll(),
            'page' => $page ? $page : null
        );
    } // priceAction()


    public function manageAction()
    {
        return array(
            'types' => $this -> getResidenceService() -> getTypeMapper() -> fetchAll()
        );
    } // manage()


    public function addAction()
    {
        return array(
            'form' => $this -> getResidenceService() -> getForm('Residence\Type\Add')
        );
    } // add


    public function addSubmitAction()
    {
        $form = $this -> getResidenceService() -> getForm('Residence\Type\Add');
        $form -> setData($this -> request -> getPost());

        if ( $form -> isValid() ) {
            $result = $this -> getResidenceService() -> add($form -> getData());
            if ( $result ) {
                $this -> flashMessenger() -> addMessage('Type was added');
                return $this -> redirect() -> toRoute('manage_residence');
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
           'form' => $form
        );
    } // addSubmit


    public function editAction()
    {
        $id = $this -> params('id');
        $form = $this -> getResidenceService() -> getPopulatedFormById($id);

        if ( !$form ) {
            return $this -> notFoundAction();
        }

        return array(
            'form' => $form
        );
    } // edit


    public function editSubmitAction()
    {
        $form = $this -> getResidenceService() -> getForm('Residence\Type\Edit');
        $form -> setData($this -> request -> getPost());

        if ( $form -> isValid() ) {
            $result = $this -> getResidenceService() -> edit($form -> getData());
            if ( $result ) {
                $this -> flashMessenger() -> addMessage('Type was edited');
                return $this -> redirect() -> toRoute('manage_residence');
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // editSubmit


    public function deleteAction()
    {
        $id = $this -> params('id');
        if ( !$id ) {
            return $this -> notFoundAction();
        }

        if ( $this -> request -> isPost() ) {
            $confirm = $this -> request -> getPost() -> get('confirm', 'no');

            if ( strtolower($confirm) === 'yes' ) {
                $this -> getResidenceService() -> getTypeMapper() -> removeById($id);
                $this -> flashMessenger() -> addMessage('Residence type was removed');
            }
            return $this -> redirect() -> toRoute('manage_residence');
        }
        return array(
            'type' => $this -> getResidenceService() -> getTypeMapper() -> findById($id)
        );
    } // delete()


    public function viewAction()
    {
        $alias = $this -> params('type');
        $type = $this -> getResidenceService() -> getTypeMapper() -> findByAlias($alias);

        if ( !$type ) {
            return $this -> notFoundAction();
        }

        $modOptions = $this -> getResidenceService() -> getOptions();
        $docRoot    = $this -> getRequest() -> getServer('DOCUMENT_ROOT');
        $uploadPath = $modOptions -> getUploadPath();
        $path       = str_replace(array('/', '\\'), DS, $docRoot . $uploadPath);
        $imagesMask = sprintf('%s%s%s%s*.{jpeg,JPEG,jpg,JPG}', $path, DS, $type -> getId(), DS);

        return array(
            'uploadPath' => sprintf('%s/%s', $uploadPath, $type -> getId()),
            'images'     => glob($imagesMask, GLOB_BRACE),
            'thumbDir'   => $modOptions -> getImageThumbDir(),
            'type' => $type,
        );
    } // view()


    protected function getResidenceService()
    {
        if ( null === $this -> residenceService ) {
            $this -> residenceService = $this -> serviceLocator -> get('Residence\Service');
        }
        return $this -> residenceService;
    } // getResidenceService()

} // IndexController
