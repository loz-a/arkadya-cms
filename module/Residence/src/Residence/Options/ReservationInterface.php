<?php
namespace Residence\Options;

interface ReservationInterface
{
    public function setReservationMailFrom($from);
    public function getReservationMailFrom();

    public function setReservationMailFromName($fromName);
    public function getReservationMailFromName();

    public function setReservationMailTo($to);
    public function getReservationMailTo();

    public function setReservationMailToName($toName);
    public function getReservationMailToName();
}
