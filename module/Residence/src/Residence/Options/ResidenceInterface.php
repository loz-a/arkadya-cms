<?php
namespace Residence\Options;

interface ResidenceInterface
{
    public function setTableName($tableName);

    public function getTableName();

    public function setTranslationTableName($tableName);

    public function getTranslationTableName();

    public function setLabelMaxLength($maxlen);

    public function getLabelMaxLength();

    public function setTitleMaxLength($maxlen);

    public function getTitleMaxLength();

    public function setUploadPath($uploadPath);

    public function getUploadPath();

    public function setImageWidth($width);

    public function getImageWidth();

    public function setImageHeight($height);

    public function getImageHeight();

    public function setImageQuality($quality);

    public function getImageQuality();

    public function setImageThumbWidth($width);

    public function getImageThumbWidth();

    public function setImageThumbHeight($height);

    public function getImageThumbHeight();

    public function setImageThumbQuality($quality);

    public function getImageThumbQuality();

    public function setImageThumbDir($thumbDir);

    public function getImageThumbDir();

    public function setReservationClientNameMaxLength($maxlen);

    public function getReservationClientNameMaxLength();

    public function setReservationPhoneMaxLength($maxlen);

    public function getReservationPhoneMaxLength();

    public function setReservationEmailMaxLength($maxlen);

    public function getReservationEmailNameMaxLength();
}
