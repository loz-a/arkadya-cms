<?php
namespace Residence\Options;

use Common\Mapper\Options\AbstractOptions;

class ModuleOptions extends AbstractOptions implements
    ResidenceInterface,
    ReservationInterface
{
    /**
     * @var string
     */
    protected $tableName = 'room_types';

    /**
     * @var string
     */
    protected $translationsTableName = 'room_types_translation';

    /**
     * @param string $tableName
     * @return ModuleOptions
     */
    public function setTranslationTableName($tableName)
    {
        $this -> translationsTableName = (string) $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTranslationTableName()
    {
        return $this -> translationsTableName;
    }


    /**
     * @var int
     */
    protected $labelMaxLength = 255;

    /**
     * @param int $maxlen
     * @return ModuleOptions
     */
    public function setLabelMaxLength($maxlen)
    {
        $this -> labelMaxLength = (int) $maxlen;
        return $this;
    }

    /**
     * @return int
     */
    public function getLabelMaxLength()
    {
        return $this -> labelMaxLength;
    }


    /**
     * @var int
     */
    protected $titleMaxLength = 255;

    /**
     * @param int $maxlen
     * @return ModuleOptions
     */
    public function setTitleMaxLength($maxlen)
    {
        $this -> titleMaxLength = (int) $maxlen;
        return $this;
    }

    /**
     * @return int
     */
    public function getTitleMaxLength()
    {
        return $this -> titleMaxLength;
    }


    protected $uploadPath = '/media/residence';

    public function setUploadPath($uploadPath)
    {
        $this -> uploadPath = (string) $uploadPath;
        return $this;
    }

    public function getUploadPath()
    {
        return $this -> uploadPath;
    }


    protected $imageWidth = 600;

    public function setImageWidth($width)
    {
        $this -> imageWidth = (int) $width;
        return $this;
    }

    public function getImageWidth()
    {
        return $this -> imageWidth;
    }


    protected $imageHeight = 600;

    public function setImageHeight($height)
    {
        $this -> imageHeight = (int) $height;
        return $this;
    }

    public function getImageHeight()
    {
        return $this -> imageHeight;
    }


    protected $imageQuality = 85;

    public function setImageQuality($quality)
    {
        $this -> imageQuality = (int) $quality;
        return $this;
    }

    public function getImageQuality()
    {
        return $this -> imageQuality;
    }


    protected $imageThumbWidth = 200;

    public function setImageThumbWidth($width)
    {
        $this -> imageThumbWidth = (int) $width;
        return $this;
    }

    public function getImageThumbWidth()
    {
        return $this -> imageThumbWidth;
    }


    protected $imageThumbHeight = 200;

    public function setImageThumbHeight($height)
    {
        $this -> imageThumbHeight = (int) $height;
        return $this;
    }

    public function getImageThumbHeight()
    {
        return $this -> imageThumbHeight;
    }


    protected $imageThumbQuality = 75;

    public function setImageThumbQuality($quality)
    {
        $this -> imageThumbQuality = (int) $quality;
        return $this;
    }

    public function getImageThumbQuality()
    {
        return $this -> imageThumbQuality;
    }


    protected $imageThumbDir = '200x200';

    public function setImageThumbDir($thumbDir)
    {
        $this -> imageThumbDir = (string) $thumbDir;
        return $this;
    }

    public function getImageThumbDir()
    {
        return $this -> imageThumbDir;
    }


    protected $reservationClientNameMaxLength = 75;

    public function setReservationClientNameMaxLength($maxlen)
    {
        $this -> reservationClientNameMaxLength = (int) $maxlen;
        return $this;
    }

    public function getReservationClientNameMaxLength()
    {
        return $this -> reservationClientNameMaxLength;
    }


    protected $reservationPhoneMaxLength = 50;

    public function setReservationPhoneMaxLength($maxlen)
    {
        $this -> reservationPhoneMaxLength = (int) $maxlen;
        return $this;
    }

    public function getReservationPhoneMaxLength()
    {
        return $this -> reservationPhoneMaxLength;
    }


    protected $reservationEamilMaxLength = 50;

    public function setReservationEmailMaxLength($maxlen)
    {
        $this -> reservationEamilMaxLength = (int) $maxlen;
        return $this;
    }

    public function getReservationEmailNameMaxLength()
    {
        return $this -> reservationEamilMaxLength;
    }


    protected $reservationMailFrom = 'fake@mail.com';

    public function setReservationMailFrom($from)
    {
        $this -> reservationMailFrom = (string) $from;
    }

    public function getReservationMailFrom()
    {
        return $this -> reservationMailFrom;
    }


    protected $reservationMailFromName = 'Fake Sender';

    public function setReservationMailFromName($fromName)
    {
        $this -> reservationMailFromName = (string) $fromName;
    }

    public function getReservationMailFromName()
    {
        return $this -> reservationMailFromName;
    }


    protected $reservationMailTo = 'fake@mail.com';

    public function setReservationMailTo($to)
    {
        $this -> reservationMailTo = (string) $to;
    }

    public function getReservationMailTo()
    {
        return $this -> reservationMailTo;
    }


    protected $reservationMailToName = 'Fake Recipient';

    public function setReservationMailToName($toName)
    {
        $this -> reservationMailToName = (string) $toName;
    }

    public function getReservationMailToName()
    {
        return $this -> reservationMailToName;
    }

} // ModuleOptions
