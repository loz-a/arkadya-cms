<?php
namespace Residence\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\AbstractOptions;
use Zend\Stdlib\Hydrator\ClassMethods as ObjectHydrator;
use Common\Mapper\AbstractDbMapper;
use Common\Filter\Translit as TranslitFilter;

class Residence implements ServiceManagerAwareInterface
{
    /**
     * @var ServiceManager
     */
    protected $sm;

    /**
     * @var AbstractOptions
     */
    protected $options;

    /**
     * @var AbstractDbMapper
     */
    protected $typeMapper;


    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     * @return Residence
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> sm = $serviceManager;
        return $this;
    } // setServiceManager()


    /**
     * @param \Zend\Stdlib\AbstractOptions $options
     * @return Residence
     */
    public function setOptions(AbstractOptions $options)
    {
        $this -> options = $options;
        return $this;
    } // setOptions()


    /**
     * @return null|\Zend\Stdlib\AbstractOptions
     */
    public function getOptions()
    {
        if ( null === $this -> options ) {
            $this -> setOptions($this -> sm -> get('residence_options'));
        }
        return $this -> options;
    } // getOptions()


    /**
     * @param \Common\Mapper\AbstractDbMapper $mapper
     * @return Residence
     */
    public function setTypeMapper(AbstractDbMapper $mapper)
    {
        $this -> typeMapper = $mapper;
        return $this;
    } // setTypeMapper()


    /**
     * @return \Common\Mapper\AbstractDbMapper
     */
    public function getTypeMapper()
    {
        if( null === $this -> typeMapper ) {
            $this -> setTypeMapper($this -> sm -> get('Residence\Mapper\Type'));
        }
        return $this -> typeMapper;
    } // getTypeMapper()


    public function add(array $data)
    {
        $translitter = new TranslitFilter();
        $translitter -> setSpaceReplacer('-');

        $data['alias'] = strtolower($translitter($data['label']));
        return $this -> getTypeMapper() -> insert($data);
    } // add()


    public function edit(array $data)
    {
        $id = (int) $data['id'];
        $type = $this -> getTypeMapper() -> findById($id);

        if ( !$type ) {
            return null;
        }

        $hydrator = new ObjectHydrator();
        $type = $hydrator -> hydrate($data, $type);
        $this -> getTypeMapper() -> update($type, array('id' => $id));
        return $type;
    } // edit()


    /**
     * @param string $alias
     * @return \Zend\Form\Form
     */
    public function getForm($alias)
    {
        return $this -> sm -> get('FormElementManager') -> get($alias);
    } // getForm()


    /**
     * @param int $id
     * @return null|\Zend\Form\Form
     */
    public function getPopulatedFormById($id)
    {
        $type = $this -> getTypeMapper() -> findById($id);
        if ( !$type ) {
            return null;
        }

        $hydrator = new ObjectHydrator();
        $form = $this -> getForm('Residence\Type\Edit');
        $form -> setData($hydrator -> extract($type));
        return $form;
    } // getPopulatedFormById()

} // Residence
