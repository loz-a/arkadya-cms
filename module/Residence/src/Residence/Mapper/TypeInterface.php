<?php
namespace Residence\Mapper;

interface TypeInterface
{
    public function fetchAll();

    public function findById($id);

    public function lookupByLabel($label, $exclude = null);

    public function lookupByTitle($title, $exclude = null);
}
