<?php
namespace Residence\Mapper;

use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Common\Mapper\AbstractDbMapper;
use CMSLocale\Service\CurrentLocale as CurLocaleService;

class Type extends AbstractDbMapper
    implements TypeInterface
{
    /**
     * @var \CMSLocale\Service\CurrentLocale
     */
    protected $curLocaleService;

    /**
     * @param \CMSLocale\Service\CurrentLocale $service
     * @return Type
     */
    public function setCurrentLocaleService(CurLocaleService $service)
    {
        $this -> curLocaleService = $service;
        return $this;
    } // setCurrentLocaleService()


    /**
     * @return \CMSLocale\Service\CurrentLocale
     * @throws \LogicException
     */
    public function getCurrentLocaleService()
    {
        if ( !$this -> curLocaleService ) {
            throw new \LogicException('Current locale service is undefined');
        }
        return $this -> curLocaleService;
    } // getCurrentLocaleService()


    public function fetchAll()
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $sql = "SELECT `t`.`id`, `t`.`label`, `t`.`alias`, `t`.`pay_per_person`, `t`.`pay_per2_persons`, `t`.`pay_per_add_place`, `t`.`order`, `tt`.`title`, `tt`.`description`
                FROM `{$this -> getTableName()}` as t
                LEFT JOIN `{$this -> options -> getTranslationTableName()}` as tt
                ON `t`.`id` = `tt`.`id` AND `tt`.`locale_id` = {$localeEntity -> getId()}
                ORDER BY `t`.`order`";

        $statement = $this -> getDbAdapter() -> query($sql);
        $result = $statement -> execute();
        $resultSet = $this -> getResultSet();
        $resultSet -> initialize($result);

        return $resultSet -> count() ? $resultSet : null;
    } // fetchAll()


    public function fetchIdAndTitlePairs()
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();
        $select = $this
            -> select()
            -> columns(array('id', 'title'))
            -> from($this -> options -> getTranslationTableName())
            -> where(array('locale_id' => $localeEntity -> getId()));

        $result = $this -> selectExecute($select);
        $result = iterator_to_array($result);

        $r = array();
        foreach ($result as $item) {
            $r[$item['id']] = $item['title'];
        }
        unset($result, $item);
        return $r;
    } // fetchIdAndTitlePairs()


    public function getResidenceById($id)
    {
        $id = (int) $id;
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $select = $this
            -> select()
            -> columns(array('title'))
            -> from($this -> options -> getTranslationTableName())
            -> where(array('id' => $id))
            -> where(array('locale_id' => $localeEntity -> getId()))
            -> limit(1);

        $result = $this -> selectExecute($select);
        $result = iterator_to_array($result);
        return current(current($result));
    } // getResidenceById()


    public function findById($id)
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();
        $id = (int) $id;

        $sql = "SELECT `t`.`id`, `t`.`label`, `t`.`alias`, `t`.`pay_per_person`, `t`.`pay_per2_persons`, `t`.`pay_per_add_place`, `t`.`order`, `tt`.`title`, `tt`.`description`
                FROM `{$this -> getTableName()}` as t
                LEFT JOIN `{$this -> options -> getTranslationTableName()}` as tt
                ON `t`.`id` = `tt`.`id` AND `tt`.`locale_id` = {$localeEntity -> getId()}
                WHERE `t`.`id` = {$id}
                LIMIT 1";

        $statement = $this -> getDbAdapter() -> query($sql);
        $result = $statement -> execute();
        $resultSet = $this -> getResultSet();
        $resultSet -> initialize($result);

        return $resultSet -> count() ? $resultSet -> current() : null;
    } // findById()


    public function findByAlias($alias)
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $aliasQi = $this -> getDbAdapter() -> getPlatform() -> quoteIdentifier('alias');
        $aliasFp = $this -> getDbAdapter() -> getDriver() -> formatParameterName('alias');

        $sql = "SELECT `t`.`id`, `t`.`label`, `t`.`alias`, `t`.`pay_per_person`, `t`.`pay_per2_persons`, `t`.`pay_per_add_place`, `t`.`order`, `tt`.`title`, `tt`.`description`
                FROM `{$this -> getTableName()}` as t
                LEFT JOIN `{$this -> options -> getTranslationTableName()}` as tt
                ON `t`.`id` = `tt`.`id` AND `tt`.`locale_id` = {$localeEntity -> getId()}
                WHERE `t`.{$aliasQi} = {$aliasFp}
                LIMIT 1";

        $params = array(
            'alias' => $alias
        );

        $statement = $this -> getDbAdapter() -> query($sql);
        $result = $statement -> execute($params);
        $resultSet = $this -> getResultSet();
        $resultSet -> initialize($result);

        return $resultSet -> count() ? $resultSet -> current() : null;
    } // findById()


    public function lookupByLabel($label, $exclude = null)
    {
        $select = $this
            -> select()
            -> columns(array('id'))
            -> from($this -> getTableName())
            -> where(array('label' => $label))
            -> limit(1);

        if ( $exclude ) {
            $select -> where($exclude);
        }
        return (bool) count($this -> selectExecute($select));
    } // lookupByLabel()


    public function lookupByTitle($title, $exclude = null)
    {
        $select = $this
            -> select()
            -> columns(array('id'))
            -> from($this -> options -> getTranslationTableName())
            -> where(array('title' => $title))
            -> where(array('locale_id' => $this -> curLocaleService -> getEntity() -> getId()))
            -> limit(1);

        if ( $exclude ) {
            $select -> where($exclude);
        }
        return (bool) count($this -> selectExecute($select));
    } // lookupByTitle()


    public function lookupTranslationById($id, $exclude = null)
    {
        $select = $this
            -> select()
            -> columns(array('id'))
            -> from($this -> options -> getTranslationTableName())
            -> where(array('id' => $id))
            -> where(array('locale_id' => $this -> curLocaleService -> getEntity() -> getId()))
            -> limit(1);

        if ( $exclude ) {
            $select -> where($exclude);
        }
        return (bool) count($this -> selectExecute($select));
    } // lookupByTitle()


    public function insert($entity, $tableName = null, HydratorInterface $hydrator = null)
    {
        $id = null;
        $rowData = $this -> entityToArray($entity, $hydrator);

        $this -> getConnection() -> beginTransaction();
        try {

            $type = array(
                'label'             => $rowData['label'],
                'alias'             => $rowData['alias'],
                'pay_per_person'    => $rowData['pay_per_person'],
                'pay_per2_persons' => $rowData['pay_per2_persons'],
                'pay_per_add_place' => $rowData['pay_per_add_place'],
                'order'             => $rowData['order']
            );

            parent::insert($type);
            $id = $this -> getConnection() -> getLastGeneratedValue();

            $this -> insertTranslationInfo($id, $rowData);

            unset($type, $rowData);
            $this -> getConnection() -> commit();
        }
        catch( \Exception $e ) {
            $this -> getConnection() -> rollback();
            throw new \Exception('Residence type can\'t create');
        }
        return $id;
    } // insert()


    public function insertTranslationInfo($id, array $data)
    {
        $t = array(
            'id'          => $id,
            'locale_id'   => $this -> getCurrentLocaleService() -> getEntity() -> getId(),
            'title'       => $data['title'],
            'description' => $data['description']
        );

        parent::insert($t, $this -> options -> getTranslationTableName());
        unset($t);
    } // insertTranslationInfo()


    public function update($entity, $where, $tableName = null, HydratorInterface $hydrator = null)
    {
        $rowData  = $this -> entityToArray($entity, $hydrator);
        $localeId = $this -> getCurrentLocaleService() -> getEntity() -> getId();
        $id = (int) $rowData['id'];

        $this -> getConnection() -> beginTransaction();
        try {
            $type = array(
                'label'             => $rowData['label'],
                'pay_per_person'    => $rowData['pay_per_person'],
                'pay_per2_persons' => $rowData['pay_per2_persons'],
                'pay_per_add_place' => $rowData['pay_per_add_place'],
                'order'             => $rowData['order']
            );
            parent::update($type, $where);

            if ( $this -> lookupTranslationById($id) ) {
                $this -> updateTranslationInfo($rowData);
            }
            else {
                $this -> insertTranslationInfo($id, $rowData);
            }
            unset($type, $rowData);
            $this -> getConnection() -> commit();
        }
        catch( \Exception $e ) {
            $this -> getConnection() -> rollback();
            throw new \Exception('Residence type can\'t be updated');
        }
        return $id;
    } // update()


    public function updateTranslationInfo(array $data)
    {
        $where = array(
            'id' => $data['id'],
            'locale_id' => $this -> getCurrentLocaleService() -> getEntity() -> getId()
        );

        $t = array(
            'title' => $data['title'],
            'description' => $data['description']
        );

        parent::update($t, $where, $this -> options -> getTranslationTableName());
        unset($t);
    } // updateTranslationInfo()


    public function removeById($id)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> getTableName());
        $del = $sql -> delete() -> where(array('id' => $id));
        $statement = $sql -> prepareStatementForSqlObject($del);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // removeById()

} // Type
