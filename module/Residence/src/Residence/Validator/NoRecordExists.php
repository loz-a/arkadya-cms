<?php
namespace Residence\Validator;

use Common\Validator\AbstractRecord;
use Common\Validator\Exception\InvalidArgumentException;
use Residence\Mapper\TypeInterface;

class NoRecordExists extends AbstractRecord
{
    /**
     * @var TypeInterface
     */
    protected $mapper;

    public function setMapper($mapper)
    {
        if ( !$mapper instanceof TypeInterface ) {
            throw new InvalidArgumentException('Wrong mapper type. Residence\Mapper\TypeInterface expected');
        }

        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    public function isValid($value)
    {
        $this -> setValue($value);

        $result = $this -> query($value);
        if ( $result ) {
            $this -> error(self::ERROR_RECORD_FOUND);
            return false;
        }
        return true;
    } // isValid()


    protected function query($value)
    {
        $result = false;

        switch ( $this -> getKey() ) {
            case 'label':
                $result = $this -> mapper -> lookupByLabel($value, $this -> getExclude());
                break;
            case 'title':
                $result = $this -> mapper -> lookupByTitle($value, $this -> getExclude());
                break;
            default:
                throw new \Exception('Invalid key used in Residence validator');
        }
        return $result;
    } // query()

} // NoRecordExists
