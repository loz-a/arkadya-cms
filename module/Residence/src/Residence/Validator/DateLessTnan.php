<?php
namespace Residence\Validator;

use Zend\Validator\Date;
use Traversable;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\DateTime;
use DateInterval;

class DateLessTnan extends Date
{
    const NOT_LESS           = 'notLessThan';
    const NOT_LESS_INCLUSIVE = 'notLessThanInclusive';
    const INVALID_TOKEN_DATE = 'dateInvalidTokenDate';

    protected $messageTemplates = array(
        self::NOT_LESS           => "The input is not less than '%max%'",
        self::NOT_LESS_INCLUSIVE => "The input is not less or equal than '%max%'",
        self::INVALID_TOKEN_DATE => "The %token% does not appear to be a valid date"
    );

    protected $messageVariables = array(
        'max' => 'max',
        'token' => 'token'
    );

    protected $max;

    protected $inclusive = false;

    protected $token;

    public function __construct($options = array())
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }

        if (!is_array($options)) {
            $options = func_get_args();
            $temp['format'] = array_shift($options);

            if (!empty($options)) {
                $temp['min'] = array_shift($options);
            }

            if (!empty($options)) {
                $temp['inclusive'] = array_shift($options);
            }

            $options = $temp;
        }

        if (array_key_exists('max', $options)) {
            $this -> setMax($options['max']);
        }

        if (array_key_exists('inclusive', $options)) {
            $this -> setInclusive($options['inclusive']);
        }

        if (array_key_exists('token', $options)) {
            $this -> setToken($options['token']);
        }

        parent::__construct($options);
    } // __construct()


    public function getMax()
    {
        if (!$this -> max) {
            $tomorrow = new DateTime();
            $tomorrow -> setTime(0, 0, 0);
            $tomorrow -> add(DateInterval::createFromDateString('1 day'));
            $this -> max = $tomorrow -> format($this -> getFormat());
        }
        return $this -> max;
    } // getMax()


    public function setMax($max)
    {
        $this -> max = $max;
        return $this;
    } // setMax()


    public function getInclusive()
    {
        return $this -> inclusive;
    } // getInclusive()


    public function setInclusive($inclusive)
    {
        $this -> inclusive = (bool) $inclusive;
        return $this;
    } // setInclusive()


    public function getToken()
    {
        return $this -> token;
    } // getToken()


    public function setToken($token)
    {
        $this -> token = $token;
        return $this;
    } // setToken()


    public function isValid($value, $context = null)
    {
        $result = parent::isValid($value);

        if (!$result) {
            return false;
        }

        if (!$value instanceof DateTime) {
            $value = $this -> convertToDateTime($value);
        }

        if (
            $token = $this -> getToken()
            and is_array($context)
            and array_key_exists($token, $context)
        ) {

            if (!parent::isValid($context[$token])) {
                $this -> error(self::INVALID_TOKEN_DATE);
                return false;
            }

            $max = $this -> convertToDateTime($context[$token]);
            $this -> setMax($max -> format($this -> getFormat()));
        }
        else {
            $max = $this -> getMax();
            $max = new DateTime($max);
        }

        if ($this -> inclusive) {
            if ($value > $max) {
                $this -> error(self::NOT_LESS_INCLUSIVE);
                return false;
            }
        }
        else {
            if ($value >= $max) {
                $this -> error(self::NOT_LESS);
                return false;
            }
        }
        return true;
    } // isValid()


    /**
     * @param $str
     * @return \DateTime|\Zend\Stdlib\DateTime
     */
    protected function convertToDateTime($str)
    {
        if (is_int($str)) {
            return new DateTime("@$str");
        }
        $format = $this -> getFormat();
        return DateTime::createFromFormat($format, $str);
    } // convertToDateTime()

} // DateLessThan
