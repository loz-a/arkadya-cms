<?php
namespace Residence\Validator;

use Zend\Validator\Date;
use Traversable;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\DateTime;

class DateGreaterThan extends Date
{
    const NOT_GREATER = 'notGreaterThan';
    const NOT_GREATER_INCLUSIVE = 'notGreaterThanInclusive';
    const INVALID_TOKEN_DATE = 'dateInvalidTokenDate';

    protected $messageTemplates = array(
        self::NOT_GREATER => "The input is not greater than '%min%'",
        self::NOT_GREATER_INCLUSIVE => "The input is not greater or equal than '%min%'",
        self::INVALID_TOKEN_DATE => "The %token% does not appear to be a valid date"
    );

    /**
     * @var array
     */
    protected $messageVariables = array(
        'min' => 'min',
        'token' => 'token'
    );

    /**
     * @var DateTime
     */
    protected $min;

    /**
     * @var bool
     */
    protected $inclusive = false;

    /**
     * @var string
     */
    protected $token;


    public function __construct($options = array())
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }

        if (!is_array($options)) {
            $options = func_get_args();
            $temp['format'] = array_shift($options);

            if (!empty($options)) {
                $temp['min'] = array_shift($options);
            }

            if (!empty($options)) {
                $temp['inclusive'] = array_shift($options);
            }

            $options = $temp;
        }

        if (array_key_exists('min', $options)) {
            $this -> setMin($options['min']);
        }

        if (array_key_exists('inclusive', $options)) {
            $this -> setInclusive($options['inclusive']);
        }

        if (array_key_exists('token', $options)) {
            $this -> setToken($options['token']);
        }

        parent::__construct($options);
    } // __construct()


    public function getMin()
    {
        if (!$this -> min) {
            $today = new DateTime();
            $today -> setTime(0, 0, 0);
            $this -> min = $today -> format($this -> getFormat());
        }
        return $this -> min;
    } // getMin()


    public function setMin($min)
    {
        $this -> min = $min;
        return $this;
    } // setMin()


    public function getInclusive()
    {
        return $this -> inclusive;
    } // getInclusive()


    public function setInclusive($inclusive)
    {
        $this -> inclusive = (bool) $inclusive;
        return $this;
    } // setInclusive()


    public function getToken()
    {
        return $this -> token;
    } // getToken()


    public function setToken($token)
    {
        $this -> token = $token;
        return $this;
    } // setToken()


    public function isValid($value, $context = null)
    {
        $result = parent::isValid($value);

        if (!$result) {
            return false;
        }

        if (!$value instanceof DateTime) {
            $value = $this -> convertToDateTime($value);
        }

        if (
            $token = $this -> getToken()
            and is_array($context)
            and array_key_exists($token, $context)
        ) {

            if (!parent::isValid($context[$token])) {
                $this -> error(self::INVALID_TOKEN_DATE);
                return false;
            }

            $min = $this -> convertToDateTime($context[$token]);
            $this -> setMin($min -> format($this -> getFormat()));
        }
        else {
            $min = $this -> getMin();
            $min = new DateTime($min);
        }

        if ($this -> inclusive) {
            if ($min > $value) {
                $this -> error(self::NOT_GREATER_INCLUSIVE);
                return false;
            }
        }
        else {
            if ($min >= $value) {
                $this -> error(self::NOT_GREATER);
                return false;
            }
        }

        return true;
    } // isValid()


    /**
     * @param $str
     * @return \DateTime|\Zend\Stdlib\DateTime
     */
    protected function convertToDateTime($str)
    {
        if (is_int($str)) {
            return new DateTime("@$str");
        }
        $format = $this -> getFormat();
        return DateTime::createFromFormat($format, $str);
    } // convertToDateTime()

} // DateGreaterThan