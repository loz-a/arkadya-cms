<?php
namespace Residence\View\Helper;

use Residence\Mapper\Exception\UndefinedMapperException;
use Residence\Mapper\Type;
use Residence\Mapper\TypeInterface;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;


class ResidencePricesWidget extends AbstractHelper
{
    /**
     * @var Type
     */
    protected $mapper;

    /**
     * @param TypeInterface $typeMapper
     */
    public function setMapper(TypeInterface $typeMapper)
    {
        $this -> mapper = $typeMapper;
    } // setMapper()


    public function getMapper()
    {
        if (null === $this -> mapper) {
            throw new UndefinedMapperException('Type mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()


    public function __invoke()
    {
        $residenceTypes = $this -> getMapper() -> fetchAll();
        $viewModel      = new ViewModel(array(
            'residenceTypes' => $residenceTypes
        ));

        $viewModel -> setTemplate('residence/widget/prices');
        return $this -> getView() -> render($viewModel);
    } // __invoke()

} 