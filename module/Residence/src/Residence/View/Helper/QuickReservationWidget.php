<?php
namespace Residence\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

class QuickReservationWidget extends AbstractHelper
{
    protected $confirm = false;

    public function setConfirm($flag)
    {
        $this -> confirm = (bool) $flag;
    } // setConfirm()


    public function isConfirm()
    {
        return $this -> confirm;
    } // isConfirm()


    public function __invoke()
    {
        $viewModel = new ViewModel();
        if ( $this -> isConfirm() ) {
            $viewModel -> setTemplate('residence/widget/confirm-reservation');
        }
        else {
            $viewModel -> setTemplate('residence/widget/quick-reservation');
        }
        return $this -> getView() -> render($viewModel);
    } // __invoke()

} // QuickReservationWidget
