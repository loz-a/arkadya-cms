<?php
namespace Residence\Entity;

class Type implements TypeInterface
{
    protected $id;
    protected $alias;
    protected $label;
    protected $payPerPerson;
    protected $payPer2Persons;
    protected $payPerAddPlace;
    protected $order;
    protected $title;
    protected $description;


    public function setId($id)
    {
        $this -> id = (int) $id;
        return $this;
    }

    public function getId()
    {
        return $this -> id;
    }


    public function setAlias($alias)
    {
        $this -> alias = (string) $alias;
        return $this;
    } // setAlias()


    public function getAlias()
    {
        return $this -> alias;
    } // getAlias()


    public function setLabel($label)
    {
        $this -> label = (string) $label;
        return $this;
    }

    public function getLabel()
    {
        return $this -> label;
    }


    public function setPayPerPerson($payPerPerson)
    {
        $this -> payPerPerson = $payPerPerson;
        return $this;
    }

    public function getPayPerPerson()
    {
        return $this -> payPerPerson;
    }


    public function setPayPer2Persons($payPer2Persons)
    {
        $this -> payPer2Persons = $payPer2Persons;
        return $this;
    }

    public function getPayPer2Persons()
    {
        return $this -> payPer2Persons;
    }


    public function setPayPerAddPlace($payPerAddPlace)
    {
        $this -> payPerAddPlace = $payPerAddPlace;
        return $this;
    }

    public function getPayPerAddPlace()
    {
        return $this -> payPerAddPlace;
    }

    public function setOrder($order)
    {
        $this -> order = (int) $order;
        return $this;
    }

    public function getOrder()
    {
        return $this -> order;
    }


    public function setTitle($title)
    {
        $this -> title = (string) $title;
        return $this;
    }

    public function getTitle()
    {
        return $this -> title;
    }


    public function setDescription($description)
    {
        $this -> description = (string) $description;
        return $this;
    }

    public function getDescription()
    {
        return $this -> description;
    }

} // Type
