<?php
namespace Residence\Entity;

interface TypeInterface
{
    public function setId($id);

    public function getId();

    public function setAlias($alias);

    public function getAlias();

    public function setLabel($label);

    public function getLabel();

    public function setPayPerPerson($payPerPerson);

    public function getPayPerPerson();

    public function setPayPer2Persons($payPer2Persons);

    public function getPayPer2Persons();

    public function setPayPerAddPlace($payPerAddPlace);

    public function getPayPerAddPlace();

    public function setOrder($order);

    public function getOrder();

    public function setTitle($title);

    public function getTitle();

    public function setDescription($description);

    public function getDescription();
}
