<?php
namespace Residence\Form;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Reservation extends Form
    implements ServiceLocatorAwareInterface
{
    /**
     * @var \Zend\Form\FormElementManager
     */
    protected $fem;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this -> fem = $serviceLocator;
        return $this;
    } // setServiceLocator()


    public function getServiceLocator()
    {
        return $this -> fem;
    } // getServiceLocator()


    public function init()
    {
        $this
            -> setName('Reservation')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8')
            -> setAttribute('class', 'form')
            -> setAttribute('id', 'reservation');


        $this
            -> add(array(
                'name' => 'check_in',
                'options' => array(
                    'label' => 'Check in:'
                ),
                'attributes' => array(
                    'required' => true,
                    'id'       => 'check-in',
                    'class'    => 'calendar-icon',
                    'value'    => date('d.m.Y', strtotime('now'))
                )
            ))
            -> add(array(
                'name' => 'check_out',
                'options' => array(
                    'label' => 'Check out:'
                ),
                'attributes' => array(
                    'required' => true,
                    'id'       => 'check-out',
                    'class'    => 'calendar-icon',
                    'value'    => date('d.m.Y', strtotime('+1 day'))
                )
            ))
            -> add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'residence_type',
                'options' => array(
                    'label' => 'Residence type:',
                    'value_options' => $this -> fem -> getServiceLocator() -> get('Residence\Mapper\Type') -> fetchIdAndTitlePairs()
                ),
                'attributes' => array(
                    'required' => true,
                )
            ))
            -> add(array(
                'name' => 'client_name',
                'options' => array(
                    'label' => 'Name:'
                ),
                'attributes' => array(
                    'required' => true,
                    'maxlength' => $this -> fem -> getServiceLocator() -> get('residence_options') -> getReservationClientNameMaxLength()
                )
            ))
            -> add(array(
                'name' => 'client_phone',
                'options' => array(
                    'label' => 'Phone:'
                ),
                'attributes' => array(
                    'required' => false
                )
            ))
            -> add(array(
                'name' => 'client_email',
                'options' => array(
                    'label' => 'Email:'
                ),
                'attributes' => array(
                    'required' => false
                )
            ))
            -> add(array(
                'type' => 'Zend\Form\Element\Csrf',
                'name' => 'csrf',
                'options' => array(
                    'csrf_options' => array(
                        'timeout' => 6000
                    )
                )
            ))
            -> add(array(
                'type' => 'Zend\Form\Element\Submit',
                'name' => 'submit',
                'attributes' => array(
                    'type'  => 'submit',
                    'value' => 'Ok'
                )
            ));

    } // init()


    public function getActionRouteName()
    {
        return 'reservation/submit';
    } // getActionRouteName()

} // Reservation
