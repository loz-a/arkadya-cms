<?php
namespace Residence\Form;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\AbstractOptions;
use Common\Filter\Translit as TranslitFilter;
use Common\Filter\Imagick\Strategy\CropThumbnail;

class UploadImage extends Form
    implements ServiceLocatorAwareInterface
{
    /**
     * @var \Zend\Form\FormElementManager
     */
    protected $fem;

    public function init()
    {
        $modOptions = $this -> fem -> getServiceLocator() -> get('residence_options');

        $this
            -> setName('Upload image')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add(array(
                'name' => 'type_id',
                'type' => 'Zend\Form\Element\Hidden',
            ))
            -> add(array(
                'name' => 'image_file',
                'type' => 'Zend\Form\Element\File',
                'options' => array(
                    'label' => 'Image:'
                ),
                'attributes' => array(
                    'required' => true,
                    'class' => 'span4'
                )
            ))
            -> add(array(
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf'
            ))
            -> add(array(
                'name' => 'submit',
                'attributes' => array(
                    'class' => 'btn btn-primary',
                    'type'  => 'submit',
                    'value' => 'Ok'
                )
            ));

        $this -> addInputFilter($modOptions);
    } // init()


    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this -> fem = $serviceLocator;
        return $this;
    } // setServiceLocator()


    public function getServiceLocator()
    {
        return $this -> fem;
    } // getServiceLocator()


    protected function addInputFilter(AbstractOptions $modOptions)
    {
        $inputFilter = new InputFilter();

        $inputFilter
            -> add(array(
                'name' => 'typeid',
                'required' => true,
                'filters' => array(
                    array('name' => 'Int')
                )
            ));

        $inputFilter
            -> add(array(
                'name' => 'image_file',
                'required' => true,
                'validators' => array(
                    array('name' => 'File\IsImage'),
                    array(
                        'name'    => 'File\Extension',
                        'options' => array(
                             'extension' => array('jpeg', 'jpg')
                        )
                    ),
                    array(
                        'name' => 'File\Size',
                        'options' => array(
                            'max' => '2MB'
                        )
                    )
                ),
                'filters' => array(
                    array(
                        'name'    => 'FileRenameUpload',
                        'options' => array(
                            'target' => $this -> getFileTarget($modOptions),
                            'randomize' => true
                        )
                    ),
                    array(
                        'name' => 'Common\Filter\Imagick\Resizer',
                        'options' => array(
                            'width'     => $modOptions -> getImageWidth(),
                            'height'    => $modOptions -> getImageHeight(),
                            'quality'   => $modOptions -> getImageQuality(),
                        )
                    ),
                    array(
                        'name' => 'Common\Filter\Imagick\Resizer',
                        'options' => array(
                            'strategy'  => new CropThumbnail(),
                            'width'     => $modOptions -> getImageThumbWidth(),
                            'height'    => $modOptions -> getImageThumbHeight(),
                            'quality'   => $modOptions -> getImageThumbQuality(),
                            'thumb_dir' => $modOptions -> getImageThumbDir()
                        )
                    )
                )
            ));

        $this -> setInputFilter($inputFilter);
    } // addInputFilter()


    protected function getFileTarget(AbstractOptions $modOptions)
    {
        $request = $this -> fem -> getServiceLocator() -> get('Request');
        if ( !$request -> isPost()) {
            return '';
        }

        $doctRoot   = $request -> getServer('DOCUMENT_ROOT');
        $uploadPath = $modOptions -> getUploadPath();
        $typeId     = (int) $request -> getPost() -> get('type_id');

        $targetDir = sprintf('%s%s%s%s', $doctRoot, $uploadPath, DIRECTORY_SEPARATOR, $typeId);

        if ( !is_dir($targetDir) ) {
            mkdir($targetDir, 0777, true);
        }

        $translit = new TranslitFilter();
        $translit -> setSpaceReplacer('-');
        $file  = $request -> getFiles() -> get('image_file');

        return $targetDir . DIRECTORY_SEPARATOR . $translit($file['name']);
    } // getFileTarget()

} // UploadImage
