<?php
namespace Residence\Form;

use Zend\InputFilter\InputFilter;
use Residence\Options\ResidenceInterface as ResidenceOptionsInterface;
use Zend\Stdlib\DateTime;

class ReservationFilter extends InputFilter
{
    public function __construct(ResidenceOptionsInterface $residenceOptions)
    {
        $this
            -> add(array(
                'name' => 'check_in',
                'required' => true,
                'break_chain_on_failure' => true,
                'validators' => array(
                    array(
                        'name' => 'Residence\Validator\DateGreaterThan',
                        'options' => array(
                            'format' => 'd.m.Y'
                        )
                    ),
                    array(
                        'name' => 'Residence\Validator\DateLessTnan',
                        'options' => array(
                            'format' => 'd.m.Y',
                            'token'  => 'check_out',
                            'inclusive' => true
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'check_out',
                'required' => true,
                'break_chain_on_failure' => true,
                'validators' => array(
                    array(
                        'name' => 'Residence\Validator\DateGreaterThan',
                        'options' => array(
                            'format' => 'd.m.Y',
                            'token'  => 'check_in',
                            'inclusive' => true
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'residence_type',
                'required' => true,
                'break_chain_on_failure' => true,
            ))
            -> add(array(
                 'name' => 'client_name',
                 'required' => true,
                 'break_chain_on_failure' => true,
                 'filters'  => array(
                     array('name' => 'StripTags'),
                     array('name' => 'StringTrim')
                 ),
                 'validators' => array(
                     array(
                         'name' => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'max' => $residenceOptions -> getReservationClientNameMaxLength()
                         )
                     )
                 )
            ))
            -> add(array(
                        'name' => 'client_phone',
                'required' => false,
                'break_chain_on_failure' => true,
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $residenceOptions -> getReservationPhoneMaxLength()
                        )
                    ),
                    array(
                        'name' => 'Regex',
                        'options' => array(
                            'pattern' => '/^(\+[0-9]{1})?\s?(\([0-9]+\))?[0-9\s\-]+$/',
                            'messages' => array('regexNotMatch' => 'Wrong phone number'),
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'client_email',
                'required' => false,
                'break_chain_on_failure' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $residenceOptions -> getReservationEmailNameMaxLength()
                        )
                    ),
                    array(
                        'name' => 'EmailAddress'
                    )
                )
            ));
    } // __construct()

} // ReservationFilter
