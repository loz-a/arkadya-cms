<?php
namespace Residence\Form\Type;

class Edit extends Add
{
    public function init()
    {
        parent::init();
        $this
            -> setName('Edit type')
            -> add(array(
                'name' => 'id',
                'attributes' => array(
                    'type' => 'hidden'
                )
            ));
    } // init()


    public function getActionRouteName()
    {
        return 'manage_residence/edit/submit';
    } // getActionRouteName()

} // Edit
