<?php
namespace Residence\Form\Type;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class Add extends Form
    implements ServiceLocatorAwareInterface
{
    /**
     * @var \Zend\Form\FormElementManager
     */
    protected $formElementManager;

    public function init()
    {
        $modOptions = $this -> formElementManager -> getServiceLocator() -> get('residence_options');

        $this
            -> setName('Add room type')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add(array(
                'name' => 'label',
                'options' => array(
                    'label' => 'Label:'
                ),
                'attributes' => array(
                    'class'     => 'span4',
                    'required'  => true,
                    'maxlength' => $modOptions -> getLabelMaxLength()
                )
            ))
            -> add(array(
                'name' => 'title',
                'options' => array(
                    'label' => 'Title:'
                ),
                'attributes' => array(
                    'class'     => 'span4',
                    'required'  => true,
                    'maxlength' => $modOptions -> getTitleMaxLength()
                )
            ))
            -> add(array(
                'name' => 'description',
                'type' => 'Zend\Form\Element\Textarea',
                'options' => array(
                    'label' => 'Description:'
                ),
                'attributes' => array(
                    'required' => true,
                    'class' => 'span10',
                    'rows'  => 2,
                    'cols'  => 5
                )
            ))
            -> add(array(
                'name' => 'pay_per_person',
                'options' => array(
                    'label' => 'Pay per person:'
                ),
                'attributes' => array(
                    'class' => 'span1',
                    'required' => true
                )
            ))
            -> add(array(
                'name' => 'pay_per2_persons',
                'options' => array(
                    'label' => 'Pay per 2 persons:'
                ),
                'attributes' => array(
                    'class' => 'span1',
                    'required' => true
                )
            ))
            -> add(array(
                'name' => 'pay_per_add_place',
                'options' => array(
                    'label' => 'Pay per additional place:'
                ),
                'attributes' => array(
                    'class' => 'span1',
                    'required' => false,
                    'value' => 0
                )
            ))
            -> add(array(
                'name' => 'order',
                'options' => array(
                    'label' => 'Order:'
                ),
                'attributes' => array(
                    'class' => 'span1',
                    'required' => true,
                )
            ))
            -> add(array(
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf'
            ))
            -> add(array(
                'name' => 'submit',
                'attributes' => array(
                    'class' => 'btn btn-primary',
                    'type'  => 'submit',
                    'value' => 'Ok'
                )
            ));
    } // init()


    public function setServiceLocator(ServiceLocatorInterface $formElementManager)
    {
        $this -> formElementManager = $formElementManager;
        return $this;
    } // setServiceLocator()


    public function getServiceLocator()
    {
        return $this -> formElementManager;
    } // getServiceLocator()


    public function getActionRouteName()
    {
        return 'manage_residence/add/submit';
    } // getActionRouteName()

} // Add
