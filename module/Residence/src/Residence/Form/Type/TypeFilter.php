<?php
namespace Residence\Form\Type;

use Common\Form\InputFilter\InputFilter;

class TypeFilter extends InputFilter
{
    public function init()
    {
        $modOptions = $this -> serviceManager -> get('residence_options');
        $id         = $this -> serviceManager -> get('Request') -> getPost('id');

        if ( $id ) {
            $this
                -> add(array(
                    'name' => 'id',
                    'required' => true,
                    'filters'  => array(
                        array('name' => 'Int')
                    )
                ));
        }

        $this
            -> add(array(
                'name' => 'label',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => $modOptions -> getLabelMaxLength()
                        )
                    ),
                    array(
                        'name' => 'Residence\Validator\NoRecordExists',
                        'options' => array(
                            'mapper'  => $this -> serviceManager -> get('Residence\Mapper\Type'),
                            'key'     => 'label',
                            'exclude' => $id ? sprintf('id != %s', $id): null
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max'      => $modOptions -> getTitleMaxLength()
                        )
                    ),
                    array(
                        'name' => 'Residence\Validator\NoRecordExists',
                        'options' => array(
                            'mapper'  => $this -> serviceManager -> get('Residence\Mapper\Type'),
                            'key'     => 'title',
                            'exclude' => $id ? sprintf('id != %s', $id): null
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'description',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                )
            ))
            -> add(array(
                'name' => 'pay_per_person',
                'required' => true,
//                'validators'  => array(
//                    array('name' => 'Digits')
//                )
            ))
            -> add(array(
                'name' => 'pay_per2_persons',
                'required' => true,
//                'validators'  => array(
//                    array('name' => 'Digits')
//                )
            ))
            -> add(array(
                'name' => 'pay_per_add_place',
                'required' => true,
//                'validators'  => array(
//                    array('name' => 'Digits')
//                )
            ))
            -> add(array(
                'name' => 'order',
                'required' => true,
                'validators'  => array(
                    array('name' => 'Digits')
                )
            ));

    } // init()
    
} // TypeFilter
