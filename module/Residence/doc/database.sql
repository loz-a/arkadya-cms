CREATE TABLE `room_types` (
	`id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
	`label` VARCHAR(255) NOT NULL,
	`alias` VARCHAR(255) NOT NULL,
	`pay_per_person` SMALLINT(5) UNSIGNED NOT NULL,
	`pay_per2_persons` SMALLINT(5) UNSIGNED NOT NULL,
	`pay_per_add_place` SMALLINT(5) UNSIGNED NULL DEFAULT '0',
	`order` SMALLINT(3) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `label` (`label`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT

CREATE TABLE `room_types_translation` (
	`id` SMALLINT(5) UNSIGNED NOT NULL,
	`locale_id` SMALLINT(10) UNSIGNED NOT NULL,
	`title` VARCHAR(255) NOT NULL,
	`description` TEXT NOT NULL,
	PRIMARY KEY (`id`, `locale_id`),
	UNIQUE INDEX `title` (`title`, `locale_id`),
	INDEX `FK_room_types_translation_locales` (`locale_id`),
	CONSTRAINT `FK_room_types_translation_locales` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`),
	CONSTRAINT `FK_room_types_translation_room_types` FOREIGN KEY (`id`) REFERENCES `room_types` (`id`) ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB