<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'CMSPages\Controller\Index' => 'CMSPages\Controller\IndexController'
        )
    ),

    'view_manager' => array(
        'template_map' => array(
            'cms-pages/index/index'         => __DIR__ . '/../view/cms-pages/index/index.phtml',
            'cms-pages/index/create'        => __DIR__ . '/../view/cms-pages/index/create.phtml',
            'cms-pages/index/create-submit' => __DIR__ . '/../view/cms-pages/index/create.phtml',
            'cms-pages/index/edit'          => __DIR__ . '/../view/cms-pages/index/create.phtml',
            'cms-pages/index/edit-submit'   => __DIR__ . '/../view/cms-pages/index/create.phtml',
            'cms-pages/index/delete'        => __DIR__ . '/../view/cms-pages/index/delete.phtml',
            'cms-pages/index/view'          => __DIR__ . '/../view/cms-pages/index/view.phtml'
        ),
        'template_path_stack' => array(
            'cms-pages' => __DIR__ . '/../view'
        )
    ),

    'router' => array(
        'routes' => array(
            'page' => array(
                'type'  => 'Regex',
                'options' => array(
                    'regex' => '/(?<alias>[a-z0-9_\-]+)\.html',
                    'defaults' => array(
                        'controller' => 'CMSPages\Controller\Index',
                        'action'     => 'view'
                    ),
                    'spec' => '/%alias%.html'
                )
            ),
            'pages' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/pages',
                    'defaults' => array(
                        'controller' => 'CMSPages\Controller\Index',
                        'action'     => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'create' => array(
                        'type'    => 'Literal',
                        'options' => array(
                            'route' => '/create'
                        ),
                        'may_terminate' => false,
                        'child_routes' => array(
                            'set_data' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb'     => 'get',
                                    'defaults' => array(
                                        'action' => 'create'
                                    )
                                ),
                                'may_terminate' => true
                            ),
                            'submit' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb'     => 'post',
                                    'defaults' => array(
                                        'action' => 'create-submit'
                                    )
                                ),
                                'may_terminate' => true
                            )
                        )
                    ),
                    'edit' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/edit/:id',
                            'constraints' => array(
                                'id' => '\d+'
                            )
                        ),
                        'may_terminate' => false,
                        'child_routes' => array(
                            'edit_data' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb'     => 'get',
                                    'defaults' => array(
                                        'action' => 'edit'
                                    )
                                ),
                                'may_terminate' => true
                            ),
                            'submit' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb' => 'post',
                                    'defaults' => array(
                                        'action' => 'edit-submit'
                                    )
                                ),
                                'may_terminate' => true
                            )
                        )
                    ),
                    'delete' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/delete/:id',
                            'constraints' => array(
                                'id' => '\d+'
                            ),
                            'defaults' => array(
                                'action' => 'delete'
                            )
                        )
                    )
                )
            )
        )
    ),

    'route_layouts' => array(
        'page'                  => 'layout/layout',
        'pages'                 => 'layout/admin',
        'pages/create/set_data' => 'layout/admin',
        'pages/create/submit'   => 'layout/admin',
        'pages/edit/edit_data'  => 'layout/admin',
        'pages/edit/submit'     => 'layout/admin',
        'pages/delete'          => 'layout/admin',
    ),

    'admin_navigation' => array(
        'cms_pages' => array(
            'index' => 1,
            'header' => array(
                'label' => 'Pages',
                'icon' => 'icon-file',
                'class_name' => 'file',
            ),
            'items' => array(
                array(
                    'label' => 'All Pages',
                    'route_name' => 'pages'
                ),
            )
        )
    ),

);