<?php
namespace CMSPages\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    /**
     * @var \CMSPages\Service\Pages
     */
    protected $pagesService;


    public function indexAction()
    {
        return array(
            'pages' => $this -> getPagesService() -> getMapper() -> fetchAll()
        );
    }// index


    public function createAction()
    {
        return array(
            'form' => $this -> getPagesService() -> getCreateForm()
        );
    } // create


    public function createSubmitAction()
    {
        $form = $this -> getPagesService() -> getCreateForm();
        $form -> setData($this -> request -> getPost());

        if ( $form -> isValid() ) {
            $result = $this -> getPagesService() -> create($form -> getData());
            if ( $result ) {
                $this -> flashMessenger() -> addMessage('Page was added');
                return $this -> redirect() -> toRoute('pages');
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // createSubmit


    public function editAction()
    {
        $id = $this -> params('id');
        $form = $this -> getPagesService() -> getPopulatedFormById($id);

        if ( !$form ) {
            return $this -> notFoundAction();
        }

        return array(
            'form' => $form
        );
    } // edit


    public function editSubmitAction()
    {
        $form = $this -> getPagesService() -> getEditForm();
        $form -> setData($this -> request -> getPost());

        if ( $form -> isValid() ) {
            $result = $this -> getPagesService() -> edit($form -> getData());
            if ( $result ) {
                $this -> flashMessenger() -> addMessage('Page was edited');
                return $this -> redirect() -> toRoute('pages');
            }
            return $this -> notFoundAction();
        }
        else {
            $this -> flashMessenger() -> addMessage('Something was wrong');
        }

        return array(
            'form' => $form
        );
    } // editSubmit


    public function deleteAction()
    {
        $id = $this -> params('id');
        if ( !$id ) {
            return $this -> notFoundAction();
        }

        if ( $this -> request -> isPost() ) {
            $confirm = $this -> request -> getPost() -> get('confirm', 'no');

            if ( strtolower($confirm) === 'yes') {
                $this -> getPagesService() -> getMapper() -> removeById($id);
                $this -> flashMessenger() -> addMessage('Page was removed');
            }
            return $this -> redirect() -> toRoute('pages');
        }
        return array(
            'page' => $this -> getPagesService() -> getMapper() -> findPage($id)
        );
    } // delete


    public function viewAction()
    {
        $alias = $this -> params('alias');
        if ( !$alias ) {
            $this -> notFoundAction();
        }

        $page = $this -> getPagesService() -> getMapper() -> findByAlias($alias);
        if ( !$page ) {
            return $this -> notFoundAction();
        }

        $date = gmdate("D, d M Y H:i:s", strtotime($page -> getCreated()));
        $lastMod = gmdate("D, d M Y H:i:s", strtotime($page -> getUpdated()));

        $this
            -> getRequest()
            -> getHeaders()
            -> addHeaders(array(
                sprintf('Date: %s GMT', $date),
                sprintf('Last-Modified: %s GMT', $lastMod)
            ));

        return array(
            'page' => $page
        );
    } // view


    public function getPagesService()
    {
        if ( null === $this -> pagesService ) {
            $this -> pagesService = $this -> serviceLocator -> get('CMSPages\Service');
        }
        return $this -> pagesService;
    } // getPagesService()

} // IndexController
