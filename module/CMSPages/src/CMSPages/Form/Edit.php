<?php
namespace CMSPages\Form;

class Edit extends Create
{
    public function init()
    {
        parent::init();
        $this -> setName('Edit page');

        $this
            -> add(array(
                'name' => 'id',
                'attributes' => array(
                    'type' => 'hidden'
                )
            ))
            -> add(array(
                'name' => 'translation_id',
                'attributes' => array(
                    'type' => 'hidden'
                )
            ));
    } // init()

} // Edit
