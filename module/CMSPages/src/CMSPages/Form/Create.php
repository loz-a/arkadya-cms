<?php
namespace CMSPages\Form;

use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;
use Common\Form\Form;

class Create extends Form
{
    public function init()
    {
        $this
            -> setName('Create page')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add(array(
                'name' => 'title',
                'options' => array(
                    'label' => 'Title:'
                ),
                'attributes' => array(
                    'class' => 'span4',
                    'required' => true,
                    'maxlength' => $this -> getModuleOptions() -> getTitleMaxLength()
                )
            ))
            -> add(array(
                'name' => 'head_title',
                'options' => array(
                    'label' => 'Head title:'
                ),
                'attributes' => array(
                    'class' => 'span4',
                    'required' => true,
                    'maxlength' => $this -> getModuleOptions() -> getHeadTitleMaxLength()
                )
            ))
            -> add(array(
                'name' => 'meta_author',
                'options' => array(
                    'label' => 'Author:'
                ),
                'attributes' => array(
                    'class' => 'span4',
                    'maxlength' => $this -> getModuleOptions() -> getMetaAuthorMaxLength()
                )
            ))
            -> add(array(
                'name' => 'meta_description',
                'type' => 'Zend\Form\Element\Textarea',
                'options' => array(
                    'label' => 'Description:'
                ),
                'attributes' => array(
                    'class' => 'span10',
                    'rows' => 2,
                    'cols' => 5,
                    'maxlength' => $this -> getModuleOptions() -> getMetaDescriptionMaxLength()
                )
            ))
            -> add(array(
                'name' => 'css_class',
                'options' => array(
                    'label' => 'Css class which belongs content wrapper:'
                ),
                'attributes' => array(
                    'class' => 'span4',
                    'maxlength' => $this -> getModuleOptions() -> getCssClassMaxLength()
                )
            ))
            -> add(array(
                'name' => 'preparsed_content',
                'type' => 'Zend\Form\Element\Textarea',
                'options' => array(
                    'label' => 'Content:'
                ),
                'attributes' => array(
                    'class' => 'span10',
                    'rows' => 20,
                    'required' => true
                )
            ))
            -> add(array(
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf',
                'options' => array(
                    'csrf_options' => array(
                        'timeout' => 6000
                    )
                )
            ))
            -> add(array(
                'name' => 'submit',
                'attributes' => array(
                    'class' => 'btn btn-primary',
                    'type'  => 'submit',
                    'value' => 'Ok'
                )
            ));

    } // init()


    /**
     * @return null|\CMSPages\Options\ModuleOptions
     */
    public function getModuleOptions()
    {
        return parent::getModuleOptions('cms_pages_options');
    } // getModuleOptions()

} // Create
