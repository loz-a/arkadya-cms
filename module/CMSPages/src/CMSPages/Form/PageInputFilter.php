<?php
namespace CMSPages\Form;

use Common\Form\InputFilter\InputFilter;

class PageInputFilter extends Inputfilter
{
    public function init()
    {
        $options = $this -> serviceManager -> get('cms_pages_options');
        $id = $this -> serviceManager -> get('Request') -> getPost('id');

        if ( $id ) {
            $this
                -> add(array(
                    'name' => 'id',
                    'required' => true,
                    'filters'  => array(
                        array('name' => 'Int')
                    )
                ))
                -> add(array(
                    'name' => 'translation_id',
                    'required' => true,
                    'filters'  => array(
                        array('name' => 'Int')
                    )
                ));
        }

        $this
            -> add(array(
                'name' => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $options -> getTitleMaxLength()
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'head_title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $options -> getHeadTitleMaxLength()
                        )
                    ),
                    array(
                        'name' => 'CMSPages\Validator\NoRecordExists',
                        'options' => array(
                            'mapper'  => $this -> serviceManager -> get('CMSPages\Mapper'),
                            'key'     => 'head_title',
                            'exclude' => $id ? sprintf('page_id != %s', $id) : null
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'meta_author',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $options -> getMetaAuthorMaxLength()
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'meta_description',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $options -> getMetaDescriptionMaxLength()
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'css_class',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $options -> getCssClassMaxLength()
                        )
                    )
                )
            ))
            -> add(array(
                'name' => 'preparsed_content',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim')
                )
            ));

    } // init()

} // PageInputFilter
