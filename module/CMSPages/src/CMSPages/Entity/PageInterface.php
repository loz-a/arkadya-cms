<?php
namespace CMSPages\Entity;

interface PageInterface
{
    public function setId($id);

    public function getId();

    public function setAlias($alias);

    public function getAlias();

    public function setTitle($title);

    public function getTitle();

    public function setCreated($created);

    public function getCreated();

    public function setUpdated($updated);

    public function getUpdated();

    public function setViewsCount($viewsCount);

    public function getViewsCount();

    public function setAuthorId($id);

    public function getAuthorId();

    public function setHeadTitle($headTitle);

    public function getHeadTitle();

    public function setMetaDescription($metaDescription = '');

    public function getMetaDescription();

    public function setMetaAuthor($metaAuthor = '');

    public function getMetaAuthor();

    public function setContent($content);

    public function getContent();

    public function setPreparsedContent($preparsedContent);

    public function getPreparsedContent();

    public function setCssClass($cssClass);

    public function getCssClass();

} // PageInterface
