<?php
namespace CMSPages\Entity;

class Page implements PageInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @param int $id
     * @return Page
     */
    public function setId($id)
    {
        $this -> id = (int) $id;
        return $this;
    } // setId()

    /**
     * @return int
     */
    public function getId()
    {
        return $this -> id;
    } // getId()


    /**
     * @var string
     */
    protected $alias;

    /**
     * @param string $alias
     * @return Page
     */
    public function setAlias($alias)
    {
        $this -> alias = (string) $alias;
        return $this;
    } // setAlias()

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this -> alias;
    } // getAlias()


    /**
     * @var string
     */
    protected $title;

    /**
     * @param $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this -> title = (string) $title;
        return $this;
    } // setTitle()

    public function getTitle()
    {
        return $this -> title;
    } // getTitle()


    /**
     * @var string
     */
    protected $created;

    /**
     * @param $created
     * @return Page
     */
    public function setCreated($created)
    {
        $this -> created = (string) $created;
        return $this;
    } // setCreated()

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this -> created;
    } // getCreated()


    /**
     * @var string
     */
    protected $updated;

    /**
     * @param string $updated
     * @return Page
     */
    public function setUpdated($updated)
    {
        $this -> updated = (string) $updated;
        return $this;
    } // setUpdated()

    /**
     * @return string
     */
    public function getUpdated()
    {
        return $this -> updated;
    } // getUpdated()


    /**
     * @var int
     */
    protected $viewsCount;

    /**
     * @param int $viewsCount
     * @return Page
     */
    public function setViewsCount($viewsCount)
    {
        $this -> viewsCount = (int) $viewsCount;
        return $this;
    } // setViewCount()

    /**
     * @return int
     */
    public function getViewsCount()
    {
        return $this -> viewsCount;
    } // getViewCount()


    /**
     * @var int
     */
    protected $authorId;

    /**
     * @param $id
     * @return Page
     */
    public function setAuthorId($id)
    {
        $this -> authorId = (int) $id;
        return $this;
    } // setAuthorId()

    /**
     * @return int
     */
    public function getAuthorId()
    {
        return $this -> authorId;
    } // getAuthorId()


    /**
     * @var string
     */
    protected $headTitle;

    /**
     * @param $headTitle
     * @return Page
     */
    public function setHeadTitle($headTitle)
    {
        $this -> headTitle = (string) $headTitle;
        return $this;
    } // setHeadTitle()

    /**
     * @return string
     */
    public function getHeadTitle()
    {
        return $this -> headTitle;
    } // getHeadTitle()


    /**
     * @var string
     */
    protected $metaDescription;

    /**
     * @param string $metaDescription
     * @return Page
     */
    public function setMetaDescription($metaDescription = '')
    {
        $this -> metaDescription = $metaDescription;
        return $this;
    } // setMetaDescription()

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this -> metaDescription;
    } // getMetaDescription()


    /**
     * @var string
     */
    protected $metaAuthor;

    /**
     * @param string $metaAuthor
     * @return Page
     */
    public function setMetaAuthor($metaAuthor = '')
    {
        $this -> metaAuthor = (string) $metaAuthor;
        return $this;
    } // setMetaAuthor()

    /**
     * @return string
     */
    public function getMetaAuthor()
    {
        return $this -> metaAuthor;
    } // getMetaAuthor()


    /**
     * @var string
     */
    protected $content;

    /**
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this -> content = $content;
        return $this;
    } // setContent()

    /**
     * @return string
     */
    public function getContent()
    {
        return $this -> content;
    } // getContent()


    /**
     * @var string
     */
    protected $preparsedContent;

    /**
     * @param $preparsedContent
     * @return Page
     */
    public function setPreparsedContent($preparsedContent)
    {
        $this -> preparsedContent = $preparsedContent;
        return $this;
    } // setPreparsedContent()

    /**
     * @return string
     */
    public function getPreparsedContent()
    {
        return $this -> preparsedContent;
    } // getPreparsedContent()


    /**
     * @var int
     */
    protected $translationId;

    /**
     * @param $translationId
     * @return Page
     */
    public function setTranslationId($translationId)
    {
        $this -> translationId = $translationId;
        return $this;
    } // setTranslationId()

    /**
     * @return int
     */
    public function getTranslationId()
    {
        return $this -> translationId;
    } // getTraslationId()


    /**
     * @var string
     */
    protected $cssClass;

    /**
     * @param string $cssClass
     * @return Page
     */
    public function setCssClass($cssClass)
    {
        $this -> cssClass = (string) $cssClass;
        return $this;
    } // setCssClass()


    public function getCssClass()
    {
        return $this -> cssClass;
    } // getCssClass()

} // Page
