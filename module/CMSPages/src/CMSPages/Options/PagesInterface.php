<?php
namespace CMSPages\Options;

interface PagesInterface
{
    public function setTableName($tableName);

    public function getTableName();

    public function setPagesTranslationTableName($tableName);

    public function getPagesTranslationTableName();

    public function setPreparsedContentTableName($tableName);

    public function getPreparsedContentTableName();

    public function setPagesCountsTableName($tableName);

    public function getPagesCountsTableName();

    public function setTitleMaxLength($maxLength);

    public function getTitleMaxLength();

    public function setHeadTitleMaxLength($maxLength);

    public function getHeadTitleMaxLength();

    public function setMetaDescriptionMaxLength($maxLength);

    public function getMetaDescriptionMaxLength();

    public function setMetaAuthorMaxLength($maxLength);

    public function getMetaAuthorMaxLength();

    public function setCssClassMaxLength($maxLength);

    public function getCssClassMaxLength();
}
