<?php
namespace CMSPages\Options;

use Common\Mapper\Options\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements PagesInterface
{
    /**
     * @var string
     */
    protected $tableName = 'pages';

    /**
     * @var string
     */
    protected $pagesTranslationTableName = 'pages_translation';

    /**
     * @param string $tableName
     * @return ModuleOptions
     */
    public function setPagesTranslationTableName($tableName)
    {
        $this -> pagesTranslationTableName = (string) $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPagesTranslationTableName()
    {
        return $this -> pagesTranslationTableName;
    }


    /**
     * @var string
     */
    protected $preparsedContentTableName = 'preparsed_content';

    /**
     * @param string $tableName
     * @return ModuleOptions
     */
    public function setPreparsedContentTableName($tableName)
    {
        $this -> preparsedContentTableName = (string) $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPreparsedContentTableName()
    {
        return $this -> preparsedContentTableName;
    }


    /**
     * @var string
     */
    protected $pagesCountsTableName = 'pages_counts';

    /**
     * @param string $tableName
     * @return string
     */
    public function setPagesCountsTableName($tableName)
    {
        $this -> pagesCountsTableName = (string) $tableName;
        return $this -> pagesCountsTableName;
    }

    /**
     * @return string
     */
    public function getPagesCountsTableName()
    {
        return $this -> pagesCountsTableName;
    }


    protected $titleMaxLength = 255;

    public function setTitleMaxLength($maxLength)
    {
        $this -> titleMaxLength = (int) $maxLength;
        return $this;
    } // setTitleMaxLength()

    public function getTitleMaxLength()
    {
        return $this -> titleMaxLength;
    } // getTitleMaxLength()


    /**
     * @var int
     */
    protected $headTitleMaxLength = 70;

    /**
     * @param int $maxLength
     * @return ModuleOptions
     */
    public function setHeadTitleMaxLength($maxLength)
    {
        $this -> headTitleMaxLength = (int) $maxLength;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeadTitleMaxLength()
    {
        return $this -> headTitleMaxLength;
    }


    /**
     * @var int
     */
    protected $metaDescriptionMaxLength = 160;

    /**
     * @param int $maxLength
     * @return ModuleOptions
     */
    public function setMetaDescriptionMaxLength($maxLength)
    {
        $this -> metaDescriptionMaxLength = (int) $maxLength;
        return $this;
    }

    /**
     * @return int
     */
    public function getMetaDescriptionMaxLength()
    {
        return $this -> metaDescriptionMaxLength;
    }


    /**
     * @var int
     */
    protected $metaAuthorMaxLength = 255;

    /**
     * @param int $maxLength
     * @return ModuleOptions
     */
    public function setMetaAuthorMaxLength($maxLength)
    {
        $this -> metaAuthorMaxLength = (int) $maxLength;
        return $this;
    }

    /**
     * @return int
     */
    public function getMetaAuthorMaxLength()
    {
        return $this -> metaAuthorMaxLength;
    }


    /**
     * @var int
     */
    protected $cssClassMaxLength = 50;

    /**
     * @param int $maxLength
     * @return ModuleOptions
     */
    public function setCssClassMaxLength($maxLength)
    {
        $this -> cssClassMaxLength = (int) $maxLength;
        return $this;
    } // setCssClassMaxLength()

    /**
     * @return int
     */
    public function getCssClassMaxLength()
    {
       return $this -> cssClassMaxLength;
    } // getCssClassMaxLength()

} // ModuleOptions
