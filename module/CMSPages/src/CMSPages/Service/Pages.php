<?php
namespace CMSPages\Service;

use Zend\Stdlib\Hydrator\ClassMethods as ObjectHydrator;
use Common\Service\AbstractService;
use CMSPages\Entity\Page as PageEntity;
use Markdown\Filter\Markdown as MarkdownFilter;
use Common\Filter\Translit as TranslitFilter;

class Pages extends AbstractService
{
    /**
     * @return \CMSPages\Options\ModuleOptions
     */
    public function getOptions()
    {
        return parent::getOptions('cms_pages_options');
    } // getOptions()


    /**
     * @return \CMSPages\Mapper\Pages
     */
    public function getMapper()
    {
        return parent::getMapper('CMSPages\Mapper');
    } // getMapper()


    public function getCreateForm()
    {
        return parent::getCreateForm('CMSPages\Form\Create');
    } // getCreateForm()


    public function getEditForm()
    {
        return parent::getEditForm('CMSPages\Form\Edit');
    } // getEditForm()


    protected function preCreate(array $data)
    {
        $markdown = new MarkdownFilter($this -> serviceManager -> get('Markdown\Parser'));
        $translit = new TranslitFilter();
        $translit -> setSpaceReplacer('-');

        $data['content'] = $markdown($data['preparsed_content']);
        $data['alias']   = strtolower(preg_replace('/[^a-zA-Z0-9_\-]/', '', $translit($data['title'])));
        $data['alias'] = str_replace('--', '-', $data['alias']);

        //TODO map to user id
        $data['author_id'] = 1;
        $data['created'] = date('Y-m-d H:i:s', time());
        $data['updated'] = $data['created'];
        $data['css_class'] = $data['css_class'] ?: null;

        return $data;
    } // preCreate()


    public function create(array $data)
    {
        $data = $this -> preCreate($data);
        $result = $this -> getMapper() -> insert($data);
        return $result;
    } // create()


    protected function preEdit(array $data)
    {
        $markdown = new MarkdownFilter($this -> serviceManager -> get('Markdown\Parser'));
        $translit = new TranslitFilter();
        $translit -> setSpaceReplacer('-');

        $data['content'] = $markdown($data['preparsed_content']);
        $data['updated'] = date('Y-m-d H:i:s', time());
        $data['css_class'] = $data['css_class'] ?: null;

        return $data;
    } // preEdit()


    public function edit(array $data)
    {
        $data = $this -> preEdit($data);
        $id = (int) $data['id'];
        $page = $this -> getMapper() -> findPreparsedById($id);

        if ( !$page ) {
            return null;
        }

        $hydrator = new ObjectHydrator();
        $page = $hydrator -> hydrate($data, $page);

        $this -> getMapper() -> update($page, array('id' => $id));
        return $page;
    } // edit()


    public function getPopulatedFormById($id)
    {
        $page = $this -> getMapper() -> findPreparsedById($id);
        if ( !$page ) {
            return null;
        }
        else {
            $page -> setId($id);
        }

        $hydrator = new ObjectHydrator();
        $form = $this -> getEditForm();
        $form -> setData($hydrator -> extract($page));
        return $form;
    } // getPopulatedFormByid()

} // Pages
