<?php
namespace CMSPages\Validator;

use Common\Validator\AbstractRecord;
use Common\Validator\Exception\InvalidArgumentException;
use CMSPages\Mapper\PagesInterface;

class NoRecordExists extends AbstractRecord
{
    public function setMapper($mapper)
    {
        if ( !$mapper instanceof PagesInterface ) {
            throw new InvalidArgumentException('Wrong mapper type. CMSPages\Mapper\PagesInterface expected');
        }

        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    public function isValid($value)
    {
        $this -> setValue($value);

        $result = $this -> getMapper() -> lookupByHeadTitle($value, $this -> getExclude());
        if ( $result ) {
            $this -> error(self::ERROR_RECORD_FOUND);
            return false;
        }
        return true;
    } // isValid()

} // NoRecordExists
