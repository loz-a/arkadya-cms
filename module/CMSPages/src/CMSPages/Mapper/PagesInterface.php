<?php
namespace CMSPages\Mapper;

interface PagesInterface
{
    public function fetchAll();

    public function findById($id);

    public function findByAlias($alias);

    public function lookupByHeadTitle($headTitle, $exclude = null);

    public function removeById($id);

} // PagesInterface
