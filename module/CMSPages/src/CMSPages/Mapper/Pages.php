<?php
namespace CMSPages\Mapper;

use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Common\Mapper\AbstractDbMapper;
use CMSLocale\Service\CurrentLocale as CurLocaleService;

class Pages extends AbstractDbMapper
    implements PagesInterface
{
    /**
     * @var \CMSLocale\Service\CurrentLocale
     */
    protected $curLocaleService;

    /**
     * @param \CMSLocale\Service\CurrentLocale $service
     * @return Pages
     */
    public function setCurrentLocaleService(CurLocaleService $service)
    {
        $this -> curLocaleService = $service;
        return $this;
    } // setCurrentLocaleService()


    /**
     * @return \CMSLocale\Service\CurrentLocale
     * @throws \LogicException
     */
    public function getCurrentLocaleService()
    {
        if ( !$this -> curLocaleService ) {
            throw new \LogicException('Current locale service is undefined');
        }
        return $this -> curLocaleService;
    } // getCurrentLocaleService()


    public function fetchAll()
    {
        $select = $this
            -> select()
            -> columns(array('id', 'alias', 'title', 'created', 'updated', 'author_id'))
            -> from($this -> getTableName());

        $result = $this -> selectWith($select);
        return $result -> count() ? $result : null;
    } // fetchAll()


    public function fetchAliases()
    {
        $select = $this
            -> select()
            -> columns(array('alias'))
            -> from($this -> getTableName());

        $result = $this -> selectExecute($select);
        return count($result) ? $result : null;
    } // fetchAliases()


    public function findPage($id)
    {
        $select = $this
            -> select()
            -> columns(array('id', 'alias', 'title', 'created', 'updated', 'author_id', 'css_class'))
            -> from(array('p' => $this -> getTableName()))
            -> where(array('p.id' => $id))
            -> limit(1);

        $result = $this -> selectWith($select);
        return $result -> count() ? $result -> current() : null;
    } // findPage()

    public function findById($id)
    {
        $localeId = $this -> getCurrentLocaleService() -> getEntity() -> getId();

        $select = $this
            -> select()
            -> columns(array('id', 'alias', 'title', 'created', 'updated', 'author_id', 'css_class'))
            -> from(array('p' => $this -> getTableName()))
            -> join(
                array('t' => $this -> options -> getPagesTranslationTableName()),
                'p.id = t.page_id',
                array('translation_id' => 'id', 'head_title', 'meta_description', 'meta_author', 'content'),
                'left'
            )
            -> where(array('p.id' => $id))
            -> where(array('t.locale_id' => $localeId))
            -> limit(1);

        $result = $this -> selectWith($select);
        return $result -> count() ? $result -> current() : null;
    } // findById()


    public function findPreparsedById($id)
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $id = (int) $id;
        $sql = "SELECT `p`.`id`, `p`.`title`, `p`.`css_class`, `t`.`id` as translation_id, `t`.`head_title`, `t`.`meta_description`, `t`.`meta_author`, `pp`.`content` as preparsed_content
                FROM `{$this -> getTableName()}` as p
                LEFT JOIN `{$this -> options -> getPagesTranslationTableName()}` AS t
                ON `p`.`id` = `t`.`page_id` AND `t`.`locale_id` = {$localeEntity -> getId()}
                LEFT JOIN `{$this -> options -> getPreparsedContentTableName()}` AS pp
                ON `t`.`id` = `pp`.`id`
                WHERE `p`.`id` = {$id}
                LIMIT 1";

        $statement = $this -> getDbAdapter() -> query($sql);
        $result = $statement -> execute();
        $resultSet = $this -> getResultSet();
        $resultSet -> initialize($result);

        return $resultSet -> count() ? $resultSet -> current() : null;
    } // findPreparsedById()


    public function findByAlias($alias)
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $select = $this
            -> select()
            -> columns(array('id', 'title', 'alias', 'css_class', 'created', 'updated', 'author_id'))
            -> from(array('p' => $this -> getTableName()))
            -> join(
            array('t' => $this -> options -> getPagesTranslationTableName()),
            'p.id = t.page_id',
            array('head_title', 'meta_description', 'meta_author', 'content')
        )
            -> where(array('p.alias' => $alias))
            -> where(array('t.locale_id' => $localeEntity -> getId()))
            -> limit(1);

        $result = $this -> selectWith($select);
        return $result -> count() ? $result -> current() : null;
    } // findByAlias()


    /**
     * @param $headTitle
     * @param null $exclude
     * @return bool
     */
    public function lookupByHeadTitle($headTitle, $exclude = null)
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $select = $this
            -> select()
            -> columns(array('id'))
            -> from($this -> options -> getPagesTranslationTableName())
            -> where(array('head_title' => $headTitle))
            -> where(array('locale_id' => $localeEntity -> getId()))
            -> limit(1);

        if ( $exclude ) {
            $select -> where($exclude);
        }
        return (bool) count($this -> selectExecute($select));
    } // lookupByHeadTitle()


    public function lookupByPageIdAndLocaleId($pageId, $localeId)
    {
        $localeEntity = $this -> getCurrentLocaleService() -> getEntity();

        $select = $this
            -> select()
            -> columns(array('id'))
            -> from($this -> options -> getPagesTranslationTableName())
            -> where(array('page_id' => $pageId))
            -> where(array('locale_id' => $localeId))
            -> limit(1);

        return (bool) count($this -> selectExecute($select));
    } // lookupByPageIdAndLocaleId()


    public function removeById($id)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> getTableName());
        $del = $sql -> delete() -> where(array('id' => $id));
        $statement = $sql -> prepareStatementForSqlObject($del);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // removeById()


    public function insert($entity, $tableName = null, HydratorInterface $hydrator = null)
    {
        $id = null;
        $rowData = $this -> entityToArray($entity, $hydrator);

        $this -> getConnection() -> beginTransaction();
        try {
            $page = array();
            $page['title']     = $rowData['title'];
            $page['alias']     = $rowData['alias'];
            $page['created']   = $rowData['created'];
            $page['updated']   = $rowData['updated'];
            $page['author_id'] = $rowData['author_id'];
            $page['css_class'] = $rowData['css_class'];

            parent::insert($page);
            $id = $this -> getConnection() -> getLastGeneratedValue();

            $this -> insertTranslationInfo($id, $rowData);

            unset($page, $rowData);
            $this -> getConnection() -> commit();
        }
        catch(\Exception $e) {
            $this -> getConnection() -> rollback();
            throw new \Exception('Page can\'t be updated');
        }
        return $id;
    } // insert()


    protected function insertTranslationInfo($pageId, array $rowData)
    {
        $transl = array();
        $transl['page_id']    = $pageId;
        $transl['locale_id']  = $this -> getCurrentLocaleService() -> getEntity() -> getId();
        $transl['head_title'] = $rowData['head_title'];
        $transl['content']    = $rowData['content'];
        $transl['meta_description'] = $rowData['meta_description'];
        $transl['meta_author']      = $rowData['meta_author'];
        parent::insert($transl, $this -> options -> getPagesTranslationTableName());

        $preparsed = array();
        $preparsed['id'] = $this -> getConnection() -> getLastGeneratedValue();
        $preparsed['content'] = $rowData['preparsed_content'];
        parent::insert($preparsed, $this -> options -> getPreparsedContentTableName());

        unset($transl, $preparsed);
    } // insertTranslationInfo()


    public function update($entity, $where, $tableName = null, HydratorInterface $hydrator = null)
    {
        $rowData  = $this -> entityToArray($entity, $hydrator);
        $localeId = $this -> getCurrentLocaleService() -> getEntity() -> getId();
        $pageId   = (int) $rowData['id'];

        $this -> getConnection() -> beginTransaction();
        try {
            $page = array();
            $page['title']   = $rowData['title'];
            $page['updated'] = $rowData['updated'];
            $page['css_class'] = $rowData['css_class'];
            parent::update($page, $where);

            $isTranslateExists = $this -> lookupByPageIdAndLocaleId($pageId, $localeId);

            if ( $isTranslateExists ) {
                $this -> updateTranslationInfo($rowData);
            }
            else {
                $this -> insertTranslationInfo($pageId, $rowData);
            }
            unset($rowData, $page);
            $this -> getConnection() -> commit();
        }
        catch(\Exception $e) {
            $this -> getConnection() -> rollback();
            throw new \Exception('Page can\'t be updated');
        }
        return $pageId;
    } // update()


    protected function updateTranslationInfo(array $rowData)
    {
        $transl = array();
        $transl['head_title']       = $rowData['head_title'];
        $transl['meta_description'] = $rowData['meta_description'];
        $transl['meta_author']      = $rowData['meta_author'];
        $transl['content']          = $rowData['content'];

        $where = array(
            'id' => $rowData['translation_id'],
            'locale_id' => $this -> getCurrentLocaleService() -> getEntity() -> getId()
        );
        parent::update($transl, $where, $this -> options -> getPagesTranslationTableName());

        $preparsed = array();
        $preparsed['content'] = $rowData['preparsed_content'];
        unset($where['locale_id']);
        parent::update($preparsed, $where, $this -> options -> getPreparsedContentTableName());

        unset($where, $transl, $preparsed);
    } // updateTranslationInfo()

} // Pages
