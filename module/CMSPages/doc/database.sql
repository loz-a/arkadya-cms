CREATE TABLE `pages` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`alias` VARCHAR(70) NOT NULL,
	`created` DATETIME NOT NULL,
	`updated` DATETIME NOT NULL,
	`author_id` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `alias` (`alias`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT

CREATE TABLE `pages_translation` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`page_id` INT(10) UNSIGNED NOT NULL,
	`locale_id` SMALLINT(5) UNSIGNED NOT NULL,
	`head_title` VARCHAR(70) NOT NULL,
	`meta_description` VARCHAR(160) NULL DEFAULT NULL,
	`meta_author` VARCHAR(255) NULL DEFAULT NULL,
	`content` TEXT NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_pages_translation_pages` (`page_id`),
	INDEX `FK_pages_translation_locales` (`locale_id`),
	CONSTRAINT `FK_pages_translation_locales` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`),
	CONSTRAINT `FK_pages_translation_pages` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT

CREATE TABLE `preparsed_content` (
	`id` INT(10) UNSIGNED NOT NULL,
	`content` TEXT NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_preparsed_content_pages_translation` FOREIGN KEY (`id`) REFERENCES `pages_translation` (`id`) ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT

CREATE TABLE `pages_counts` (
	`id` INT(10) UNSIGNED NOT NULL,
	`views_count` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_pages_counts_pages` FOREIGN KEY (`id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT