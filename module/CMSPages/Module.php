<?php
namespace CMSPages;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
//    DependencyIndicatorInterface,
    ServiceProviderInterface
{
    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    } // getAutoloaderConfig()


    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


//    public function getModuleDependencies()
//    {
//        array(
//            'Application',
//            'Common',
//            'CMSAdmin',
//            'CMSLocale'
//        );
//    } // getModuleDependencies()


    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'CMSPages\Service' => 'CMSPages\Service\Pages',
                'CMSPages\Form\PageInputFilter' => 'CMSPages\Form\PageInputFilter',
            ),
            'factories' => array(
                'cms_pages_options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['cms_pages']) ? $config['cms_pages'] : array();
                    return new Options\ModuleOptions($options);
                },
                'CMSPages\Mapper' => function($sm) {
                    $mapper = new Mapper\Pages();
                    $mapper
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setEntityPrototype(new Entity\Page())
                        -> setHydrator(new ClassMethodsHydrator())
                        -> setOptions($sm -> get('cms_pages_options'))
                        -> setCurrentLocaleService($sm -> get('CMSLocale\CurrentLocale'));
                    return $mapper;
                },
                'CMSPages\Form\Create' => function($sm) {
                    $form = new Form\Create();
                    $form -> setInputFilter($sm -> get('CMSPages\Form\PageInputFilter'));
                    return $form;
                },
                'CMSPages\Form\Edit' => function($sm) {
                    $form = new Form\Edit();
                    $form -> setInputFilter($sm -> get('CMSPages\Form\PageInputFilter'));
                    return $form;
                }
            )
        );
    } // getServiceConfig()


} // Module
