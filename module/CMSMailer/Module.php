<?php
namespace CMSMailer;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ServiceProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    } // getAutoloaderConfig()


    public function getServiceConfig()
    {
        return array(
            'shared' => array(
                'CMSMailer' => false
            ),
            'factories' => array(
                'CMSMailer\Transport\Smtp' => 'CMSMailer\SmtpTransportFactory',
                'CMSMailer\Message'        => 'CMSMailer\MessageFactory',
                'CMSMailer'                => 'CMSMailer\MailerFactory',
            )
        );
    } // getServiceConfig()
}
