<?php
namespace CMSMailer;

use Zend\Mail\Message;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MessageFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config  = $serviceLocator -> get('Configuration');
        $message = new Message();

        if (isset($config['cms_mailer'])
            and array_key_exists('default_message', $config['cms_mailer'])
        ) {
            $options = $config['cms_mailer']['default_message'];
            $from    = $options['from'];
            $to      = $options['to'];

            $message = new Message();
            $message
                -> addFrom($from['email'], $from['name'])
                -> setTo($to)
                -> setEncoding($options['encoding']);
        }
        unset($config);

        return $message;
    } // createService()

} // MessageFactory
