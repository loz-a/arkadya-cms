<?php
namespace CMSMailer;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MailerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $message   = $serviceLocator -> get('CMSMailer\Message');
        $transport = $serviceLocator -> get('CMSMailer\Transport\Smtp');
        $renderer  = $serviceLocator -> get('ViewRenderer');

        return new Mailer($message, $transport, $renderer);
    } //createService()

} // MailerFacotry
