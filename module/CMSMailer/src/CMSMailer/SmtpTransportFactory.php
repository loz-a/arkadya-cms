<?php
namespace CMSMailer;

use Zend\Mail\Transport\Sendmail;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SmtpTransportFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $transport = null;
        $config = $serviceLocator -> get('Configuration');

        if (isset($config['cms_mailer'])
            and array_key_exists('smtp_options', $config['cms_mailer'])
        ) {
            $options = new SmtpOptions($config['cms_mailer']['smtp_options']);
            $transport = new Smtp($options);
        }
        else {
            $transport = new Sendmail();
        }
        return $transport;
    } // createService()

} // TransportFactory
