<?php
namespace CMSMailer;

use Zend\Mail\Message as MailMessage;
use Zend\Mail\Transport\TransportInterface;
use Zend\View\Renderer\RendererInterface;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class Mailer
{
    /**
     * @var \Zend\Mail\Message
     */
    protected $defaultMessage;

    /**
     * @var \Zend\Mail\Transport\TransportInterface
     */
    protected $transport;

    /**
     * @var \Zend\View\Renderer\RendererInterface
     */
    protected $renderer;

    public function __construct(
        MailMessage        $defaultMessage,
        TransportInterface $transport,
        RendererInterface  $renderer
    ){
        $this -> defaultMessage = $defaultMessage;
        $this -> transport      = $transport;
        $this -> renderer       = $renderer;
    } //__construct()


    public function getDefaultMessage()
    {
        return $this -> defaultMessage;
    } //getDefaultMessage()


    public function getTransport()
    {
        return $this -> transport;
    } //getTransport()


    public function getRenderer()
    {
        return $this -> renderer;
    } //getRenderer()


    protected function prepareMessage(array $options)
    {
        $message = $this -> getDefaultMessage();

        if (isset($options['encoding'])) {
            $message -> setEncoding($options['encoding']);
        }

        if (isset($options['from'])) {
            $message -> setFrom($options['from'], $options['fromName']);
        }

        if (isset($options['to'])) {
            $message -> setTo($options['to'], $options['toName']);
        }

        if (isset($options['cc'])) {
            $message -> setCc($options['cc']);
        }

        if (isset($options['bcc'])) {
            $message -> setBcc($options['bcc']);
        }

        if (isset($options['subject'])) {
            $message -> setSubject($options['subject']);
        }

        if (isset($options['sender'])) {
            $message -> setSender($options['sender']);
        }

        if (isset($options['replyTo'])) {
            $message -> setReplyTo($options['replyTo']);
        }

        if (isset($options['body'])) {
            $message -> setBody($options['body']);
        }

        if (isset($options['template'])) {
            $vars = isset($options['template_vars']) ? $options['template_vars'] : array();
            $content = $this -> getRenderer() -> render($options['template'], $vars);
            $message -> setBody($content);
        }
        unset($options);

        return $message;
    } // prepareMessage()


    public function sendText(array $options)
    {
        $message = $this -> prepareMessage($options);
        return $this -> getTransport() -> send($message);
    } // setText()


    public function sendHtml(array $options)
    {
        $message = $this -> prepareMessage($options);

        $text = new MimePart('');
        $text -> type = 'text/plain';

        $html = new MimePart($message -> getBody());
        $html -> type = 'text/html; charset=utf-8';

        $body = new MimeMessage();
        $body -> setParts(array($text, $html));

        $message -> setBody($body);

        return $this -> getTransport() -> send($message);
    } // sendHtml()

} // Mailer
