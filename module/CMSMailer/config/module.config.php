<?php
return array(
    'cms_mailer' => array(
        'default_message' => array(
            'from' => array(
                'email' => 'test@hi.com',
                'name'  => 'John Doe'
            ),
            'encoding' => 'UTF-8'
        )
    )
);