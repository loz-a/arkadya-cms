<?php
namespace Common\Service;

trait FormTrait
{
    /**
     * @return \Common\Mapper\AbstractDbMapper
     */
    abstract public function getMapper();

    /**
     * @param string $alias
     * @return \Zend\Form\Form
     */
    public function getForm($alias)
    {
        return $this -> serviceManager -> get('FormElementManager') -> get($alias);
    } // getForm()


    /**
     * @param int $id
     * @param string $formAlias
     * @return null|\Zend\Form\Form
     */
    public function getPopulatedFormById($id, $formAlias)
    {
        $entity = $this -> getMapper() -> findById($id);
        if (!$entity) {
            return null;
        }

        $form = $this -> getForm($formAlias);
        $hydrator = $this -> getMapper() -> getHydrator();
        $form -> setData($hydrator -> extract($entity));
        return $form;
    } // getPopulatedFormById()

} // FormTrait
