<?php
namespace Common\Service;

use Common\Service\ServiceInterface;

interface ServiceAwareInterface
{
    /**
     * @return \Common\Service\ServiceInterface
     */
    public function getService();

    /**
     * @param \Common\Service\ServiceInterface $service
     * @return mixed
     */
    public function setService(ServiceInterface $service);
}
