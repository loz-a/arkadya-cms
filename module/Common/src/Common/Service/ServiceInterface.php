<?php
namespace Common\Service;

use Zend\Stdlib\AbstractOptions;
use Common\Mapper\AbstractDbMapper;

interface ServiceInterface
{
    /**
     * @param \Zend\Stdlib\AbstractOptions $options
     * @return mixed
     */
    public function setOptions(AbstractOptions $options);

    /**
     * @return \Zend\Stdlib\AbstractOptions $options
     */
    public function getOptions();

    /**
     * @param $alias
     * @return \Zend\Form\Form
     */
    public function getForm($alias);

    /**
     * @param \Common\Mapper\AbstractDbMapper $roles
     * @return mixed
     */
    public function setMapper(AbstractDbMapper $roles);

    /**
     * @return \Common\Mapper\AbstractDbMapper $roles
     */
    public function getMapper();


    public function add(array $data);

    public function edit(array $data);
}
