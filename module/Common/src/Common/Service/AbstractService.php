<?php
namespace Common\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\AbstractOptions;
use Zend\Form\Form;
use Common\Mapper\AbstractDbMapper;

abstract class AbstractService implements ServiceManagerAwareInterface
{
    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    protected $serviceManager;

    /**
     * @var \Zend\Stdlib\AbstractOptions
     */
    protected $options;

    /**
     * @var \Common\Mapper\AbstractDbMapper
     */
    protected $mapper;

    /**
     * @var \Zend\Form\Form
     */
    protected $createForm;

    /**
     * @var \Zend\Form\Form
     */
    protected $editForm;

    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     * @return AbstractService
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> serviceManager = $serviceManager;
        return $this;
    } // setServiceManager()


    /**
     * @param \Zend\Stdlib\AbstractOptions $options
     * @return AbstractService
     */
    public function setOptions(AbstractOptions $options)
    {
        $this -> options = $options;
        return $this;
    } // setOptions()


    /**
     * @param string $serviceManagerName
     * @return null|\Zend\Stdlib\AbstractOptions
     */
    public function getOptions($serviceManagerName)
    {
        if( !strval($serviceManagerName) ) {
            return null;
        }

        if ( null === $this -> options ) {
            $this -> setOptions($this -> serviceManager -> get($serviceManagerName));
        }
        return $this -> options;
    } // getOptions()


    /**
     * @param \Common\Mapper\AbstractDbMapper $mapper
     * @return AbstractService
     */
    public function setMapper(AbstractDbMapper $mapper)
    {
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @param string $serviceManagerName
     * @return \Common\Mapper\AbstractDbMapper
     */
    public function getMapper($serviceManagerName)
    {
        if( !strval($serviceManagerName) ) {
            return null;
        }

        if( null === $this -> mapper ) {
            $this -> setMapper($this -> serviceManager -> get($serviceManagerName));
        }
        return $this -> mapper;
    } // getMapper()


    /**
     * @param \Zend\Form\Form $form
     * @return AbstractService
     */
    public function setCreateForm(Form $form)
    {
        $this -> createForm = $form;
        return $this;
    } // setCreateForm()


    /**
     * @param $serviceManagerName
     * @return null|\Zend\Form\Form
     */
    public function getCreateForm($serviceManagerName)
    {
        if( !strval($serviceManagerName) ) {
            return null;
        }

        if( null === $this -> createForm ) {
            $this -> setCreateForm($this -> serviceManager -> get($serviceManagerName));
        }
        return $this -> createForm;
    } // getCreateForm()


    /**
     * @param \Zend\Form\Form $form
     * @return AbstractService
     */
    public function setEditForm(Form $form)
    {
        $this -> editForm = $form;
        return $this;
    } // setEditForm()


    /**
     * @param $serviceManagerName
     * @return null|\Zend\Form\Form
     */
    public function getEditForm($serviceManagerName)
    {
        if( !strval($serviceManagerName) ) {
            return null;
        }

        if( null === $this -> editForm ) {
            $this -> setEditForm($this -> serviceManager -> get($serviceManagerName));
        }
        return $this -> editForm;
    } // getEditForm()

} // AbstractSerivce
