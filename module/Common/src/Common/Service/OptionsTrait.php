<?php
namespace Common\Service;

use Zend\Stdlib\AbstractOptions;

trait OptionsTrait
{
    /**
     * @var \Zend\Stdlib\AbstractOptions
     */
    protected $options;

    /**
     * @param \Zend\Stdlib\AbstractOptions $options
     * @return OptionsTrait
     */
    public function setOptions(AbstractOptions $options)
    {
        $this -> options = $options;
        return $this;
    } // setOptions()


    /**
     * @return \Zend\Stdlib\AbstractOptions
     */
    public function getOptions()
    {
        if ( null === $this -> options ) {
            list($moduleName) = explode('\\', get_class($this));
            $this -> setOptions($this -> serviceManager -> get($moduleName . '\Options'));
        }
        return $this -> options;
    } // getOptions()
} // OptionsTrait
