<?php

namespace Common\View\Helper\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{}