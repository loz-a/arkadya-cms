<?php
namespace Common\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class FlashMessengerWidget extends AbstractHelper
{
    /**
     * @var FlashMessenger
     */
    protected $flashMessenger;

    public function __construct(FlashMessenger $flashMessenger)
    {
        $this -> flashMessenger = $flashMessenger;
    } // __construct()


    public function __invoke()
    {
        $result = array();
        $msgs = array_merge(
            $this -> flashMessenger -> getMessages(),
            $this -> flashMessenger -> getCurrentMessages()
        );

        if ( $len = count($msgs) ) {
            $closeBtnHtml = '<button data-dismiss="alert" class="close" type="button">×</button>';
            for ( $i = 0; $i < $len; $i++ ) {
                $result[] = sprintf('<div class="alert alert-error"> %s <strong>Alert!</strong> %s </div>', $closeBtnHtml, $msgs[$i]);
            }

            $this -> flashMessenger -> clearMessages();
            $this -> flashMessenger -> clearCurrentMessages();
        }

        return implode('', $result);
    } // __invoke()

} // FlashMessengerWidget
