<?php
namespace Common\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

class TableWidget extends AbstractHelper
{
    const HEADERS = 'headers';
    const ENTITY_FIELDS = 'entity_fields';
    const ENTITIES = 'entities';

    public function __invoke(array $data)
    {
        if ( !array_key_exists(self::HEADERS, $data) ) {
            throw new Exception\InvalidArgumentException('Array which contains table headers title is undefined');
        }

        if ( !array_key_exists(self::ENTITY_FIELDS, $data) ) {
            throw new Exception\InvalidArgumentException('Array which contains entity fields is undefined');
        }

        if ( !array_key_exists(self::ENTITIES, $data) ) {
            throw new Exception\InvalidArgumentException('Array which contains table headers title is undefined');
        }

        $callback = function($field) {
            if ( !is_array($field)) {
                return 'get' . implode('', array_map('ucfirst', explode('_', $field)));
            }
            return $field;
        };

        $vars = array(
            'headers'    => $data[self::HEADERS],
            'getMethods' => array_map($callback, $data[self::ENTITY_FIELDS]),
            'entities'   => $data[self::ENTITIES]
        );

        $viewModel = new ViewModel($vars);
        $viewModel -> setTemplate('common/widget/table');
        return $this -> getView() -> render($viewModel);
    } // __invoke()

} // TableWiget
