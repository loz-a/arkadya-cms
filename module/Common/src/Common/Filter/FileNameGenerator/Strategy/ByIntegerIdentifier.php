<?php
namespace Common\Filter\FileNameGenerator\Strategy;

use Common\Filter\FileNameGenerator\Exception\InvalidArgumentException;

class ByIntegerIdentifier extends AbstractGenerator
{
    public function generate()
    {
        $id = $this -> getIdentifier();

        $filename = strrev(sprintf('%08d', $id));
        $result = array();

        array_push($result, substr($filename, 0, 2));
        array_push($result, substr($filename, 2, 2));
        array_push($result, substr($filename, 4, 2));

        return implode($result, '/');
    } // generate()


    protected function getIdentifier()
    {
        if ( !array_key_exists('value', $this -> options) ) {
            throw new InvalidArgumentException('Identifier is undefined');
        }
        return (int) $this -> options['value'];
    } // getIdentifier()

} // ByIntegerIdentifier
