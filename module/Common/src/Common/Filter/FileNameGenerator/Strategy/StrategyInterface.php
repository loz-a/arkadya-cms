<?php
namespace Common\Filter\FileNameGenerator\Strategy;

interface StrategyInterface
{
    public function setOptions(array $options);

    public function generate();
}
