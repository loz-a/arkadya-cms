<?php
namespace Common\Filter\FileNameGenerator\Strategy;

abstract class AbstractGenerator implements StrategyInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @param array $options
     * @return AbstractGenerator
     */
    public function setOptions(array $options)
    {
        $this -> options = $options;
        return $this;
    } // setOptions()


    /**
     * @return array
     */
    public function getOptions()
    {
        return $this -> options;
    } // getOptions()

} // AbstractGenerator
