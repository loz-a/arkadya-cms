<?php
namespace Common\Filter\FileNameGenerator;

use Zend\Filter\AbstractFilter;

class Generator extends AbstractFilter
{
    /**
     * @var \Common\Filter\FileNameGenerator\Strategy\StrategyInterface
     */
    protected $strategy;

    /**
     * namespace for filename
     *
     * @var string
     */
    protected $ns;


    /**
     * @param Strategy\StrategyInterface $strategy
     * @return Generator
     */
    public function setStrategy(Strategy\StrategyInterface $strategy)
    {
        $this -> strategy = $strategy;
        return $this;
    } // setStrategy()


    /**
     * @return Strategy\StrategyInterface
     */
    public function getStrategy()
    {
        return $this -> strategy;
    } // getStrategy()


    /**
     * @param $ns
     * @return Generator
     */
    public function setNamespace($ns)
    {
        $this -> ns = (string) $ns;
        return $this;
    } // setNamespace()


    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this -> ns;
    } // getNamespace()


    public function filter($value)
    {
        $this -> strategy -> setOptions(array_merge(array('value' => $value), $this -> options));
        return $this -> getNamespace() . DIRECTORY_SEPARATOR . $this -> strategy -> generate();
    } // filter()

} // Generator
