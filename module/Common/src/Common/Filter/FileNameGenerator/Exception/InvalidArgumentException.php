<?php
namespace Common\Filter\FileNameGenerator\Exception;

class InvalidArgumentException
    extends \InvalidArgumentException
    implements ExceptionInterface
{
}
