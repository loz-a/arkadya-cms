<?php
namespace Common\Filter;

use Zend\Filter\AbstractFilter;

class Translit extends AbstractFilter
{
    protected $spaceReplacer = ' ';

    public function setSpaceReplacer($replacer)
    {
        $this -> spaceReplacer = (string) $replacer;
        return $this;
    } // setSpaceReplacer()


    public function filter($st)
    {
        // Сначала заменяем "односимвольные" фонемы.
        $st=strtr($st, array(
                            'а'=>'a', 'б'=>'b', 'в'=>'v', 'г'=>'g', 'д'=>'d',
                            'е'=>'e', 'ё'=>'e', 'з'=>'z', 'и'=>'i', 'й'=>'y',
                            'к'=>'k', 'л'=>'l', 'м'=>'m', 'н'=>'n', 'о'=>'o',
                            'п'=>'p', 'р'=>'r', 'с'=>'s', 'т'=>'t', 'у'=>'u',
                            'ф'=>'f', 'х'=>'h', 'ъ'=>"'", 'ы'=>'i', 'э'=>'e',
                            'і'=>'i'));

        $st=strtr($st, array(
                            'А'=>'A', 'Б'=>'B', 'В'=>'V', 'Г'=>'G', 'Д'=>'D',
                            'Е'=>'E', 'Ё'=>'E', 'З'=>'Z', 'И'=>'I', 'Й'=>'Y',
                            'К'=>'K', 'Л'=>'L', 'М'=>'M', 'Н'=>'N', 'О'=>'O',
                            'П'=>'P', 'Р'=>'R', 'С'=>'S', 'Т'=>'T', 'У'=>'U',
                            'Ф'=>'F', 'Х'=>'H', 'Ъ'=>"'", 'Ы'=>'I', 'Э'=>'E',
                            'І'=>'I'));

        // Затем - "многосимвольные".
        $st=strtr($st, array(
                            "ж"=>"zh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh",
                            "щ"=>"shch","ь"=>"", "ю"=>"yu", "я"=>"ya",
                            "Ж"=>"ZH", "Ц"=>"TS", "Ч"=>"CH", "Ш"=>"SH",
                            "Щ"=>"SHCH","Ь"=>"", "Ю"=>"YU", "Я"=>"YA",
                            "ї"=>"i", "Ї"=>"Yi", "є"=>"ye", "Є"=>"Ye",
                            "№"=>"N", ' '=> $this -> spaceReplacer));

        return $st;
    } // filter

} // Translit
