<?php
namespace Common\Filter\Imagick;

use Zend\Filter\AbstractFilter;
use Common\Filter\Imagick\Strategy\AbstractStrategy;

class Resizer extends AbstractFilter
{
    /**
     * Width of output image.
     * @var integer
     */
    protected $width;
    /**
     * Height of output image.
     * @var integer
     */
    protected $height;

    /**
     * Quality for JPEG output.
     * @var int
     */
    protected $quality = 75;

    /**
     * @var string
     */
    protected $outputFilename;

    /**
     * @var string
     */
    protected $thumbDir;


    /**
     * @var AbstractStrategy
     */
    protected $strategy;


    public function __construct(array $options = array())
    {
        $this -> setOptions($options);
    } // __construct()


    public function filter($value)
    {
        if ( !extension_loaded('imagick')) {
            throw new Exception\RuntimeException('Imagick extension is not available. Can\'t process image.');
        }

        if ( false === stream_resolve_include_path($value) ) {
            throw new Exception\RuntimeException(sprintf("Image %s isn't exists", $value));
        }

        if ( is_array($value) and array_key_exists('tmp_name', $value) ) {
            $this -> resize($value['tmp_name']);
        }
        else {
            $this -> resize($value);
        }
        return $value;
    } // filter()


    public function setStrategy(AbstractStrategy $strategy)
    {
        $this -> strategy = $strategy;
        return $this;
    } // setStrategy()


    public function getStrategy()
    {
        if ( null === $this -> strategy ) {
            $this -> strategy = new Strategy\Fit();
        }
        return $this -> strategy;
    } // getStrategy()


    /**
     * Set the output width in pixels.
     * @param int $width
     * @return Resizer
     */
    public function setWidth($width)
    {
        $width = (int) $width;
        $this -> width = $width > 0 ? $width : 1;
        return $this;
    } // setWidth()


    /**
     * Get the output width in pixels.
     * @return int
     */
    public function getWidth()
    {
        if ( !$this -> width ) {
            throw new Exception\UnexpectedValueException('Width is undefined');
        }
        return $this -> width;
    } // getWidth()


    /**
     * Set the output height in pixels.
     * @param int $height
     * @return Resizer
     */
    public function setHeight($height)
    {
        $height = (int) $height;
        $this -> height = $height > 0 ? $height : 1;
        return $this;
    } // setHeigth()


    /**
     * Get the output height in pixels.
     * @return int
     */
    public function getHeight()
    {
        if ( !$this -> height ) {
            throw new Exception\UnexpectedValueException('Height is undefined');
        }
        return $this -> height;
    } // getHeight()


    /**
     * Set the JPEG output compression.
     * @param int $q A value between 1 and 100.
     * @return Resizer
     */
    public function setQuality($q)
    {
        $q = (int) $q;

        if ($q > 100) {
            $q = 100;
        } elseif ($q < 1) {
            $q = 1;
        }
        $this -> quality = $q;
        return $this;
    } // setQuality()


    /**
     * Get the JPEG output compression.
     * @return int
     */
    public function getQuality()
    {
        return $this -> quality;
    } // getQuality()


    /**
     * @param $outputFilename
     * @return Resizer
     */
    public function setOutputFilename($outputFilename)
    {
        $this -> outputFilename = (string) $outputFilename;
        return $this;
    } // setOutputFilename()


    /**
     * @return string
     */
    public function getOutputFilename()
    {
        return $this -> outputFilename;
    } // getOutputFilename()


    /**
     * @param string $thumbDir
     * @return Resizer
     */
    public function setThumbDir($thumbDir)
    {
        $this -> thumbDir = (string) $thumbDir;
        return $this;
    } // setThumbDir()


    public function getThumbDir()
    {
        return $this -> thumbDir;
    } // getThumbDir()


    /**
     * Load the image and resize it using the assigned strategy.
     * @param string $filename
     * @return string Filename of the output file.
     */
    protected function resize($filename)
    {
        $this -> getStrategy() -> resize(
            $filename,
            $this -> getWidth(),
            $this -> getHeight(),
            $this -> getQuality(),
            $this -> getOutputFilename(),
            $this -> getThumbDir()
        );
    } // resize()

} // Resizer
