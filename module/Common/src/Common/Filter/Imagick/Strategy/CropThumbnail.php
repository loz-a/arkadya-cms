<?php
namespace Common\Filter\Imagick\Strategy;

class CropThumbnail extends AbstractStrategy
{
    /**
     * Return canvas resized according to the given dimensions.
     *
     * @param $filename
     * @param $width
     * @param $height
     * @param $quality
     * @param null $outputFilename
     * @param null $thumbDir
     */
    public function resize($filename, $width, $height, $quality, $outputFilename = null, $thumbDir = null)
    {
        $image = new \Imagick($filename);
        $image -> setimagecompression(\Imagick::COMPRESSION_JPEG);
        $image -> setimagecompressionquality($quality);
        $image -> stripimage();

        $geo = $image->getimagegeometry();
        $ratio = min($geo['width'], $geo['height']) / max($width, $height);

        $rWidth = $width * $ratio;
        $rHeight = $height * $ratio;

        $image -> cropthumbnailimage($width, $height);

        $this -> _writeImage($image, $outputFilename, $thumbDir);
    } // resize()

} // CropThumbnail
