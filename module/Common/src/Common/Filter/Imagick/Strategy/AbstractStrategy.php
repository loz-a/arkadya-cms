<?php
namespace Common\Filter\Imagick\Strategy;

use Common\Filter\Imagick\Exception\LogicException as ImagickLogicExcetion;

abstract class AbstractStrategy implements ResizeStrategyInterface
{
    /**
     * Return canvas resized according to the given dimensions.
     *
     * @param $filename
     * @param $width
     * @param $height
     * @param $quality
     * @param $outputFilename
     * @throws \Common\Filter\Imagick\Exception\LogicException
     */
    public function resize($filename, $width, $height, $quality, $outputFilename = null)
    {
        throw new ImagickLogicExcetion(sprintf('Method "%" should be overridden', __METHOD__));
    } // resize()


    protected function _writeImage(\Imagick $image, $outputFilename = null, $thumbDir = null)
    {
        $filename = $outputFilename ? $outputFilename : $image -> getimagefilename();

        if ( $thumbDir ) {
            $pi = pathinfo($filename);
            $targetDir = sprintf('%s%s%s', $pi['dirname'], DIRECTORY_SEPARATOR, $thumbDir);

            if ( !is_dir($targetDir) ) {
                mkdir($targetDir, 0777, true);
            }
            $filename = sprintf('%s%s%s', $targetDir, DIRECTORY_SEPARATOR, $pi['basename']);
        }

        $filename = str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, $filename);

        $image -> writeimage($filename);
        $image -> destroy();
    } // _writeImage

} // AbstractStrategy
