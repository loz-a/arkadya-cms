<?php
namespace Common\Filter\Imagick\Strategy;

interface ResizeStrategyInterface
{
    public function resize($filename, $width, $height, $quality, $outputFilename = null);
}
