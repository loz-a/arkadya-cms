<?php
namespace Common\Filter\Imagick\Strategy;

class Fit extends AbstractStrategy
{
    /**
     * Return canvas resized according to the given dimensions.
     *
     * @param $filename
     * @param $width
     * @param $height
     * @param $quality
     * @param null $outputFilename
     * @param null $thumbDir
     */

    public function resize($filename, $width, $height, $quality, $outputFilename = null, $thumbDir = null)
    {
        $image = new \Imagick($filename);
        $image -> setimagecompression(\Imagick::COMPRESSION_JPEG);
        $image -> setimagecompressionquality($quality);
        $image -> stripimage();

        $geo = $image -> getimagegeometry();

        if ($geo['height'] <= $geo['width']) {
            $image -> resizeimage($width, 0, \Imagick::FILTER_LANCZOS, 1);
        }
        else {
            $image -> resizeimage(0, $height, \Imagick::FILTER_LANCZOS, 1);
        }

        $this -> _writeImage($image, $outputFilename, $thumbDir);
    } // resize()

} // Fit
