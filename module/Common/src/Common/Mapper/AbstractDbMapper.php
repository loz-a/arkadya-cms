<?php
namespace Common\Mapper;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\Adapter\Driver\ConnectionInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Common\EventManager\EventProvider;

abstract class AbstractDbMapper extends EventProvider
{
    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var \Zend\Db\Adapter\Driver\ConnectionInterface
     */
    protected $connection;

    /**
     * @var Adapter
     */
    protected $dbAdapter;

    /**
     * @var HydratorInterface
     */
    protected $hydrator;

    /**
     * @var object
     */
    protected $entityPrototype;

    /**
     * @var HydratingResultSet
     */
    protected $resultSetPrototype;

    /**
     * @var Select
     */
    protected $selectPrototype;

    /**
     * @var Options\AbstractOptions
     */
    protected $options;


    /**
     * @return string
     */
    public function getTableName()
    {
        if ( null === $this -> tableName ) {
            $this -> tableName = $this -> options -> getTableName();
        }
        return $this -> tableName;
    } // getTableName()


    /**
     * @return \Zend\Db\Adapter\Driver\ConnectionInterface
     */
    public function getConnection()
    {
        if ( null === $this -> connection ) {
            $this -> connection = $this -> getDbAdapter() -> getDriver() -> getConnection();
        }
        return $this -> connection;
    } // getConnection()


    /**
     * @param Options\AbstractOptions $options
     * @return AbstractDbMapper
     */
    public function setOptions(Options\AbstractOptions $options)
    {
        $this -> options = $options;
        return $this;
    } // setOptions()


    /**
     * @param \Zend\Db\Sql\Select $select
     * @param null $entityPrototype
     * @param null|\Zend\Stdlib\Hydrator\HydratorInterface $hydrator
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function selectWith(Select $select, $entityPrototype = null, HydratorInterface $hydrator = null)
    {
//        $adapter = $this->getDbAdapter();
//        $statement = $adapter->createStatement();
//        $select->prepareStatement($adapter, $statement);
//        $result = $statement->execute();

        $result = $this -> selectExecute($select);
        $resultSet = $this->getResultSet();

        if (isset($entityPrototype)) {
            $resultSet->setObjectPrototype($entityPrototype);
        }

        if (isset($hydrator)) {
            $resultSet->setHydrator($hydrator);
        }

        $resultSet->initialize($result);

        return $resultSet;
    }

    /**
     * @param $entity
     * @param null $tableName
     * @param null|\Zend\Stdlib\Hydrator\HydratorInterface $hydrator
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function insert($entity, $tableName = null, HydratorInterface $hydrator = null)
    {
        $tableName = $tableName ?: $this -> getTableName();

        $rowData = $this->entityToArray($entity, $hydrator);

        $sql = new Sql($this->getDbAdapter(), $tableName);
        $insert = $sql->insert();
        $insert->values($rowData);

        $statement = $sql->prepareStatementForSqlObject($insert);
        return $statement->execute();
    }

    /**
     * @param $entity
     * @param $where
     * @param null $tableName
     * @param null|\Zend\Stdlib\Hydrator\HydratorInterface $hydrator
     * @return int
     */
    public function update($entity, $where, $tableName = null, HydratorInterface $hydrator = null)
    {
        $tableName = $tableName ?: $this -> getTableName();

        $rowData = array_filter($this->entityToArray($entity, $hydrator),
            function($element) {
                return null !== $element;
            });

        $sql = new Sql($this->getDbAdapter(), $tableName);

        $update = $sql->update();
        $update->set($rowData);
        $update->where($where);
        $statement = $sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();

        return $result->getAffectedRows();
    }

    /**
     * @return object
     */
    public function getEntityPrototype()
    {
        return $this->entityPrototype;
    }

    /**
     * @param $entityPrototype
     * @return AbstractDbMapper
     */
    public function setEntityPrototype($entityPrototype)
    {
        $this->entityPrototype = $entityPrototype;
        $this->resultSetPrototype = null;
        return $this;
    }

    /**
     * @return Adapter
     */
    public function getDbAdapter()
    {
        return $this->dbAdapter;
    }

    /**
     * @param \Zend\Db\Adapter\Adapter $dbAdapter
     * @return AbstractDbMapper
     */
    public function setDbAdapter(Adapter $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
        return $this;
    }

    /**
     * @return HydratorInterface
     */
    public function getHydrator()
    {
        if (!$this->hydrator) {
            $this->hydrator = new ClassMethods(false);
        }
        return $this->hydrator;
    }

    /**
     * @param \Zend\Stdlib\Hydrator\HydratorInterface $hydrator
     * @return AbstractDbMapper
     */
    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->hydrator = $hydrator;
        $this->resultSetPrototype = null;
        return $this;
    }


    /**
     * @param \Zend\Db\Sql\Select $select
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function selectExecute(Select $select)
    {
        $adapter = $this->getDbAdapter();
        $statement = $adapter->createStatement();
        $select->prepareStatement($adapter, $statement);
        return $statement->execute();
    } // sqlExecute()


    /**
     * @return HydratingResultSet
     */
    protected function getResultSet()
    {
        if (!$this->resultSetPrototype) {
            $this->resultSetPrototype = new HydratingResultSet;
            $this->resultSetPrototype->setHydrator($this->getHydrator());
            $this->resultSetPrototype->setObjectPrototype($this->getEntityPrototype());
        }
        return clone $this->resultSetPrototype;
    }

    /**
     * select
     *
     * @return Select
     */
    protected function select()
    {
        if (!$this->selectPrototype) {
            $this->selectPrototype = new Select;
        }
        return clone $this->selectPrototype;
    }

    /**
     * @param $entity
     * @param null|\Zend\Stdlib\Hydrator\HydratorInterface $hydrator
     * @return mixed
     * @throws Exception\InvalidArgumentException
     */
    protected function entityToArray($entity, HydratorInterface $hydrator = null)
    {
        if (is_array($entity)) {
            return $entity; // cut down on duplicate code
        } elseif (is_object($entity)) {
            if (!$hydrator) {
                $hydrator = $this->getHydrator();
            }
            return $hydrator->extract($entity);
        }
        throw new Exception\InvalidArgumentException('Entity passed to db mapper should be an array or object.');
    }

}
