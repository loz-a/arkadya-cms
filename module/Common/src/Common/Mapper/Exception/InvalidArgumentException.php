<?php

namespace Common\Mapper\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{}