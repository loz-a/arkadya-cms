<?php
namespace Common\Mapper\Options;

use Zend\Stdlib\AbstractOptions as BaseOptions;

abstract class AbstractOptions extends BaseOptions
{
    /**
     * @var string
     */
    protected $tableName;

    /**
     * @param string $tableName
     * @return AbstractOptions
     */
    public function setTableName($tableName)
    {
        $this -> tableName = (string) $tableName;
        return $this;
    } // setPagesTableName()

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this -> tableName;
    } // getPagesTableName()

} // AbstractOptions
