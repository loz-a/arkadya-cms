<?php
namespace Common\Mapper;

trait MapperTrait
{
    /**
     * @var AbstractDbMapper
     */
    protected $mapper;

    /**
     * @param AbstractDbMapper $roles
     * @return MapperTrait
     */
    public function setMapper(AbstractDbMapper $roles)
    {
        $this -> mapper = $roles;
        return $this;
    } // setMapper()


    /**
     * @return AbstractDbMapper
     */
    public function getMapper()
    {
        if (null === $this -> mapper) {
            list($moduleName, , $className) = explode('\\', get_class($this));
            $alias = implode('\\', array($moduleName, 'Mapper', $className));
            $this -> setMapper($this -> serviceManager -> get($alias));
        }
        return $this -> mapper;
    } // getMapper()

} // MapperTrait
