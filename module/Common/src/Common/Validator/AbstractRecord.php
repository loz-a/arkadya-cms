<?php
namespace Common\Validator;

use Zend\Validator\AbstractValidator;
use Common\Mapper\AbstractDbMapper;

abstract class AbstractRecord extends AbstractValidator
{
    /**
     * Error constants
     */
    const ERROR_NO_RECORD_FOUND = 'noRecordFound';
    const ERROR_RECORD_FOUND    = 'recordFound';

    /**
     * @var array Message templates
     */
    protected $messageTemplates = array(
        self::ERROR_NO_RECORD_FOUND => "No record matching '%value%' was found",
        self::ERROR_RECORD_FOUND    => "A record matching '%value%' was found",
    );

    /**
     * @var \Common\Mapper\AbstractDbMapper
     */
    protected $mapper;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $exclude;

    public function __construct(array $options)
    {
        if ( array_key_exists('key', $options) ) {
            $this -> setKey($options['key']);
        }
        if ( array_key_exists('mapper', $options) ) {
            $this -> setMapper($options['mapper']);
        }
        if ( array_key_exists('exclude', $options) ) {
            $this -> setExclude($options['exclude']);
        }
        parent::__construct($options);
    } // __construct()

    /**
     * @abstract
     * @param $mapper
     */
    abstract public function setMapper($mapper);

    /**
     * getMapper
     *
     * @return AbstractRecord
     */
    public function getMapper()
    {
        if ( null === $this -> mapper ) {
            throw new Exception\LogicException('Mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()


    /**
     * Get key.
     *
     * @return string
     */
    public function getKey()
    {
        if ( null === $this -> key ) {
            throw new Exception\LogicException('No key provided');
        }
        return $this -> key;
    } // getKey()


    /**
     * @param $key
     * @return AbstractRecord
     */
    public function setKey($key)
    {
        $this -> key = $key;
        return $this;
    } // setKey()


    public function getExclude()
    {
        return $this -> exclude;
    } // getExclude()


    public function setExclude($exclude)
    {
        $this -> exclude = $exclude;
        return $this;
    } // setExclude()

} // AbstractRecord
