<?php
namespace Common\Validator\Exception;

class LogicException extends \LogicException implements ExceptionInterface
{}