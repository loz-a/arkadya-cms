<?php
namespace Common\Validator\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{}