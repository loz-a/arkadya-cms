<?php
namespace Common\Validator;

use Zend\Validator\Identical;

class NotIdentical extends Identical
{
    /**
     * Error codes
     * @const string
     */
    const SAME = 'same';
    const MISSING_TOKEN = 'missingToken';

    /**
     * Error messages
     * @var array
     */
    protected $messageTemplates = array(
        self::SAME      => "The two given tokens is match",
        self::MISSING_TOKEN => 'No token was provided to match against',
    );


    /**
     * Returns true if and only if a token has been set and the provided value
     * matches that token.
     *
     * @param  mixed $value
     * @param  array $context
     * @return bool
     */
    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        if (($context !== null) && isset($context) && array_key_exists($this->getToken(), $context)) {
            $token = $context[$this->getToken()];
        } else {
            $token = $this->getToken();
        }

        if ($token === null) {
            $this->error(self::MISSING_TOKEN);
            return false;
        }

        $strict = $this->getStrict();
        if (($strict && ($value === $token)) || (!$strict && ($value == $token))) {
            $this->error(self::SAME);
            return false;
        }

        return true;
    }

} // NotIdentical
