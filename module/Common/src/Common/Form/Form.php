<?php
namespace Common\Form;

use Zend\Form\Form as BaseForm;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;

class Form extends BaseForm
    implements ServiceManagerAwareInterface
{
    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * @var \Zend\Stdlib\AbstractOptions
     */
    protected $moduleOptions;


    public function init()
    {} // init()


    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> serviceManager = $serviceManager;
        $this -> init();
        return $this;
    } // setServiceManager


    /**
     * @param $moduleOptionsAlias
     * @return null|\Zend\Stdlib\AbstractOptions
     */
    public function getModuleOptions($moduleOptionsAlias)
    {
        if( !strval($moduleOptionsAlias) ) {
            return null;
        }

        if( null === $this -> moduleOptions ) {
            $this -> moduleOptions = $this -> serviceManager -> get($moduleOptionsAlias);
        }
        return $this -> moduleOptions;
    } // getModuleOptions()

} // Form
