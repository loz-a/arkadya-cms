<?php
namespace Common\Form\InputFilter;

use Zend\InputFilter\InputFilter as BaseInputFilter;

use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;

class InputFilter extends BaseInputFilter
    implements ServiceManagerAwareInterface
{
    /**
     * @var ServiceManager
     */
    protected $serviceManager;


    public function init()
    {} // init()


    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> serviceManager = $serviceManager;
        $this -> init();
        return $this;
    } // setServiceManager

} // InputFilter
