<?php
namespace Common\Image;

class ThumbsPathsResolver
{
    const ABSOLUTE_PATH = 'absolute';
    const RELATIVE_PATH = 'relative';

    /**
     * @var array
     */
    protected $originPaths = array();

    /**
     * @var array
     */
    protected $resolvedPaths = array();


    /**
     * @return bool
     */
    public function isAbsolutePath()
    {
        return isset($this -> originPaths[self::ABSOLUTE_PATH]['path']);
    } // isAbsolutePath()


    /**
     * @param string $path
     * @return ThumbsPathsResolver
     */
    public function setAbsolutePath($path)
    {
        $path = (string) $path;
        $this -> originPaths[self::ABSOLUTE_PATH]['path'] = $path;
        $this -> originPaths[self::ABSOLUTE_PATH]['pathinfo'] = pathinfo($path);
        $this -> resolvedPaths[self::ABSOLUTE_PATH] = array();
        return $this;
    } // setAbsolutePath()


    /**
     * @param string $dirname
     * @return string
     */
    public function getAbsolutePath($dirname = null)
    {
        if ($dirname) {
            if (!$this -> resolvedPaths[self::ABSOLUTE_PATH][$dirname]) {
                $this -> resolvedPaths[self::ABSOLUTE_PATH][$dirname] = $this -> resolvePath($dirname, self::ABSOLUTE_PATH);
            }
            return $this -> resolvedPaths[self::ABSOLUTE_PATH][$dirname];
        }
        return $this -> originPaths[self::ABSOLUTE_PATH]['path'];
    } // getAbsolutePath()


    /**
     * @return bool
     */
    public function isRelativePath()
    {
        return isset($this -> originPaths[self::RELATIVE_PATH]['path']);
    } // isRelativePath()


    /**
     * @param string $path
     * @return ThumbsPathsResolver
     */
    public function setRelativePath($path)
    {
        $path = (string) $path;
        $this -> originPaths[self::RELATIVE_PATH]['path'] = $path;
        $this -> originPaths[self::RELATIVE_PATH]['pathinfo'] = pathinfo($path);
        $this -> resolvedPaths[self::RELATIVE_PATH] = array();
        return $this;
    } // setRelativePath()


    /**
     * @param string $dirname
     * @return string
     */
    public function getRelativePath($dirname = null)
    {
        if ($dirname) {
            if (!$this -> resolvedPaths[self::RELATIVE_PATH][$dirname]) {
                $this -> resolvedPaths[self::RELATIVE_PATH][$dirname] = $this -> resolvePath($dirname, self::RELATIVE_PATH);
            }
            return $this -> resolvedPaths[self::RELATIVE_PATH][$dirname];
        }
        return $this -> originPaths[self::RELATIVE_PATH]['path'];
    } // getRelativePath()


    public function __call($name, $args)
    {
        if (strpos($name, 'getAbsolutePathTo') === 0) {
            $dirname = strtolower(str_replace('getAbsolutePathTo', '', $name));
            return $this -> getAbsolutePath($dirname);
        }
        else if (strpos($name, 'getRelativePathTo') === 0) {
            $dirname = strtolower(str_replace('getRelativePathTo', '', $name));
            return $this -> getRelativePath($dirname);
        }
        return null;
    } // __call()


    /**
     * @param string $dirname
     * @param string $pathType
     * @return string
     * @throws \InvalidArgumentException
     */
    protected function resolvePath($dirname, $pathType)
    {
        $path = '';
        switch ($pathType) {

            case self::ABSOLUTE_PATH:
                $path = sprintf('%s%s%s%s%s',
                    $this -> originPaths[self::ABSOLUTE_PATH]['pathinfo']['dirname'],
                    DIRECTORY_SEPARATOR,
                    $dirname,
                    DIRECTORY_SEPARATOR,
                    $this -> originPaths[self::ABSOLUTE_PATH]['pathinfo']['basename']
                );
                break;

            case self::RELATIVE_PATH:
                $path = sprintf('%s/%s/%s',
                    $this -> originPaths[self::RELATIVE_PATH]['pathinfo']['dirname'],
                    $dirname,
                    $this -> originPaths[self::RELATIVE_PATH]['pathinfo']['basename']
                );
                break;

            default:
                throw new \InvalidArgumentException('Wrong path type');
        }
        return $path;
    } // resolvePath()


    public function clear()
    {
        $this -> originPaths[self::ABSOLUTE_PATH] = array();
        $this -> originPaths[self::RELATIVE_PATH] = array();

        $this -> resolvedPaths[self::RELATIVE_PATH] = array();
        $this -> resolvedPaths[self::ABSOLUTE_PATH] = array();
    } // clear()

} // PathResolver
