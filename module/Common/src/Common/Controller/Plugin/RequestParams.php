<?php
namespace Common\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class RequestParams extends AbstractPlugin
{
    public function __invoke()
    {
        return $this;
    } // __invoke()

    public function toArray()
    {
        $request = $this -> getController() -> getRequest();
        $result = array();

        $method = strtolower($request -> getMethod());
        if ( $method === 'get' ) {
            $result = $request -> getQuery();
        }
        else if ( $method === 'post' ) {
            $result = $request -> getPost();
        }
        $result = $result -> toArray();

        $files = $request -> getFiles();
        if ( $files -> count() ) {
            $files = $files -> toArray();
            foreach ( $files as $file => $context ) {
                if ( isset($result[$file]) ) {
                    throw new \Exception(sprintf('Parameter %s already exists'));
                }
                $result[$file] = $context['name'];
            }
        }
        return $result;
    } // toArray()

} // RequestParams
