<?php
namespace Common\Controller;

use Common\Service\ServiceInterface;

trait CrudTrait
{
    abstract public function getAddFormAlias();

    abstract public function getEditFormAlias();

    abstract public function getDeleteFormAlias();

    abstract public function getManageRouteName();

    /**
     * @return \Common\Service\ServiceInterface
     */
    abstract public function getService();

    /**
     * @param \Common\Service\ServiceInterface $service
     * @return mixed
     */
    abstract public function setService(ServiceInterface $service);


    public function manageAction()
    {
        return array(
            'entities' => $this -> getService() -> getMapper() -> fetchAll()
        );
    } // indexAction()


    public function addAction()
    {
        return array(
            'form' => $this -> getService() -> getForm($this -> getAddFormAlias())
        );
    } // addAction()


    public function addSubmitAction()
    {
        $form = $this -> getService() -> getForm($this -> getAddFormAlias());
        $form -> setData($this -> request -> getPost());

        if ($form -> isValid()) {
            $result = $this -> getService() -> add($form -> getData());
            if ($result) {
                $this -> flashMessenger() -> addMessage('Adding was successfully');
                return $this -> redirect() -> toRoute($this -> getManageRouteName());
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // addSubmitAction()


    public function editAction()
    {
        $id = $this -> params('id');
        $form = $this -> getService() -> getPopulatedFormById($id, $this -> getEditFormAlias());

        if (!$form) {
            return $this -> notFoundAction();
        }
        return array(
            'form' => $form
        );
    } // editAction()


    public function editSubmitAction()
    {
        $form = $this -> getService() -> getForm($this -> getEditFormAlias());
        $form -> setData($this -> request -> getPost());

        if ($form -> isValid()) {
            $result = $this -> getService() -> edit($form -> getData());
            if ($result) {
                $this -> flashMessenger() -> addMessage('Editing was successfully');
                return $this -> redirect() -> toRoute($this -> getManageRouteName());
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // editSubmitAction()


    public function deleteAction()
    {
        $id   = $this -> params('id');
        $form = $this -> getService() -> getPopulatedFormById($id, $this -> getDeleteFormAlias());

        if (!$form) {
            return $this -> notFoundAction();
        }

        return array(
            'form' => $form
        );
    } // deleteAction()


    public function deleteConfirmAction()
    {
        $id = $this -> params('id');

        $form = $this -> getService() -> getForm($this -> getDeleteFormAlias());
        $form -> setData($this -> request -> getPost());

        $confirm = $form -> get('confirm') -> getValue();

        if (strtolower($confirm) === 'no') {
            return $this -> redirect() -> toRoute($this -> getManageRouteName());
        }

        if ($form -> isValid()) {
            $this -> getService() -> getMapper() -> deleteById($id);
            $this -> flashMessenger() -> addMessage('deleting was successfully');
            return $this -> redirect() -> toRoute($this -> getManageRouteName());
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // deleteConfirmAction()

} // CrudTrait
