<?php
namespace Common;

use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\EventManager\EventInterface;
use Zend\Session\Container as SessionContainer;
use Zend\Mvc\MvcEvent;


class Module implements
    BootstrapListenerInterface,
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ControllerPluginProviderInterface,
    ViewHelperProviderInterface
{
    public function onBootstrap(EventInterface $e)
    {
        $e  -> getApplication()
            -> getEventManager()
            -> attach(MvcEvent::EVENT_FINISH, function($e) {
                $sessContainer = new SessionContainer();
                $sessManager = $sessContainer -> getDefaultManager();

                if ( $sessManager -> sessionExists() ) {
                    $sessManager -> writeClose();
                }
            });
    } // onBootstrap()


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    } // getAutoloaderConfig()


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getViewHelperConfig()
    {
        return array(
            'invokables' => array(
                'tableWidget' => 'Common\View\Helper\TableWidget',
            ),
            'factories' => array(
                'flashMessengerWidget' => function($sm) {
                    $plugin = $sm
                        -> getServiceLocator()
                        -> get('ControllerPluginManager')
                        -> get('flashMessenger');
                    return new View\Helper\FlashMessengerWidget($plugin);
                },
            )
        );
    } // getViewHelperConfig()


    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getControllerPluginConfig()
    {
        return array(
            'factories' => array(
                'RequestParams' => 'Common\Controller\Plugin\RequestParams'
            )
        );
    } // getControllerPluginConfig()

} // Module