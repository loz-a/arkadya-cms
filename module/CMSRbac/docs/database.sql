CREATE TABLE `rbac_roles` (
	`id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
	`parent_id` SMALLINT(5) UNSIGNED NULL DEFAULT NULL,
	`role` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `role` (`role`),
	INDEX `FK_roles_roles` (`parent_id`),
	CONSTRAINT `FK_roles_roles` FOREIGN KEY (`parent_id`) REFERENCES `rbac_roles` (`id`) ON DELETE SET NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT

CREATE TABLE `rbac_resources` (
	`id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
	`resource` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT

CREATE TABLE `rbac_permissions` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`resource_id` SMALLINT(5) UNSIGNED NOT NULL,
	`role_id` SMALLINT(5) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_rbac_permissions_rbac_resources` (`resource_id`),
	INDEX `FK_rbac_permissions_rbac_roles` (`role_id`),
	CONSTRAINT `FK_rbac_permissions_rbac_resources` FOREIGN KEY (`resource_id`) REFERENCES `rbac_resources` (`id`),
	CONSTRAINT `FK_rbac_permissions_rbac_roles` FOREIGN KEY (`role_id`) REFERENCES `rbac_roles` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
ROW_FORMAT=DEFAULT