<?php
namespace CMSRbac\Validator\Resource;

use Common\Validator\AbstractRecord;
use Common\Validator\Exception\InvalidArgumentException;
use CMSRbac\Mapper\PermissionsInterface;

class NoRecordExists extends AbstractRecord
{
    public function setMapper($mapper)
    {
        if ( !$mapper instanceof PermissionsInterface ) {
            throw new InvalidArgumentException('Wrong mapper type. CMSRbac\Mapper\RolesInterface expected');
        }

        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    public function isValid($value)
    {
        $this -> setValue($value);

        $result = $this -> getMapper() -> lookupByResource($value, $this -> getExclude());
        if ( $result ) {
            $this -> error(self::ERROR_RECORD_FOUND);
            return false;
        }
        return true;
    } // isValid()

} // NoRecordExists