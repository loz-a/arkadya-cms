<?php
namespace CMSRbac\Validator;

use Zend\Validator\AbstractValidator;
use CMSRbac\Mapper\Roles as RolesMapper;
use LogicException;

class NotInHierarchy extends AbstractValidator
{
    const IN_HIERARCHY = 'inHierarchy';
    const REFERENCE_ITSELF = 'referenceHimself';

    protected $messageTemplates = array(
        self::IN_HIERARCHY => "Role already exists in parents hierarchy",
        self::REFERENCE_ITSELF => "Role can not refer to itself",
    );

    /**
     * @var RolesMapper
     */
    protected $mapper;


    public function __construct(array $options)
    {
        if (array_key_exists('mapper', $options)) {
            $this -> setMapper($options['mapper']);
        }
        parent::__construct($options);
    } // __construct()


    /**
     * @param \CMSRbac\Mapper\Roles $mapper
     * @return NotInHierarchy
     */
    public function setMapper(RolesMapper $mapper)
    {
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @return RolesMapper
     * @throws \LogicException
     */
    public function getMapper()
    {
        if ( null === $this -> mapper ) {
            throw new LogicException('Mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()


    public function isValid($value, $context = null)
    {
        $this -> setValue($value);

        if (null === $value or !isset($context['id'])) {
            return true;
        }

        if ($context['id'] === $value) {
            $this -> error(self::REFERENCE_ITSELF);
            return false;
        }

        $hierarchy = $this -> getMapper() -> fetchHierarchy();
        $parent = (int) $hierarchy[$value];

        do {
            if ($parent == $context['id']) {
                $this -> error(self::IN_HIERARCHY);
                return false;
            }
        } while (null !== ($parent = $hierarchy[$parent]));

        return true;
    } // isValid()


} // NoInHierarhy
