<?php
namespace CMSRbac\Options;

use Common\Mapper\Options\AbstractOptions;

class ModuleOptions extends AbstractOptions implements
    RolesInterface,
    PermissionsInterface,
    ResourcesInterface
{
    protected $rolesTableName = 'rbac_roles';

    public function setRolesTableName($tableName)
    {
        $this -> rolesTableName = (string) $tableName;
    }

    public function getRolesTableName()
    {
        return $this -> rolesTableName;
    }


    protected $roleMaxLength = 50;

    public function setRoleMaxLength($maxlen)
    {
        $this -> roleMaxLength = (int) $maxlen;
    }

    public function getRoleMaxLength()
    {
        return $this -> roleMaxLength;
    }


    protected $permissionsTableName = 'rbac_permissions';

    public function setPermissionsTableName($tableName)
    {
        $this -> permissionsTableName = (string) $tableName;
    }

    public function getPermissionsTableName()
    {
        return $this -> permissionsTableName;
    }


    protected $resourcesTableName = 'rbac_resources';

    public function setResourcesTableName($tableName)
    {
        $this -> resourcesTableName = (string) $tableName;
    } // setResourcesTableName()

    public function getResourcesTableName()
    {
        return $this -> resourcesTableName;
    } // getResourcesTableName()

} // ModuleOptions
