<?php
namespace CMSRbac\Options;

interface ResourcesInterface
{
    public function setResourcesTableName($tableName);

    public function getResourcesTableName();
}
