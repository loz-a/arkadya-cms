<?php
namespace CMSRbac\Options;

interface PermissionsInterface
{
    public function setPermissionsTableName($tableName);

    public function getPermissionsTableName();
}
