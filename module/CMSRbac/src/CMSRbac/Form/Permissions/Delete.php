<?php
namespace CMSRbac\Form\Permissions;

use Zend\Form\Form;

class Delete extends Form
{
    public function getActionRouteName()
    {
        return 'manage_permissions/delete/confirm';
    } // getActionRouteName()


    public function init()
    {
        $this
            -> setName('Delete permission')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add([
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden',
                'options' => [
                    'type' => 'hidden'
                ]
            ])
            -> add([
                'name' => 'role_id',
                'type' => 'Zend\Form\Element\Hidden',
                'options' => [
                    'type' => 'hidden'
                ]
            ])
            -> add([
                'name' => 'resource',
                'type' => 'Zend\Form\Element\Hidden',
                'options' => [
                    'type' => 'hidden'
                ]
            ])
            -> add([
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf',
                'options' => [
                    'csrf_options' => [
                        'timeout' => 600
                    ]
                ]
            ])
            -> add([
                'name' => 'confirm',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => [
                    'name'  => 'confirm',
                    'class' => 'btn btn-danger',
                    'type'  => 'submit',
                    'value' => 'yes'
                ]
            ]);
    } // init()

} // Delete
