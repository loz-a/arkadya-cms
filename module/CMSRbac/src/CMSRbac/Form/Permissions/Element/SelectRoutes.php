<?php
namespace CMSRbac\Form\Permissions\Element;

use Zend\Form\Element\Select;

class SelectRoutes extends Select
{
    protected $routes = [];
    protected $ignoreEndingsRoutes = [];

    public function setRouterConfig(array $routerConfig)
    {
        $this -> parseRoutesConfig($routerConfig['routes']);
        $this -> setValueOptions($this -> routes);
        return $this;
    } // init()


    public function setIgnoreEndingsRoutes(array $ignoreEndingsRoutes)
    {
        $this -> ignoreEndingsRoutes = $ignoreEndingsRoutes;
        return $this;
    } // setIgnoreEndingsRoutes()


    protected function parseRoutesConfig(array $routerConfig)
    {
        foreach ($routerConfig as $name => $route) {
            $this -> parseRoute($route, $name);
        }
    } // parseRouterConfig()


    protected function parseRoute($route, $parent)
    {
        if (array_key_exists('child_routes', $route)) {
            foreach ($route['child_routes'] as $name => $r) {

                if (in_array($name, $this -> ignoreEndingsRoutes)) {
                    $this -> routes[$parent] = $parent;
                    break;
                }
                else {
                    $this -> parseRoute($r, sprintf('%s/%s', $parent, $name));
                }
            }
        }
        else {
            if (!array_key_exists($parent, $this -> routes)) {
                $this -> routes[$parent] = $parent;
            }
        }
    } // parseRoute()

} // SelectRoutes
