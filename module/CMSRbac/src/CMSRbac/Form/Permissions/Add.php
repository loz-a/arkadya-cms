<?php
namespace CMSRbac\Form\Permissions;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Add extends Form
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function getActionRouteName()
    {
        return 'manage_permissions/add/submit';
    } // getActionRouteName()


    public function init()
    {
        $sl = $this -> getServiceLocator() -> getServiceLocator();
        $modOptions  = $sl -> get('CMSRbac\Options');
        $rolesMapper = $sl -> get('CMSRbac\Mapper\Roles');
        $roleId      = $sl -> get('Application') -> getMvcEvent() -> getRouteMatch() -> getParam('role_id');


        $this
            -> setName('Add Permission')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add([
                'name' => 'role_id',
                'type' => 'Zend\Form\Element\Hidden',
                'attributes' => [
                    'value' => $roleId
                ]
            ])
            -> add([
                'name' => 'resource',
                'type' => 'CMSRbac\Form\Permissions\Element\SelectRoutes',
                'options' => [
                    'label' => 'Resource name:'
                ],
                'attributes' => [
                    'required' => true
                ]
            ])
            -> add([
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf',
                'options' => [
                    'csrf_options' => [
                        'timeout' => 6000
                    ]
                ]
            ])
            -> add([
                'name' => 'submit',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => [
                    'class' => 'btn btn-primary',
                    'type'  => 'submit',
                    'value' => 'Ok'
                ]
            ]);

    } // init()

} // AddPermission
