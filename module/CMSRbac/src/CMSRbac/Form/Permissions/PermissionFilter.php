<?php
namespace CMSRbac\Form\Permissions;

use Common\Form\InputFilter\InputFilter;

class PermissionFilter extends InputFilter
{
    public function init()
    {
        $application = $this -> serviceManager -> get('Application');
        $roleId      = $application -> getMvcEvent() -> getRouteMatch() -> getParam('role_id');

        $this
            -> add([
                'name' => 'role_id',
                'required' => true,
            ])
            -> add([
                'name' => 'resource',
                'required' => true,
                'validators' => [
                    ['name' => 'CMSRbac\Validator\Resource\NoRecordExists',
                        'options' => [
                            'mapper' => $this -> serviceManager -> get('CMSRbac\Mapper\Permissions'),
                            'key'    => 'resource',
                            'exclude' => sprintf('role_id = %s', $roleId)
                        ]
                    ]
                ]
            ])
        ;

    } // init()

} // PermissionFilter
