<?php
namespace CMSRbac\Form\Role;

use Common\Form\InputFilter\InputFilter;

class RoleFilter extends InputFilter
{
    public function init()
    {
        $id      = $this -> serviceManager -> get('Request') -> getPost('id');
        $options = $this -> serviceManager -> get('CMSRbac\Options');
        $mapper  = $this -> serviceManager -> get('CMSRbac\Mapper\Roles');

        $this
            -> add([
                'name'     => 'id',
                'required' => true,
                'allow_empty' => true,
                'filters'  => [
                    ['name' => 'Int']
                ]
            ])
            -> add([
                'name'     => 'role',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim']
                ],
                'validators' => [
                    ['name' => 'StringLength',
                     'options' => [
                        'encoding' => 'UTF-8',
                        'max' => $options -> getRoleMaxLength()]
                    ],
                    ['name' => 'CMSRbac\Validator\Role\NoRecordExists',
                    'options' => [
                        'mapper'  => $this -> serviceManager -> get('CMSRbac\Mapper\Roles'),
                        'key'     => 'role',
                        'exclude' => $id ? sprintf('id != %s', $id): null]
                    ]
                ]
            ])
            -> add([
                'name' => 'parent_id',
                'required' => false,
                'validators' => [
                    ['name' => 'Common\Validator\NotIdentical',
                        'options' => [
                            'token' => 'role_id'
                        ]
                    ],
                    ['name' => 'CMSRbac\Validator\NotInHierarchy',
                        'options' => [
                            'mapper' => $mapper
                        ]
                    ],
                ]
            ]);

    } // init()

} // RoleFilter
