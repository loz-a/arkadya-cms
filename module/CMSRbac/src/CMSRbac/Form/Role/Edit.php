<?php
namespace CMSRbac\Form\Role;

class Edit extends Add
{
    public function getActionRouteName()
    {
        return 'manage_roles/edit/submit';
    } // getActionRouteName()


    public function init()
    {
        parent::init();

        $this
            -> setName('Edit role')
            -> add([
                'name' => 'id',
                'type' => 'Zend\Form\Element\Hidden',
                'attributes' => [
                    'type' => 'hidden'
                ]
            ]);
    } // init()


} // Edit
