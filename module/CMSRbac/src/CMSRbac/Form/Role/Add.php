<?php
namespace CMSRbac\Form\Role;

use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Add extends Form
    implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function getActionRouteName()
    {
        return 'manage_roles/add/submit';
    } // getActionRouteName()


    public function init()
    {
        $sl = $this -> getServiceLocator();
        $modOptions  = $sl -> getServiceLocator() -> get('CMSRbac\Options');
        $rolesMapper = $sl -> getServiceLocator() -> get('CMSRbac\Mapper\Roles');

        $this
            -> setName('Add role')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add([
                'name' => 'role',
                'options' => [
                    'label' => 'Role'
                ],
                'attributes' => [
                    'required'  => true,
                    'maxlength' => $modOptions -> getRoleMaxLength()
                ]
            ])
            -> add([
                'name' => 'parent_id',
                'type' => 'Zend\Form\Element\Select',
                'options' => [
                    'label' => 'Parent role:',
                    'value_options' => $rolesMapper -> getRolesForSelect()
                ],
                'attributes' => [
                    'required' => false
                ]
            ])
            -> add([
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf',
                'options' => [
                    'csrf_options' => [
                        'timeout' => 6000
                    ]
                ]
            ])
            -> add([
                'name' => 'submit',
                'type' => 'Zend\Form\Element\Submit',
                'attributes' => [
                    'class' => 'btn btn-primary',
                    'type'  => 'submit',
                    'value' => 'Ok'
                ]
            ]);

    } // init()

} // AddRoles
