<?php
namespace CMSRbac\Mapper;

use Zend\Db\Sql\Sql;
use Common\Mapper\AbstractDbMapper;

class Resources extends AbstractDbMapper
{
    public function fetchAll()
    {
        $select = $this
                -> select()
                -> columns(['id', 'resource'])
                -> from($this -> options -> getResourcesTableName());

        $result = $this -> selectExecute($select);
        if ($result -> count()) {
            return iterator_to_array($result);
        }
        return null;
    } // fetchAll()


    public function findById($id)
    {
        $select = $this
            -> select()
            -> columns(['id', 'resource'])
            -> from($this -> options -> getResourcesTableName())
            -> where(['id' => $id])
            -> limit(1);

        $result = $this -> selectExecute($select);
        if ($result -> count()) {
            return $result -> current();
        }
        return null;
    } // findById()


    public function findByResource($resource)
    {
        $select = $this
            -> select()
            -> columns(['id', 'resource'])
            -> from($this -> options -> getResourcesTableName())
            -> where(['resource' => $resource])
            -> limit(1);

        $result = $this -> selectExecute($select);
        if ($result -> count()) {
            return $result -> current();
        }
        return null;
    } // findById()

} // Resources
