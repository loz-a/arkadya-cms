<?php
namespace CMSRbac\Mapper;

use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\Db\Sql\Sql;
use Common\Mapper\AbstractDbMapper;
use \Exception;

class Permissions extends AbstractDbMapper
    implements PermissionsInterface
{
    protected $resourceMapper;

    public function setResourceMapper(Resources $mapper)
    {
        $this -> resourceMapper = $mapper;
        return $this;
    } // setResourceMapper


    public function getResourceMapper()
    {
        return $this -> resourceMapper;
    } // getResourceMapper()


    public function fetchAll()
    {
        $select = $this
            -> select()
            -> columns(['id', 'role', 'parent_id'])
            -> from(['r' => $this -> options -> getRolesTableName()])
            -> join(
                ['p' => $this -> options -> getPermissionsTableName()],
                'r.id = p.role_id',
                [],
                'left'
            )
            -> join(
                ['res' => $this -> options -> getResourcesTableName()],
                'p.resource_id = res.id',
                ['resource'],
                'left'
            );

        $result = $this -> selectExecute($select);
        if ($result -> count()) {
            return iterator_to_array($result);
        }
        return null;
    } // fetchAll()


    public function findById($id)
    {
        $select = $this
            -> select()
            -> columns(['id', 'role_id'])
            -> from(['p' => $this -> options -> getPermissionsTableName()])
            -> join(
                ['r' => $this -> options -> getResourcesTableName()],
                'p.resource_id = r.id',
                ['resource']
            )
            -> where(['p.id' => $id])
            -> limit(1);

        return $this -> selectWith($select) -> current();
    } // findById()


    public function fetchByRoleId($roleId)
    {
        $select = $this
            -> select()
            -> columns(['id', 'role_id'])
            -> from(['p' => $this -> options -> getPermissionsTableName()])
            -> join(
                ['r' => $this -> options -> getResourcesTableName()],
                'p.resource_id = r.id',
                ['resource']
            )
            -> where(['role_id' => $roleId]);

        $result = $this -> selectExecute($select);
        if ($result -> count()) {
            return iterator_to_array($result);
        }
        return null;
    } // fetchByRoleId()


    public function deleteById($id)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> options -> getPermissionsTableName());
        $del = $sql -> delete() -> where(['id' => $id]);
        $statement = $sql -> prepareStatementForSqlObject($del);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // deleteById()


    public function lookupByResource($resource, $where)
    {
        $select = $this
            -> select()
            -> columns(['id'])
            -> from(['p' => $this -> options -> getPermissionsTableName()])
            -> join(
                ['r' => $this -> options -> getResourcesTableName()],
                'p.resource_id = r.id',
                ['resource']
            )
            -> where(['resource' => $resource])
            -> limit(1);

        if ($where) {
            $select -> where($where);
        }
        return $this -> selectExecute($select) -> count();
    } // lookupByRoute()


    public function insert($entity, $tableName = null, HydratorInterface $hydrator = null)
    {
        $rowData = $this -> entityToArray($entity, $hydrator);
        $permissionId = null;
        $this -> getConnection() -> beginTransaction();
        try {
            $resouerce = $this -> getResourceMapper() -> findByResource($rowData['resource']);

            $resourceId = null;
            if ($resouerce) {
                $resourceId = $resouerce['id'];
            }
            else {
                $result = parent::insert(
                    ['resource' => $rowData['resource']],
                    $this -> options -> getResourcesTableName()
                );
                $resourceId = $result -> getGeneratedValue();
            }

            $result = parent::insert(
                ['resource_id' => $resourceId, 'role_id' => $rowData['role_id']],
                $this -> options -> getPermissionsTableName()
            );
            $permissionId = $result -> getGeneratedValue();
            $this -> getConnection() -> commit();
        } 
        catch(Exception $e) {
            $this -> getConnection() -> rollback();
            throw new Exception('Permissin can\'t addded');
        }
        return $permissionId;
    } // insert()

} // Permissions
