<?php
namespace CMSRbac\Mapper;

interface RolesInterface
{
    public function fetchAll();

    public function findById($id);

    public function lookupByRole($role, $where);

    public function deleteById($id);
}
