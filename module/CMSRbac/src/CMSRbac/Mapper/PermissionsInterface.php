<?php
namespace CMSRbac\Mapper;

interface PermissionsInterface
{
    public function fetchAll();

    public function findById($id);

    public function fetchByRoleId($roleId);

    public function deleteById($roleId);

    public function lookupByResource($resource, $where);

    public function setResourceMapper(Resources $mapper);

    public function getResourceMapper();
}
