<?php
namespace CMSRbac\Mapper;

use Zend\Db\Sql\Sql;
use Common\Mapper\AbstractDbMapper;

class Roles extends AbstractDbMapper
    implements RolesInterface
{
    public function fetchAll()
    {
        $tableName = $this -> options -> getRolesTableName();

        $select = $this
            -> select()
            -> columns(['id', 'role'])
            -> from(['r' => $tableName])
            -> join(['p' => $tableName], 'r.parent_id = p.id', ['parent_role' => 'role'], 'left');

        $result = $this -> selectExecute($select);
        if ($result -> count()) {
            return iterator_to_array($result);
        }
        return null;
    } // fetchAll()


    public function fetchHierarchy()
    {
        $select = $this
            -> select()
            -> columns(['id', 'parent_id'])
            -> from($this -> options -> getRolesTableName());

        $r = $this -> selectExecute($select);

        if ($r -> count()) {
            $result = [];
            $r = iterator_to_array($r);
            foreach ($r  as $item) {
                $result[$item['id']] = $item['parent_id'];
            }
            return $result;
        }
        return null;
    } // fetchHierarchy()


    public function getRolesForSelect()
    {
        $result[0] = '';
        $roles = $this -> fetchAll();
        foreach ($roles as $role) {
            $result[$role['id']] = $role['role'];
        }
        return $result;
    } // getRolesForSelect()


    public function findById($id)
    {
        $select = $this
            -> select()
            -> columns(['id', 'role'])
            -> from($this -> options -> getRolesTableName())
            -> where(['id' => $id])
            -> limit(1);

        return $this -> selectWith($select) -> current();
    } // findById()


    public function lookupByRole($role, $where)
    {
        $select = $this
            -> select()
            -> columns(['id'])
            -> from($this -> options -> getRolesTableName())
            -> where(['role' => $role])
            -> limit(1);

        if ($where) {
            $select -> where($where);
        }
        return $this -> selectExecute($select) -> count();
    } // lookupByRole()


    public function update($entity, $where, $tableName = null, HydratorInterface $hydrator = null)
    {
        $tableName = $tableName ?: $this -> getTableName();

        $rowData = $this->entityToArray($entity, $hydrator);

        $sql = new Sql($this->getDbAdapter(), $tableName);

        $update = $sql->update();
        $update->set($rowData);
        $update->where($where);
        $statement = $sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();

        return $result->getAffectedRows();
    } // update()


    public function deleteById($id)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> options -> getRolesTableName());
        $del = $sql -> delete() -> where(['id' => $id]);
        $statement = $sql -> prepareStatementForSqlObject($del);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // deleteById()

} // Roles
