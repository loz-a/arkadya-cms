<?php
namespace CMSRbac\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Common\Service\ServiceInterface;
use Common\Service\ServiceAwareInterface;
use Common\Controller\CrudTrait;
use CMSRbac\Entity\RoleInterface as Role;

class RolesController extends AbstractActionController
    implements ServiceAwareInterface
{
    use CrudTrait;

    /**
     * @var \CMSUser\Service\Roles
     */
    protected $rolesService;


    public function getAddFormAlias()
    {
        return 'CMSRbac\AddRole';
    } // getAddFormAlias()


    public function getEditFormAlias()
    {
        return 'CMSRbac\EditRole';
    } // getEditFormAlias()


    public function getDeleteFormAlias()
    {
        return 'CMSRbac\DeleteRole';
    } // getDeleteFormAlias()


    public function getManageRouteName()
    {
        return 'manage_roles';
    } // getManageRouteName()


    public function editSubmitAction()
    {
        $form = $this -> getService() -> getForm($this -> getEditFormAlias());
        $form -> setData($this -> request -> getPost());
        
        $role = $form -> get('role') -> getValue();
        if ($role === Role::ADMIN_ROLE) {
            $this -> flashMessenger() -> addMessage('You can\'t edit administrator role');
            return $this -> redirect() -> toRoute($this -> getManageRouteName());
        }
        
        if ($form -> isValid()) {
            $result = $this -> getService() -> edit($form -> getData());
            if ($result) {
                $this -> flashMessenger() -> addMessage('Editing was successfully');
                return $this -> redirect() -> toRoute($this -> getManageRouteName());
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // editSubmitAction()
    

    public function deleteConfirmAction()
    {
        $id = $this -> params('id');

        $form = $this -> getService() -> getForm($this -> getDeleteFormAlias());
        $form -> setData($this -> request -> getPost());

        $role = $form -> get('role') -> getValue();
        if ($role === Role::ADMIN_ROLE) {
            $this -> flashMessenger() -> addMessage('You can\'t remove administrator role');
            return $this -> redirect() -> toRoute($this -> getManageRouteName());
        }
        
        if ($form -> isValid()) {
            $confirm = $form -> get('confirm') -> getValue();

            if (strtolower($confirm) === 'yes') {
                $this -> getService() -> getMapper() -> deleteById($id);
                $this -> flashMessenger() -> addMessage('deleting was successfully');
            }
            return $this -> redirect() -> toRoute($this -> getManageRouteName());
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // deleteConfirmAction()


    public function getService()
    {
        if (null === $this -> rolesService) {
            $service = $this -> serviceLocator -> get('CMSRbac\Service\Roles');
            $this -> setService($service);
        }
        return $this -> rolesService;
    } // getService()


    public function setService(ServiceInterface $service)
    {
        $this -> rolesService = $service;
    } // setService()

} // RolesManagerController
