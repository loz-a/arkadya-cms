<?php
namespace CMSRbac\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Common\Service\ServiceInterface;
use Common\Service\ServiceAwareInterface;

class PermissionsController extends AbstractActionController
    implements ServiceAwareInterface
{

    protected $permissionsService;


    public function indexAction()
    {
        return [
            'roles' => $this -> getServiceLocator() -> get('CMSRbac\Mapper\Roles') -> fetchAll()
        ];
    } // indexAction()


    public function manageAction()
    {
        $roleId = $this -> params('role_id');
        $role = $this -> getServiceLocator() -> get('CMSRbac\Mapper\Roles') -> findById($roleId);

        if (!$role) {
            $this -> notFoundAction();
        }

        return [
            'role' => $role,
            'routes' => $this -> getService() -> getMapper() -> fetchByRoleId($roleId) ?: array()
        ];
    } // indexAction()


    public function addAction()
    {
        return array(
            'form' => $this -> getService() -> getForm($this -> getAddFormAlias())
        );
    } // addAction()


    public function addSubmitAction()
    {
        $form = $this -> getService() -> getForm($this -> getAddFormAlias());
        $form -> setData($this -> request -> getPost());

        if ($form -> isValid()) {
            $result = $this -> getService() -> add($form -> getData());
            if ($result) {
                $this -> flashMessenger() -> addMessage('Adding was successfully');
                $roleId = $this -> params('role_id');
                return $this -> redirect() -> toRoute('manage_permissions/manage', ['role_id' => $roleId]);
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // addSubmitAction()


    public function deleteAction()
    {
        $id   = $this -> params('id');
        $form = $this -> getService() -> getPopulatedFormById($id, $this -> getDeleteFormAlias());

        if (!$form) {
            return $this -> notFoundAction();
        }

        return array(
            'form' => $form
        );
    } // deleteAction()


    public function deleteConfirmAction()
    {
        $id = $this -> params('id');
        $form = $this -> getService() -> getForm($this -> getDeleteFormAlias());
        $form -> setData($this -> request -> getPost());

        if ($form -> isValid()) {
            $confirm = $form -> get('confirm') -> getValue();

            if (strtolower($confirm) === 'yes') {
                $this -> getService() -> getMapper() -> deleteById($id);
                $this -> flashMessenger() -> addMessage('deleting was successfully');
            }
            $roleId = $this -> params('role_id');
            return $this -> redirect() -> toRoute('manage_permissions/manage', ['role_id' => $roleId]);
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        return array(
            'form' => $form
        );
    } // deleteConfirmAction()


    public function getAddFormAlias()
    {
        return 'CMSRbac\AddPermission';
    }


    public function getDeleteFormAlias()
    {
        return 'CMSRbac\DeletePermission';
    }


    public function getService()
    {
        if (null === $this -> permissionsService) {
            $service = $this -> serviceLocator -> get('CMSRbac\Service\Permissions');
            $this -> setService($service);
        }
        return $this -> permissionsService;
    } // getService()


    public function setService(ServiceInterface $service)
    {
        $this -> permissionsService = $service;
    } // setService()
}
