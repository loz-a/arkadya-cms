<?php
namespace CMSRbac\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Response as HttpResponse;

class ErrorController extends AbstractActionController
{
    public function accessDeniedAction()
    {
        $this
            -> getResponse()
            -> setStatusCode(HttpResponse::STATUS_CODE_403)
            -> sendHeaders();

        return [];
    } // accessDeniedAction()

} // ErrorController
