<?php
namespace CMSRbac\Entity;

class Role implements RoleInterface
{
    protected $id;
    protected $role;
    protected $parentId;

    public function setId($id)
    {
        $this -> id = (int) $id;
        return $this;
    }

    public function getId()
    {
        return $this -> id;
    }


    public function setRole($role)
    {
        $this -> role = (string) $role;
        return $this;
    }

    public function getRole()
    {
        return $this -> role;
    }


    public function setParentId($pid)
    {
        $this -> parentId = (int) $pid;
        return $this;
    }

    public function getParentId()
    {
        return ($this -> parentId ?: null);
    }
}
