<?php
namespace CMSRbac\Entity;

class Permission implements PermissionInterface
{
    protected $id;
    protected $roleId;
    protected $resource;

    public function setId($id)
    {
        $this -> id = (int) $id;
        return $this;
    }

    public function getId()
    {
        return $this -> id;
    }


    public function setRoleId($roleId)
    {
        $this -> roleId = (int) $roleId;
        return $this;
    }

    public function getRoleId()
    {
        return $this -> roleId;
    }


    public function setResource($resource)
    {
        $this -> resource = (string) $resource;
        return $this;
    }

    public function getResource()
    {
        return $this -> resource;
    }

}
