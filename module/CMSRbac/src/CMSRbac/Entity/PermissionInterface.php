<?php
namespace CMSRbac\Entity;

interface PermissionInterface
{
    public function setId($id);

    public function getId();

    public function setRoleId($roleId);

    public function getRoleId();

    public function setResource($resource);

    public function getResource();
}
