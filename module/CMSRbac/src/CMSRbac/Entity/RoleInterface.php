<?php
namespace CMSRbac\Entity;

interface RoleInterface
{
    const ADMIN_ROLE = 'admin';
    const GUEST_ROLE = 'guest';

    public function setId($id);

    public function getId();

    public function setRole($role);

    public function getRole();

    public function setParentId($pid);

    public function getParentId();
}
