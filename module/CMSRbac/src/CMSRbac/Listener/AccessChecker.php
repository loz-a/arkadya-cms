<?php
namespace CMSRbac\Listener;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

use CMSRbac\Entity\RoleInterface as Role;

class AccessChecker implements ListenerAggregateInterface
{
    /**
     * @var array
     */
    protected $listeners = array();

    /**
     * @param \Zend\EventManager\EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this -> listeners[] = $events -> attach(MvcEvent::EVENT_DISPATCH, array($this, 'checkAccess'), $priority);
    } // attach()


    /**
     * @param \Zend\EventManager\EventManagerInterface $events
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ( $this -> listeners as $key => $listener ) {
            $events -> detach($listener);
            unset($this -> listeners[$key]);
            unset($listener);
        }
    } // detach()


    public function checkAccess(MvcEvent $e)
    {
        $sm = $e -> getApplication() -> getServiceManager();
        $identity = $sm -> get('CMSUser\Identity') -> getIdentity();
        $config = $sm -> get('Config')['rbac'] ?: [];

        $role = !$identity ? Role::GUEST_ROLE : $identity -> role;
        if (in_array($role, $config['ignore_roles'])) {
            return true;
        }

        $rbac = $sm -> get('CMSRbac\Service');
        if (!$rbac -> hasRole($role) and $role !== Role::GUEST_ROLE) {
            return $e -> getTarget() -> redirect() -> toRoute('access_denied');
        }

        $routeName = $e -> getRouteMatch() -> getMatchedRouteName();
        $tmp = explode('/', $routeName);
        $ending = end($tmp);
        if (in_array($ending, $config['ignore_endings_routes'])) {
            $routeName = implode('/', array_slice($tmp, 0, -1));
        }
        unset($tmp, $ending);

        if (in_array($routeName, $config['ignore_routes'])) {
            return true;
        }

        $assert = isset($config['assertions'][$role][$routeName])
            ? $sm -> get($config['assertions'][$role][$routeName]) : null;

        if (!$rbac -> isGranted($role, $routeName, $assert)) {
            return $e -> getTarget() -> redirect() -> toRoute('access_denied');
        }
        return true;
    } // checkAccess()

} // AccessChecker
