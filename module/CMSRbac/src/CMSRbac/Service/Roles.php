<?php
namespace CMSRbac\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Common\Service\ServiceInterface;
use Common\Service\OptionsTrait;
use Common\Service\FormTrait;
use Common\Mapper\MapperTrait;

class Roles implements ServiceManagerAwareInterface, ServiceInterface
{
    use OptionsTrait, MapperTrait, FormTrait;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> serviceManager = $serviceManager;
    } // setServiceManager()


    public function add(array $data)
    {
        $entity = clone $this -> getMapper() -> getEntityPrototype();
        
        $entity
            -> setRole($data['role'])
            -> setParentId($data['parent_id']);

        return $this -> getMapper() -> insert($entity, $this -> getOptions() -> getRolesTableName());
    } // add()


    public function edit(array $data)
    {
        $id = $data['id'];
        $role = $this -> getMapper() -> findById($id);

        if (!$role) {
            return null;
        }

        $hydrator = $this -> getMapper() -> getHydrator();
        $role = $hydrator -> hydrate($data, $role);
        $this -> getMapper() -> update($role, array('id' => $id), $this -> getOptions() -> getRolesTableName());
        return $role;
    } // edit()

} // Roles
