<?php
namespace CMSRbac\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Permissions\Rbac\Rbac;

class RbacFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $rbac = new Rbac();
        $permissions = $serviceLocator -> get('CMSRbac\Mapper\Permissions') -> fetchAll();
        $roles = [];

        foreach ($permissions as $p ) {
            if (!$rbac -> hasRole($p['role'])) {
                $rbac -> addRole($p['role']);
                $roles[$p['id']] = [
                    'role' => $p['role'],
                    'parent_id' => $p['parent_id']
                ];
            }
            $rbac -> getRole($p['role']) -> addPermission($p['resource']);
        }

        foreach ($roles as $roleId => $role) {
            if (null !== $role['parent_id']) {
                $rbac
                    -> getRole($rbac -> getRole($role['role']))
                    -> addChild($rbac -> getRole($roles[$role['parent_id']]['role']))   ;
            }
        }
        return $rbac;
    } // createService()

} // RbacFactory
