<?php
namespace CMSRbac\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Common\Service\ServiceInterface;
use Common\Service\OptionsTrait;
use Common\Service\FormTrait;
use Common\Mapper\MapperTrait;

class Permissions implements
    ServiceManagerAwareInterface,
    ServiceInterface
{
    use OptionsTrait, MapperTrait, FormTrait;

    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> serviceManager = $serviceManager;
    } // setServiceManager()


    public function add(array $data)
    {
        $entity = clone $this -> getMapper() -> getEntityPrototype();
        $entity
            -> setRoleId($data['role_id'])
            -> setResource($data['resource']);

        return $this -> getMapper() -> insert($entity, $this -> getOptions() -> getPermissionsTableName());
    } // add()


    public function edit(array $data)
    {
    } // edit()

} // Permissions
