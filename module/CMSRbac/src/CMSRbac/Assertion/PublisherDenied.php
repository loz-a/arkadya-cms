<?php
namespace CMSRbac\Assertion;

use Zend\Permissions\Rbac\AssertionInterface;
use Zend\Permissions\Rbac\Rbac;
use \stdClass;
use \LogicException;
use CMSRbac\Entity\RoleInterface as Role;

class PublisherDenied implements AssertionInterface
{
    const PUBLISHER_ROLE = 'publisher';

    /**
     * @var string
     */
    protected $role;

    public function setUserRole($role)
    {
        $this -> role = (string) $role;
        return $this;
    } // setUserRole()


    public function getUserRole()
    {
        if (null === $this -> role) {
            $this -> role = Role::GUEST_ROLE;
        }
        return $this -> role;
    } // getUserRole()


    public function assert(Rbac $rbac)
    {
        return $this -> getUserRole() !== self::PUBLISHER_ROLE;
    } // assert()

} // AdminDenied
