<?php
namespace CMSRbac;

use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\EventManager\EventInterface;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class Module implements
    DependencyIndicatorInterface,
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ServiceProviderInterface,
    BootstrapListenerInterface,
    FormElementProviderInterface
{
    public function getModuleDependencies()
    {
        return ['CMSUser'];
    } // getModuleDependencies()


    public function onBootstrap(EventInterface $e)
    {
        $app = $e -> getApplication();
        $sm  = $app -> getServiceManager();
        $app -> getEventManager() -> attachAggregate($sm -> get('CMSRbac\Listener\AccessChecker'));
    } // onBootstrap()


    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\ClassMapAutoloader' => [ __DIR__ . '/autoload_classmap.php'],
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [ __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__ ]
            ]
        ];
    } // getAutoloaderConfig()


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getFormElementConfig()
    {
        return [
            'invokables' => [
                'CMSRbac\DeleteRole' => 'CMSRbac\Form\Role\Delete',
                'CMSRbac\DeletePermission' => 'CMSRbac\Form\Permissions\Delete',
            ],
            'factories' => [
                'CMSRbac\Form\Permissions\Element\SelectRoutes' => function($sm) {
                    $config = $sm -> getServiceLocator() -> get('Config');
                    return (new Form\Permissions\Element\SelectRoutes())
                        -> setIgnoreEndingsRoutes($config['rbac']['ignore_endings_routes'])
                        -> setRouterConfig($config['router']);
                },
                'CMSRbac\AddRole' => function($sm) {
                    $sl = $sm -> getServiceLocator();
                    return (new Form\Role\Add()) -> setInputFilter($sl -> get('CMSRbac\RoleFilter'));
                },
                'CMSRbac\EditRole' => function($sm) {
                    $sl = $sm -> getServiceLocator();
                    return (new Form\Role\Edit()) -> setInputFilter($sl -> get('CMSRbac\RoleFilter'));
                },
                'CMSRbac\AddPermission' => function($sm) {
                    $sl = $sm -> getServiceLocator();
                    return (new Form\Permissions\Add()) -> setInputFilter($sl -> get('CMSRbac\PermissionFilter'));
                }
            ],
        ];
    } // getFormElementConfig()


    public function getServiceConfig()
    {
        return [
            'invokables' => [
                'CMSRbac\RoleFilter' => 'CMSRbac\Form\Role\RoleFilter',
                'CMSRbac\PermissionFilter' => 'CMSRbac\Form\Permissions\PermissionFilter',
                'CMSRbac\Service\Roles' => 'CMSRbac\Service\Roles',
                'CMSRbac\Service\Permissions' => 'CMSRbac\Service\Permissions',
                'CMSRbac\Listener\AccessChecker' => 'CMSRbac\Listener\AccessChecker',
            ],
            'factories' => [
                'CMSRbac\Options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['cms_rbac_options']) ? $config['cms_rbac_options'] : [];
                    return new Options\ModuleOptions($options);
                },
                'CMSRbac\Mapper\Roles' => function($sm) {
                    return (new Mapper\Roles())
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setOptions($sm -> get('CMSRbac\Options'))
                        -> setHydrator(new ClassMethodsHydrator())
                        -> setEntityPrototype(new Entity\Role());
                },
                'CMSRbac\Mapper\Permissions' => function($sm) {
                    return (new Mapper\Permissions())
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setOptions($sm -> get('CMSRbac\Options'))
                        -> setHydrator(new ClassMethodsHydrator())
                        -> setEntityPrototype(new Entity\Permission())
                        -> setResourceMapper($sm -> get('CMSRbac\Mapper\Resources'));
                },
                'CMSRbac\Mapper\Resources' => function($sm) {
                    return (new Mapper\Resources())
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setOptions($sm -> get('CMSRbac\Options'));
                },
                'CMSRbac\Service' => 'CMSRbac\Service\RbacFactory',
//                'CMSRbac\Assertion\PublisherDenied' => function($sm) {
//                    $identity = $sm -> get('CMSUser\Identity') -> getIdentity();
//                    $role = $identity ? $identity -> role : Entity\RoleInterface::GUEST_ROLE;
//                    return (new Assertion\PublisherDenied()) -> setUserRole($role);
//                },
            ]
        ];
    } // getServiceConfig()

} // Module
