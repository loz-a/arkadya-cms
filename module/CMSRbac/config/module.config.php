<?php
return [
    'controllers' => [
        'invokables' => [
            'CMSRbac\Controller\RolesManager' => 'CMSRbac\Controller\RolesController',
            'CMSRbac\Controller\PermissionsManager' => 'CMSRbac\Controller\PermissionsController',
            'CMSRbac\Controller\Error' => 'CMSRbac\Controller\ErrorController',
        ]
    ], // controllers


    'view_manager' => [
        'template_map' => [
            'cms-rbac/roles/manage'         => __DIR__ . '/../view/cms-rbac/roles/manage.phtml',
            'cms-rbac/roles/add'            => __DIR__ . '/../view/cms-rbac/roles/add.phtml',
            'cms-rbac/roles/add-submit'     => __DIR__ . '/../view/cms-rbac/roles/add.phtml',
            'cms-rbac/roles/edit'           => __DIR__ . '/../view/cms-rbac/roles/add.phtml',
            'cms-rbac/roles/edit-submit'    => __DIR__ . '/../view/cms-rbac/roles/add.phtml',
            'cms-rbac/roles/delete'         => __DIR__ . '/../view/cms-rbac/roles/delete.phtml',
            'cms-rbac/roles/delete-confirm' => __DIR__ . '/../view/cms-rbac/roles/delete.phtml',

            'cms-rbac/permissions/index'          => __DIR__ . '/../view/cms-rbac/permissions/index.phtml',
            'cms-rbac/permissions/manage'         => __DIR__ . '/../view/cms-rbac/permissions/manage.phtml',
            'cms-rbac/permissions/add'            => __DIR__ . '/../view/cms-rbac/permissions/add.phtml',
            'cms-rbac/permissions/add-submit'     => __DIR__ . '/../view/cms-rbac/permissions/add.phtml',
            'cms-rbac/permissions/edit'           => __DIR__ . '/../view/cms-rbac/permissions/add.phtml',
            'cms-rbac/permissions/edit-submit'    => __DIR__ . '/../view/cms-rbac/permissions/add.phtml',
            'cms-rbac/permissions/delete'         => __DIR__ . '/../view/cms-rbac/permissions/delete.phtml',
            'cms-rbac/permissions/delete-confirm' => __DIR__ . '/../view/cms-rbac/permissions/delete.phtml',

            'cms-rbac/error/access-denied'  => __DIR__ . '/../view/cms-rbac/error/access-denied.phtml',
        ],
        'template_path_stack' => [
            'cms-rbac' => __DIR__ . '/../view'
        ]
    ], // view_manager


    'router' => [
        'routes' => [

            'manage_roles' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/manage-roles',
                    'defaults' => [
                        'controller' => 'CMSRbac\Controller\RolesManager',
                        'action'     => 'manage'
                    ]
                ],
                'may_terminate' => true,
                'child_routes'  => [

                    'add' => [
                        'type'    => 'Literal',
                        'options' => ['route' => '/add'],
                        'may_terminate' => false,
                        'child_routes' => [
                            'send' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'add']
                                ],
                                'may_terminate' => true,
                            ],
                            'submit' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'add-submit']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ],
                    'edit' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/edit/:id',
                            'constraints' => ['id' => '\d+'],
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'send' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'edit']
                                ],
                                'may_terminate' => true
                            ],
                            'submit' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'edit-submit']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ],
                    'delete' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/delete/:id',
                            'constraints' => ['id' => '\d+'],
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'send' => [
                                'type' => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'delete']
                                ],
                                'may_terminate' => true
                            ],
                            'confirm' => [
                                'type' => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'deleteConfirm']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ]
                ]
            ], // manage_roles

            'manage_permissions' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/manage-permissions',
                    'defaults' => [
                        'controller' => 'CMSRbac\Controller\PermissionsManager',
                        'action'     => 'index'
                    ]
                ],
                'may_terminate' => true,
                'child_routes'  => [

                    'manage' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/role/:role_id',
                            'constraints' => ['role_id' => '\d+'],
                            'defaults' => ['action' => 'manage']
                        ],
                        'may_terminate' => true
                    ],
                    'add' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route' => '/add/:role_id',
                            'constraints' => ['role_id' => '\d+'],
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'send' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'add']
                                ],
                                'may_terminate' => true,
                            ],
                            'submit' => [
                                'type'    => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'add-submit']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ],
                    'delete' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/delete/:role_id/:id',
                            'constraints' => [
                                'role_id' => '\d+',
                                'id' => '\d+',
                            ],
                        ],
                        'may_terminate' => false,
                        'child_routes' => [
                            'send' => [
                                'type' => 'Method',
                                'options' => [
                                    'verb'     => 'get',
                                    'defaults' => ['action' => 'delete']
                                ],
                                'may_terminate' => true
                            ],
                            'confirm' => [
                                'type' => 'Method',
                                'options' => [
                                    'verb'     => 'post',
                                    'defaults' => ['action' => 'deleteConfirm']
                                ],
                                'may_terminate' => true
                            ]
                        ]
                    ]
                ]
            ], // manage_permissions

            'access_denied' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/access-denied',
                    'defaults' => [
                        'controller' => 'CMSRbac\Controller\Error',
                        'action'     => 'access-denied'
                    ]
                ]
            ] // access_denied
        ]
    ], // route


    'route_layouts' => [
        'manage_roles'                => 'layout/admin',
        'manage_roles/add/send'       => 'layout/admin',
        'manage_roles/add/submit'     => 'layout/admin',
        'manage_roles/edit/send'      => 'layout/admin',
        'manage_roles/edit/submit'    => 'layout/admin',
        'manage_roles/delete/send'    => 'layout/admin',
        'manage_roles/delete/confirm' => 'layout/admin',

        'manage_permissions'                => 'layout/admin',
        'manage_permissions/manage'         => 'layout/admin',
        'manage_permissions/add/send'       => 'layout/admin',
        'manage_permissions/add/submit'     => 'layout/admin',
        'manage_permissions/edit/send'      => 'layout/admin',
        'manage_permissions/edit/submit'    => 'layout/admin',
        'manage_permissions/delete/send'    => 'layout/admin',
        'manage_permissions/delete/confirm' => 'layout/admin',

        'access_denied' => 'layout/layout'
    ], // route_layouts


    'admin_navigation' => [
        'users' => [
            'items' => [
                [
                    'label' => 'Roles',
                    'route_name' => 'manage_roles',
                ],
                [
                    'label' => 'Permissions',
                    'route_name' => 'manage_permissions',
                ],
            ]
        ]
    ], // admin_navigation


    'rbac' => [
        'ignore_routes' => ['login', 'access_denied'],
        'ignore_endings_routes' => ['set_data', 'send', 'edit_data', 'submit', 'query'],
        'ignore_roles'  => ['admin'],
        'assertions' => [
//            'publisher' => [
//                'home' => 'CMSRbac\Assertion\PublisherDenied'
//            ]
        ]
    ], // rbac

];