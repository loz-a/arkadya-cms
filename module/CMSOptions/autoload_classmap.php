<?php
return array(
    'CMSOptions\Module'                  => __DIR__ . '/Module.php',
    'CMSOptions\Entity\Option'           => __DIR__ . '/src/CMSOptions/Entity/Option.php',
    'CMSOptions\Entity\OptionInterface'  => __DIR__ . '/src/CMSOptions/Entity/OptionInterface.php',
    'CMSOptions\Mapper\Options'          => __DIR__ . '/src/CMSOptions/Mapper/Options.php',
    'CMSOptions\Mapper\OptionsInterface' => __DIR__ . '/src/CMSOptions/Mapper/OptionsInterface.php'
);