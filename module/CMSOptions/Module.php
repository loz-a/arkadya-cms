<?php
namespace CMSOptions;

use CMSOptions\Service\Options as OptionsService;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ServiceProviderInterface
{
    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    } // getAutoloaderConfig()


    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'cms_options_options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['cms_options']) ? $config['cms_options'] : array();
                    return new Options\ModuleOptions($options);
                },
                'CMSOptions\Mapper' => 'CMSOptions\Mapper\Factory',
                'CMSOptions\Service' => function($sm) {
                    $service = new OptionsService();
                    $service -> setMapper($sm -> get('CMSOptions\Mapper'));
                    return $service;
                }
            )
        );
    } // getServiceConfig()


} // Module
