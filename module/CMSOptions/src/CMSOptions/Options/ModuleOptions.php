<?php
namespace CMSOptions\Options;

use Common\Mapper\Options\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements OptionsInterface
{
    protected $tableName = 'options';

} 