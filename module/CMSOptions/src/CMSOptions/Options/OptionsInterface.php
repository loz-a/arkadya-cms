<?php
namespace CMSOptions\Options;


interface OptionsInterface
{
    public function setTableName($tableName);

    public function getTableName();
} 