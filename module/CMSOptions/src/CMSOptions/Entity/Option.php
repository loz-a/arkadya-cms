<?php
namespace CMSOptions\Entity;


class Option implements OptionInterface
{
    protected $id;

    public function setId($id)
    {
        $this -> id = $id;
        return $this;
    }

    public function getId()
    {
        return $this -> id;
    }


    protected $key;

    public function setKey($key)
    {
        $this -> key = $key;
        return $this;
    }

    public function getKey()
    {
        return $this -> key;
    }


    protected $value;

    public function setValue($value)
    {
        $this -> value = $value;
        return $this;
    }

    public function getValue()
    {
        return $this -> value;
    }

} 