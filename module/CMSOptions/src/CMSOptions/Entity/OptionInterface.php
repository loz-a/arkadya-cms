<?php
namespace CMSOptions\Entity;


interface OptionInterface
{
    public function setId($id);

    public function getId();

    public function setKey($key);

    public function getKey();

    public function setValue($value);

    public function getValue();
} 