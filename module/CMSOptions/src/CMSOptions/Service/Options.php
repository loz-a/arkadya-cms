<?php
namespace CMSOptions\Service;

use CMSOptions\Service\Exception\UndefinedMapperException;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use CMSOptions\Mapper\Options as OptionsMapper;

class Options implements ServiceManagerAwareInterface
{
    /**
     * @var ServiceManager
     */
    protected $sm;

    /**
     * @var OptionsMapper
     */
    protected $mapper;

    /**
     * Set service manager
     *
     * @param ServiceManager $serviceManager
     * @return $this
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> sm = $serviceManager;
        return $this;
    } // serviceManager()


    /**
     * @return OptionsMapper
     * @throws Exception\UndefinedMapperException
     */
    public function getMapper()
    {
        if (null === $this -> mapper) {
            throw new UndefinedMapperException('Options mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()


    /**
     * @param OptionsMapper $mapper
     * @return $this
     */
    public function setMapper(OptionsMapper $mapper)
    {
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @param $key
     * @param $value
     * @return int|\Zend\Db\Adapter\Driver\ResultInterface
     */
    public function setOption($key, $value)
    {
        $option = $this -> getOption($key);
        if (null !== $option) {
            return $this
                -> getMapper()
                -> update(array('value' => $value), array('key' => $key));
        }

        return $this
            -> getMapper()
            -> insert(array('key' => $key, 'value' => $value));
    } // setOption()


    /**
     * @param $key
     * @return null|\Zend\Db\ResultSet\HydratingResultSet
     */
    public function getOption($key)
    {
        $key = (string) $key;
        return $this -> getMapper() -> findByKey($key);
    } // getOptions()


    /**
     * @param $key
     * @return int
     */
    public function removeOption($key)
    {
        $key = (string) $key;
        return $this -> getMapper() -> removeByKey($key);
    } // removeOption()

} 