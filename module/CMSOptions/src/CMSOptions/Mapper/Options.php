<?php
namespace CMSOptions\Mapper;

use Common\Mapper\AbstractDbMapper;
use Zend\Db\Sql\Sql;

class Options extends AbstractDbMapper
    implements OptionsInterface
{
    public function fetchAll()
    {
        $select = $this
            -> select()
            -> columns(array('id', 'key', 'value'))
            -> from($this -> getTableName());

        $result = $this -> selectWith($select);
        return $result -> count() ? $result : null;
    } // fetchAll()


    public function findById($id)
    {
        $select = $this
            -> select()
            -> columns(array('id', 'key', 'value'))
            -> from($this -> getTableName())
            -> where(array('id' => $id))
            -> limit(1);

        $result = $this -> selectWith($select);
        return $result -> count() ? $result -> current() : null;
    } // findById()


    public function removeById($id)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> getTableName());
        $del = $sql -> delete() -> where(array('id' => $id));
        $statement = $sql -> prepareStatementForSqlObject($del);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // removeById()


    public function removeByKey($key)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> getTableName());
        $del = $sql -> delete() -> where(array('key' => $key));
        $statement = $sql -> prepareStatementForSqlObject($del);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // removeByKey()


    public function findByKey($key)
    {
        $select = $this
            -> select()
            -> columns(array('id', 'key', 'value'))
            -> from($this -> getTableName())
            -> where(array('key' => $key))
            -> limit(1);

        $result = $this -> selectWith($select);
        return $result -> count() ? $result -> current() : null;
    } // findByKey()

} 