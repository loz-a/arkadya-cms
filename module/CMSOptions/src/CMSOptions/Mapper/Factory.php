<?php
namespace CMSOptions\Mapper;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;
use CMSOptions\Entity\Option;

class Factory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $mapper = new Options();
        $mapper
            -> setDbAdapter($serviceLocator -> get('Zend\Db\Adapter\Adapter'))
            -> setEntityPrototype(new Option())
            -> setHydrator(new ClassMethodsHydrator())
            -> setOptions($serviceLocator -> get('cms_options_options'));

        return $mapper;
    } // createService()

} 