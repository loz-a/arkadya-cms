<?php
namespace CMSOptions\Mapper;

interface OptionsInterface
{
    public function fetchAll();

    public function findById($id);

    public function removeById($id);

    public function findByKey($key);
}