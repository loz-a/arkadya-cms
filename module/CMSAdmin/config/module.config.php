<?php
return array(
    'router' => array(
        'routes' => array(
            'admin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route'    => '/admin',
                    'defaults' => array(
                        'controller' => 'CMSAdmin\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'CMSAdmin\Controller\Index' => 'CMSAdmin\Controller\IndexController'
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'layout/admin'                => __DIR__ . '/../view/layout/layout.phtml',
            'cms-admin/index/index'       => __DIR__ . '/../view/cms-admin/index/index.phtml',
            'cms-admin/widget/navigation' => __DIR__ . '/../view/cms-admin/widget/navigation.phtml',
         ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    'route_layouts' => array(
        'admin' => 'layout/admin',
    ),
);
