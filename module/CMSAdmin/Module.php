<?php
namespace CMSAdmin;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ViewHelperProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }


    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'adminNavigationWidget' => function($sm) {
                    $options = $sm -> getServiceLocator() -> get('Config');
                    $widget = new View\Helper\NavigationWidget();

                    if ( isset($options['admin_navigation']) ) {
                        $widget -> setConfig($options['admin_navigation']);
                    }
                    return $widget;
                }
            )
        );
    }

}
