<?php
namespace CMSAdmin\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;
use Common\Stdlib\MinPriorityQueue;

class NavigationWidget extends AbstractHelper
{
    protected $config = array();

    public function setConfig(array $config)
    {
        $this -> config = $config;
        return $this;
    } // setConfig()


    public function getConfig()
    {
        return $this -> config;
    } // getConfig()


    public function __invoke()
    {
        $config = $this -> config;
        $spq = new MinPriorityQueue();
        $spq -> setExtractFlags(MinPriorityQueue::EXTR_DATA);

        foreach ($config as $item) {
            $spq -> insert($item, $item['index']);
        }

        $viewModel = new ViewModel(array('navigation' => $spq));
        $viewModel -> setTemplate('cms-admin/widget/navigation');
        return $this -> getView() -> render($viewModel);
    } // __invoke()

} // NavigationWidget
