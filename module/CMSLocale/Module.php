<?php
namespace CMSLocale;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\EventManager\EventInterface;
use CMSLocale\Options\LocaleOptionsInterface as IOptions;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ViewHelperProviderInterface,
    ServiceProviderInterface,
    BootstrapListenerInterface
{
    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    } // getAutoloaderConfig()


    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'multilingualWidget' => 'CMSLocale\View\Helper\MultilingualWidgetFactory'
            )
        );
    } // getViewHelperConfig()


    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'CMSLocale\CookieListener'     => 'CMSLocale\Listener\CookieListener',
                'CMSLocale\UrlListener'        => 'CMSLocale\Listener\UrlListener',
                'CMSLocale\SessionListener'    => 'CMSLocale\Listener\SessionListener',
                'CMSLocale\HttpAcceptLanguageListener'  => 'CMSLocale\Listener\HttpAcceptLanguageListener',
                'CMSLocale\Service'            => 'CMSLocale\Service\Locales',
                'CMSLocale\CurrentLocale'      => 'CMSLocale\Service\CurrentLocale',
                'CMSLocale\InputFilter\Locale' => 'CMSLocale\Form\InputFilter\Locale',
            ),
            'factories' => array(
                'cms_locale_options' => function($sm) {
                    $config = $sm -> get('Config');
                    $options = isset($config['cms_locale']) ? $config['cms_locale'] : array();

                    if ( !isset($options['data_source']) ) {
                        $options['data_source'] = IOptions::DATA_SOURCE_CONFIG_FILE;
                    }

                    if ( $options['data_source'] === IOptions::DATA_SOURCE_DATA_BASE ) {
                        $options['supported'] = $sm -> get('CMSLocale\Mapper') -> fetchSuported();
                    }
                    return new Options\ModuleOptions($options);
                },
                'CMSLocale\Mapper' => function($sm) {
                    $mapper = new Mapper\Locales();
                    $mapper
                        -> setDbAdapter($sm -> get('Zend\Db\Adapter\Adapter'))
                        -> setEntityPrototype(new Entity\Locale())
                        -> setHydrator(new ClassMethods());
                    return $mapper;
                },
                'CMSLocale\Form\Add' => function($sm) {
                    $form = new Form\Add();
                    $form -> setInputFilter($sm -> get('CMSLocale\InputFilter\Locale'));
                    return $form;
                },
                'CMSLocale\Form\Edit' => function($sm) {
                    $form = new Form\Edit();
                    $form -> setInputFilter($sm -> get('CMSLocale\InputFilter\Locale'));
                    return $form;
                },
                'CMSLocale\Detector' => 'CMSLocale\DetectorFactory',
                'Router' => 'CMSLocale\Mvc\Service\MultilingualRouterFactory',
            )
        );
    } // getServiceConfig()


    /**
     * Listen to the bootstrap event
     *
     * @param EventInterface $e
     *
     * @return array
     */
    public function onBootstrap(EventInterface $e)
    {
        $app = $e -> getApplication();
        $sm  = $app -> getServiceManager();
        $config = $sm -> get('Config');

        $detector = $sm -> get('CMSLocale\Detector');
        $locale = $detector -> detect($app -> getRequest(), $app -> getResponse());

        if ( null !==  $locale ) {

            \Locale::setDefault($locale);
            $sm -> get('translator') -> setLocale(\Locale::getDefault());

            if ( in_array('Url', $config['cms_locale']['listeners'])) {
                $lang = $detector -> getAlias($locale);
                if ( !$lang ) {
                    $lang = substr($locale, 0, 2);
                }
                $sm -> get('Router') -> setDefaultParam('lang' , $lang);
            }
            return;
        }

        if ( $detector -> throwExceceptionIfNotFound() ) {
            throw new Exception\LocaleNotFoundException('No locale found in locale detection');
        }

    } // onBootstrap()


} // Module
