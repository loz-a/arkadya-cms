<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'CMSLocale\Controller\Index' => 'CMSLocale\Controller\IndexController',
        )
    ),

    'cms_locale' => array(
        'listeners' => array()
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'cms-locale' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'cmslocale/widget/multilingual' => __DIR__ . '/../view/cms-locale/widget/multilingual.phtml'
        ),
    ),

    'router' => array(
        'routes' => array(
            'multilingual_home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/:lang/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'manage_locales' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/manage-locales',
                    'defaults' => array(
                        'controller' => 'CMSLocale\Controller\Index',
                        'action'     => 'index'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'add' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/add',
                        ),
                        'may_terminate' => false,
                        'child_routes' => array(
                            'set_data' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb'     => 'get',
                                    'defaults' => array(
                                        'action' => 'add'
                                    )
                                ),
                                'may_terminate' => true
                            ),
                            'submit' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb'    => 'post',
                                    'defaults' => array(
                                        'action' => 'add-submit'
                                    )
                                ),
                                'may_terminate' => true
                            )
                        )
                    ),
                    'edit' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/edit/:id',
                            'constraints' => array(
                                'id' => '\d+'
                            )
                        ),
                        'may_terminate' => false,
                        'child_routes' => array(
                            'edit_data' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb'     => 'get',
                                    'defaults' => array(
                                        'action' => 'edit'
                                    )
                                ),
                                'may_terminate' => true
                            ),
                            'submit' => array(
                                'type' => 'Method',
                                'options' => array(
                                    'verb'    => 'post',
                                    'defaults' => array(
                                        'action' => 'edit-submit'
                                    )
                                ),
                                'may_terminate' => true
                            )
                        )
                    ),
                    'delete' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/delete/:id',
                            'constraints' => array(
                                'id' => '\d+'
                            ),
                            'defaults' => array(
                                'action' => 'delete'
                            )
                        )
                    )
                )
            )
        ),
    ),

    'route_layouts' => array(
        'multilingual_home'             => 'layout/layout',
        'manage_locales'                => 'layout/admin',
        'manage_locales/add/set_data'   => 'layout/admin',
        'manage_locales/add/submit'     => 'layout/admin',
        'manage_locales/edit/edit_data' => 'layout/admin',
        'manage_locales/edit/submit'    => 'layout/admin',
        'manage_locales/delete'         => 'layout/admin',
    ),

    'admin_navigation' => array(
        'cms_locale' => array(
            'index' => 3,
            'header' => array(
                'label' => 'Locales',
                'icon' => 'icon-flag',
                'class_name' => 'flag',
            ),
            'items' => array(
                array(
                    'label' => 'All Locales',
                    'route_name' => 'manage_locales'
                ),
                array(
                    'label' => 'Add Locale',
                    'route_name' => 'manage_locales/add/set_data'
                )
            )
        )
    ),

);