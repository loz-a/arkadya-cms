<?php
namespace CMSLocale\Mvc\Router\Http;

use Locale;
use Zend\Mvc\Router\Http\TreeRouteStack;
use Zend\Mvc\Router\Http\RouteInterface;

class MultilingualTreeRouteStack extends TreeRouteStack
{
    /**
     * addRoute(): defined by RouteStackInterface interface.
     *
     * @see    RouteStack::addRoute()
     * @param  string  $name
     * @param  mixed   $route
     * @param  integer $priority
     * @return TreeRouteStack
     */
    public function addRoute($name, $route, $priority = null)
    {
        if (!$route instanceof RouteInterface) {

            if ( $route['type'] === 'Regex') {
                $route['options']['regex'] = '/(?<lang>[a-zA-Z]{2})' . $route['options']['regex'];
                $route['options']['spec']  = '/%lang%' . $route['options']['spec'];
            }
            else {
                $route['type'] = 'Segment';
                $route['options']['route'] =  '/:lang' . $route['options']['route'];
                $route['options']['constraints']['lang'] = '[a-zA-z]{2}';
            }

            $route = $this -> routeFromArray($route);
        }
        return parent::addRoute($name, $route, $priority);
    } // addRoute()


    public function assemble(array $params = array(), array $options = array())
    {
        if ( !isset($params['lang']) ) {
            $params['lang'] = Locale::getPrimaryLanguage(Locale::getDefault());
        }
        return parent::assemble($params, $options);
    } // assemble()

} // MultilingualTreeRouteStack
