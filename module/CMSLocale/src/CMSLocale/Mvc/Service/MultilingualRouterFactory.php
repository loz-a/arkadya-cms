<?php
namespace CMSLocale\Mvc\Service;

use Locale;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Console\Console;
use Zend\Mvc\Router\Console\SimpleRouteStack as ConsoleRouter;
use Zend\Mvc\Router\Http\TreeRouteStack;
use CMSLocale\Mvc\Router\Http\MultilingualTreeRouteStack;

class MultilingualRouterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator, $cName = null, $rName = null)
    {
        $config = $serviceLocator->get('Config');

        $router = null;
        $routerConfig = [];

        if ($rName === 'ConsoleRouter'                           // force console router
            or ($cName === 'router' and Console::isConsole())    // auto detect console
        ) {
            // We are in a console, use console router.
            if (isset($config['console']) && isset($config['console']['router'])) {
                $routerConfig = $config['console']['router'];
            }

            $router = ConsoleRouter::factory($routerConfig);
        } else {
            if (isset($config['router'])) {
                $routerConfig = $config['router'];
            }

            if (in_array('Url', $config['cms_locale']['listeners'])) {
                $router = MultilingualTreeRouteStack::factory($routerConfig);
            }
            else {
                $router = TreeRouteStack::factory($routerConfig);
            }
        }

        return $router;
    } // createService()

} // MultilingualRouterFactory
