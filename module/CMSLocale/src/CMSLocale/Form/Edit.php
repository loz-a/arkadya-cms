<?php
namespace CMSLocale\Form;

class Edit extends Add
{
    public function init()
    {
        parent::init();
        $this -> setName('Edit locale');

        $this
            -> add(array(
                'name' => 'id',
                'attributes' => array(
                    'type' => 'hidden'
                )
            ));
    } // init()

} // Edit
