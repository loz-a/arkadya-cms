<?php
namespace CMSLocale\Form\InputFilter;

use Common\Form\InputFilter\InputFilter;

class Locale extends InputFilter
{
    public function init()
    {
        $options = $this -> serviceManager -> get('cms_locale_options');
        $id = $this -> serviceManager -> get('Request') -> getPost('id');

        if ( $id ) {
            $this
                -> add(array(
                    'name' => 'id',
                    'required' => true,
                    'filters'  => array(
                        array('name' => 'Int')
                    )
                ));
        }

        $this
            -> add(array(
                'name' => 'locale',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'max' => $options -> getLocaleMaxlength()
                        )
                    ),
                    array(
                        'name' => 'CMSLocale\Validator\NoRecordExists',
                        'options' => array(
                            'mapper'  => $this -> serviceManager -> get('CMSLocale\Mapper'),
                            'key'     => 'locale',
                            'exclude' => $id ? sprintf('id != %s', $id) : null
                        )
                    )
                )
            ))
            -> add(array(
               'name' => 'alias',
               'required' => true,
               'filters'  => array(
                   array('name' => 'StripTags'),
                   array('name' => 'StringTrim')
               ),
               'validators' => array(
                   array(
                       'name' => 'StringLength',
                       'options' => array(
                           'encoding' => 'UTF-8',
                           'max' => $options -> getAliasMaxlength()
                       )
                   ),
                   array(
                       'name' => 'CMSLocale\Validator\NoRecordExists',
                       'options' => array(
                           'mapper'  => $this -> serviceManager -> get('CMSLocale\Mapper'),
                           'key'     => 'alias',
                           'exclude' => $id ? sprintf('id != %s', $id) : null
                       )
                   )
               )
            ))
            -> add(array(
                'name' => 'is_enable',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int')
                )
            ));
        ;

    } // init()

} // Locale
