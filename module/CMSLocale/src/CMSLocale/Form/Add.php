<?php
namespace CMSLocale\Form;

use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;
use Common\Form\Form;

class Add extends Form
{
    public function init()
    {
        $this
            -> setName('Add locale')
            -> setAttribute('method', 'post')
            -> setAttribute('accept-charset', 'UTF-8');

        $this
            -> add(array(
                'name' => 'locale',
                'options' => array(
                    'label' => 'Locale'
                ),
                'attributes' => array(
                    'maxlength' => $this -> getModuleOptions() -> getLocaleMaxlength()
                )
            ))
            -> add(array(
                'name' => 'alias',
                'options' => array(
                    'label' => 'Alias'
                ),
                'attributes' => array(
                    'maxlength' => $this -> getModuleOptions() -> getAliasMaxlength()
                )
            ))
            -> add(array(
                'name' => 'is_enable',
                'type' => 'Zend\Form\Element\Radio',
                'options' => array(
                    'label' => 'Is enable',
                    'value_options' => array(
                        1 => 'enable',
                        0 => 'disable'
                    ),
                    'label_attributes' => array('class' => 'radio'),
                ),
                'attributes' => array('value' => 1)
            ))
            -> add(array(
                'name' => 'csrf',
                'type' => 'Zend\Form\Element\Csrf'
            ))
            -> add(array(
                'name' => 'submit',
                'attributes' => array(
                    'class' => 'btn btn-primary',
                    'type'  => 'submit',
                    'value' => 'Ok'
                )
            ));
    } // init()


    /**
     * @return null|\CMSLocale\Options\ModuleOptions
     */
    public function getModuleOptions()
    {
        return parent::getModuleOptions('cms_locale_options');
    } // getModuleOptions()

} // Add
