<?php
namespace CMSLocale;

use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use CMSLocale\Listener\ListenerInterface;
use Zend\Stdlib\RequestInterface;
use Zend\Stdlib\ResponseInterface;
use CMSLocale\Event;

class Detector implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

     /**
     * Default locale
     *
     * @var string
     */
    protected $defaultLocale;

    /**
     * Optional list of supported locales
     *
     * @var array
     */
    protected $supported;

    /**
     * @var array
     */
    protected $aliases;

    /**
     * @var array
     */
    protected $redirectExclude = [];

    /**
     * @param Listener\ListenerInterface $lisener
     * @param int $priority
     * @return Detector
     */
    public function addListener(ListenerInterface $lisener, $priority = 1)
    {
        $this -> getEventManager() -> attachAggregate($lisener, $priority);
        return $this;
    } // addListener()


    /**
     * @return string
     */
    public function getDefault()
    {
        return $this -> defaultLocale;
    } // getDefault()


    /**
     * @param $defaultLocale
     * @return Detector
     */
    public function setDefault($defaultLocale)
    {
        $this -> defaultLocale = $defaultLocale;
        return $this;
    } // setDefault()


    /**
     * @return array
     */
    public function getSupported()
    {
        return $this -> supported;
    } // getSupported()


    /**
     * @param array $supproted
     * @return Detector
     */
    public function setSupported(array $supproted)
    {
        $this -> supported = array_unique($supproted);
        return $this;
    } // setSupported()


    /**
     * @return bool
     */
    public function hasSupported()
    {
        return is_array($this -> supported) and count($this -> supported);
    } // hasSupproted()


    public function setRedirectExclude(array $excludes)
    {
        $this -> redirectExclude = $excludes;
        return $this;
    } // setRedirectExclude()

    public function getRedirectExclude()
    {
        return $this -> redirectExclude;
    } // getRedirectExclude()


    /**
     * @param $locale
     * @return bool
     */
    public function isAlias($locale)
    {
        return is_array($this -> supported) and array_key_exists($locale, $this -> supported);
    } // hasAliases()


    /**
     * @param $locale
     * @return mixed
     */
    public function getCanonical($locale)
    {
        if ( $this -> hasSupported() ) {
            if ( $this -> isAlias($locale) ) {
                return $this -> supported[$locale];
            }
            else if ( false !== array_search($locale, $this -> supported) ) {
                return $locale;
            }
        }
        return null;
    } // getCanonical()


    public function getAlias($locale)
    {
        $alias = array_search($locale, $this -> supported);
        if ( false !== $alias and !is_int($alias) ) {
            return $alias;
        }
        return null;
    } // hasAlias()


    /**
     * @param \Zend\Stdlib\RequestInterface $request
     * @param \Zend\Stdlib\ResponseInterface $response
     * @return mixed
     */
    public function detect(RequestInterface $request, ResponseInterface $response)
    {
        $event = new Event(Event::EVENT_DETECT, $this);
        $event
            -> setDefaultLocale($this -> getDefault())
            -> setRequest($request)
            -> setResponse($response);

        if ( $this -> hasSupported() ) {
            $event -> setSupported($this -> getSupported());
        }

        $results = $this -> getEventManager() -> trigger($event, function($r) {
            return is_string($r);
        });

        if ( !$results -> stopped() ) {
            return $this -> found($this -> getDefault(), $event);
        }
        $locale = $this -> getCanonical($results -> last());

        if ( null !== $locale ) {
            return $this -> found($locale, $event);
        }
        return $this -> found($this -> getDefault(), $event);
    } // detect()


    /**
     * @param $locale
     * @param Event $event
     * @return mixed
     */
    public function found($locale, Event $event)
    {
        $alias = $this -> getAlias($locale);
        if ( !$alias ) {
            $alias = substr($locale, 0, 2);
        }

        $event
            -> setName(Event::EVENT_FOUND)
            -> setLocale($locale)
            -> setAlias($alias);

        $this -> getEventManager() -> trigger($event);
        return $locale;
    } // found()

} // Detector
