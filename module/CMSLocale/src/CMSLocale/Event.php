<?php
namespace CMSLocale;

use Zend\EventManager\Event as BaseEvent;
use Zend\Stdlib\RequestInterface;
use Zend\Stdlib\ResponseInterface;

class Event extends BaseEvent
{
    const EVENT_DETECT = 'detect';
    const EVENT_FOUND = 'found';

    const DEFAULT_LOCALE_KEY = 'defaultLocale';
    const REQUEST_KEY = 'request';
    const RESPONSE_KEY = 'response';
    const SUPPORTED_KEY = 'supported';
    const LOCALE_KEY = 'locale';
    const ALIAS_KEY = 'alias';

    /**
     * @var string
     */
    protected $defaultLocale;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var array
     */
    protected $supported;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @var string;
     */
    protected $alias;


    /**
     * @return string
     */
    public function getDefaultLocale()
    {
        return $this -> defaultLocale;
    } // getDefaultLocale()


    /**
     * @param string $locale
     * @return Event
     */
    public function setDefaultLocale($locale)
    {
        $this -> setParam(self::DEFAULT_LOCALE_KEY, $locale);
        $this -> defaultLocale = $locale;
        return $this;
    } // setDefaultLocale()


    /**
     * @return \Zend\Stdlib\RequestInterface
     */
    public function getRequest()
    {
        return $this -> request;
    } // getRequest()


    /**
     * @param \Zend\Stdlib\RequestInterface $request
     * @return Event
     */
    public function setRequest(RequestInterface $request)
    {
        $this -> setParam(self::REQUEST_KEY, $request);
        $this -> request = $request;
        return $this;
    } // setRequest()


    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getResponse()
    {
        return $this -> response;
    } // getResponse()


    /**
     * @param \Zend\Stdlib\ResponseInterface $response
     * @return Event
     */
    public function setResponse(ResponseInterface $response)
    {
        $this -> setParam(self::RESPONSE_KEY, $response);
        $this -> response = $response;
        return $this;
    } // setResponse()


    /**
     * @return array
     */
    public function getSupported()
    {
        return $this -> supported;
    } // getSupported()


    /**
     * @param array $supported
     * @return Event
     */
    public function setSupported(array $supported)
    {
        $this -> setParam(self::SUPPORTED_KEY, $supported);
        $this -> supported = $supported;
        return $this;
    } // setSuported()


    public function getSupportedLocalesAndAliases()
    {
        $temp =  array_merge(array_keys($this -> supported), array_values($this -> supported));
        return array_filter($temp);
    } // getSupprotedLocalesAndAliases()

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this -> locale;
    } // getLocale()


    /**
     * @param $locale
     * @return Event
     */
    public function setLocale($locale)
    {
        $this -> setParam(self::LOCALE_KEY, $locale);
        $this -> locale = $locale;
        return $this;
    } // setLocale()


    /**
     * @return string
     */
    public function getAlias()
    {
        return $this -> alias;
    } // getAlias()


    /**
     * @param $alias
     * @return Event
     */
    public function setAlias($alias)
    {
        $this -> setParam(self::ALIAS_KEY, $alias);
        $this -> alias = $alias;
        return $this;
    } // setAlias()

} // Event
