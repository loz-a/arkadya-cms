<?php
namespace CMSLocale;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use CMSLocale\Detector;

class DetectorFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator -> get('cms_locale_options');

        $detector = new Detector();
        $detector -> setEventManager($serviceLocator -> get('EventManager'));
        $this -> addListeners($detector, $config -> getListeners(), $serviceLocator);

        $detector
            -> setDefault($config -> getDefault())
            -> setSupported($config -> getSupported())
            -> setRedirectExclude($config -> getRedirectExclude());

        return $detector;
    } // createService()


    protected function addListeners(Detector $detector, array $listeners, ServiceLocatorInterface $serviceLocator)
    {
        foreach ($listeners as $listener) {
            if ( is_string($listener) ) {
                $class = $serviceLocator -> get($listener);
                $detector -> addListener($class);
            }
            elseif ( is_array($listener) ) {
                $name = $listener['name'];
                $class = $serviceLocator->get($name);

                if (array_key_exists('options', $listener) && method_exists($class, 'setOptions')) {
                    $class->setOptions($listener['options']);
                }

                $priority = 1;
                if (array_key_exists('priority', $listener)) {
                    $priority = $listener['priority'];
                }
                $detector->addListener($class, $priority);
            } else {
                throw new \CMSLocale\Exception\ListenerConfigurationException('Listener configuration must be a string or an array');
            }
        }
    } // addStrategies()

} // DetectorFactory
