<?php
namespace CMSLocale\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    /**
     * @var \CMSLocale\Service\Locales
     */
    protected $localesService;

    public function indexAction()
    {
        return array(
            'locales' => $this -> getLocalesService() -> getMapper() -> fetchAll()
        );
    } // index


    public function addAction()
    {
        return array(
            'form' => $this -> getLocalesService() -> getAddForm()
        );
    } // add


    public function addSubmitAction()
    {
        $form = $this -> getLocalesService() -> getAddForm();
        $form -> setData($this -> request -> getPost());

        if ( $form -> isValid() ) {
            $result = $this -> getLocalesService() -> add($form -> getData());
            if ( $result ) {
                $this -> flashMessenger() -> addMessage('Locale was added');
                $this -> redirect() -> toRoute('manage_locales');
            }
        }
        $this -> flashMessenger() -> addMessage('Something was wrong');

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view -> setTemplate('cms-locale/index/add');
    } // addSubmitAction()


    public function editAction()
    {
        $id   = $this -> params('id');
        $form = $this -> getLocalesService() -> getPopulatedFormById($id);
        if ( !$form ) {
            return $this -> notFoundAction();
        }

        $view = new ViewModel(array('form' => $form));
        return $view -> setTemplate('cms-locale/index/add');
    } // edit


    public function editSubmitAction()
    {
        $form = $this -> getLocalesService() -> getEditForm();
        $form -> setData($this -> request -> getPost());

        if ( $form -> isValid() ) {
            $result = $this -> getLocalesService() -> edit($form -> getData());
            if ( $result ) {
                $this -> flashMessenger() -> addMessage('Locale was edited');
                $this -> redirect() -> toRoute('manage_locales');
            }
        }
        else {
            $this -> flashMessenger() -> addMessage('Something was wrong');
        }

        $view = new ViewModel(array('form' => $form));
        return $view -> setTemplate('cms-locale/index/add');
    } // editSubmitAction()


    public function deleteAction()
    {
        $id = $this -> params('id');
        if ( !$id ) {
            return $this -> notFoundAction();
        }

        if ( $this -> request -> isPost() ) {
            $confirm = $this -> request -> getPost() -> get('confirm', 'no');
            if ( strtolower($confirm) === 'yes' ) {
                $this -> getLocalesService() -> getMapper() -> removeById($id);
                $this -> flashMessenger() -> addMessage('Locale was removed');
            }
            return $this -> redirect() -> toRoute('manage_locales');
        }
        return array(
            'locale' => $this -> getLocalesService() -> getMapper() -> findById($id)
        );
    } // delete


    /**
     * @return \CMSLocale\Service\Locales
     */
    public function getLocalesService()
    {
        if( null === $this -> localesService ) {
            $this -> localesService = $this -> getServiceLocator() -> get('CMSLocale\Service');
        }
        return $this -> localesService;
    } // getLocalesService()

} // IndexController
