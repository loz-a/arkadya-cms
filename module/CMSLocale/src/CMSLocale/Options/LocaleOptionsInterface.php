<?php
namespace CMSLocale\Options;

interface LocaleOptionsInterface
{
    const DATA_SOURCE_CONFIG_FILE = 'config-file';
    const DATA_SOURCE_DATA_BASE = 'data-base';

    public function setDefault($default);

    public function getDefault();

    public function setSupported(array $supported);

    public function getSupported();

    public function setDataSource($dataSource);

    public function getDataSource();

    public function setListeners(array $listeners);

    public function getListeners();

    public function setRedirectExclude(array $excludes);

    public function getRedirectExclude();

    public function setAliasMaxlength($maxlen);

    public function getAliasMaxlength();

    public function setLocaleMaxlength($maxlen);

    public function getLocaleMaxlength();

} // LocaleOptionsInterface
