<?php
namespace CMSLocale\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements LocaleOptionsInterface
{
    /**
     * @var string
     */
    protected $defaultLocale = 'uk_UA';

    /**
     * @param string $default
     * @return ModuleOptions
     */
    public function setDefault($default)
    {
        $this -> defaultLocale = $default;
        return $this;
    } // setDefault()

    /**
     * @return string
     */
    public function getDefault()
    {
        return $this -> defaultLocale;
    } // getDefault()


    /**
     * @var array
     */
    protected $supported = ['uk' => 'uk_UA'];

    /**
     * @param array $supported
     * @return ModuleOptions
     */
    public function setSupported(array $supported)
    {
        $this -> supported = $supported;
        return $this;
    } // setSupported()

    /**
     * @return array
     */
    public function getSupported()
    {
        return $this -> supported;
    } // getSupported()


    /**
     * @var string
     */
    protected $dataSource = LocaleOptionsInterface::DATA_SOURCE_CONFIG_FILE;

    /**
     * @param string $dataSource
     * @return ModuleOptions
     */
    public function setDataSource($dataSource)
    {
        $this -> dataSource = (string) $dataSource;
        return $this;
    } // setDataSource()


    public function getDataSource()
    {
        return $this -> dataSource;
    } // getDataSource()


    /**
     * @var array
     */
    protected $listeners = [];

    /**
     * @param array $listeners
     * @return ModuleOptions
     */
    public function setListeners(array $listeners)
    {
        $namespace = explode('\\', __NAMESPACE__)[0];

        foreach ($listeners as $listener) {
            $this -> listeners[] = sprintf('%s\%sListener', $namespace, $listener) ;
        }
        return $this;
    } // $setListeners()

    /**
     * @return array
     */
    public function getListeners()
    {
        return $this -> listeners;
    } // getListeners()


    protected $redirectExclude = [];

    public function setRedirectExclude(array $excludes)
    {
        $this -> redirectExclude = $excludes;
        return $this;
    } // setRedirectExclude()

    public function getRedirectExclude()
    {
        return $this -> redirectExclude;
    } // getRedirectExclude()


    protected $aliasMaxlength = 2;

    public function setAliasMaxlength($maxlen)
    {
        $this -> aliasMaxlength = $maxlen;
        return $this;
    } // setAliasMaxlength()


    public function getAliasMaxlength()
    {
        return $this -> aliasMaxlength;
    } // getAliasMaxLength()


    protected $localeMaxlength = 10;

    public function setLocaleMaxlength($maxlen)
    {
        $this -> localeMaxlength = $maxlen;
        return $this;
    } // setLocaleMaxlength()


    public function getLocaleMaxlength()
    {
        return $this -> localeMaxlength;
    } // getLocaleMaxlength()

} // ModuleOptions
