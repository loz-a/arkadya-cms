<?php
namespace CMSLocale\Listener;

use Zend\Http\Header\Cookie;
use Zend\Http\Header\SetCookie;
use Zend\Http\Request as HttpRequest;
use CMSLocale\Event;

class CookieListener extends AbstractListener
{
    const COOKIE_NAME = 'cms_locale';

    public function detect(Event $event)
    {
        $request = $event -> getRequest();
        $supported = $event -> getSupportedLocalesAndAliases();

        if ( !$request instanceof HttpRequest
            or !count($supported)
        ) {
            return null;
        }

        $cookie = $request -> getCookie();
        if ( !$cookie or !$cookie -> offsetExists(self::COOKIE_NAME) ) {
            return null;
        }

        $locale = $cookie -> offsetGet(self::COOKIE_NAME);

        if ( !in_array($locale, $supported) ) {
            return null;
        }
        return $locale;
    } // detect()


    public function found(Event $event)
    {
        $locale  = $event -> getLocale();
        $request = $event -> getRequest();

        if ( !$request instanceof HttpRequest ) {
            return null;
        }

        $cookie = $request -> getCookie();
        // Omit Set-Cookie header when cookie is present
        if ( $cookie instanceof Cookie
            and $cookie -> offsetExists(self::COOKIE_NAME)
            and $locale === $cookie -> offsetGet(self::COOKIE_NAME)
        ) {
            return null;
        }

        $response = $event -> getResponse();
        $cookie   = $response -> getCookie();

        $setCookie = new SetCookie(self::COOKIE_NAME, $locale);
        $response -> getHeaders() -> addHeader($setCookie);
    } // found()

} // CookieListener
