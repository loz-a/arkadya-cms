<?php
namespace CMSLocale\Listener;

use Zend\Console\Console;
use CMSLocale\Event;

class UrlListener extends AbstractListener
{
    const REDIRECT_STATUS_CODE = 301;

    protected $needRedirect = false;
    protected $regexp = "#^/([a-zA-Z]{2})($|/)#";

    public function detect(Event $event)
    {
        if (Console::isConsole()) {
            return;
        }

        $path = $event -> getRequest() -> getUri() -> getPath();
        $lang = '';

        if ( preg_match($this -> regexp, $path, $matches)) {
            $lang = $matches[1];
        }
        else {
            $this -> needRedirect = true;
            return null;
        }
        $supproted = $event -> getSupportedLocalesAndAliases();

        if ( !in_array($lang, $supproted) ) {
            if ( $lang !== substr($event -> getDefaultLocale(), 0, 2) ) {
                $this -> needRedirect = true;
            }
            return $lang;
        }
        return $lang;
    } // detect()


    public function found(Event $event)
    {
        if ( $this -> needRedirect ) {
            $this -> redirect($event);
        }
    } // found()


    protected function redirect(Event $event)
    {
        $redirectExclude = $event -> getTarget() -> getRedirectExclude();
        $requestUri = $event -> getRequest() -> getRequestUri();

        if ($this -> isRedirectExclude($redirectExclude, $requestUri)) {
            return;
        }

        $uri = $event -> getRequest() -> getUri();
        $lang = $event -> getAlias();

        $path = preg_replace($this -> regexp, '/', $uri -> getPath());
        $location = sprintf('/%s%s', $lang, $path);

        $response = $event -> getResponse();
        $response -> setStatusCode(self::REDIRECT_STATUS_CODE);
        $response -> getHeaders() -> addHeaderLine('Location', $location);
        $response -> send();
        die;
    } // redirect()


    protected function isRedirectExclude(array $redirectExclude, $requestUri)
    {
        if (array_key_exists('uri', $redirectExclude)) {
            if (in_array($requestUri, $redirectExclude['uri'])) {
                return true;
            }
        }

        if (array_key_exists('file_extensions', $redirectExclude)) {
            $ext = pathinfo($requestUri, PATHINFO_EXTENSION);
            if (in_array($ext, $redirectExclude['file_extensions'])) {
                return true;
            }
        }
        return false;
    } // isRedirectExclude()

} // RouteListener

