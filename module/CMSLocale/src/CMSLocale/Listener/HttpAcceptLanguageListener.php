<?php
namespace CMSLocale\Listener;

use Locale;
use Zend\Http\Request as HttpRequest;
use CMSLocale\Event;

class HttpAcceptLanguageListener extends AbstractListener
{
    public function detect(Event $event)
    {
        $request   = $event -> getRequest();
        $supported = $event -> getSupportedLocalesAndAliases();

        if ( !$request instanceof HttpRequest
            or !count($supported)
        ) {
            return null;
        }

        $headers = $request -> getHeaders();
        if ( $headers -> has('Accept-Language') ) {
            $locales = $headers -> get('Accept-Language') -> getPrioritized();

            foreach ( $locales as $locale ) {
                $locale = $locale -> getLanguage();
                if ( Locale::lookup($supported, $locale) ) {
                    return $locale;
                }
            }
        }

    } // detect()

} // HttpAcceptLanguageListener
