<?php
namespace CMSLocale\Listener;

use CMSLocale\Event;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractListener implements ListenerInterface
{
    /**
     * Listeners we've registered
     *
     * @var array
     */
    protected $listeners = array();

    /**
     * Attach "detect" and "found" listeners
     *
     * @param \Zend\EventManager\EventManagerInterface $events
     * @param int $priority
     */

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this -> listeners[] = $events -> attach(Event::EVENT_DETECT, array($this, 'detect'), $priority);
        $this -> listeners[] = $events -> attach(Event::EVENT_FOUND, array($this, 'found'), $priority);
    } // attach()

    /**
     * Detach all previously attached listeners
     *
     * @param \Zend\EventManager\EventManagerInterface $events
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this -> listeners as $index => $listener) {
            if ($events -> detach($listener)) {
                unset($this -> listeners[$index]);
            }
        }
    } // detach()


    public function detect(Event $event)
    {
    } // detect()


    public function found(Event $event)
    {
    } // found()

} // AbstractListener