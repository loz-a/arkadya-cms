<?php
namespace CMSLocale\Listener;

use Zend\Session\Container as SessionContainer;
use CMSLocale\Event;

class SessionListener extends AbstractListener
{
    const SESSION_NAMESPACE = 'CMS';
    const SESSION_LOCALE_VARNAME = 'cms_locale';
    const SESSION_ALIAS_VARNAME  = 'cms_locale_alias';

    /**
     * @var \Zend\Session\Container
     */
    protected $session;

    public function detect(Event $event)
    {
        $locale = null;
        $supported = $event -> getSupportedLocalesAndAliases();
        $this -> session = new SessionContainer(self::SESSION_NAMESPACE);

        if ( isset($this -> session[self::SESSION_LOCALE_VARNAME]) ) {
            $locale = $this -> session[self::SESSION_LOCALE_VARNAME];
        }

        if ( null !== $locale and in_array($locale, $supported) ) {
            return $locale;
        }
        return null;
    } // detect()


    public function found(Event $event)
    {
        $locale = $event -> getLocale();
        $alias  = $event -> getAlias();

        if ( isset($this -> session[self::SESSION_LOCALE_VARNAME])
            and $this -> session[self::SESSION_LOCALE_VARNAME] !== $locale
        ) {
            $this -> session -> offsetSet(self::SESSION_LOCALE_VARNAME, $locale);
            $this -> session -> offsetSet(self::SESSION_ALIAS_VARNAME, $alias);
        }
    } // found()

} // SessionListener
