<?php
namespace CMSLocale\Listener;

use Zend\EventManager\ListenerAggregateInterface;

interface ListenerInterface extends ListenerAggregateInterface
{
}
