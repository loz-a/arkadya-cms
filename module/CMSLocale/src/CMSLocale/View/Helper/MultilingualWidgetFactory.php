<?php
namespace CMSLocale\View\Helper;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MultilingualWidgetFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sl = $serviceLocator -> getServiceLocator();
        $options = $sl -> get('cms_locale_options');
        $routeMatch = $sl -> get('Application') -> getMvcEvent() -> getRouteMatch();

        $helper = new MultilingualWidget();
        if ( null !== $routeMatch ) {
            $helper -> setRouteMatch($routeMatch);
        }
        $helper -> setSupprotedLocales($options -> getSupported());
        return $helper;
    } // createService()
}
