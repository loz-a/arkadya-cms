<?php
namespace CMSLocale\View\Helper;

use Locale;
use Zend\View\Helper\Url as BaseUrl;

class LangUrl extends BaseUrl
{
    public function __invoke($name = null, array $params = array(), $options = array(), $reuseMatchedParams = false)
    {
        if ( $this -> routeMatch === null ) {
            $name = 'multilingual_home';
        }

        return parent::__invoke($name, $params, $options, $reuseMatchedParams);
    } // __invoke()

} // Url
