<?php
namespace CMSLocale\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Router\RouteMatch;

class MultilingualWidget extends AbstractHelper
{
    /**
     * RouteInterface match returned by the router.
     *
     * @var RouteMatch.
     */
    protected $routeMatch;

    /**
     * @var array
     */
    protected $supprotedLocales = array();

    /**
     * Set route match returned by the router.
     *
     * @param \Zend\Mvc\Router\RouteMatch $routeMatch
     * @return MultilingualWidget
     */
    public function setRouteMatch(RouteMatch $routeMatch)
    {
        $this->routeMatch = $routeMatch;
        return $this;
    } // setRouterMatch()


    /**
     * @param array $supproted
     * @return MultilingualWidget
     */
    public function setSupprotedLocales(array $supproted)
    {
        $this -> supprotedLocales = $supproted;
        return $this;
    } // setSupprotedLocales()


    /**
     * @return string
     */
    public function __invoke($isCurrentVisible = true)
    {
        $languages = array();
        $currentLang = strtolower(\Locale::getPrimaryLanguage(\Locale::getDefault()));

        foreach ( $this -> supprotedLocales as $alias => $locale ) {
            $lang = is_int($alias) ? substr($locale, 0, 2) : $alias;
            $languages[] = strtolower($lang);
        }

        $routeName = '';
        $params = array();

        if ( null !== $this -> routeMatch ) {
            $routeName = $this -> routeMatch -> getMatchedRouteName();
            $params = $this -> routeMatch -> getParams();
            unset($params['lang'], $params['controller'], $params['action']);
        }
        else {
            $routeName = 'multilingual_home';
        }

        $vars = array(
            'routeName' => $routeName,
            'languages' => $languages,
            'current' => $currentLang,
            'params' => $params,
            'isCurrentVisible' => $isCurrentVisible
        );

        $viewModel = new ViewModel($vars);
        $viewModel -> setTemplate('cmslocale/widget/multilingual');
        return $this -> getView() -> render($viewModel);
    } // __invoke()

} // MultilingualWidget
