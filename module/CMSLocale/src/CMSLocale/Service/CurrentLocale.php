<?php
namespace CMSLocale\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Session\Container as SessionContainer;
use CMSLocale\Mapper\LocalesInterface;
use CMSLocale\Entity\Locale as LocaleEntity;
use CMSLocale\Entity\LocaleInterface;
use CMSLocale\Options\LocaleOptionsInterface as IOptions;
use \Locale;

class CurrentLocale implements ServiceManagerAwareInterface
{
    const SESSION_NAMESPACE = 'CMS';
    const SESSION_VARNAME = 'current_locale';

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    protected $sm;

    /**
     * @var \CMSLocale\Entity\LocaleInterface;
     */
    protected $localeEntity;


    /**
     * @var \Zend\Session\Container
     */
    protected $session;


    /**
     * @param ServiceManager $serviceManager
     * @return CurrentLocale
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this -> sm = $serviceManager;
        return $this;
    } // setServiceManager()


    public function getEntity()
    {
        return $this -> __invoke();
    } // getEntity()


    /**
     * @return \CMSLocale\Entity\LocaleInterface|null
     */
    public function __invoke()
    {
        $locale = Locale::getDefault();

        if ( $this -> localeEntity instanceof LocaleInterface
            and $locale === $this -> localeEntity -> getLocale()
        ) {
            return $this -> localeEntity;
        }

        $localeFromSession = $this -> getFromSession();
        if ( $localeFromSession
            and $locale === $localeFromSession -> getLocale()
        ) {
            $this -> localeEntity = $localeFromSession;
            return $this -> localeEntity;
        }

        $localeFromDb = $this -> getFromDb();
        if ( $localeFromDb
            and $locale === $localeFromDb -> getLocale()
        ) {
            $this -> localeEntity = $localeFromDb;
            $this -> session[self::SESSION_VARNAME] = $localeFromDb;
            return $this -> localeEntity;
        }
        return null;
    } // __invoke()


    /**
     * @return null|\CMSLocale\Entity\LocaleInterface
     */
    protected function getFromSession()
    {
        $this -> session = new SessionContainer(self::SESSION_NAMESPACE);

        if ( !isset($this -> session[self::SESSION_VARNAME]) ) {
            return null;
        }

        $curLocale = $this -> session[self::SESSION_VARNAME];
        if ( !$curLocale instanceof LocaleInterface ) {
            return null;
        }
        return $curLocale;
    } // getFromSession()


    /**
     * @return \CMSLocale\Entity\LocaleInterface
     * @throws \LogicException
     */
    protected function getFromDb()
    {
        $options = $this -> sm -> get('cms_locale_options');

        if ( $options -> getDataSource() === IOptions::DATA_SOURCE_DATA_BASE ) {
            return $this -> sm -> get('CMSLocale\Mapper') -> findByLocale(Locale::getDefault());
        }
        throw new \LogicException(sprintf('Wrong data_source for this service. Expected data_source - "%s"', IOptions::DATA_SOURCE_DATA_BASE));
    } // getFromSupportedLocales()

} // CmsLocale
