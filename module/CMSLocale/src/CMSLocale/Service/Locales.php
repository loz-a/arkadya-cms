<?php
namespace CMSLocale\Service;

use Zend\Stdlib\Hydrator\ClassMethods as ObjectHydrator;
use Common\Service\AbstractService;
use CMSLocale\Entity\Locale as LocaleEntity;

class Locales extends AbstractService
{
    /**
     * @return \CMSLocale\Options\ModuleOptions
     */
    public function getOptions()
    {
        return self::getOptions('cms_locale_options');
    } // getOptions()


    /**
     * @return \CMSLocale\Mapper\Locales
     */
    public function getMapper()
    {
        return parent::getMapper('CMSLocale\Mapper');
    } // getMapper()


    /**
     * @return null|\CMSLocale\Form\Add
     */
    public function getAddForm()
    {
        return parent::getCreateForm('CMSLocale\Form\Add');
    } // getAddForm()


    /**
     * @return null|\CMSLocale\Form\Edit
     */
    public function getEditForm()
    {
        return parent::getCreateForm('CMSLocale\Form\Edit');
    } // getAddForm()


    public function add(array $data)
    {
        $locale = new LocaleEntity();

        $locale
            -> setAlias($data['alias'])
            -> setLocale($data['locale'])
            -> setIsEnable($data['is_enable']);

        $reuslt = $this -> getMapper() -> insert($locale);
        return $locale;
    } // addLocale();


    public function edit(array $data)
    {
        $locale = $this -> getMapper() -> findById($data['id']);

        if ( !$locale ) {
            return null;
        }
        $locale
            -> setAlias($data['alias'])
            -> setLocale($data['locale'])
            -> setIsEnable($data['is_enable']);

        $reuslt = $this -> getMapper() -> update($locale, array('id' => $data['id']));
        return $locale;
    } // editLocale()


    public function removeById($id)
    {
        return $this -> getMapper() -> removeById($id);
    } // deleteLocale()


    public function getPopulatedFormById($id)
    {
        $locale = $this -> getMapper() -> findById($id);
        if ( !$locale ) {
            return null;
        }

        $hydrator = new ObjectHydrator();
        $form = $this -> getEditForm();
        $form -> setData($hydrator -> extract($locale));
        return $form;
    } // getPopulatedFormById()

} // Locales
