<?php
namespace CMSLocale\Mapper;

use Zend\Db\Sql\Sql;
use Common\Mapper\AbstractDbMapper;

class Locales extends AbstractDbMapper
    implements LocalesInterface
{
    protected $tableName = 'locales';


    public function fetchAll()
    {
        $result = $this -> selectWith(
            $this -> select() -> from($this -> getTableName())
        );
        return $result -> count() ? $result : null;
    } // findAll()


    public function findById($id)
    {
        $select = $this
            -> select()
            -> columns(array('id', 'locale', 'alias', 'is_enable'))
            -> from($this -> getTableName())
            -> where(array('id' => (int) $id))
            -> limit(1);

        $result = $this -> selectWith($select);
        return $result -> count() ? $result -> current() : null;
    } // findById()


    public function findByAlias($alias, $exclude = null)
    {
        $select = $this
            -> select()
            -> columns(array('id', 'locale', 'alias', 'is_enable'))
            -> from($this -> getTableName())
            -> where(array('alias' => $alias))
            -> limit(1);

        if ( $exclude ) {
            $select -> where($exclude);
        }

        $result = $this -> selectWith($select);
        return $result -> count() ? $result -> current() : null;
    } // findByAlias()


    public function findByLocale($locale, $exclude = null)
    {
        $select = $this
            -> select()
            -> columns(array('id', 'locale', 'alias', 'is_enable'))
            -> from($this -> getTableName())
            -> where(array('locale' => $locale))
            -> limit(1);

        if ( $exclude ) {
            $select -> where($exclude);
        }

        $result = $this -> selectWith($select);
        return $result -> count() ? $result -> current() : null;
    } // findByLocale()


    public function fetchSuported()
    {
        $select = $this
            -> select()
            -> columns(array('locale', 'alias'))
            -> from($this -> getTableName())
            -> where(array('is_enable' => 1));

        $adapter   = $this -> getDbAdapter();
        $statement = $adapter -> createStatement();
        $select -> prepareStatement($adapter, $statement);
        $r = iterator_to_array($statement -> execute());

        $result = array();
        array_walk($r, function($value) use (&$result) {
            $result[$value['alias']] = $value['locale'];
        });
        return $result;
    } // fetchSupported()


    public function removeById($id)
    {
        $sql = new Sql($this -> getDbAdapter(), $this -> getTableName());
        $delete = $sql -> delete() -> where(array('id' => $id));
        $statement = $sql -> prepareStatementForSqlObject($delete);
        $result = $statement -> execute();
        return $result -> getAffectedRows();
    } // removeById()

} // Locales
