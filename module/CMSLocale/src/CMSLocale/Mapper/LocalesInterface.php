<?php
namespace CMSLocale\Mapper;

interface LocalesInterface
{
    public function fetchAll();

    public function findById($id);

    public function findByAlias($alias, $exclude = null);

    public function findByLocale($locale, $exclude = null);

    public function removeById($id);
}
