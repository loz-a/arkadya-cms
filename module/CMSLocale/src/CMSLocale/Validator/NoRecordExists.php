<?php
namespace CMSLocale\Validator;

use Common\Validator\AbstractRecord;
use Common\Validator\Exception\InvalidArgumentException;
use CMSLocale\Mapper\LocalesInterface;

class NoRecordExists extends AbstractRecord
{
    /**
     * @param $mapper
     * @return NoRecordExists
     */
    public function setMapper($mapper)
    {
        if ( !$mapper instanceof LocalesInterface ) {
            throw new InvalidArgumentException('Wrong mapper type. CMSLocale\Mapper\LocalesInterface expected');
        }

        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    public function isValid($value)
    {
        $this -> setValue($value);

        $result = $this -> query($value);
        if ( $result ) {
            $this -> error(self::ERROR_RECORD_FOUND);
            return false;
        }
        return true;
    } // isValid()


    protected function query($value)
    {
        $result = false;

        switch ( $this -> getKey() ) {
            case 'alias':
                $result = $this -> getMapper() -> findByAlias($value, $this -> getExclude());
                break;
            case 'locale':
                $result = $this -> getMapper() -> findByLocale($value, $this -> getExclude());
                break;
            default:
                throw new \Exception('Invalid key used in Locale validator');
        }
        return $result;
    } // query()

} // NoRecordExists
