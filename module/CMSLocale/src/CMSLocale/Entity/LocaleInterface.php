<?php
namespace CMSLocale\Entity;

interface LocaleInterface
{
    public function setId($id);

    public function getId();

    public function setLocale($locale);

    public function getLocale();

    public function setAlias($alias);

    public function getAlias();

    public function setIsEnable($flag = 0);

    public function getIsEnable();

    public function isEnable();
}
