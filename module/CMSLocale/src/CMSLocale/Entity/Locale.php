<?php
namespace CMSLocale\Entity;

class Locale implements LocaleInterface
{
    protected $id;
    protected $locale;
    protected $alias;
    protected $isEnable = 1;

    /**
     * @param $id
     * @return Locale
     */
    public function setId($id)
    {
        $this -> id = (int) $id;
        return $this;
    } // setId()


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this -> id;
    } // getId()


    /**
     * @param $locale
     * @return Locale
     */
    public function setLocale($locale)
    {
        $this -> locale = (string) $locale;
        return $this;
    } // setLocale()


    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this -> locale;
    } // getLocale()


    /**
     * @param $alias
     * @return Locale
     */
    public function setAlias($alias)
    {
        $this -> alias = (string) $alias;
        return $this;
    } // setAlias()


    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this -> alias;
    } // getAlias()


    /**
     * @param int $flag
     * @return Locale
     */
    public function setIsEnable($flag = 0)
    {
        $this -> isEnable = $flag;
        return $this;
    } // setIsEnabled()


    /**
     * @return int
     */
    public function getIsEnable()
    {
        return $this -> isEnable;
    } // getIsEnabled()


    /**
     * @return int
     */
    public function isEnable()
    {
        return $this -> isEnable;
    } // isEnabled()

} // Locale
