<?php
namespace HtmlPurifier\Filter\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{
} // InvalidArgumentException
