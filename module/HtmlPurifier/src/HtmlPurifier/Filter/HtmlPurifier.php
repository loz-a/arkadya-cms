<?php
namespace HtmlPurifier\Filter;

use Zend\Filter\AbstractFilter;
use \Traversable;
use Zend\Stdlib\ArrayUtils;
use \HTMLPurifier as HTMLPurifierBase;

class HtmlPurifier extends AbstractFilter
{
    /**
     * @var \HTMLPurifier
     */
    protected $purifier;

    public function __construct($options)
    {
        if ( !$options ) {
            $options = array();
        }
        else if ( $options instanceof Traversable ) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        else if ( is_object($options) ) {
            $options = array('purifier' => $options);
        }

        if ( !is_array($options) ) {
            throw new Exception\InvalidArgumentException('Invalid options argument provided to filter');
        }
        $this -> setOptions($options);
    } // __construct()


    public function setPurifier(HTMLPurifierBase $purifier)
    {
        $this -> purifier = $purifier;
        return $this;
    } // setPurifier()


    public function filter($value)
    {
        if ( null === $this -> purifier ) {
            throw new Exception\LogicException('HtmlPurifier is undefined');
        }
        $value = (string) $value;
        return $this -> purifier -> purify($value);
    } // filter()

} // HtmlPurifier
