<?php
namespace HtmlPurifier;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        require_once __DIR__ . '/htmlpurifier-4.4.0-lite/library/HTMLPurifier.auto.php';

        $config = $serviceLocator -> get('Configuration');

        $purifierConfig = \HTMLPurifier_Config::createDefault();
        if ( !empty($config['html_purifier']) ) {
            $purifierConfig -> loadArray($config['html_purifier']);
        }

        $purifier = new \HTMLPurifier($purifierConfig);
        return $purifier;
    } // createService()

} // ServiceFactory
