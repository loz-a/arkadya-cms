<?php
return array(
    'html_purifier' => array(
        'HTML.Allowed' => 'p,h1,h2,h3,h4,h5,h6,blockquote,ul,ol,li,a[href|title],em,strong,img[src|alt|width|height]',
        'HTML.Doctype' => 'HTML 4.01 Strict',
        'HTML.Nofollow' =>  true,
        'Cache.SerializerPath' => getcwd() . '/data/cache/htmlpurifier'
    )
);