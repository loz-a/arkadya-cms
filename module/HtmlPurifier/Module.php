<?php
namespace HtmlPurifier;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements
    AutoloaderProviderInterface,
    ServiceProviderInterface,
    ConfigProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'HTMLPurifier' => __DIR__ . '/src/HtmlPurifier/htmlpurifier-4.4.0-lite/library/HTMLPurifier',
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    } // getAutoloaderConfig()



    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    } // getConfig()


    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'HtmlPurifier\Service' => 'HtmlPurifier\ServiceFactory'
            )
        );
    } // getServiceConfig()

} //  Module
