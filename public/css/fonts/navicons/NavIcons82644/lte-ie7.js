/* Use this script if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'NavIcons\'">' + entity + '</span>' + html;
	}
	var icons = {
			'nav-icons-coffee' : '&#xe000;',
			'nav-icons-pictures' : '&#xe001;',
			'nav-icons-directions' : '&#xe002;',
			'nav-icons-newspaper' : '&#xe003;',
			'nav-icons-book' : '&#xe004;',
			'nav-icons-map' : '&#xe005;',
			'nav-icons-popup' : '&#xe006;',
			'nav-icons-arrow-left' : '&#xe007;',
			'nav-icons-arrow-right' : '&#xe008;',
			'nav-icons-mail' : '&#xe009;',
			'nav-icons-phone' : '&#xe00a;',
			'nav-icons-checkmark' : '&#xe00b;',
			'nav-icons-list' : '&#xe00c;',
			'nav-icons-cross' : '&#xe00d;',
			'nav-icons-tag' : '&#xe00e;',
			'nav-icons-house' : '&#xe00f;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; i < els.length; i += 1) {
		el = els[i];
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/nav-icons-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};