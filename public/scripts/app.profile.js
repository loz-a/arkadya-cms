var profile = (function(){
    return {
        basePath: "./",
        releaseDir: "./min",
//        releaseName: "lib",
        action: "release",
        layerOptimize: "closure",
        optimize: "closure",
        cssOptimize: "comments",
        mini: false,
        stripConsole: "all",
        selectorEngine: "lite",
        localeList: "en,ru,uk",

//        defaultConfig: {
//            hasCache:{
//                "dojo-built": 1,
//                "dojo-loader": 1,
//                "dom": 1,
//                "host-browser": 1,
//                "config-selectorEngine": "lite"
//            },
//            async: 1
//        },

        staticHasFeatures: {
            // The trace & log APIs are used for debugging the loader, so we do not need them in the build.
            'dojo-trace-api': 0,
            'dojo-log-api': 0,

            // This causes normally private loader data to be exposed for debugging. In a release build, we do not need
            // that either.
            'dojo-publish-privates': 0,

            // This application is pure AMD, so get rid of the legacy loader.
            'dojo-sync-loader': 0,

            // `dojo-xhr-factory` relies on `dojo-sync-loader`, which we have removed.
            'dojo-xhr-factory': 0,

            // We are not loading tests in production, so we can get rid of some test sniffing code.
            'dojo-test-sniff': 0,
            'dojo-requirejs-api': 1
        },

        packages:[{
            name: "dojo",
            location: "dojo"
        },{
            name: "dijit",
            location: "dijit"
        },{
            name: 'put-selector',
            location: 'put-selector'
        },{
            name: 'app',
            location: 'app'
        }],

        layers: {
            "dojo/dojo": {
                include: [
                    "dojo/query",
                    "dojo/domReady",
                    "dojo/ready",
                    "dojo/dom-class",
                    "dojo/NodeList-traverse",
                    'dojo/Stateful',
                    'dojo/_base/lang',
                    'dojo/dom',
                    'dojo/dom-geometry',
                    'dojo/dom-style',
                    'dojo/dom',
                    'dojo/dom-attr',
                    'dojo/on',
                    'dojo/DeferredList',
                    'dojo/date',
                    'dojo/date/locale',
                    'dojo/i18n',
                    'dojo/string',
                    'dojo/request',
                    'dijit/CalendarLite'
                ],
                customBase: true,
                boot: true
            }
        }
    };
})();