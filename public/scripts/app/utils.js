define([
    'dojo/query'
], function(query) {
    "use strict";

    var utils = {

        getTextContent: function(DOMNode) {
            if ( !DOMNode.nodeType
                || DOMNode.nodeType != 1
            ) {
                throw new Error('Element is not DOM node');
            }
            return DOMNode.textContent
                ? DOMNode.textContent : DOMNode.innerText;
        }, // getTextContent()


        isIELT: function(version) {
            var ieDOMNode = query('html.ie')[0];

            if ( ieDOMNode ) {
                var match = ieDOMNode.className.match(/ie\d/);
                return match && parseInt(match[0].substr(2));
            }
            return false;
        } // isIELT()

    };

    return utils;
});
