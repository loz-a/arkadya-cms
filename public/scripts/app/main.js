require([
    'dojo/query',
    'app/image-expander',
    'app/reservation',
    'app/responsive-submenu',
    'dojo/domReady!'
], function(query, ImageExpander) {
    "use strict";

    var imageExpander = new ImageExpander();

    var images = query('a[href$=".jpg"],a[href$=".jpeg"],a[href$=".JPG"],a[href$=".JPEG"]');

    images.on('click', function(evt) {
        evt.preventDefault();
        imageExpander.expand(this.href, query('img', this)[0].alt);
    });

    images.forEach(function(node, idx) {
        var image = new Image();
        image.src = node.href;

        if (idx % 2 != 0 && node.parentNode.nodeName.toLowerCase() === 'p') {
            node.className = 'float-right';
        }
    });

    if (query('.location-page', 'content').length) {
        require(['app/location']);
    }
});