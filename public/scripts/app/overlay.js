define('app/overlay', [

],
function() {

    var overlayDOMNode = null;

    var getOverlayDOMNode = function() {
        return overlayDOMNode;
    }; // getOverlayDOMNode()


    var create = function() {
        overlayDOMNode = document.createElement('div');
        overlayDOMNode.className = 'overlay';
        document.body.appendChild(overlayDOMNode);
    }; // create()


    var hide = function() {
        if ( overlayDOMNode ) {
            overlayDOMNode.style.display = 'none';
        }
    }; // hide()


    var view = function() {
        if ( overlayDOMNode == null ) {
            create();
        }
        overlayDOMNode.style.display = 'block';
    }; // view()


    return {
        getOverlayDOMNode: getOverlayDOMNode,
        hide: hide,
        view: view
    };
});
