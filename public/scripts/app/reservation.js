require([
    'dojo/dom',
    'dojo/dom-attr',
    'dojo/on',
    'dojo/date',
    'dojo/date/locale',
    'dojo/i18n',
    'dojo/dom-class',
    'dijit/CalendarLite',
    'dojo/domReady!'
], function(dom, domAttr, on, date, locale, i18n, domClass, Calendar) {
    "use strict";

    var calendarCI = null;
    var checkInDOMNode = dom.byId('check-in');

    var calendarCO = null;
    var checkOutDOMNode = dom.byId('check-out');

    var lang = i18n.normalizeLocale();

    var today = new Date();
    domAttr.set(checkInDOMNode, 'readonly', true);

    var tomorrow = date.add(new Date(), 'day', 1);
    domAttr.set(checkOutDOMNode, 'readonly', true);


    var hideCalendarCI = function() {
        if ( calendarCI && 'block' == calendarCI.domNode.style.display ) {
            calendarCI.domNode.style.display = 'none';
        }
    }; // hideCalendarCI()

    var hideCalendarCO = function() {
        if ( calendarCO && 'block' == calendarCO.domNode.style.display ) {
            calendarCO.domNode.style.display = 'none';
        }
    }; // hideCalendarCO()


    on(checkInDOMNode, 'click', function() {

        if ( null == calendarCI ) { // if callendar no exists
            calendarCI = new Calendar({
                value: today,
                isDisabledDate: function(d) {
                    var d = new Date(d); d.setHours(0, 0, 0, 0);
                    var today = new Date(); today.setHours(0, 0, 0, 0);
                    var checkOutDate = (calendarCO == null) ? tomorrow : calendarCO.get('value');

                    if ( !checkOutDate ) {
                        checkOutDate = tomorrow;
                    }
                    checkOutDate.setHours(0, 0, 0, 0);
                    return date.compare(d, today) < 0 || date.compare(d, checkOutDate) > 0;
                },
                onChange: function() {
                    this._populateGrid();
                    this._markSelectedDates([this.value]);

                    if ( null != calendarCO ) {
                        calendarCO._populateGrid();
                        calendarCO._markSelectedDates([calendarCO.get('value')]);
                    }
                }
            }, 'check-in-calendar');

            calendarCI.watch('value', function(name, oldValue, value) {
                 var date = locale.format(value, {
                    selector: 'date',
                    fullYear: true,
                    locale: lang
                });

                checkInDOMNode.value = date;
                calendarCI.domNode.style.display = 'none';
            });
        }

        if ( calendarCO ) {
            calendarCO.domNode.style.display = 'none';
        }
        //calendarCI._markSelectedDates([calendarCI.get('value')]);
        calendarCI.domNode.style.display = 'block';
    });


    on(checkOutDOMNode, 'click', function() {
        if ( null == calendarCO ) { // if callendar no exists
            calendarCO = new Calendar({
                value: tomorrow,
                isDisabledDate: function(d) {
                    var d = new Date(d);
                    var checkInDate = (null != calendarCI) ? date.add(calendarCI.get('value'), 'day', 1) : tomorrow;
                    return date.compare(d, checkInDate) < 0;
                },
                onChange: function() {
                    this._populateGrid();
                    this._markSelectedDates([this.value]);

                    if ( null != calendarCI ) {
                        calendarCI._populateGrid();
                        calendarCI._markSelectedDates([calendarCI.get('value')])
                    }
                },
                onBlur: function() {
                    this.domNode.style.display = 'none';
                }
            }, 'check-out-calendar');

            calendarCO.watch('value', function(name, oldValue, value) {
                var date = locale.format(value, {
                    selector: 'date',
                    fullYear: true,
                    locale: lang
                });

                checkOutDOMNode.value = date;
                calendarCO.domNode.style.display = 'none';
            });
        }

        if ( calendarCI ) {
            calendarCI.domNode.style.display = 'none';
        }
        calendarCO._markSelectedDates([calendarCO.get('value')]);
        calendarCO.domNode.style.display = 'block';
    });


    on(document.body, 'click', function(evt) {

            if ( !calendarCI && !calendarCO
//                || domClass.contains(evt.target, 'dijitCalendarMonthLabel')
            ) {
                return;
            }

            var node = evt.target;
            var result = null;

            do {
                result = node == checkInDOMNode;
                result = result || calendarCI && node == calendarCI.domNode;
                result = result || node == checkOutDOMNode;
                result = result || (calendarCO && node == calendarCO.domNode);

                if ( result ) {
                    return;
                }
            } while(node != document.body && (node = node.parentNode) && node.nodeType == 1);

            hideCalendarCI();
            hideCalendarCO();
    })

});