require(['dojo/query'], function(query) {
    "use strict";

    //http://code.google.com/intl/ru/apis/maps/documentation/javascript/basics.html#Welcome
    window.initialize = function() {
        var latlng = new google.maps.LatLng(49.14758, 23.87260);
        var opts = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.HYBRID
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), opts);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            animation: google.maps.Animation.BOUNCE
            //title: title
        });

        var infoWindow = new google.maps.InfoWindow({
            content: [
                '<div id="info-wind-content" style="color: #000;">',
                '<img style="float: left; margin: 0 2px 2px 0;" src="../img/ship.png">',
                ' Arkadia ',
                '</div>'
            ].join('')
        });

        google.maps.event.addListener(marker, 'mouseover', function() {
            marker.setAnimation(null);
            infoWindow.open(map, marker);
        });
        google.maps.event.addListener(marker, 'mouseout', function() {
            infoWindow.close();
            marker.setAnimation(google.maps.Animation.BOUNCE);
        });
    }; // initialize()

    var locationContainer = query('.location-page', 'content')[0];
    if (undefined != locationContainer) {
        var mapCanvas = document.createElement('div');
        mapCanvas.id = 'map-canvas';
        locationContainer.appendChild(mapCanvas);
    }

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&language=ru&callback=initialize";
    document.body.appendChild(script);
});
