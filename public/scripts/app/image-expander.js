define([
    'dojo/Stateful',
    'dojo/_base/lang',
    'dojo/dom',
    'dojo/query',
    'dojo/dom-geometry',
    'dojo/dom-style',
    'dojo/dom-class',
    'put-selector/put',
    'app/overlay',
    'app/utils'
], function(
    Stateful,
    lang,
    dom,
    query,
    domGeom,
    domStyle,
    domClass,
    put,
    overlay,
    utils
) {
    "use strict";

    var ImageExpander = function() {
        this.expanderDOMNode = null;
        this.imgDOMNode = null;
        this.ieVersion = null;
        this.overlay = overlay;

        if ( window.matchMedia ) {
            var mqlPortrait = window.matchMedia("all and (orientation:portrait)");
            var mqlLandscape = window.matchMedia("all and (orientation:landscape)");

            var self = this;
            var mqHandler = function(mql) {
                self.adjust();
            };
            mqlLandscape.addListener(mqHandler);
            mqlLandscape.addListener(mqHandler);
        }

    }; // ImageExpander()

    ImageExpander.prototype = new Stateful();
    ImageExpander.prototype.constructor = ImageExpander;

    ImageExpander.prototype.getExpanderDOMNode = function() {
        if ( !this.expanderDOMNode ) {
            throw new Error('Expander DOM node is not exists');
        }
        return this.expanderDOMNode;
    }; // getExpanderDOMNode()


    ImageExpander.prototype.isExpanderCreated = function() {
        return !!this.expanderDOMNode;
    }; // isExpanderCreated()


    ImageExpander.prototype.getImgDOMNode = function() {
        if ( !this.imgDOMNode ) {
            throw new Error('Expanded image DOM node is not exists');
        }
        return this.imgDOMNode;
    }; // getImgDOMNode()


    ImageExpander.prototype.isIELT = function(version) {
        if ( null === this.ieVersion ) {
            this.ieVersion = utils.isIELT(version);
        }
        return this.ieVersion && this.ieVersion < version;
    }; // isIELT()


    ImageExpander.prototype.adjust = function() {
        var overlayDOMNode  = overlay.getOverlayDOMNode();
        var expanderDOMNode = this.getExpanderDOMNode();

        var overlayInfo = domGeom.position(overlayDOMNode, true);
        var closeInfo   = domGeom.position(query('.close-expander', expanderDOMNode)[0]);
        domStyle.set(overlayDOMNode, 'lineHeight', (overlayInfo.h - closeInfo.h)  + 'px');
    }; // adjust()


    ImageExpander.prototype.createExpander = function(src, alt) {
        var selector = [];
        if ( this.isIELT(9) ) {
            selector.push('div.inner-wrapper+');
        }
        selector.push('div#expander');
        selector.push('>span.close-expander.nav-icons-cross $');
        selector.push('+img.expanded-image[src=$][alt=$]');
        selector.push('+span.expanded-title');
        selector.push("> $ <");

        var overlayDOMNode = overlay.getOverlayDOMNode();
        if ( overlayDOMNode != null ) {
            this.expanderDOMNode = put(
                overlayDOMNode,
                selector.join(''),
                'Закрыть',
                src,
                alt,
                alt
            );
        }
        else {
            throw new Error('Overlay DOMNode not exists');
        }
        this.imgDOMNode = query('.expanded-image', this.expanderDOMNode)[0];
        query('.close-expander', this.expanderDOMNode).on('click', lang.hitch(this, 'hide'));
        return this.expanderDOMNode;
    }; // createExpander()


    ImageExpander.prototype.replaceImage = function(src, alt) {
        var imgDOMNode = this.getImgDOMNode();
        imgDOMNode.src = src;
        imgDOMNode.alt = alt;

        query('.expanded-title', this.getExpanderDOMNode())[0].lastChild.nodeValue = alt;
    }; // replaceImage()


    ImageExpander.prototype.expand = function(src, alt) {
        overlay.view();

        var expanderDOMNode = null;
        if ( !this.isExpanderCreated() ) {
            expanderDOMNode = this.createExpander(src, alt);
        }
        else {
            expanderDOMNode = this.getExpanderDOMNode();
            this.replaceImage(src, alt);
            domStyle.set(expanderDOMNode, {
                display: 'inline-block',
                opacity: 1
            });
        }
        this.adjust();
        domClass.add(expanderDOMNode, 'fadeIn');
    }; // expand()


    ImageExpander.prototype.hide = function() {
        var expanderDOMNode = this.getExpanderDOMNode();

        domStyle.set(expanderDOMNode, {
            display: 'none',
            opacity: 0
        });
        domClass.remove(expanderDOMNode, 'fadeIn');
        overlay.hide();
    }; // hide()

    return ImageExpander;
});
