require([
    'dojo/ready',
    'put-selector/put',
    'dojo/dom-class',
    'dojo/query',
    'dojo/NodeList-traverse'
], function(
    ready,
    put,
    domClass,
    domQuery
) {
    "use strict";

    var Slider = function(images, sliderId) {

        this.images = images;
        this.sliderDOMNode = null;
        this.slider = null;
        this.mql767 = window.matchMedia ? window.matchMedia('only screen and (max-width: 767px)') : false;

        var init = function(images, sliderDOMNode, mql767) {
            var isMore767 = true;

            put(domQuery('img', sliderDOMNode)[0], '!');

            if ( mql767 && mql767.matches ) {
                isMore767 = false;
            }

            var selector = [];
            for (var i = 0; i < images.length; i++ ) {
                if ( images[i] !== undefined ) { // for ie 8
                    selector[i] = 'img[src="'
                        + ( isMore767 ? images[i].src : images[i].minSrc )
                        + '"][alt="' + images[i].alt + '"].scale-with-grid';

                    if ( i == 0 ) {
                        selector[i] += '.visible';
                    }
                }

            }
            put(sliderDOMNode, selector.join(' +'));
        }; // init()


        var findDOMNodeById = function(id) {
            var sliderDOMNode = document.getElementById(id);
            if ( !sliderDOMNode ) {
                throw Error('Slider DOMNode is undefined');
            }
            return sliderDOMNode;
        }; // findDOMNodeById()

        this.sliderDOMNode = findDOMNodeById(sliderId);
        init(this.images, this.sliderDOMNode, this.mql767);
        return this;
    } // slider()


    Slider.prototype.replaceImages = function() {
        var isMore767 = true;

        if ( this.mql767 && this.mql767.matches ) {
            isMore767 = false;
        }

        this.sliderDOMNode.innerHTML = '';
        var selector = [];
        for (var i = 0; i < this.images.length; i++ ) {
            selector[i] = 'img[src="'
                + ( isMore767 ? this.images[i].src : this.images[i].minSrc )
                + '"][alt="' + this.images[i].alt + '"].scale-with-grid';

            if ( i == 0 ) {
                selector[i] += '.visible';
            }
        }
        put(this.sliderDOMNode, selector.join(' +'));
        return this;
    }; // replaceImages()


    Slider.prototype.run = function() {
        var sliderDOMNode = this.sliderDOMNode;

        setInterval(function() {
            var visImgDOMNode  = domQuery('.visible', sliderDOMNode)[0];
            var nextImgDOMNode = domQuery(visImgDOMNode).next('img')[0];

            if ( !nextImgDOMNode ) {
                nextImgDOMNode = domQuery('img', sliderDOMNode).first()[0];
            }

            domClass.add(nextImgDOMNode, 'next');
            domClass.add(visImgDOMNode, 'fadeOut');

            setTimeout(function() {
                domClass.replace(nextImgDOMNode, 'visible', 'next');
                domClass.remove(visImgDOMNode, ['visible', 'fadeOut']);
            }, 1500)

        }, 5000);

        return this;
    }; // run()


    ready(function() {
        var images = [
            {src: '/img/1.jpg', alt:'Arkadya', minSrc: '/img/1-min.jpg'},
            {src: '/img/2.jpg', alt:'Arkadya', minSrc: '/img/2-min.jpg'},
            {src: '/img/3.jpg', alt:'Arkadya', minSrc: '/img/3-min.jpg'},
            {src: '/img/4.jpg', alt:'Arkadya', minSrc: '/img/4-min.jpg'},
            {src: '/img/5.jpg', alt:'Arkadya', minSrc: '/img/5-min.jpg'},
        ];

        var sliderId = 'slider';
        var slider = new Slider(images, sliderId);
        slider.run();

        if ( slider.mql767 ) {
            slider.mql767.addListener(function(mql) {
                slider.replaceImages();
            });
        }
    });

    return Slider;
});
