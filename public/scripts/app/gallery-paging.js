define("app/gallery-paging", [
    'dojo/_base/lang',
    'dojo/query',
    'dojo/dom-class',
    'dojo/dom-style',
    'dojo/Stateful',
    'dojo/request',
    'dojo/DeferredList',
    'dojo/on',
    'app/utils'
], function(
    lang,
    query,
    domClass,
    domStyle,
    Stateful,
    request,
    DeferredList,
    on,
    utils
) {
    "use strict";

    var GalleryPaging = function(imageExpander, galleryDOMNode) {
        this.imageExpander = imageExpander;

        this.viewModel = {
            gallery:    galleryDOMNode,
            imagesList: query('.images-list', galleryDOMNode)[0],
            paging:     query('.images-paging', galleryDOMNode)[0]
        };

        this.imagesAmount = query('img', this.viewModel.imagesList).length;

        this.curImageIdx = 0;
        this._curImageIdxSetter = function(value) {
            this.curImageIdx = parseInt(value);
            return this;
        };

        this._curImageIdxGetter = function() {
            return this.curImageIdx;
        };

        this.watch('curImageIdx', function(name, oldValue, value) {
            this.imageExpander.set('imageNum', value);

            if ( !this.imageExpander.isIELT(8) ) {
                var viewModel = this.imageExpander.viewModel;
                viewModel.prevIcon.style.display = this.hasPrev() ? 'inline-block' : 'none';
                viewModel.nextIcon.style.display = this.hasNext() ? 'inline-block' : 'none';
            }

            var imagesList = query('img', this.viewModel.imagesList);

            imagesList.forEach(function(node, idx, list) {
                if (idx == value) {
                    node.style.borderColor = '#D21313';
                    return;
                }
                node.style.borderColor = '#fff';
            });
        });


        this.activePageNum = utils.getTextContent(query('.active', this.viewModel.paging)[0]);
        this._activePageNumSetter = function(value) {
            this.activePageNum = parseInt( typeof value === 'object' ? value['num'] : value );
            return this;
        };

        this._activePageNumGetter = function() {
            return this.activePageNum
        };

        this.watch('activePageNum', function(name, oldValue, value) {
            var activePageDOMNode = query('.active', this.viewModel.paging)[0];
            if ( activePageDOMNode ) {
                domClass.remove(activePageDOMNode, 'active');
            }
            if ( value.DOMNode ) {
                domClass.add(value.DOMNode.parentNode, 'active');
            }

            var activePageNum = parseInt((typeof value === 'object') ? value['num'] : value);
            query('.images-list', this.viewModel.gallery)
                .forEach(lang.hitch(this, function(node, idx) {
                    if (activePageNum === (idx + 1)) {
                        node.style.display = 'block';
                        this.viewModel.imagesList = node;
                    }
                    else {
                        node.style.display = 'none';
                    }
                }));

            var firstImageDOMNode = query('img', this.viewModel.imagesList)[0];
            this.imageExpander.set('imageInViewer', {
                alt: firstImageDOMNode.alt,
                src: firstImageDOMNode.parentNode.href,
                idx: 0
            });

        });


        var self = this;
        var mql767 = window.matchMedia && window.matchMedia("only screen and (max-width: 767px)");

        query(this.viewModel.imagesList.parentNode)
            .on('click', function(evt) {

                if (evt.target.nodeName.toLowerCase() === 'img') {
                    self.imageExpander.set('imageInViewer', {
                        alt: evt.target.alt,
                        src: evt.target.parentNode.href,
                        idx: query('li', evt.target.parentNode.parentNode.parentNode).indexOf(evt.target.parentNode.parentNode)
                    });

                    if ( window.matchMedia && mql767.matches
                        || parseInt(domStyle.getComputedStyle(query('body>.container')[0]).width) < 768
                    ) {
                        self.imageExpander.set('expanded', true);
                    }
                }
                evt.preventDefault();
            });

//        query('.images-list a', this.viewModel.imagesList.parentNode)
//            .on('click', function(evt) {
//                evt.preventDefault();
//
//                var imgDOMNode = query('img', this)[0];
//                self.imageExpander.set('imageInViewer', {
//                    alt: imgDOMNode.alt,
//                    src: this.href,
//                    idx: query('li', this.parentNode.parentNode).indexOf(this.parentNode)
//                });
//
//                if ( window.matchMedia && mql767.matches
//                    || parseInt(domStyle.getComputedStyle(query('body>.container')[0]).width) < 768
//                ) {
//                    self.imageExpander.set('expanded', true);
//                }
//            });


//        query('a', this.viewModel.paging)
//            .on('click', function(evt) {
//                evt.preventDefault();
//
//                self.set('activePageNum', {
//                    DOMNode : this,
//                    num: utils.getTextContent(this)
//                });
//            });


        function pagingClickHandler(evt) {
            evt.preventDefault();

            self.set('activePageNum', {
                DOMNode : this,
                num: utils.getTextContent(this)
            });
        }

        var promises = [];
        var promisesAgregator = [];

        query('a', this.viewModel.paging)
            .forEach(function(node, idx, nodes) {
                if (node.parentNode.className != 'active') {
                    var promise = request(node.href).then(function(response) {
                        promisesAgregator[idx] = response;
                    });
                    promises.push(promise);
                }
                on(node, 'click', pagingClickHandler);
            });

        var dl = new DeferredList(promises);
        dl.then(function(result) {
            self.viewModel.imagesList.insertAdjacentHTML('afterEnd', promisesAgregator.join(' '));
        });
    }; // GalleryPaging

    GalleryPaging.prototype = new Stateful();

    GalleryPaging.ITEMS_PER_PAGE = 10;
    GalleryPaging.prototype.getItemsPerPage = function() {
        return GalleryPaging.ITEMS_PER_PAGE;
    }; // getItemPerPage()


    GalleryPaging.prototype.hasNext = function() {
        return this.imagesAmount > 1 && this.curImageIdx != this.imagesAmount - 1;
    }; // hasNext()


    GalleryPaging.prototype.hasPrev = function() {
        return this.imagesAmount > 1 && this.curImageIdx != 0;
    }; // hasNext()

    return GalleryPaging;
});
