require([
    'dojo/query',
    'dojo/on',
    'dojo/dom-class',
    'put-selector/put',
    'dojo/string',
    'dojo/i18n!app/nls/submenu',
    'dojo/ready'
], function(
    domQuery,
    on,
    domClass,
    put,
    str,
    i18n,
    ready
) {
    "use strict";


    var navItems = {
        quickReservation: {
            title: i18n.quickReservation,
            iconClass: 'nav-icons-tag'
        },
        goto: {
            title: i18n.goto,
            iconClass: 'nav-icons-list'
        },
        close: {
            title: i18n.close,
            iconClass: 'nav-icons-cross'
        }
    };

    var navDOMNode = domQuery('header nav')[0];

    var viewGotoItem = function(DOMNode) {
        domClass.replace(DOMNode, navItems.close.iconClass, navItems.goto.iconClass);
        DOMNode.innerHTML = navItems.close.title;
    }; // viewGotoItem()


    var viewCloseItem = function(DOMNode) {
        domClass.replace(DOMNode, navItems.goto.iconClass, navItems.close.iconClass);
        DOMNode.innerHTML = navItems.goto.title;
    }; // viewCloseItem()


    var addOpenSubmenuListener = function() {
        var gotoDOMNode = domQuery('.' + navItems.goto.iconClass, navDOMNode)[0];
        var submenuDOMNode = domQuery('.responsive-submenu', navDOMNode)[0];

        if ( !gotoDOMNode || !submenuDOMNode ) {
            throw new Error('Responsive submenu DOMNode is not exists');
        }

        on(gotoDOMNode, 'click', function(evt) {
            evt.preventDefault();

            if ( domClass.contains(this, navItems.goto.iconClass) ) {
                viewGotoItem(this);
                domClass.remove(submenuDOMNode, 'hidden');
            }
            else if (domClass.contains(this, navItems.close.iconClass)) {
                viewCloseItem(this);
                domClass.add(submenuDOMNode, 'hidden');
            }
        });
    }; // addOpenSubmenuListener()


    var buildResponseMenu = function() {
        var submenu = buildResponseSubmenu();
        var quickReservForm = domQuery('form', 'quick-reservation');

        put(
            navDOMNode,
            "ul.clearfix.responsive-menu.visible-phone li a[href=$] span.$ $<<< li a[href=#] span.$ $",
            quickReservForm.length ? quickReservForm[0].getAttribute('action') : '#',
            navItems.quickReservation.iconClass,
            navItems.quickReservation.title,
            navItems.goto.iconClass,
            navItems.goto.title
        );
        navDOMNode.appendChild(submenu);
        addOpenSubmenuListener();
    }; // builResponseMenu()


    var buildResponseSubmenu = function()
    {
        var container = put('ul.clearfix.responsive-submenu.hidden');

        domQuery('a', navDOMNode)
            .concat(domQuery('aside .aside-nav a'))
            .forEach(function(item, idx) {
                put(container, 'li a[href=$] span.$ $',
                    item.href,
                    item.getAttribute('data-mobile-icon'),
                    str.trim(item.textContent || item.innerText)
                );
            });

        return container;
    }; // buildResponseSubmenu()


    var isResponsiveMenuBuilded = function() {
        return !! domQuery(navDOMNode).query('.responsive-menu').length;
    }; // isResponsiveMenuBuilded()


    ready(function() {

        if ( window.matchMedia ) {
            var mql767 = window.matchMedia("only screen and (max-width: 767px)");

            if ( mql767.matches ) {
                buildResponseMenu();
            }

            mql767.addListener(function(mql) {
                if ( !domQuery('.responsive-menu', navDOMNode).length ) {
                    buildResponseMenu();
                }
            });
        }
        else {
            var htmlNodeList = domQuery('html.ie');
            if ( htmlNodeList.length ) {
                var match = htmlNodeList[0].className.match(/ie\d/);
                if ( match ) {
                    if ( parseInt(match[0].substr(2)) == 9 ) {
                        buildResponseMenu();
                    }
                }
            }
        }

    });

});
