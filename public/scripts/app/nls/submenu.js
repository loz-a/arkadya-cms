define({
    root: {
        quickReservation: 'Quick Reservation',
        goto: 'Go to',
        close: 'Close'
    },

    "uk": true,
    "uk-UA": true,
    "ru": true,
    "ru-RU": true
});
