define([
    'dojo/_base/lang',
    'dojo/dom',
    'dojo/query',
    'put-selector/put',
    'app/image-expander',
    'app/gallery-paging',
    'dojo/NodeList-traverse'
], function(
    lang,
    dom,
    query,
    put,
    ImageExpander,
    GalleryPaging
) {
    "use strict";

    var GalleryImageExpander = function() {
//        ImageExpander.apply(this);

        this.viewModel = {
            gallery:   dom.byId('gallery'),
            count:     query('.count', this.gallery)[0],
            imgViewer: query('.image-viewer img', this.gallery)[0],
            imgTitle:  query('.title', this.gallery)[0],
            prevIcon:  query('.prev-image', this.gallery)[0],
            nextIcon:  query('.next-image', this.gallery)[0],
            expander: {
                prevIcon: null,
                nextIcon: null
            }
        };

        this.uploadPath = this.viewModel.gallery.getAttribute('data-uploadpath');
        if (!this.uploadPath) {
            throw new Error('Upload path is undefined');
        }

        this.galleryPaging = new GalleryPaging(this, this.viewModel.gallery);

        this.count = null;
        this._countGetter = function() {
            return this.count;
        };

        this._countSetter = function(value) {
            this.count = value;
            return this;
        };

        this.watch('count', function(name, oldValue, value) {
            var countDOMNode = this.viewModel.count;
            if ( null != countDOMNode ) {
                var result = countDOMNode.innerHTML.split('/');
                result[1] = value;
                countDOMNode.innerHTML = result.join('/');
            }
        });


        this.imageNum = null;
        this._imageNumGetter = function() {
            return this.imageNum;
        };

        this._imageNumSetter = function(value) {
            this.imageNum = value;
            return this;
        };

        this.watch('imageNum', function(name, oldValue, value) {
            var countDOMNode = this.viewModel.count;
            if ( null != countDOMNode ) {
                var activePageNum = this.galleryPaging.get('activePageNum');
                var itemsPerPage   = this.galleryPaging.getItemsPerPage();

                var result = countDOMNode.innerHTML.split('/');
                result[0] = (activePageNum - 1) * itemsPerPage + value + 1;
                countDOMNode.innerHTML = result.join('/');
            }
        });


        this.imageInViewer = null;
        this._imageInViewerGetter = function() {
            return this.imageViewer;
        };

        this._imageInViewerGetter = function(value) {
            this.imageViewer = value;
            return this;
        };

        this.watch('imageInViewer', function(name, oldValue, value) {
            var imgViewerDOMNode = this.viewModel.imgViewer;
            var imgTitleDOMNode  = this.viewModel.imgTitle;

            imgViewerDOMNode.alt = value.alt;
            imgViewerDOMNode.src = value.src;
            imgTitleDOMNode.innerHTML = value.alt;

            this.galleryPaging.set('curImageIdx', value.idx);
        });


        this.expanded = false;
        this._expandedGetter = function() {
            return this.expanded;
        };

        this._expandedSetter = function(value) {
            this.expanded = !!value;
            return this;
        };

        this.isExpanded = function() {
            return this.get('expanded');
        };

        this.watch('expanded', function(name, oldValue, value) {
            if ( !!value == false ) return;

            var imgViewerDOMNode = this.viewModel.imgViewer;
            var src = imgViewerDOMNode.src.split('/');
            var expandSrc = this.uploadPath + '/' + src.pop();
            this.expand(expandSrc, imgViewerDOMNode.alt);

            if ( !this.isIELT(8) ) {
                this.viewModel.expander.prevIcon.style.display = this.galleryPaging.hasPrev() ? 'inline-block' : 'none';
                this.viewModel.expander.nextIcon.style.display = this.galleryPaging.hasNext() ? 'inline-block' : 'none';
            }
        });

        query('.expand', this.viewModel.gallery)
            .on('click', lang.hitch(this, function(evt) {
                this.set('expanded', true);
            }));

        query('.prev-image', this.viewModel.gallery)
            .on('click', lang.hitch(this, 'viewPrevImage'));

        query('.next-image', this.viewModel.gallery)
            .on('click', lang.hitch(this, 'viewNextImage'));

    } // GalleryImageExpander()


    GalleryImageExpander.prototype = new ImageExpander();
    GalleryImageExpander.prototype.constructor = GalleryImageExpander;

    GalleryImageExpander.prototype.createExpander = function(src, alt) {
        var selector = [];
        if ( this.isIELT(9) ) {
            selector.push('div.inner-wrapper+');
        }
        selector.push('div#expander');
        selector.push('>span.close-expander.nav-icons-cross $');
        selector.push('+img.expanded-image[src=$][alt=$]');
        selector.push('+span.expanded-title');
        selector.push('>span.nav-icons-arrow-left.prev-image $');
        selector.push('+span.nav-icons-arrow-right.next-image $');
        selector.push("+ $ <");

        var overlayDOMNode = this.overlay.getOverlayDOMNode();
        if ( null != overlayDOMNode ) {
            this.expanderDOMNode = put(
                overlayDOMNode,
                selector.join(''),
                'Закрыть',
                src,
                alt,
                this.isIELT(8) ? 'Пред.' : ' ',
                this.isIELT(8) ? 'След.' : ' ',
                alt
            );
        }
        else {
            throw new Error('Overlay DOMNode not exists');
        }

        this.imgDOMNode = query('.expanded-image', this.expanderDOMNode)[0];
        this.viewModel.expander.prevIcon = query('.prev-image', this.expanderDOMNode)[0];
        this.viewModel.expander.nextIcon = query('.next-image', this.expanderDOMNode)[0];

        query('.close-expander', this.expanderDOMNode)
            .on('click', lang.hitch(this, function() {
                this.set('expanded', false);
                this.hide();
            }));

        query('.next-image', this.expanderDOMNode)
            .on('click', lang.hitch(this, function() {
                this.viewNextImage();
                this.set('expanded', true);
            }));

        query('.prev-image', this.expanderDOMNode)
            .on('click', lang.hitch(this, function() {
                this.viewPrevImage();
                this.set('expanded', true);
            }));

        return this.expanderDOMNode;
    }; // createExpander()


    GalleryImageExpander.prototype.viewNextImage = function() {
        var paging = this.galleryPaging;

        if ( paging.hasNext() ) {
            var idx = paging.get('curImageIdx');
            var imageDOMNode = query('img', paging.viewModel.imagesList)[idx + 1];
            if ( imageDOMNode ) {
                this.set('imageInViewer', {
                    alt: imageDOMNode.alt,
                    src: imageDOMNode.parentNode.href,
                    idx: idx + 1
                });
            }
        }
    }; // viewNextImage()


    GalleryImageExpander.prototype.viewPrevImage = function() {
        var paging = this.galleryPaging;

        if ( paging.hasPrev() ) {
            var idx = paging.get('curImageIdx');
            var imageDOMNode = query('img', paging.viewModel.imagesList)[idx - 1];
            if ( imageDOMNode ) {
                this.set('imageInViewer', {
                    alt: imageDOMNode.alt,
                    src: imageDOMNode.parentNode.href,
                    idx: idx - 1
                });
            }
        }
    }; // viewPrevImage()


    return GalleryImageExpander;
});
