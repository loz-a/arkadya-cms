require([
    'dojo/query',
    'dojo/cookie',
    'app/image-expander'
], function(query, cookie, ImageExpander) { "use strict";

    var isBannerShown = cookie('bannerShown');

    if (isBannerShown) return;

    var iExp = new ImageExpander();
    iExp.expand('/img/startup-banner.jpg', '');

    var imgDOMNode = iExp.getImgDOMNode();
    imgDOMNode.style.border = 'none';
    imgDOMNode.style.width = 'auto';

    var titleDOMNode = query('.expanded-title', iExp.getExpanderDOMNode())[0];
    titleDOMNode.style.display = 'none';

    cookie('bannerShown', true, { expires: 1, path: '/' });

});