require([
    'dojo/query',
    'dojo/NodeList-traverse',
    'dojo/domReady!'
], function(query) {
    "use strict";

    query('.alert .close')
        .on('click', function() {
            var alertContainer = query(this.parentNode).closest('.alert')[0];
            alertContainer.parentNode.removeChild(alertContainer);
        });
});