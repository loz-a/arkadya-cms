require([
    'dojo/dom',
    'dojo/query',
    'app/gallery-image-expander',
    'app/reservation',
    'app/responsive-submenu',
    'dojo/domReady!'
], function(dom, query, ImageExpander) {
    "use strict";

    new ImageExpander();
});