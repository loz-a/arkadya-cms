<?php
    ob_start();
    header('HTTP/1.1 503 Service Temporarily Unavailable');
    header('Status: 503 Service Temporarily Unavailable');
    header('Retry-After: 3600');
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>503 Service Temporarily Unavailable</title>
</head>
<body>
    <h1>
        Сайт временно недоступен
    </h1>
    <p>
        На сайте проводятся работы по техническому обслуживанию. Зайдите позже.
    </p>
</body>
</html>

<?php
    ob_end_flush();