<?php
/**
 * SlmLocale Configuration
 *
 * If you have a ./config/autoload/ directory set up for your project, you can
 * drop this config file in it and change the values as you wish.
 */
$settings = array(
    /**
     * Default locale
     *
     * Some good description here. Default is something
     *
     * Accepted is something else
     */
    'default' => 'uk_UA',

    /**
     * Supported locales
     *
     * Some good description here. Default is something
     *
     * Accepted is something else
     */
    'supported' => array(
        'uk' => 'uk_UA'
    ),

    /**
     * where from get supported locales
     *
     * avaiable values: config-file or data-base
     */
    'data_source' => 'data-base',

    /**
     * Listeners
     *
     * Some good description here. Default is something
     *
     * Accepted is something else
     */
    'listeners' => array(
        'Url',
        'Cookie',
        'HttpAcceptLanguage'
//        'Session',
    ),

    /**
     * Throw exception when no locale is found
     *
     * Some good description here. Default is something
     *
     * Accepted is something else
     */
    //'throw_exception' => false,

    'redirect_exclude' => [
        'file_extensions' => ['css', 'gif', 'ico', 'jpg', 'js', 'png', 'swf', 'txt'],
        'uri' => ['/sitemap.xml', '/humans.txt'],
    ],
);

/**
 * You do not need to edit below this line
 */
return array(
    'cms_locale' => $settings
);